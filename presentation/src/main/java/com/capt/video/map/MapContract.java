package com.capt.video.map;

import android.location.Location;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;

/**
 * Created by root on 05.04.16.
 */
public interface MapContract {

	interface View extends BaseView {
		void navigateToDescriptionScreen (LocationModel _locationModel);
		LocationModel getCurrentLocation();
		void setTitle(String title);
	}


	abstract class Presenter extends BasePresenter<MapContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void navigateToDescription();
		public abstract void getAddress();
	}
}
