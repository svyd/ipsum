package com.capt.video.camera.custom_view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.capt.R;

/**
 * Created by richi on 2016.03.27..
 */
public class OverlayView extends View {

    private Rect mTopRect;
    private Rect mRightRect;
    private Rect mLeftRect;
    private Rect mBottomRect;

    private Paint mRectPaint;

    private float mHalfWidth, mHalfHeight;
    private float mScaleX = 1, mScaleY = 1;
    private float mRotationRect;

    private AnimatorSet mAnimator;

    public OverlayView(Context context) {
        this(context, null);
    }

    public OverlayView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mRectPaint = new Paint();
        mRectPaint.setAntiAlias(true);
        mRectPaint.setColor(ContextCompat.getColor(getContext(), R.color.gray_op));
    }

    public void animateToPortrait(Animator.AnimatorListener _listener) {
        AnimationModel animationModel = AnimationModel.create(mRotationRect, 0)
                .setRotationInterpolator(new AccelerateInterpolator())
                .setScaleX(mScaleX, 1)
                .setXInterpolator(new DecelerateInterpolator())
                .setScaleY(mScaleY, 1)
                .setYInterpolator(new DecelerateInterpolator());

        animateTo(animationModel, _listener);
    }

    public void animateToLandscape(int _orientation, Animator.AnimatorListener _listener) {
        if (_orientation > 225 && _orientation <= 315)
            mRotationRect = 90f;
        else
            mRotationRect = -90f;

        mScaleX = (mHalfHeight * 2) / (mHalfWidth * 2);
        mScaleY = (float) ((mHalfWidth * 2) / ((mHalfHeight * 2) / 3.5));

        AnimationModel animationModel = AnimationModel.create(0, mRotationRect)
                .setRotationInterpolator(new DecelerateInterpolator())
                .setScaleX(1, mScaleX)
                .setXInterpolator(new AccelerateInterpolator())
                .setScaleY(1, mScaleY)
                .setYInterpolator(new AccelerateInterpolator());

        animateTo(animationModel, _listener);
    }

    private void animateTo(AnimationModel _animationModel, Animator.AnimatorListener _listener) {
        ValueAnimator rotationAnim = ValueAnimator.ofFloat(_animationModel.mFromRotation,
                _animationModel.mToRotation);
        rotationAnim.setInterpolator(_animationModel.mRotationInterpolator);
        rotationAnim.addUpdateListener(animation -> mRotationRect = ((float) animation.getAnimatedValue()));

        ValueAnimator scaleXAnim = ValueAnimator.ofFloat(_animationModel.mFromScaleX,
                _animationModel.mToScaleX);
        scaleXAnim.setInterpolator(_animationModel.mScaleXInterpolator);
        scaleXAnim.addUpdateListener(animation -> mScaleX = ((float) animation.getAnimatedValue()));

        ValueAnimator scaleYAnim = ValueAnimator.ofFloat(_animationModel.mFromScaleY,
                _animationModel.mToScaleY);
        scaleYAnim.setInterpolator(_animationModel.mScaleYInterpolator);
        scaleYAnim.addUpdateListener(animation -> {
            mScaleY = ((float) animation.getAnimatedValue());
            invalidate();
        });

        stopCurrentAnimation();

        mAnimator = new AnimatorSet();
        mAnimator.setDuration(500);
        mAnimator.playTogether(rotationAnim, scaleXAnim, scaleYAnim);
        mAnimator.addListener(_listener);
        mAnimator.start();
    }

    private void stopCurrentAnimation() {
        if (mAnimator != null && mAnimator.isRunning())
            mAnimator.cancel();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mHalfHeight = h / 2;
        mHalfWidth = w / 2;

        mTopRect = new Rect(0, -(2 * h / 3), w, h / 3);
        mBottomRect = new Rect(0, 2 * h / 3, w, h + h / 3);
        mLeftRect = new Rect(-(w + w / 3), 0, 0, h);
        mRightRect = new Rect(w, 0, 2 * w + w / 3, h);
//        mTopRect = new Rect(- w / 2, - h, w + w / 2, 0);
//        mBottomRect = new Rect(- w / 2, h, w + w / 2, 2 * h);
//        mLeftRect = new Rect(- w / 2, 0, (w / 2) - ((w / 3) / 2), h);
//        mRightRect = new Rect((w / 2) + ((w / 3) / 2), 0, w + w / 2, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.scale(mScaleX, mScaleY, mHalfWidth, mHalfHeight);
        canvas.rotate(mRotationRect, mHalfWidth, mHalfHeight);

        canvas.drawRect(mTopRect, mRectPaint);
        canvas.drawRect(mBottomRect, mRectPaint);
        canvas.drawRect(mRightRect, mRectPaint);
        canvas.drawRect(mLeftRect, mRectPaint);
    }

    private static class AnimationModel {
        private float mFromScaleX, mToScaleX;
        private TimeInterpolator mScaleXInterpolator;

        private float mFromScaleY, mToScaleY;
        private TimeInterpolator mScaleYInterpolator;

        private float mFromRotation, mToRotation;
        private TimeInterpolator mRotationInterpolator;

        public static AnimationModel create(float _fromRotation, float _toRotation) {
            AnimationModel model = new AnimationModel();
            model.mFromRotation = _fromRotation;
            model.mToRotation = _toRotation;

            return model;
        }

        public AnimationModel setScaleX(float _fromScaleX, float _toScaleX) {
            mFromScaleX = _fromScaleX;
            mToScaleX = _toScaleX;
            return this;
        }

        public AnimationModel setScaleY(float _fromScaleY, float _toScaleY) {
            mFromScaleY = _fromScaleY;
            mToScaleY = _toScaleY;
            return this;
        }

        public AnimationModel setRotationInterpolator(TimeInterpolator _interpolator) {
            mRotationInterpolator = _interpolator;
            return this;
        }

        public AnimationModel setXInterpolator(TimeInterpolator _interpolator) {
            mScaleXInterpolator = _interpolator;
            return this;
        }

        public AnimationModel setYInterpolator(TimeInterpolator _interpolator) {
            mScaleYInterpolator = _interpolator;
            return this;
        }
    }
}
