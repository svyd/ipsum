package com.capt.domain.profile.settings.model;

import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;

/**
 * Created by Svyd on 20.04.2016.
 */
public class Account extends BaseItems {
    @Override
    public String getHeaderTitle() {
        return "Account";
    }

    @Override
    public String getHeaderDescription() {
        return "";
    }
}
