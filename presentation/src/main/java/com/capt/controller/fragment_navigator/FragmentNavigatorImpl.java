package com.capt.controller.fragment_navigator;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;

import com.capt.R;
import com.capt.base.BaseActivity;
import com.capt.base.BaseFragment;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.22..
 */
public class FragmentNavigatorImpl extends FragmentNavigator {

    private BaseActivity mActivity;

    @Inject
    public FragmentNavigatorImpl(BaseActivity _activity, FragmentManager _manager, @IdRes int _containerId) {
        super(_manager, _containerId);
        mActivity = _activity;
    }

    @Override
    public void clearBackStack() {
        int entryCount = mFragmentManager.getBackStackEntryCount();
        if (entryCount <= 0)
            return;

        FragmentManager.BackStackEntry entry = mFragmentManager.getBackStackEntryAt(0);
        int id = entry.getId();
        mActivity.runOnUiThread(() ->
                mFragmentManager.popBackStackImmediate(id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        );

    }

    @Override
    public void replaceFragmentWithoutBackStack(BaseFragment _fragment) {
        clearBackStack();
        getTransaction()
                .replace(mContainerId, _fragment)
                .commit();
    }

    @Override
    public void addFragmentWithoutBackStack(BaseFragment _fragment) {
        clearBackStack();
        getTransaction()
                .add(mContainerId, _fragment)
                .commit();
    }

    @Override
    public void addFragmentWithContainer(BaseFragment _fragment, @IdRes int _containerId) {
        getTransaction()
                .replace(_containerId, _fragment)
                .commit();
    }

    @Override
    public void replaceFragmentWithBackStack(BaseFragment _fragment, @IdRes int _containerId) {
        getTransaction()
                .setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout)
                .replace(_containerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void replaceFragmentWithBackStack(BaseFragment _fragment) {
        getTransaction()
                .setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout)
                .replace(mContainerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void addFragmentWithBackStack(BaseFragment _fragment) {
        getTransaction()
                .setCustomAnimations(R.anim.fadein, R.anim.fadeout, R.anim.fadein, R.anim.fadeout)
                .add(mContainerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void popBackStack() {
        mFragmentManager.popBackStack();
    }

    @Override
    public BaseFragment getTopFragment() {
        return (BaseFragment) mFragmentManager.findFragmentById(mContainerId);
    }

    @Override
    public BaseFragment getTopFragment(@IdRes int _containerId) {
        return (BaseFragment) mFragmentManager.findFragmentById(_containerId);

    }
}
