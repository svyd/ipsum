package com.capt.domain.profile.assignment.cache;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 07.07.2016.
 */
public class GetAssignmentInteractor extends BaseInteractor {

    AssignmentCache mCache;

    @Inject
    public GetAssignmentInteractor(PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
    }

    @Override
    protected Observable buildGetObserver() {
        return mCache.getAssignment();
    }
}
