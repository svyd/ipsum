package com.capt.profile.dashboard.ui.adapter;

/**
 * Created by Svyd on 28.03.2016.
 */
public abstract class ViewTypeConstants {
    public static final int VIEW_TYPE_ASSIGNMENTS = 0;
    public static final int VIEW_TYPE_MARKETPLACE = 1;
    public static final int VIEW_TYPE_TUTORIAL = 2;
    public static final int VIEW_TYPE_VIDEO = 3;
    public static final int VIEW_TYPE_PROGRESS = 4;
    public static final int VIEW_TYPE_REMINDER = 5;
}
