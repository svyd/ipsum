package com.capt.profile.settings.currency.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.settings.currency.CurrencySettingsFragment;

import dagger.Subcomponent;

/**
 * Created by root on 29.04.16.
 */
@PerFragment
@Subcomponent(modules = {CurrencySettingsModule.class})
public interface CurrencySettingsComponent {
	void inject (CurrencySettingsFragment _fragment);
}
