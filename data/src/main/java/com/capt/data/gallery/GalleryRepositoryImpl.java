package com.capt.data.gallery;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;

import com.capt.domain.profile.gallery.GalleryRepository;
import com.capt.domain.profile.gallery.model.GalleryModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 4/23/16.
 */
public class GalleryRepositoryImpl implements GalleryRepository {

    private Context mContext;

    @Inject
    public GalleryRepositoryImpl(Context _context) {
        mContext = _context;
    }

    @Override
    public Observable<List<GalleryModel>> getVideoList() {
        return Observable.create(subscriber -> {
            try {
                List<GalleryModel> list = new ArrayList<>();
                String orderBy = android.provider.MediaStore.Video.Media.DATE_TAKEN + " DESC";
                Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

                ContentResolver contentResolver = mContext.getContentResolver();
                Cursor cursor = contentResolver.query(uri, null, null, null, orderBy);
                if (cursor == null) {
                    subscriber.onCompleted();
                    return;
                }

                while (cursor.moveToNext()) {
                    String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
                    if (path.contains(" ")) {
                        continue;
                    }
                    long duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.VideoColumns.DURATION));
                    GalleryModel model = new GalleryModel();
                    model.setPath(path);
                    model.setRightDuration(duration > 10000);
                    list.add(model);
                }
                if (subscriber.isUnsubscribed()) {
                    return;
                }
                subscriber.onNext(list);
                subscriber.onCompleted();
            } catch (Exception _e) {
                subscriber.onError(_e);
            }
        });
    }
}
