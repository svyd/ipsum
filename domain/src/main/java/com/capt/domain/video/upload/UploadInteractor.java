package com.capt.domain.video.upload;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.model.Video;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by richi on 2016.03.11..
 */
public class UploadInteractor extends BasePostInteractor<Video> {

    private VideoUploadRepository mRepository;

    @Inject
    public UploadInteractor(VideoUploadRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);

        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(Video _data) {
        return mRepository.uploadToCloud(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return mRepository.uploadPendingVideos();
    }
}
