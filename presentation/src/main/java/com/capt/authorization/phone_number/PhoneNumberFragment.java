package com.capt.authorization.phone_number;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.capt.R;
import com.capt.authorization.code_verification.CodeVerifyFragment;
import com.capt.authorization.country_code.CountryFragment;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.phone_number.dagger.PhoneNumberComponent;
import com.capt.authorization.phone_number.dagger.PhoneNumberModule;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.BaseFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.global.Constants;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by richi on 2016.03.17..
 */
public class PhoneNumberFragment extends BaseToolbarFragment implements PhoneNumberContract.View {

    public static final int COUNTRY_REQUEST_CODE = 101;

    @Inject PhoneNumberContract.Presenter mPresenter;
    @Inject ToolbarManager mToolbar;

    @Bind(R.id.tvCode_FPN)      TextView tvCode;
    @Bind(R.id.btnNext_FPN)     Button btnNext;

    private PhoneNumberComponent mComponent;

    private String mCountryCode;

    public static BaseFragment newInstance(CodeSignUpModel _model) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_CODE_SIGN_UP, _model);

        BaseFragment fragment = new PhoneNumberFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_phone_number);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        showToolbar();
        checkExtra();
        initPresenter(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.savedInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void initComponent() {
        if (mComponent == null) {
            mComponent = getComponent(AuthActivityComponent.class)
                    .createPhoneNumberComponent(new PhoneNumberModule(this));
        }
    }

    private void inject() {
        if (mPresenter == null)
            mComponent.inject(this);
    }

    private void checkExtra() {
        CodeSignUpModel model = null;
        Bundle bundle = getArguments();
        if (bundle != null && !bundle.isEmpty()) {
            model = (CodeSignUpModel) bundle.getSerializable(Constants.Extra.EXTRA_CODE_SIGN_UP);
        }

        mPresenter.setModel(model);
    }

    private void initPresenter(Bundle savedInstanceState) {
        mPresenter.restoreInstanceState(savedInstanceState);
        mPresenter.setCountryCode(mCountryCode);
        mPresenter.initialize();
    }

    private void showToolbar() {
        mToolbar.showToolbar();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void startCountryCodeFragment() {
        BaseFragment fragment = CountryFragment.newInstance();
        fragment.setTargetFragment(this, COUNTRY_REQUEST_CODE);

        getFragmentNavigator().replaceFragmentWithBackStack(fragment);
    }

    @Override
    public void onCountryCodeSet(String _code) {
        tvCode.setText(_code);
    }

    @Override
    public void setEnableNext(boolean _enable) {
        btnNext.setEnabled(_enable);
    }

    @Override
    public void startCodeConfirmScreen() {
        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_CODE_SIGN_UP, mPresenter.getModel());
        getFragmentNavigator().replaceFragmentWithBackStack(CodeVerifyFragment.newInstance(args));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == COUNTRY_REQUEST_CODE) {
            mCountryCode = data.getStringExtra(Constants.Extra.EXTRA_STRING);
        }
    }

    @OnTextChanged(R.id.etPhone_FPN)
    protected void onTextChanged(CharSequence _text) {
        mPresenter.onTextChanged(_text.toString());
    }

    @OnClick(R.id.tvCode_FPN)
    protected void onCodeClick() {
        mPresenter.onCodeClick();
    }

    @OnClick(R.id.btnNext_FPN)
    protected void onNext() {
        mPresenter.onNext();
    }

    @Override
    public void navigateToJoinScreen() {
        startCodeConfirmScreen();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
