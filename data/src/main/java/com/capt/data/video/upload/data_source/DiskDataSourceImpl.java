package com.capt.data.video.upload.data_source;

import com.capt.data.base.BaseDAO;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.DiskDataSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 14.04.2016.
 */
public class DiskDataSourceImpl implements DiskDataSource {

    private BaseDAO<Video> mDao;

    @Inject
    public DiskDataSourceImpl(BaseDAO<Video> _dao) {
        mDao = _dao;
    }

    @Override
    @SuppressWarnings("Convert2streamapi")
    public Observable<List<Video>> getAll() {
        return Observable.create((Observable.OnSubscribe<List<Video>>) subscriber -> {
            try {
                List<Video> videos = mDao.getAll();
                List<Video> result = new ArrayList<>();
                for (Video video : videos) {
                    if (!video.isUploading() && video.isDraft()) {
                        result.add(video);
                    }
                }
                subscriber.onNext(result);
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    @SuppressWarnings("Convert2streamapi")
    public Observable<List<Video>> getAllUploading() {
        return Observable.create((Observable.OnSubscribe<List<Video>>) subscriber -> {
            try {
                List<Video> videos = mDao.getAll();
                List<Video> result = new ArrayList<>();
                for (Video video : videos) {
                    if (video.isUploading() && !video.isDraft()) {
                        result.add(video);
                    }
                }
                subscriber.onNext(sortQueue(result));
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<Video> get(String _id) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(mDao.get(_id));
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<Long> add(Video _model) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(mDao.add(_model));
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<Long> update(Video _model) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(mDao.updateDraft(_model));
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<Video> updatePending(Video _model) {
        return Observable.create(subscriber -> {
            try {
                mDao.updatePending(_model);
                subscriber.onNext(_model);
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<Integer> deleteByDraftId(String _id) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(mDao.deleteByDraftId(_id));
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<Integer> deleteById(String _id) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(mDao.deleteById(_id));
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    private List<Video> sortQueue(List<Video> _videos) {
        Collections.sort(_videos);
        for (Video video: _videos) {
            if (video.isCurrentlyUploading()) {
                _videos.remove(video);
                _videos.add(0, video);
                break;
            }
        }
        return _videos;
    }

}
