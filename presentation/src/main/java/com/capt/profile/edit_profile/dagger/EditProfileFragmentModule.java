package com.capt.profile.edit_profile.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.edit_profile.EditProfileFragmentContract;
import com.capt.profile.edit_profile.EditProfilePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 4/29/16.
 */
@Module
public class EditProfileFragmentModule {

    private EditProfileFragmentContract.View mView;

    public EditProfileFragmentModule(EditProfileFragmentContract.View _view) {
        mView = _view;
    }

    @PerFragment
    @Provides
    protected EditProfileFragmentContract.View provideView() {
        return mView;
    }

    @PerFragment
    @Provides
    protected EditProfileFragmentContract.Presenter providePresenter(EditProfilePresenter _presenter) {
        return _presenter;
    }
}
