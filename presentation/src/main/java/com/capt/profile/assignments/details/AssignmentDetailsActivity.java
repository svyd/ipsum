package com.capt.profile.assignments.details;

import android.os.Bundle;
import android.view.MenuItem;

import com.capt.R;
import com.capt.application.dagger.HasComponent;
import com.capt.authorization.dagger.RestModule;
import com.capt.base.BaseActivity;
import com.capt.data.authorization.net.ApiConstants;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.profile.assignments.details.dagger.DetailsActivityComponent;
import com.capt.profile.assignments.details.dagger.DetailsActivityModule;

/**
 * Created by Svyd on 28.04.2016.
 */
public class AssignmentDetailsActivity extends BaseActivity implements HasComponent<DetailsActivityComponent> {

    private DetailsActivityComponent mComponent;
    private String mType;
    private Assignment mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_details);
        initComponent();
        inject();
        checkExtra();
        if (savedInstanceState == null) {
            navigateToFragment();
        }
    }

    private void checkExtra() {
        Bundle ars = getIntent().getExtras();
        if (ars != null &&
                ars.containsKey(Constants.Extra.EXTRA_STRING)
                && ars.containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            mType = ars.getString(Constants.Extra.EXTRA_STRING);
            mData = (Assignment) ars.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        } else {
            throw new IllegalStateException("AssignmentDetailsActivity should contain extra");
        }
    }

    private void navigateToFragment() {
        getFragmentNavigator()
                .replaceFragmentWithoutBackStack(DetailsFragment.newInstance(mData, mType));
    }

    @Override
    public int getContainerId() {
        return R.id.flContainer_AAD;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AAS;
    }

    private void initComponent() {
        mComponent = getApplicationComponent()
                .createDetailsActivityComponent(
                        new DetailsActivityModule(this),
                        new RestModule(ApiConstants.GOOGLE_MAPS_API_ENDPOINT)
                );
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public DetailsActivityComponent getComponent() {
        return mComponent;
    }
}
