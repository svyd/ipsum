package com.capt.data.payout;

import com.capt.data.base.TypeMapper;
import com.capt.domain.profile.payout.PayoutModel;
import com.capt.domain.profile.payout.PayOutRepository;
import com.capt.domain.profile.payout.PayoutScreenModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by rMozes on 8/3/16.
 */
public class PayoutRepositoryImpl implements PayOutRepository {

    private PayoutService mDelegate;
    private TypeMapper<String, String> mDateConverter;

    @Inject
    public PayoutRepositoryImpl(PayoutService _service, TypeMapper<String, String> _converter) {
        mDelegate = _service;
        mDateConverter = _converter;
    }

    @Override
    public Observable<PayoutScreenModel> getPayouts() {
        return mDelegate.getPayoutLis().map(payoutModels -> {
            PayoutScreenModel screenModel = new PayoutScreenModel();

            String symbol = "";
            double totalPrice = 0;

            if (!payoutModels.isEmpty() && payoutModels.get(0).getCurrency().getSymbol() != null)
                symbol = payoutModels.get(0).getCurrency().getSymbol();

            for (PayoutModel payoutModel : payoutModels) {
                payoutModel.setUpdatedAt(mDateConverter.convert(payoutModel.getUpdatedAt()));
                totalPrice += Double.parseDouble(payoutModel.getPrice());
                payoutModel.setPrice(payoutModel.getCurrency().getSymbol() + " " + payoutModel.getPrice());
            }

            screenModel.setTotalPayout(symbol + " " + String.valueOf(totalPrice));
            screenModel.setModelList(payoutModels);

            return screenModel;
        });
    }
}
