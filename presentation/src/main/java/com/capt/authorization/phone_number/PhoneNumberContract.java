package com.capt.authorization.phone_number;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.CodeSignUpModel;

/**
 * Created by richi on 2016.03.17..
 */
public interface PhoneNumberContract {

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onTextChanged(String _text);
        public abstract void onCodeClick();
        public abstract void onNext();
        public abstract void setCountryCode(String _code);
        public abstract void savedInstanceState(Bundle _bundle);
        public abstract void restoreInstanceState(Bundle _bundle);
        public abstract void setModel(CodeSignUpModel _model);
        public abstract CodeSignUpModel getModel();
    }

    interface View extends BaseView {
        void setEnableNext(boolean _enable);
        void onCountryCodeSet(String _code);
        void startCountryCodeFragment();
        void startCodeConfirmScreen();
    }
}
