package com.capt.video.trim.file_manager;

import java.io.File;

/**
 * Created by richi on 2016.02.29..
 */
public abstract class FileManager {
    public abstract File getTrimPath(String _name);
    public abstract File getVideoPath(String _name);
    public abstract boolean renameFile(File _file, String _name);
}
