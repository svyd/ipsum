package com.capt.video.category.flow;

import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.video.model.Video;
import com.capt.video.category.CategoryContract;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by Svyd on 02.05.2016.
 */
public class CategoryEditPresenter extends AbstractCategoryPresenter {

    private static final String TAG = CategoryEditPresenter.class.getSimpleName();
    private BasePostInteractor<Video> mPatchInteractor;

    @Inject
    public CategoryEditPresenter(BasePostInteractor<Video> _patchInteractor,
                                 BaseInteractor _interactor,
                                 CategoryContract.View _view, BaseExceptionDelegate _delegate) {
        super(_interactor, _view, _delegate);
        mPatchInteractor = _patchInteractor;
    }

    @Override
    protected void doNext() {
        getView().showProgress();
        mPatchInteractor.execute(getData(), new PatchObserver(this));
    }

    @Override
    public void onStop() {
        super.onStop();
        mPatchInteractor.unSubscribe();
    }

    class PatchObserver extends BaseObserver<Video> {
        public PatchObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            getView().hideProgress();
            getView().navigateBack();
        }

        @Override
        public void onNext(Video video) {
            Log.d(TAG, "onNext() called with: " + "video = [" + video + "]");
        }
    }
}
