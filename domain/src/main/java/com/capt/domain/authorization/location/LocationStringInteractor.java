package com.capt.domain.authorization.location;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 16.03.2016.
 */

public class LocationStringInteractor extends BasePostInteractor<String> {

    private LocationDataStore mStore;

    @Inject
    public LocationStringInteractor(LocationDataStore _store, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mStore = _store;
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mStore.getAddressList(_data);
    }

}
