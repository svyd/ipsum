package com.capt.domain.profile.update_profile;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 5/18/16.
 */
public class UpdateProfileLocationInteractor extends BasePostInteractor<LocationModel> {

    private AuthRepository mRepository;

    @Inject
    public UpdateProfileLocationInteractor(PostExecutionThread postExecutionThread, AuthRepository _repository) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(LocationModel _data) {
        return mRepository.updateUserModel(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
