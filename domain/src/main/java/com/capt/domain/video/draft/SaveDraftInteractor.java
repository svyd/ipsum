package com.capt.domain.video.draft;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.model.Video;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 25.04.2016.
 */
public class SaveDraftInteractor extends BasePostInteractor<Video> {

    private DraftRepository mDataSource;

    @Inject
    public SaveDraftInteractor(DraftRepository _dataSource, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mDataSource = _dataSource;
    }

    @Override
    protected Observable buildPostObservable(Video _data) {
        return mDataSource.add(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
