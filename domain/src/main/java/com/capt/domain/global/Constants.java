package com.capt.domain.global;

/**
 * Created by Svyd on 24.02.2016.
 */
public abstract class Constants {

    public static final String DEVICE_GCM_TOKEN = "device_gcm_token";

    public static final String REQUEST_CODE = "request_code";
    public static final String RESULT_CODE = "result_code";
    public static final String RESULT_BUNDLE = "result_bundle";

    //bundle keys
    public static final String BUNDLE_INSTAGRAM_MODEL = "insta_model";
    public static final String BUNDLE_REDIRECTED_URI = "redirected_uri";
    public static final String BUNDLE_USER_MODEL = "user_model";
    public static final String BUNDLE_VIDEO_UPLOAD_MODEL = "video_upload_model";
    public static final String BUNDLE_VIDEO = "video";
    public static final String BUNDLE_VIDEO_PATH = "video_path";
    public static final String BUNDLE_ASSIGNMENT = "assignment";

    //shared preferences
    public static final String PREFERENCES_USER_PROFILE = "user_profile";
    public static final String PREFERENCES_DASHBOARD = "dashboard";

    //intent request codes
    public static final int INSTA_AUTH_ACTIVITY_REQUEST_CODE = 0;

    //other stuff
    public static final String PREFERENCE_NAME_COOKIES = "cookies";

    public static final String ACCESS_TOKEN = "access_token";
    public static final String INSTAGRAM_PASSWORD_LINK = "https://www.instagram.com/accounts/password/reset/";

    public static final String FLAG_TO_RECENT = "to_recent";

    public static final String SUCCESS = "Ok";

    public static final int CONVERT_TO_MB_COEF = 1048576;
    public static final String ASSIGNMENT_PREFIX = "assignment";

    public enum MarketplaceItems {Recent, Drafts, Sold}

    public class RequestCode {
        public static final int SHOULD_UPDATE = 10;
        public static final int REMINDER = 11;
        public static final int ASSIGNMENT = 12;
    }

    public class MarketplaceTypes {
        public static final String RECENT = "recent";
        public static final String DRAFTS = "drafts";
        public static final String SOLD = "sold";
    }

    public class AssignmentTypes {
        public static final String FOR_GRABS = "pending";
        public static final String IN_PROGRESS = "active";
        public static final String FINISHED = "completed";
    }

    public class AuthType {
        public static final String INSTAGRAM = "instagram";
        public static final String FACEBOOK = "facebook";
        public static final String TWITTER = "twitter";
    }

    public static final String FLOW = "flow";


    public class NamedAnnotation {
        public static final String HEADER_INTERCEPTOR = "header_interceptor";
        public static final String BODY_INTERCEPTOR = "body_interceptor";

        public static final String PREFERENCES_DEFAULT = "preferences_default";
        public static final String PREFERENCES_COOKIES = "preferences_cookies";

        public static final String SOC_NET_AUTH_INTERACTOR = "soc_net_auth";
        public static final String VERIFICATION_INTERACTOR = "verification";
        public static final String CODE_CHECK_INTERACTOR = "code_check";
        public static final String SIGN_IN_INTERACTOR = "sign_in";
        public static final String SIGN_OUT_INTERACTOR = "sign_out";
        public static final String CHECK_EMAIL_INTERACTOR = "check_email";

        public static final String DASHBOARD_INTERACTOR = "dashboard_interactor";
        public static final String COUNTRY_CODE_INTERACTOR = "country_code_interactor";
        public static final String LOCATION_STRING_INTERACTOR = "location_string_interactor";
        public static final String FORGOT_PASSWORD_INTERACTOR = "forgot_password_interactor";
        public static final String LOCATION_LAT_LNG_INTERACTOR = "location_lat_lng_interactor";
        public static final String LOCATION_DETAILS_INTERACTOR = "location_details_interactor";
        public static final String MAPS_API_RETROFIT = "maps_api_retrofit";
        public static final String FORGOT_PASSWORD_RETROFIT = "forgot_password_retrofit";
        public static final String MAIN_RETROFIT = "main_app_retrofit";
        public static final String CURRENCY_INTERACTOR = "currency_interactor";
        public static final String GET_VIDEO_INTERACTOR = "get_video_interactor";
        public static final String CATEGORY_INTERACTOR = "category_interactor";
        public static final String VIDEO_PATCH_INTERACTOR = "video_patch_interactor";
        public static final String VIDEO_DELETE_INTERACTOR = "video_delete_interactor";
        public static final String DRAFT_DELETE_INTERACTOR = "draft_delete_interactor";
        public static final String DRAFT_UPDATE_INTERACTOR = "draft_update_interactor";
        public static final String UPLOAD_REJECTED_INTERACTOR = "upload_rejected_interactor";
        public static final String DELETE_REJECTED_INTERACTOR = "delete_rejected_interactor";
        public static final String SAVE_DRAFT_INTERACTOR = "save_draft_interactor";

        public static final String UPDATE_PHOTOT_INTERACTOR = "update_photo_interactor";

        public static final String GALLERY_INTERACTOR = "gallery_interactor";
        public static final String SETTINGS_INTERACTOR = "settings_interactor";
        public static final String GET_SETTINGS_INTERACTOR = "get_settings_interactor";
        public static final String UPDATE_PROFILE_INTERACTOR = "update_profile_interactor";
        public static final String UPDATE_LOCATION_INTERACTOR = "update_location_profile_interactor";
        public static final String PENDING_VIDEO_INTERACTOR = "pending_video_interactor";
        public static final String PENDING_UPDATE_INTERACTOR = "pending_update_interactor";
        public static final String PENDING_UPDATE_TOKEN_INTERACTOR = "pending_update_token_interactor";

        public static final String CHANGE_PASSWORD_INTERACTOR = "change_password_interactor";
        public static final String ASSIGNMENT_CACHE_UPDATE = "update_cache_interactor";
        public static final String ASSIGNMENT_CACHE_CLEAR = "clear_cache_assignment_interactor";
        public static final String NON_EXCLUDED_GSON = "non_excluded_gson";

        public static final String PAYOUT = "payout_interactor";

        public class Mapper {
            public static final String ASSIGNMENTS_MAPPER = "assignments_mapper";
            public static final String MARKETPLACE_MAPPER = "marketplace_mapper";
            public static final String LOCATION_MAPPER = "location_mapper";
            public static final String SETTINGS_MAPPER = "settings_mapper";
        }

        public class Delegate {
            public static final String ASSIGNMENTS_DELEGATE = "assignments_delegate";
            public static final String MARKETPLACE_DELEGATE = "marketplace_delegate";
            public static final String TUTORIAL_DELEGATE = "tutorial_delegate";
            public static final String VIDEO_DELEGATE = "video_delegate";
            public static final String PROGRESS_DELEGATE = "progress_delegate";
            public static final String REMINDER_DELEGATE = "reminder_delegate";
        }

        public static final String TOP_ADAPTER = "top_adapter";
        public static final String BOTTOM_ADAPTER = "bottom_adapter";

    }

    public class Profile {
        public static final String GALLERY = "gallery";
        public static final String ASSIGNMENTS = "assignments";
    }

    public class Credentials {
        public static final String REDIRECT_URI = "http://capt.world";

        //PayPal
        public static final String PAYPAL_CONFIG_CLIENT_ID = "AQh4va4KcspuNuoTM4u8Rt2ELWmbzD-CUMuHNl61jLfQ8RCPze3mqmarGJ6h4vOEHOXYNlniIzeS_0Z3";
        public static final String PAYPAL_REGISTERED = "PayPalRegistered";

        //instagram credentials
        public static final String INSTAGRAM_CLIENT_ID = "96e5460d007c4c25bc2ffc5c98a2069b";
        public static final String INSTAGRAM_AUTH_URL = "https://api.instagram.com/oauth/authorize/";
        public static final String INSTAGRAM_CLIENT_SECRET = "89145dc1bce64545ad73a1f000c96552";
    }

    public class Settings {
        public class Account {
            public static final String NOTIFICATIONS = "NOTIFICATIONS";
            public static final String PERMISSIONS = "PERMISSIONS";
            public static final String CURRENCY = "CURRENCY";
            public static final String PAYMENT = "PAYMENT";
        }

        public class Profile {
            public static final String EDIT_PROFILE = "EDIT PROFILE";
            public static final String CHANGE_PASSWORD = "CHANGE PASSWORD";
            public static final String LOG_OUT = "LOG OUT";
        }

        public class Support {
            public static final String REPORT = "REPORT A PROBLEM";
            public static final String PRIVACY = "PRIVACY POLICY";
            public static final String TERMS = "TERMS & CONDITIONS";
        }
    }

    public class PendingVideoConstants {
        public static final String UPLOAD_FOR_ASSIGNMENT = "upload_for_assignment";
        public static final String UPLOAD_VIDEO = "upload_video";
        public static final String RETRY_UPLOAD_VIDEO = "retry_upload_video";
        public static final String UPLOAD_PENDING_VIDEOS = "upload_pending_videos";
        public static final String COMPLETE_PENDING_VIDEO_ID = "complete_pending_video_from_cache";
        public static final String CANCEL_PENDING_VIDEO_ID = "delete_pending_video_from_cache";
        public static final String PAY_PAL_INTERACTOR = "pay_pal_interactor";
    }

    public class Extra {
        public static final String EXTRA_CODE_SIGN_UP = "extra_main_sign_up_model";
        public static final String EXTRA_STRING = "extra_string";
        public static final String EXTRA_INT = "extra_int";
        public static final String EXTRA_SERIALIZABLE = "extra_serializable";
        public static final String EXTRA_CURRENCY = "extra_currency";
        public static final String EXTRA_LAT = "extra_lat";
        public static final String EXTRA_LON = "extra_lon";
        public static final String EXTAR_AUTH_FLOW = "extra_out_flow";
        public static final String EXTRA_USER = "EXTRA_USER";
    }

    public class Trim {
        public static final int MILLISEC = 1000;
    }

    public class Payment {
        public static final int CAPT_MARGIN = 20;
        public static final int CAPT_FEE = 4;
    }

    public class Gender {
        public static final int FEMALE = 1;
        public static final int MALE = 2;
    }
}
