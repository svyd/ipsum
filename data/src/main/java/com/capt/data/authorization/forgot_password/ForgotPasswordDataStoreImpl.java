package com.capt.data.authorization.forgot_password;

import com.capt.data.authorization.net.retrofit.ForgotPasswordService;
import com.capt.domain.authorization.forgot_password.ForgotPasswordDataStore;
import com.capt.domain.authorization.forgot_password.model.ForgotPasswordModel;
import com.capt.domain.authorization.model.SuccessModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 22.03.2016.
 */
public class ForgotPasswordDataStoreImpl implements ForgotPasswordDataStore {

    private ForgotPasswordService mService;

    @Inject
    public ForgotPasswordDataStoreImpl(ForgotPasswordService _service) {
        mService = _service;
    }

    @Override
    public Observable<SuccessModel> passEmail(ForgotPasswordModel _email) {
        return mService.forgotPassword(_email);
    }
}
