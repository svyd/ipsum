package com.capt.profile.assignments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.capt.R;
import com.capt.authorization.sign_up.CircleTransform;
import com.capt.data.annotation.PerFragment;
import com.capt.data.base.DateTimeUtility;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by root on 15.04.16.
 */
@PerFragment
public class AssignmentsAdapter extends RecyclerView.Adapter<AssignmentsAdapter.AssignmentViewHolder> {


    private Context mContext;
    private List<Assignment> mAssignments = new ArrayList<>();
    private boolean isForGrabs;
    private OnAssignmentClickListener assignmentClickListener;
    private DateTimeUtility mDateUtility;

    @Inject
    public AssignmentsAdapter(DateTimeUtility _utility, Context _context) {
        mContext = _context;
        mDateUtility = _utility;

    }

    public void setData(List<Assignment> _data, String type) {
        mAssignments = _data;
        isForGrabs = type.equals(Constants.AssignmentTypes.FOR_GRABS);
        notifyDataSetChanged();
    }

    public void addModels(List<Assignment> models) {
        int size = getItemCount();
        mAssignments.addAll(models);
        notifyItemRangeInserted(size, models.size());
    }

    public void clear() {
        mAssignments.clear();
        notifyDataSetChanged();
    }

    @Override
    public AssignmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.list_item_user_assignments, parent, false);
        return new AssignmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AssignmentViewHolder holder, int position) {
        Assignment assignment = mAssignments.get(position);

        Glide.with(mContext)
                .load(assignment.creator.getLogo())
                .transform(new CircleTransform(mContext))
//				.diskCacheStrategy(DiskCacheStrategy.NONE)
//				.skipMemoryCache(true)
                .into(holder.ivIcon);

        holder.tvCompany.setText(assignment.creator.getCompany());
        holder.tvTitle.setText(assignment.title);
        if (isForGrabs) {
            holder.tvTime.setVisibility(View.VISIBLE);
            holder.tvTime.setText(mDateUtility.formatDate(assignment.date, false));
        }

        if (assignment.video != null) {
            holder.tvState.setVisibility(View.VISIBLE);
            holder.tvState.setText(assignment.video.state);
        }

        holder.view.setOnClickListener(v -> assignmentClickListener.onAssignmentClicked(assignment));
    }

    @Override
    public int getItemCount() {
        return mAssignments.size();
    }

    public void setOnAssignmentClickListener(OnAssignmentClickListener listener) {
        this.assignmentClickListener = listener;
    }

    public void updateItem(String _id, String  _status) {
        for (Assignment assignment: mAssignments) {
            if (assignment.id.equals(_id)) {
                assignment.video.state = _status;
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void removeItem(String _id) {
        Iterator<Assignment> iterator = mAssignments.iterator();
        while (iterator.hasNext()) {
            Assignment assignment = iterator.next();
            if (assignment.id.equals(_id)) {
                iterator.remove();
                break;
            }
        }
        notifyDataSetChanged();
    }

    public interface OnAssignmentClickListener {
        void onAssignmentClicked(Assignment assignment);
    }

    public static class AssignmentViewHolder extends RecyclerView.ViewHolder {

        public View view;

        @Bind(R.id.iv_icon_LIUA)
        ImageView ivIcon;

        @Bind(R.id.tv_company_LIUA)
        TextView tvCompany;

        @Bind(R.id.tv_time_LIUA)
        TextView tvTime;

        @Bind(R.id.tv_title_LIUA)
        TextView tvTitle;

        @Bind(R.id.tvState_LIUA)
        TextView tvState;

        public AssignmentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }
}
