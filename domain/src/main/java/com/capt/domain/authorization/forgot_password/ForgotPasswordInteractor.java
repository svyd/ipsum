package com.capt.domain.authorization.forgot_password;

import com.capt.domain.authorization.forgot_password.model.ForgotPasswordModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 18.03.2016.
 */
public class ForgotPasswordInteractor extends BasePostInteractor<ForgotPasswordModel>{

    private ForgotPasswordDataStore mStore;

    @Inject
    public ForgotPasswordInteractor(ForgotPasswordDataStore _store, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mStore = _store;
    }

    @Override
    protected Observable buildPostObservable(ForgotPasswordModel _data) {
        return mStore.passEmail(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
