package com.capt.data.base;


/**
 * Created by richi on 2016.02.29..
 */
public interface TimeUtil {
    String getTimeFromInteger(long _time);
    String getTimeFromIntegerMinutes(long _time);
    long getTimeFromPercentage(int _percentage, long _duration);
    String getTimeStringFromPercentage(int _percentage, long _duration);

}
