package com.capt.profile.assignments.details.dagger;

import com.capt.authorization.location.LocationInteractorManager;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.annotation.PerFragment;
import com.capt.data.assignment.accept.AcceptAssignmentRepositoryImpl;
import com.capt.data.assignment.accept.AcceptAssignmentService;
import com.capt.data.base.DateTimeUtility;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.accept.AcceptAssignmentInteractor;
import com.capt.domain.profile.assignment.accept.AcceptAssignmentRepository;
import com.capt.domain.profile.assignment.accept.model.AcceptAssignmentModel;
import com.capt.profile.assignments.details.DetailsContract;
import com.capt.profile.assignments.details.flow.DetailsPresenter;
import com.capt.profile.assignments.details.flow.ForGrabsFlow;
import com.capt.profile.assignments.details.flow.InProgressPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by root on 20.04.16.
 */
@Module
public class DetailsModule {

    DetailsContract.View mView;
    String mFlow;

    public DetailsModule(DetailsContract.View _view,
                         String _flow) {
        mView = _view;
        mFlow = _flow;
    }

    @Provides
    @PerFragment
    protected DetailsContract.Presenter providePresenter(DateTimeUtility _utility,
                                                         DetailsContract.View _view,
                                                         BasePostInteractor<AcceptAssignmentModel> _interactor,
                                                         LocationInteractorManager _manager,
                                                         BaseExceptionDelegate _delegate) {
        switch (mFlow) {
            case Constants.AssignmentTypes.IN_PROGRESS:
                return new InProgressPresenter(_utility, _view, _interactor, _manager, _delegate);
            case Constants.AssignmentTypes.FOR_GRABS:
                return new ForGrabsFlow(_utility, _view, _interactor, _manager, _delegate);
            default:
                throw new RuntimeException("Unsupported flow");
        }
    }

    @Provides
    @PerFragment
    protected DetailsContract.View provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    AcceptAssignmentService provideAcceptAssignmentService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                           Retrofit _retrofit) {
        return _retrofit.create(AcceptAssignmentService.class);
    }

    @Provides
    @PerFragment
    AcceptAssignmentRepository provideAcceptAssignmentRepository(AcceptAssignmentRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    protected BasePostInteractor<AcceptAssignmentModel> provideAcceptAssignmentInteractor(AcceptAssignmentInteractor _interactor) {
        return _interactor;
    }
}
