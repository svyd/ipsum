package com.capt.authorization.sign_up;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.capt.R;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.location.LocationFragment;
import com.capt.authorization.phone_number.PhoneNumberFragment;
import com.capt.authorization.sign_up.dagger.SignUpComponent;
import com.capt.authorization.sign_up.dagger.SignUpModule;
import com.capt.authorization.sign_up.image_picker.ImagePickerFragment;
import com.capt.base.BaseFragment;
import com.capt.base.BaseLocationFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.SignUpModel;
import com.capt.domain.global.Constants;
import com.capt.video.Flow;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by richi on 2016.03.17..
 */

public class SignUpFragment extends BaseLocationFragment implements SignUpContract.View {

    public static final int REQUEST_CODE_LOCATION       = 134;
    public static final int REQUEST_PICK_PHOTO          = 135;

    @Inject SignUpContract.Presenter mPresenter;
    @Inject CircleTransform mTransform;
    @Inject ToolbarManager mToolbarManager;

    @Bind(R.id.etEmail_FSU)     EditText etEmail;
    @Bind(R.id.etFullName_FSU)  EditText etFullName;
    @Bind(R.id.etPassword_FSU)  EditText etPassword;
    @Bind(R.id.etLocation_FSU)  EditText etLocation;
    @Bind(R.id.etDay_FSU)       EditText etDay;
    @Bind(R.id.etMonth_FSU)     EditText etMonth;
    @Bind(R.id.etYear_FSU)      EditText etYear;
    @Bind(R.id.rgGender_FSU)    RadioGroup rgGender;
    @Bind(R.id.ivCamera_FSU)    ImageView ivCamera;
    @Bind(R.id.btnNext_FSU)     Button btnNext;
    @Bind(R.id.pbEmail_FSU)     ProgressBar pbEmail;
    @Bind(R.id.ivEye_FSU)       ImageView ivEye;

    @Bind(R.id.etConfirmPassword_FSU)
    EditText etConfirmPassword;

    @Bind(R.id.ivEyeConfirm_FSU)
    ImageView ivEyeConfirm;

    private Toast mToast;

    private SignUpComponent mComponent;

    private String mLocationName;
    private String mPhotoPath;

    public static BaseFragment newInstance() {
        return new SignUpFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sign_up);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        initComponent();
        inject();
        super.onActivityCreated(savedInstanceState);

        initPresenter(savedInstanceState);
        setUpToolbar();
        addListener();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(AuthActivityComponent.class)
                .createSignUpComponent(new SignUpModule(this));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void initPresenter(Bundle _bundle) {
        mPresenter.onRestoreInstanceState(_bundle);
        mPresenter.setPhotoPath(mPhotoPath);
        mPresenter.locationNameSelected(mLocationName);
        mPresenter.initialize();
    }

    private void setUpToolbar() {
        mToolbarManager.showToolbar();
        getActivity().setTitle("Sign up");
    }

    private void addListener() {
        rgGender.setOnCheckedChangeListener(mGenderListener);
    }

    private RadioGroup.OnCheckedChangeListener mGenderListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.rbFemale_FSU:
                    mPresenter.onGenderChanged(Constants.Gender.FEMALE);
                    break;
                case R.id.rbMale_FSU:
                    mPresenter.onGenderChanged(Constants.Gender.MALE);
                    break;
            }
        }
    };

    @Override
    public String getPassword() {
        return etPassword.getText().toString();
    }

    @Override
    public String getConfirmedPassword() {
        return etConfirmPassword.getText().toString();
    }

    @Override
    public void startLocationFragment() {
        Bundle args = new Bundle();
        args.putInt(Constants.FLOW, Flow.EDIT_PROFILE);

        BaseFragment fragment = LocationFragment.newInstance(args);
        fragment.setTargetFragment(this, REQUEST_CODE_LOCATION);

        getFragmentNavigator().replaceFragmentWithBackStack(fragment);
    }

    @Override
    public void startPhoneNumberFragment(CodeSignUpModel _signUpModel) {
        getFragmentNavigator().replaceFragmentWithBackStack(PhoneNumberFragment.newInstance(_signUpModel));
    }

    @Override
    public void takePhoto() {
        ImagePickerFragment.showWithTarget(this, REQUEST_PICK_PHOTO);
    }

    @Override
    public void enableNextBtn(boolean _isEnable) {
        if (btnNext != null)
            btnNext.setEnabled(_isEnable);
    }

    @Override
    public void showMessage(String _message) {
        if (mToast != null)
            mToast.cancel();
        mToast = Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent _data) {
        if (requestCode == REQUEST_CODE_LOCATION && resultCode == Activity.RESULT_OK) {
            locationResult(_data);
        } if (requestCode == REQUEST_CODE_LOCATION && resultCode == Activity.RESULT_CANCELED) {
            locationResult(_data);
        } else if (requestCode == REQUEST_PICK_PHOTO && resultCode == Activity.RESULT_OK) {
            pickImageResult(_data);
        }
        super.onActivityResult(requestCode, resultCode, _data);
    }

    @Override
    public void needToUpdateLocation(boolean _isNeeded) {
        this.needUpdateLocation(_isNeeded);
    }

    @Override
    public void showDatePickerDialog(int _year, int _month, int _day) {
        DatePickerDialog.newInstance(mBirthdayListener, _year, _month, _day)
                .show(getActivity().getFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    private DatePickerDialog.OnDateSetListener mBirthdayListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            mPresenter.onBirthdayChanged(year, monthOfYear, dayOfMonth);
        }
    };

    @Override
    public void renderBirthday(String _year, String _month, String _day) {
        etYear.setText(_year);
        etMonth.setText(_month);
        etDay.setText(_day);
    }

    private void locationResult(Intent _data) {
        if (_data != null) {
            LocationModel model = (LocationModel) _data
                    .getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE);
            mLocationName = model.result.address;
            mPresenter.setLatLng(model.getLatLngArray());
        } else {
            mLocationName = null;
            mPresenter.locationNameSelected("");
            needUpdateLocation(true);
        }
    }

    private void pickImageResult(Intent _data) {
        mPhotoPath = _data.getStringExtra(Constants.Extra.EXTRA_STRING);
        if (mPresenter != null)
            mPresenter.setPhotoPath(mPhotoPath);
    }

    @Override
    public void loadAvatarFrom(String _path) {
        Glide.with(this)
                .load(_path)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .bitmapTransform(mTransform)
                .error(R.drawable.ic_camera)
                .placeholder(R.drawable.ic_camera)
                .into(ivCamera);
    }

    @Override
    public void setLocationName(String _name) {
        if (etLocation != null)
            etLocation.setText(_name);
    }

    @Override
    public void showEmailIsUsed(boolean _show) {
        if (etEmail != null)
            etEmail.setError(_show ? "Email is already used" : null);
    }

    private SignUpModel.Builder getSignUpCredentials() {
        return new SignUpModel.Builder()
                .setEmail(etEmail.getText().toString())
                .setFullName(etFullName.getText().toString())
                .setPassword(etPassword.getText().toString());
    }

    @Override
    public void showPassword() {
        etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        ivEye.setImageResource(R.drawable.ic_eye2);
    }

    @Override
    public void hidePassword() {
        etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        ivEye.setImageResource(R.drawable.ic_eye);
    }

    @Override
    public void showConfirmPassword() {
        etConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        ivEyeConfirm.setImageResource(R.drawable.ic_eye2);
    }

    @Override
    public void hideConfirmPassword() {
        etConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        ivEyeConfirm.setImageResource(R.drawable.ic_eye);
    }

    @Override
    public void onLocationChanged(Location location) {
        mPresenter.onLocationChanged(location);
    }

    //OnClick
    @OnClick(R.id.etLocation_FSU)
    protected void onLocationChooseClick() {
        mPresenter.onLocationChooseClick();
    }

    @OnClick(R.id.btnNext_FSU)
    protected void onNextClick() {
        SignUpModel.Builder builder = getSignUpCredentials();

        mPresenter.onNextClick(builder);
    }

    @OnClick(R.id.ivCamera_FSU)
    protected void onCameraClick() {
        mPresenter.onCameraClick();
    }

    @OnClick(R.id.ivEye_FSU)
    protected void onEyeClick() {
        mPresenter.onEyeClick();
    }

    @OnClick(R.id.ivEyeConfirm_FSU)
    protected void onConfirmEyeClick() {
        mPresenter.onConfirmEyeClick();
    }

    @OnTextChanged({R.id.etFullName_FSU, R.id.etPassword_FSU, R.id.etLocation_FSU})
    protected void onCredentialsChanged() {
        mPresenter.onValidateCredentials(getSignUpCredentials().build());
    }

    @OnTextChanged(R.id.etEmail_FSU)
    protected void onEmailTextChanged(CharSequence _email) {
        mPresenter.onEmailTextChanged(_email.toString());
    }

    @OnClick({R.id.rlBirthdayContainer_FSU, R.id.etDay_FSU, R.id.etYear_FSU, R.id.etMonth_FSU})
    protected void onBirthdayClicked() {
        mPresenter.onBirthdayClicked();
    }

    @Override
    public void showProgress() {
        pbEmail.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (pbEmail != null)
            pbEmail.setVisibility(View.GONE);
    }
}
