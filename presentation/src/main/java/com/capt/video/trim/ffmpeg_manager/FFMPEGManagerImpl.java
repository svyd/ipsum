package com.capt.video.trim.ffmpeg_manager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.FileDescriptorBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.VideoBitmapDecoder;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.model.Quality;
import com.capt.video.trim.ffmpeg_command_builder.FFMPEGCommandBuilder;
import com.capt.video.trim.file_manager.FileManager;
import com.netcompss.ffmpeg4android.CommandValidationException;
import com.netcompss.ffmpeg4android.GeneralUtils;
import com.netcompss.loader.LoadJNI;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import butterknife.Bind;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by richi on 2016.03.04..
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
@PerFragment
public class FFMPEGManagerImpl implements FFMPEGManager {

    private final String START_FPS_INDEX = "kb/s, ";
    private final String END_FPS_INDEX = " fps,";

    private final int MILLISECOND = 1000;
    private final int MICROSECOND = MILLISECOND * MILLISECOND;

    private Context mContext;
    private FileManager mFileManager;
    private FFMPEGCommandBuilder mCommandBuilder;
    private LoadJNI mFFmpeg;
    private String mRootPath;
    private String mLogPath;
    private String mSourcePath;

    private Subscription mSubscription = Subscriptions.empty();

    @Inject
    @SuppressWarnings("ConstantConditions")
    public FFMPEGManagerImpl(Context _context,
                             LoadJNI _loadJni,
                             FileManager _fileManager,
                             FFMPEGCommandBuilder _commandBuilder) {
        mContext = _context;
        mFFmpeg = _loadJni;
        mFileManager = _fileManager;
        mCommandBuilder = _commandBuilder;
        mRootPath = mContext.getExternalFilesDir(null).getAbsolutePath() + "/";
        mLogPath = mRootPath + "vk.log";
    }

    @SuppressWarnings("unchecked")
    @Override
    public void trimVideo(long _startTime, long _endTime, String _input, Observer _observer) {
        mSourcePath = _input;
        mSubscription = getObservable(_startTime, _endTime, _input)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(_observer);
    }

    @Override
    public void unBind() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }

    private Observable<String> getTrimObservable(final long _startTime, final long _endTime,
                                                 final String _input) {
        return Observable.create(subscriber -> {
                    try {
                        subscriber.onNext(trim(_startTime, _endTime, _input));
                        subscriber.onCompleted();
                    } catch (Throwable e) {
                        e.printStackTrace();
                        subscriber.onError(e);
                    }
                }
        );
    }

    private String trim(final long _startTime, final long _endTime,
                        final String _input) throws CommandValidationException {
        File path = mFileManager.getTrimPath(String.valueOf(System.currentTimeMillis()) + ".mp4");
        String command = mCommandBuilder.getTrimVideoCommand(_startTime, _endTime, _input,
                path.getAbsolutePath());
        mFFmpeg.run(GeneralUtils.utilConvertToComplex(command),
                mContext.getExternalFilesDir(null).getAbsolutePath() + "/", mContext);
        return path.getAbsolutePath();
    }

    private Observable<Video> getObservable(final long _startTime, final long _endTime,
                                            final String _input) {
        return getTrimObservable(_startTime, _endTime, _input)
                .flatMap(this::getVideoModel);
    }

    private Observable<Video> getVideoModel(String _path) {
        return Observable.create(subscriber -> {
            try {
                Video video = new Video();
                video.path = _path;
                video.sourcePath = mSourcePath;
                video.quality = getQuality(_path);
                video.thumbnail = getThumbnail(_path, video.quality);
                subscriber.onNext(video);
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    private String getThumbnail(String _path, Quality _quality) throws IOException, ExecutionException, InterruptedException {

        BitmapPool bitmapPool = Glide.get(mContext).getBitmapPool();
        FileDescriptorBitmapDecoder decoder = new FileDescriptorBitmapDecoder(
                new VideoBitmapDecoder(MICROSECOND * (_quality.duration / 2)),
                bitmapPool,
                DecodeFormat.PREFER_ARGB_8888);

        Bitmap bitmap = Glide.with(mContext)
                .load(_path)
                .asBitmap()
                .videoDecoder(decoder)
                .override(Integer.parseInt(_quality.width), Integer.parseInt(_quality.height))
                .into(Integer.parseInt(_quality.width), Integer.parseInt(_quality.height))
                .get();

        return storeImage(bitmap);
    }

    private String storeImage(Bitmap imageData) {
        //get path to external storage (SD card)
        String iconsStoragePath = mRootPath + "thumbnails/";
        File sdIconStorageDir = new File(iconsStoragePath);

        //create storage directories, if they don't exist
        sdIconStorageDir.mkdirs();
        String path = "";
        try {
            path = sdIconStorageDir.toString() + System.currentTimeMillis() + ".png";
            FileOutputStream fileOutputStream = new FileOutputStream(path);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            //choose another format if PNG doesn't suit you
            imageData.compress(Bitmap.CompressFormat.PNG, 100, bos);

            bos.flush();
            bos.close();

        } catch (IOException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            e.printStackTrace();
        }

        return path;
    }

    private Quality getQuality(String _path) {
        try {
            mFFmpeg.run(GeneralUtils.utilConvertToComplex("ffmpeg -i " + _path),
                    mRootPath, mContext);

            MediaMetadataRetriever mMetadataRetriever = new MediaMetadataRetriever();
            mMetadataRetriever.setDataSource(_path);
            String width = mMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
            String height = mMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
            int duration = Integer.parseInt(mMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            String fps = getFPSFromVideo(mLogPath);
            return new Quality.Builder()
                    .setWidth(width)
                    .setHeight(height)
                    .setFps(fps)
                    .setDuration(duration)
                    .build();
        } catch (CommandValidationException _e) {
            _e.printStackTrace();
            return null;
        }
    }

    private String getFPSFromVideo(String _path) {
        String fps = "";
        try {
            File file = new File(_path);
            InputStream inputStream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(END_FPS_INDEX)) {
                    int start = line.indexOf(START_FPS_INDEX);
                    int end = line.indexOf(END_FPS_INDEX);
                    fps = line.substring(start + START_FPS_INDEX.length(), end);
                }
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fps;
    }
}