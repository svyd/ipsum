package com.capt.video.price;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.video.model.Video;

/**
 * Created by root on 25.03.16.
 */
public interface SetPriceContract {

	interface View extends BaseView {
		void showCurrency(CurrencyModel currencyModel);
		void showMessage(String _message);
		void showValidateMessage();
		void navigateToCurrencyScreen();
		void navigateToCategoryScreen(Bundle _data);
		void renderRealPrice(String _price);
		void setPrice(String price);
		void setCurrency(CurrencyModel price);
		CurrencyModel getSelectedCurrency();
		String getPrice();
	}

	abstract class Presenter extends BasePresenter<SetPriceContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void navigateToCurrencySelection();
		public abstract void navigateToCategory();
		public abstract void setVideoUploadModel(Video _model);
		public abstract void onPriceChanged(CharSequence _price);
	}
}
