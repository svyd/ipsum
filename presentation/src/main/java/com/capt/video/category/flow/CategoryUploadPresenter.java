package com.capt.video.category.flow;

import android.os.Bundle;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.video.Flow;
import com.capt.video.category.CategoryContract;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Svyd on 02.05.2016.
 */
public class CategoryUploadPresenter extends AbstractCategoryPresenter {

    @Inject
    public CategoryUploadPresenter(BaseInteractor _interactor,
                                   CategoryContract.View _view, BaseExceptionDelegate _delegate) {
        super(_interactor, _view, _delegate);
    }

    @Override
    protected void doNext() {
        Bundle args = new Bundle();
        args.putSerializable(Constants.BUNDLE_VIDEO, getData());
        args.putInt(Constants.FLOW, Flow.VIDEO_UPLOAD);

        getView().navigateToVideoViewScreen(args);
    }
}
