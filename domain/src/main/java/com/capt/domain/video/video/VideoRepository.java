package com.capt.domain.video.video;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.video.model.Video;
import com.capt.domain.model.RequestParamsModel;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public interface VideoRepository {
    Observable<List<Video>> getVideos(RequestParamsModel _model);
    Observable<List<Video>> getPendingVideos();
    Observable<Video> patchVideo(Video _video);
    Observable<SuccessModel> deleteVideo(String _id);

}
