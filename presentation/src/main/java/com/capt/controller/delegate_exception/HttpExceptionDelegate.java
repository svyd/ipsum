package com.capt.controller.delegate_exception;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Svyd on 7/28/16.
 */
public class HttpExceptionDelegate extends BaseExceptionDelegate<HttpException> {

    private static final int UNAUTHORIZED = 401;
    private static final int EXCEPTION = 403;

    public HttpExceptionDelegate(BaseExceptionDelegate _delegate, Retrofit _retrofit) {
        mDelegate = _delegate;
    }

    @Override
    public void handleException(HttpException _exception) {
        mBaseView.hideProgress();
        if (_exception.code() == UNAUTHORIZED || _exception.code() == EXCEPTION) {
            mBaseView.navigateToJoinScreen();
        } else {
            // TODO: 7/28/16 Convert the response to MyError POJO class via retrofit
            Response response = _exception.response();
            mBaseView.showError(response.message());
        }
    }
}
