package com.capt.profile.payout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.base.BaseFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.profile.payout.PayoutModel;
import com.capt.profile.payout.di.PayoutComponent;
import com.capt.profile.payout.di.PayoutsModule;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rMozes on 8/3/16.
 */
public class PayoutsFragment extends BaseToolbarFragment implements PayoutsContract.View {

    @Bind(R.id.llContainer_FP)
    LinearLayout llContainer;

    @Bind(R.id.rvPayouts_FP)
    RecyclerView rvPayouts;

    @Bind(R.id.tvPrice_FP)
    TextView tvPrice;

    @Bind(R.id.pbProgress_FP)
    ProgressBar pbProgress;

    @Bind(R.id.tvEmptyView_Fp)
    TextView tvEmptyView;

    @Inject
    BasePayoutsAdapter<PayoutsAdapter.PayOutViewHolder, PayoutModel> mAdapter;

    @Inject
    PayoutsContract.PayoutsPresenter mPresenter;

    private PayoutComponent mComponent;

    public static PayoutsFragment newInstance() {
        
        Bundle args = new Bundle();
        
        PayoutsFragment fragment = new PayoutsFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payouts);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        createInjector();
        inject();
        addAdapterToRV();
        mPresenter.initialize();
    }

    private void createInjector() {
        if (mComponent != null) return;

        mComponent = CaptApplication.getApplication().getAppComponent()
                .createPayoutComponent(new PayoutsModule(this));
    }

    private void inject() {
        if (mAdapter != null) return;

        mComponent.inject(this);
    }

    private void addAdapterToRV() {
        rvPayouts.setAdapter(mAdapter);
    }

    @Override
    public void setDataList(List<PayoutModel> _modelList) {
        mAdapter.setData(_modelList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setTotalPrice(String _price) {
        if (tvPrice != null)
            tvPrice.setText(_price);
    }

    @Override
    public void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbProgress.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyView() {
        tvEmptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        tvEmptyView.setVisibility(View.GONE);
    }

    @Override
    public void showContainer() {
        llContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideContainer() {
        llContainer.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }
}
