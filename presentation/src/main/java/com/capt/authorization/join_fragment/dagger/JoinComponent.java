package com.capt.authorization.join_fragment.dagger;

import com.capt.authorization.join_fragment.JoinFragment;
import com.capt.data.annotation.PerFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.02.23..
 */
@PerFragment
@Subcomponent(modules = {JoinModule.class, StrategyModule.class})
public interface JoinComponent {
    void inject(JoinFragment _fragment);
}
