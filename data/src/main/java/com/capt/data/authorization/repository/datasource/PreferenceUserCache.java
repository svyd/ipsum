package com.capt.data.authorization.repository.datasource;

import android.content.SharedPreferences;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;

/**
 * Created by Svyd on 4/22/16.
 */
public class PreferenceUserCache implements UserCache {

    private Gson mGson;
    private SharedPreferences mPreferences;

    @Inject
    public PreferenceUserCache(Gson _gson,
                               SharedPreferences _sharedPref) {
        mGson = _gson;
        mPreferences = _sharedPref;
    }

    @Override
    public Observable<UserModel> cacheUser(UserModel _model) {
        return Observable.create(subscriber -> {
            if (_model.getPhone().getPhone().equals("")) {
                subscriber.onNext(_model);
                subscriber.onCompleted();
            } else {
                try {
                    String user = mGson.toJson(_model);
                    mPreferences
                            .edit()
                            .putString(Constants.PREFERENCES_USER_PROFILE, user)
                            .commit();
                    subscriber.onNext(_model);
                    subscriber.onCompleted();
                } catch (Exception _e) {
                    subscriber.onError(_e);
                }
            }
        });
    }

    @Override
    public Observable<UserModel> getUser() {
        return Observable.create(subscriber -> {
            try {
                String user = mPreferences.getString(Constants.PREFERENCES_USER_PROFILE, "");
                UserModel model = mGson.fromJson(user, UserModel.class);
                subscriber.onNext(model);
                subscriber.onCompleted();
            } catch (Exception _e) {
                subscriber.onError(_e);
            }
        });
    }

    @Override
    public Observable<SuccessModel> clear() {
        return Observable.create(subscriber -> {
            try {
                mPreferences.edit().clear().commit();
                subscriber.onNext(new SuccessModel());
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }
}
