package com.capt.authorization.forgot_password;

import android.util.Patterns;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.base.EmailValidator;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.forgot_password.model.ForgotPasswordModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 18.03.2016.
 */
public class ForgotPasswordPresenter extends ForgotPasswordContract.ForgotPasswordPresenter {

    private ForgotPasswordContract.ForgotPasswordView mView;
    private BasePostInteractor<String> mEmailCheckInteractor;
    private EmailValidator mEmailValidator;
    private BasePostInteractor<ForgotPasswordModel> mForgotPasswordInteractor;
    private PublishSubject<String> mTextSubject;

    @Inject
    public ForgotPasswordPresenter(ForgotPasswordContract.ForgotPasswordView _view,
                                   EmailValidator _validator,
                                   @Named(Constants.NamedAnnotation.CHECK_EMAIL_INTERACTOR)
                                   BasePostInteractor<String> _emailCheckInteractor,
                                   @Named(Constants.NamedAnnotation.FORGOT_PASSWORD_INTERACTOR)
                                   BasePostInteractor<ForgotPasswordModel> _forgotPasswordInteractor,
                                   BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mEmailCheckInteractor = _emailCheckInteractor;
        mEmailValidator = _validator;
        mForgotPasswordInteractor = _forgotPasswordInteractor;
        initSubject();
    }

    private void initSubject() {
        mTextSubject = PublishSubject.create();
        mTextSubject
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(this::validateEmail);
    }

    @Override
    public void passEmail(String _email) {
        mForgotPasswordInteractor.execute(new ForgotPasswordModel(_email), new ForgotPasswordObserver(this));
    }

    @Override
    public void checkEmail(String _email) {
        mView.enableButton(false);
        if (!mEmailValidator.isEmailValid(_email)) {
            return;
        }
        mView.showProgress();
        mTextSubject.onNext(_email);
    }

    @Override
    public void initialize() {

    }

    @Override
    public void onStart() {

    }

    public boolean isEmailValid(String _email) {
        return _email != null && Patterns.EMAIL_ADDRESS.matcher(_email).matches() ;
    }

    private void validateEmail(String s) {
        if (!isEmailValid(s)) {
            mView.setEmailError(CaptApplication.getApplication().getString(R.string.error_invalid_email));
            mView.onEmailChecked(false);
            mView.enableButton(false);
        } else {
            mView.onEmailChecked(true);
            mView.enableButton(true);
        }
        mView.hideProgress();
    }

    @Override
    public void onStop() {
        mEmailCheckInteractor.unSubscribe();
        mForgotPasswordInteractor.unSubscribe();
    }

    private class ForgotPasswordObserver extends BaseObserver<SuccessModel> {
        public ForgotPasswordObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(SuccessModel successModel) {
            mView.onEmailPassed();
        }
    }
}
