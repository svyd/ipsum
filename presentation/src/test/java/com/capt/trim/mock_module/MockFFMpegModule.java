//package com.capt.trim.mock_module;
//
//import com.capt.video.trim.dagger.FFMpegModule;
//import com.capt.video.trim.ffmpeg_command_builder.CommandBuilderImpl;
//import com.capt.video.trim.ffmpeg_command_builder.FFMPEGCommandBuilder;
//import com.capt.video.trim.file_manager.FileManager;
//import com.capt.video.trim.file_manager.FileManagerImpl;
//import com.capt.video.trim.time_util.TimeUtil;
//import com.capt.video.trim.time_util.TimeUtilImpl;
//
///**
// * Created by richi on 2016.02.29..
// */
//public class MockFFMpegModule extends FFMpegModule {
//
//    protected TimeUtil mTimeUtil;
//    protected FileManager mFileManager;
//    protected FFMPEGCommandBuilder mCommandBuilder;
//
//    public MockFFMpegModule(TimeUtil _timeUtil, FileManager _fileManager, FFMPEGCommandBuilder _commandBuilder) {
//        mTimeUtil = _timeUtil;
//        mFileManager = _fileManager;
//        mCommandBuilder = _commandBuilder;
//    }
//
//    public MockFFMpegModule(TimeUtil _timeUtil) {
//        mTimeUtil = _timeUtil;
//    }
//
//    @Override
//    protected TimeUtil provideTimeUtil(TimeUtilImpl _timeUtil) {
//        if (mTimeUtil == null)
//            return super.provideTimeUtil(_timeUtil);
//        return mTimeUtil;
//    }
//
//    @Override
//    protected FileManager provideFileManager(FileManagerImpl _fileManager) {
//        if (mFileManager == null)
//            return super.provideFileManager(_fileManager);
//        return mFileManager;
//    }
//
//    @Override
//    protected FFMPEGCommandBuilder provideCommandBuilder(CommandBuilderImpl _builder) {
//        if (mCommandBuilder == null)
//            return super.provideCommandBuilder(_builder);
//        return mCommandBuilder;
//    }
//}
