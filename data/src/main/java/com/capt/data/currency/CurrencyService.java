package com.capt.data.currency;

import com.capt.domain.currency.model.CurrencyModel;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by root on 31.03.16.
 */
public interface CurrencyService {

	@GET("/mobile/currencies")
	Observable<List<CurrencyModel>> getCurrencies();
}
