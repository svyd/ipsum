package com.capt.authorization.location.flow;

import com.capt.authorization.location.LocationContract;
import com.capt.authorization.location.LocationInteractorManager;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;

import javax.inject.Inject;

/**
 * Created by Svyd on 16.03.2016.
 */
public class LocationPresenter extends AbstractLocationPresenter {

    @Inject
    public LocationPresenter(LocationContract.LocationView _view,
                             LocationInteractorManager _manager,
                             BaseExceptionDelegate _delegate) {
        super(_view, _manager, _delegate);
    }

    @Override
    protected void doOnNext(LocationModel locationModel) {
        getView().passActivityResult(locationModel);
    }

    @Override
    public void onHeaderClicked() {
        getView().navigateToBack();
    }
}
