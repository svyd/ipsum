package com.capt.profile.edit_profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.application.dagger.HasComponent;
import com.capt.authorization.dagger.RestModule;
import com.capt.authorization.location.LocationFragment;
import com.capt.base.BaseActivity;
import com.capt.data.authorization.net.ApiConstants;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.profile.edit_profile.dagger.EditProfileActivityComponent;
import com.capt.profile.edit_profile.dagger.EditProfileActivityModule;
import com.capt.profile.settings.password.PasswordFragment;
import com.capt.video.Flow;

import javax.inject.Inject;

/**
 * Created by Svyd on 4/29/16.
 */
public class EditProfileActivity extends BaseActivity implements EditProfileActivityContract.View, HasComponent<EditProfileActivityComponent> {

    public static final int REMINDER_REQUEST_CODE = 123;

    @Inject
    EditProfileActivityContract.Presenter mPresenter;

    public static void startLocation(Activity _activity) {
        Intent intent = new Intent(_activity, EditProfileActivity.class);
        intent.putExtra(Constants.FLOW, Flow.ADD_LOCATION);
        _activity.startActivityForResult(intent, REMINDER_REQUEST_CODE);
    }

    public static void startEditProfileFlow(Activity _activity) {
        Intent intent = new Intent(_activity, EditProfileActivity.class);
        intent.putExtra(Constants.FLOW, Flow.UPDATE_PROFILE);
        _activity.startActivityForResult(intent, REMINDER_REQUEST_CODE);
    }

    public static void startPasswordFlow(Activity _activity) {
        Intent intent = new Intent(_activity, EditProfileActivity.class);
        intent.putExtra(Constants.FLOW, Flow.CHANGE_PASSWORD);
        _activity.startActivityForResult(intent, REMINDER_REQUEST_CODE);
    }

    private EditProfileActivityComponent mComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initComponent();
        inject();
        downloadCountryCode();
        mPresenter.setArgs(getIntent().getExtras());
        if (savedInstanceState == null)
            mPresenter.initialize();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = CaptApplication.getApplication().getAppComponent()
                .createEditProfileComponent(new EditProfileActivityModule(this),
                        new RestModule(ApiConstants.GOOGLE_MAPS_API_ENDPOINT));
    }

    private void inject() {
        mComponent.inject(this);
    }

    private void downloadCountryCode() {
        BaseInteractor interactor = mComponent.countryCodeInteractor();
        interactor.execute(new BaseInteractor.SimpleObserver<>());
    }

    @Override
    public void navigateToEditProfileFragment() {
        getFragmentNavigator().addFragmentWithoutBackStack(EditProfileFragment
                .newInstance(getIntent().getIntExtra(Constants.FLOW, -1)));
    }

    @Override
    public void navigateToLocationFragment() {
        Bundle args = new Bundle();
        args.putInt(Constants.FLOW, getIntent().getIntExtra(Constants.FLOW, -1));
        getFragmentNavigator().addFragmentWithoutBackStack(LocationFragment.newInstance(args));
    }

    @Override
    public void navigateToPasswordFragment() {
        getFragmentNavigator().addFragmentWithoutBackStack(PasswordFragment.newInstance());
    }

    @Override
    public int getContainerId() {
        return R.id.flContainer_AEP;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AEP;
    }

    @Override
    public EditProfileActivityComponent getComponent() {
        return mComponent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getFragmentNavigator().getTopFragment() != null)
            getFragmentNavigator().getTopFragment().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
