package com.capt.authorization.join_fragment;

import com.capt.authorization.auth_strategy.AuthorizationStrategy;
import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.UserModel;

/**
 * Created by richi on 2016.02.23..
 */
public interface JoinContract {

    interface View extends BaseView {
        void showToast(String _message);
        void performLogIn();
        void startDashboard(UserModel _model);
        void startPhoneNumberScreen();
        void setEnabledSocBtn(boolean _isEnable);
    }

    abstract class Presenter extends BasePresenter<View> implements AuthorizationStrategy.CaptCallback {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onFacebookClick();
        public abstract void onTwitterClick();
        public abstract void onInstagramClick();
        public abstract void onActivityResult();
    }


}
