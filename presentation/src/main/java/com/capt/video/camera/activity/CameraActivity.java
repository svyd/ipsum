package com.capt.video.camera.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.application.dagger.HasComponent;
import com.capt.base.BaseActivity;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.video.camera.RevealUtil;
import com.capt.video.camera.activity.dagger.CameraActivityComponent;
import com.capt.video.camera.activity.dagger.CameraActivityModule;
import com.capt.video.camera.fragment.CameraState;
import com.capt.video.camera.fragment.CaptCameraFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 07.04.2016.
 */
public class CameraActivity extends BaseActivity implements HasComponent<CameraActivityComponent> {

    private CameraActivityComponent mComponent;

    private View mDecorView;

    @Bind(R.id.flContainer_AC)
    FrameLayout flContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mDecorView = getWindow().getDecorView();
        if (checkLollipop()) {
            overridePendingTransition(R.anim.do_not_move, R.anim.do_not_move);
        }
        setContentView(R.layout.activity_camera);

        ButterKnife.bind(this);

        initComponent();
        inject();
        if (savedInstanceState == null) {
            if (checkLollipop()) {
                makeReveal();
            }
            initFragment();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        hideSystemUI();
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private boolean checkLollipop() {
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        return currentApiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP;
    }

    @Override
    public void onBackPressed() {
        if (getFragmentNavigator().getTopFragment() instanceof CameraState) {
            if (((CameraState) getFragmentNavigator().getTopFragment()).isCameraAlive()) {
                super.onBackPressed();
            }
        } else {
            throw new IllegalStateException("top fragment should implement CameraFragment");
        }
    }

    private void makeReveal() {

        if (!getIntent().hasExtra("x"))
            return;

        int x = (int) getIntent().getFloatExtra("x", 0);
        int y = (int) getIntent().getFloatExtra("y", 0);
        if (x == 0) {
            return;
        }
        RevealUtil.makeReveal(flContainer, x, y);
    }

    private void initComponent() {
        mComponent = CaptApplication.getApplication()
                .getAppComponent()
                .createCameraActivityComponent(new CameraActivityModule(this));
    }

    private void inject() {
        mComponent.inject(this);
    }

    private void initFragment() {
        int flow = getIntent().getIntExtra(Constants.FLOW, -1);
        Assignment assignment = (Assignment) getIntent().getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE);
        Bundle args = new Bundle();
        args.putInt(Constants.FLOW, flow);
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, assignment);
        getFragmentNavigator().addFragmentWithoutBackStack(CaptCameraFragment.newInstance(args));
    }

    @Override
    public int getContainerId() {
        return R.id.flContainer_AC;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    public CameraActivityComponent getComponent() {
        return mComponent;
    }
}