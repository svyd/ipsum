package com.capt.domain.profile.dashboard.model.base;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Svyd on 25.03.2016.
 */
public interface DisplayableItem  extends Serializable{
    String getHeaderTitle();
    String getHeaderDescription();
}
