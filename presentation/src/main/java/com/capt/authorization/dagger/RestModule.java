package com.capt.authorization.dagger;

import android.content.Context;
import android.location.Geocoder;

import com.capt.data.annotation.PerActivity;
import com.capt.data.annotation.PerFragment;
import com.capt.data.authorization.location.LocationDataStoreImpl;
import com.capt.data.authorization.location.LocationMapper;
import com.capt.data.authorization.net.retrofit.CountryService;
import com.capt.data.authorization.net.retrofit.PlacesService;
import com.capt.data.authorization.repository.country.CountryRepositoryImpl;
import com.capt.data.authorization.repository.country.cache.CountryCache;
import com.capt.data.authorization.repository.country.cache.CountryCacheImpl;
import com.capt.data.base.TypeMapper;
import com.capt.domain.authorization.country_code.CountryCodeInteractor;
import com.capt.domain.authorization.country_code.CountryRepository;
import com.capt.domain.authorization.interactor.CodeInteractor;
import com.capt.domain.authorization.interactor.EmailCheckInteractor;
import com.capt.domain.authorization.location.LocationDataStore;
import com.capt.domain.authorization.location.LocationDetailsInteractor;
import com.capt.domain.authorization.location.LocationLatLngInteractor;
import com.capt.domain.authorization.location.LocationStringInteractor;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.update_profile.UpdateProfileInteractor;
import com.capt.domain.profile.update_profile.UpdateProfileLocationInteractor;

import java.util.Locale;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Created by Svyd on 02.03.2016.
 */
@Module
public class RestModule {

    private String mPlacesUrl;

    public RestModule(String _placesUrl) {
        mPlacesUrl = _placesUrl;
    }



    @Provides @PerActivity
    protected CountryService provideCountryService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                       Retrofit _retrofit) {
        return _retrofit.create(CountryService.class);
    }

    @Provides
    @PerActivity
    protected Geocoder provideGeocoder(Context _context) {
        return new Geocoder(_context, Locale.ENGLISH);
    }

    @Provides
    @PerActivity
    protected TypeMapper provideLocationMapper(LocationMapper _mapper) {
        return _mapper;
    }

    @Provides
    @PerActivity
    protected LocationDataStore provideDataStore(LocationDataStoreImpl _store) {
        return _store;
    }

    @Provides
    @PerActivity
    @Named(Constants.NamedAnnotation.LOCATION_STRING_INTERACTOR)
    protected BasePostInteractor<String> provideLocationStringInteractor(LocationStringInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerActivity
    @Named(Constants.NamedAnnotation.LOCATION_LAT_LNG_INTERACTOR)
    protected BasePostInteractor<LocationModel> provideLocationLaInteractor(LocationLatLngInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerActivity
    @Named(Constants.NamedAnnotation.LOCATION_DETAILS_INTERACTOR)
    protected  BasePostInteractor<String> provideLocationDetailsInteractor(LocationDetailsInteractor _interactor) {
        return _interactor;
    }

    @Provides @PerActivity
    @Named(Constants.NamedAnnotation.MAPS_API_RETROFIT)
    protected Retrofit provideRetrofit(OkHttpClient _client,
                                       CallAdapter.Factory _factory,
                                       Converter.Factory _converterFactory) {
        return new Retrofit.Builder()
                .baseUrl(mPlacesUrl)
                .addCallAdapterFactory(_factory)
                .addConverterFactory(_converterFactory)
                .client(_client)
                .build();
    }


    @Provides @PerActivity
    protected PlacesService providePlacesService(@Named(Constants.NamedAnnotation.MAPS_API_RETROFIT)
                                                 Retrofit _retrofit) {
        return _retrofit.create(PlacesService.class);
    }

    @Provides
    @PerActivity
    @Named(Constants.NamedAnnotation.COUNTRY_CODE_INTERACTOR)
    protected BaseInteractor provideCountryCodeInteractor(CountryCodeInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerActivity
    @Named(Constants.NamedAnnotation.CHECK_EMAIL_INTERACTOR)
    protected BasePostInteractor<String> provideEmailCheckerInteractor(EmailCheckInteractor _interactor) {
        return _interactor;
    }
    
    @Provides @PerActivity
    protected CountryCache provideCountryCache(CountryCacheImpl _cache) {
        return _cache;
    }

    @Provides @PerActivity
    protected CountryRepository provideCountryRepository(CountryRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerActivity
    @Named(Constants.NamedAnnotation.UPDATE_PROFILE_INTERACTOR)
    protected BasePostInteractor<UserModel> provideUpdateUser(UpdateProfileInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerActivity
    @Named(Constants.NamedAnnotation.UPDATE_LOCATION_INTERACTOR)
    protected BasePostInteractor<LocationModel> provideUpdateProfileLocation(UpdateProfileLocationInteractor _interactor) {
        return _interactor;
    }
}
