package com.capt.video.overview.flow;

import android.content.SharedPreferences;

import com.capt.application.UploadReceiver;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;
import com.capt.video.overview.OverviewContract;
import com.capt.video.upload_manager.VideoUploadManager;
import com.paypal.android.sdk.payments.PayPalAuthorization;

import java.util.Date;

import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 21.04.2016.
 */
public abstract class AbstractPresenter extends OverviewContract.Presenter {

    private boolean isPayPalRegistered = false;
    private SharedPreferences mPreferences;
    private OverviewContract.View mView;
    private VideoUploadManager mUploadManager;
    private BasePostInteractor<PayPalVideoModel> mPayPalInteractor;

    public AbstractPresenter(OverviewContract.View _view, BaseExceptionDelegate _delegate,
                             BasePostInteractor<PayPalVideoModel> _payPalInteractor) {
        this(null, null, _view, _delegate, _payPalInteractor);
    }

    public AbstractPresenter(SharedPreferences _preferences,
                             VideoUploadManager _manager,
                             OverviewContract.View _view,
                             BaseExceptionDelegate _delegate,
                             BasePostInteractor<PayPalVideoModel> _payPalInteractor) {
        super(_view, _delegate);
        mPreferences = _preferences;
        mView = _view;
        mUploadManager = _manager;
        mPayPalInteractor = _payPalInteractor;
    }

    protected OverviewContract.View getView() {
        return mView;
    }

    protected Video getVideo() {
        throw new UnsupportedOperationException("Unsupported method by this implementation of presenter");
    }

    @Override
    public void initialize() {
        if (mPreferences.contains(Constants.Credentials.PAYPAL_REGISTERED)) {
            isPayPalRegistered = mPreferences.getBoolean(Constants.Credentials.PAYPAL_REGISTERED, false);
            isPayPalRegistered = true;
        }
    }

    @Override
    public void uploadVideo() {
        getVideo().date_upload = new Date();
        getVideo().license = "license";
        mUploadManager.startUploading(getVideo());
        onVideoUpload();
    }

    protected void onVideoUpload() {
        mView.navigateToDashboard();
    }

    @Override
    public void onRedBtnClick() {
        if (UploadReceiver.isAssignmentUploading()) {
            mView.showDenial();
            return;
        }

        if (isPayPalRegistered) {
            uploadVideo();
        } else {
            mView.onFuturePayment();
        }
    }

    @Override
    public void onStop() {
        mPayPalInteractor.unSubscribe();
    }

    @Override
    public void sendAuthorizationToServer(PayPalAuthorization authorization) {
        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

        getVideo().date_upload = new Date();
        getVideo().license = "license";
        mPayPalInteractor.execute(new PayPalVideoModel.Builder()
                .setAuthCode(authorization.getAuthorizationCode())
                .setVideo(getVideo())
                .build(), new PayPalObserver(this));
    }

    private class PayPalObserver extends BaseObserver<PayPalVideoModel> {

        public PayPalObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(PayPalVideoModel payPalVideoModel) {
            onVideoUpload();
        }
    }

    @Override
    public void onDetailsTextBtnClick() {
        throw new UnsupportedOperationException("Unsupported method by this implementation of presenter");
    }

    @Override
    public void onFrameBtnClick() {
        throw new UnsupportedOperationException("Unsupported method by this implementation of presenter");
    }

    @Override
    public void onPlayClick() {
        throw new UnsupportedOperationException("Unsupported method by this implementation of presenter");
    }

    @Override
    public void onTextBtnClick() {
        throw new UnsupportedOperationException("Unsupported method by this implementation of presenter");
    }

    @Override
    public void onActionEdit() {
        throw new UnsupportedOperationException("Unsupported method by this implementation of presenter");
    }

    public void onUploadError(String uploadId) {

    }

    public void onProgress(String uploadId, int progress) {

    }

    public void onUploadCompleted(String uploadId) {

    }


    public void onUploadRetryClick() {

    }

    public void onUploadCancelClick() {

    }
}
