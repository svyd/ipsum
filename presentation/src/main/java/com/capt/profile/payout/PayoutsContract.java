package com.capt.profile.payout;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.payout.PayoutModel;

import java.util.List;

/**
 * Created by rMozes on 8/3/16.
 */
public interface PayoutsContract {

    interface View extends BaseView {
        void setDataList(List<PayoutModel> _modelList);
        void setTotalPrice(String _price);
        void showEmptyView();
        void hideEmptyView();
        void showContainer();
        void hideContainer();
    }

    abstract class PayoutsPresenter extends BasePresenter<PayoutsContract.View> {
        public PayoutsPresenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }
    }
}
