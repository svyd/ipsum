package com.capt.profile.assignments.details.overview;

import android.content.Intent;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.assignment.model.Assignment;

/**
 * Created by root on 25.04.16.
 */
public interface OverviewContract {

	interface View extends BaseView {
		void navigateToAssignmentsScreen();
		void onCalendarClick();
		void setUpView(Assignment _model);
		void sendCalendarIntent(Intent intent);
	}

	abstract class Presenter extends BasePresenter<OverviewContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void navigateToAssignments();
		public abstract void setAssignmentModel(Assignment _model);
		public abstract void addToCalendar();
	}
}
