package com.capt.profile.dashboard.ui.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capt.R;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.Item;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 25.03.2016.
 */
public class NonRecycleAdapter extends BaseAdapter {

    private List<Item> mData;
    private LayoutInflater mInflater;

    @Inject
    public NonRecycleAdapter(LayoutInflater _inflater) {
        mInflater = _inflater;
    }

    public void setData(List<Item> _data) {
        mData = _data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Item getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.list_item_simple, parent, false);
        ((TextView) convertView.findViewById(R.id.tvLeft_LI)).setText(mData.get(position).left);
        ((TextView) convertView.findViewById(R.id.tvRight_LI)).setText(mData.get(position).right);
        return convertView;
    }
}
