package com.capt.domain.video.model;

import com.capt.domain.category.model.CategoryModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Svyd on 02.05.2016.
 */
public class VideoPatch {

    public VideoPatch(Video _video) {
        title = _video.title;
        description = _video.getDescription();
        price = _video.getPrice();
        currencyId = _video.currency.getCurrencyId();
        location = _video.location;
        country = _video.country;
        city = _video.city;
        categories = _video.categories;
    }

    @Expose
    @SerializedName("title")
    public String title;

    @Expose
    @SerializedName("description")
    public String description;

    @Expose
    @SerializedName("price")
    public String price;

    @Expose
    @SerializedName("currencyId")
    public String currencyId;

    @Expose
    @SerializedName("location")
    public double[] location;

    @Expose
    @SerializedName("country")
    public String country;

    @Expose
    @SerializedName("city")
    public String city;

    @Expose
    @SerializedName("categories")
    public List<CategoryModel> categories;

}
