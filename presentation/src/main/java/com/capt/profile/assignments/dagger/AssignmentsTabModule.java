package com.capt.profile.assignments.dagger;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.capt.R;
import com.capt.base.BaseActivity;
import com.capt.base.BaseFragment;
import com.capt.data.annotation.PerFragment;
import com.capt.profile.ViewPagerAdapter;
import com.capt.profile.assignments.AssignmentsTabContract;
import com.capt.profile.assignments.AssignmentsTabPresenter;
import com.capt.profile.assignments.finished.FinishedFragment;
import com.capt.profile.assignments.for_grabs.ForGrabsFragment;
import com.capt.profile.assignments.in_progress.InProgressFragment;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 15.04.16.
 */
@Module
public class AssignmentsTabModule {

	AssignmentsTabContract.View mView;
	BaseFragment mFragment;

	public AssignmentsTabModule(AssignmentsTabContract.View _view, BaseFragment _fragment) {
		mView = _view;
		mFragment = _fragment;
	}

	@Provides
	@PerFragment
	protected BaseFragment provideFragment() {
		return mFragment;
	}

	@Provides @PerFragment
	protected AssignmentsTabContract.View provideView() {
		return mView;
	}

	@Provides @PerFragment
	protected AssignmentsTabContract.Presenter providePresenter(AssignmentsTabPresenter _presenter) {
		return _presenter;
	}

	@Provides
    @PerFragment
    FragmentManager provideFragmentManager(BaseFragment _fragment) {
		return _fragment.getChildFragmentManager();
	}

	@Provides
	List<Fragment> provideFragmentList() {
		List<Fragment> fragments = new ArrayList<>();
		fragments.add(new ForGrabsFragment());
		fragments.add(new InProgressFragment());
		fragments.add(new FinishedFragment());
		return fragments;
	}

	@Provides
    ViewPagerAdapter provideViewPagerAdapter(List<Fragment> fragments, FragmentManager _manager, Context _context) {
		ViewPagerAdapter adapter = new ViewPagerAdapter(_manager);
		adapter.addFrag(fragments.get(0), _context.getString(R.string.for_grabs).toUpperCase());
		adapter.addFrag(fragments.get(1), _context.getString(R.string.in_progress).toUpperCase());
		adapter.addFrag(fragments.get(2), _context.getString(R.string.finished).toUpperCase());
		return adapter;
	}
}
