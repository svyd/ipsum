package com.capt.profile.marketplace.base;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.profile.ScrollForLoadMoreListener;
import com.capt.profile.marketplace.OnVideoClickListener;
import com.capt.profile.marketplace.VideoAdapter;
import com.capt.profile.marketplace.base.dagger.MarketplaceListModule;
import com.capt.profile.marketplace.base.dagger.BaseMarketPlaceListComponent;
import com.capt.profile.marketplace.dagger.MarketplaceActivityComponent;
import com.capt.video.VideoActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by root on 14.04.16.
 */
public abstract class BaseMarketPlaceListFragment extends BaseFragment
        implements BaseMarketplaceListContract.View, ScrollForLoadMoreListener.OnLoadMoreListener, OnVideoClickListener {

    private boolean isItemSelected = false;

    @Inject
    protected BaseMarketplaceListContract.Presenter mPresenter;

    @Inject
    protected VideoAdapter mAdapter;

    @Bind(R.id.rv_videos_FL)
    protected RecyclerView rvVideos;
    @Bind(R.id.pb_fl)
    ProgressBar progressBar;
    @Bind(R.id.tv_rv_is_empty)
    protected TextView tvEmptyList;

    private ScrollForLoadMoreListener mScrollListener;
    private BaseMarketPlaceListComponent mComponent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_list);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        setUpView();
        mPresenter.initialize();
        mAdapter.setListener(this);
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(MarketplaceActivityComponent.class)
                .createBaseMarketPlaceListComponent(new MarketplaceListModule(this, getType()));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    @Override
    public void hideNoInternetConnectionError() {

    }

    @Override
    public void removeItem(int _position) {
        if (_position >= 0) {
            mAdapter.remove(_position);
        }
    }

    @Override
    public void showProgress() {
        if (tvEmptyList == null)
            return;

        tvEmptyList.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setPending(int _position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    private void setUpView() {
        mScrollListener = new ScrollForLoadMoreListener(this);
        rvVideos.setLayoutManager(new LinearLayoutManager(getActivity(),
                android.support.v7.widget.LinearLayoutManager.VERTICAL, false));
        rvVideos.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void showEmptyList() {
        if (rvVideos != null) {
            rvVideos.setVisibility(View.GONE);
        }
        if (tvEmptyList != null) {
            tvEmptyList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setResult(int _result) {
        getActivity().setResult(_result);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onStart();
        isItemSelected = false;
    }

    @Override
    public void showNoInternetConnectionError() {

    }

    @Override
    public void scrollTo(int _position) {
        rvVideos.scrollToPosition(_position);
    }

    @Override
    public void onVideoClick(Video _video) {
        if (!isItemSelected) {
            isItemSelected = true;
            mPresenter.onVideoClick(_video);
        }
    }

    @Override
    public void showMessage(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showVideos(List<Video> videoModels) {
        Log.d("pizdos", "#showVideos" + videoModels.size());
        mAdapter.setData(videoModels);
        rvVideos.setVisibility(View.VISIBLE);
        tvEmptyList.setVisibility(View.GONE);
        rvVideos.addOnScrollListener(mScrollListener);
    }

    @Override
    public void addVideos(List<Video> newVideoModels) {
        Log.d("pizdos", "#addVideos" + newVideoModels.size());
        mAdapter.addModels(newVideoModels);
        mScrollListener.enable();
        if (rvVideos != null) {
            rvVideos.addOnScrollListener(mScrollListener);
        }
    }

    @Override
    public void replaceItem(int _position, Video _new) {
        mAdapter.replace(_position, _new);
    }

    @Override
    public void navigateToDraftDetails(Video _video) {
        getActivity()
                .getIntent()
                .putExtra(Constants.Extra.EXTRA_SERIALIZABLE, getItemType());
        VideoActivity.startDraftOverview(_video, getActivity());
    }

    @Override
    public void navigateToVideoDetails(Video _video) {
        getActivity()
                .getIntent()
                .putExtra(Constants.Extra.EXTRA_SERIALIZABLE, getItemType());
        VideoActivity.startOverview(_video, getActivity());
    }

    @Override
    public void notifyChanges() {
        mAdapter.notifyDataSetChanged();
    }

    protected abstract String getType();

    protected abstract Constants.MarketplaceItems getItemType();

    @Override
    public void onLoadMore() {
        mPresenter.getMoreVideoItems();
    }

    @Override
    public void disableLoadMore() {
        rvVideos.removeOnScrollListener(mScrollListener);
    }

    //region unused

    @Override
    public void onSaveDraft(Video _draft) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setProgress(int _position, int _progress) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setError(int _position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void swap(int _first, int _second) {
        throw new UnsupportedOperationException();
    }

    //endregion
}
