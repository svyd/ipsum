package com.capt.domain.profile.assignment.cache;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.profile.assignment.model.Assignment;

import rx.Observable;

/**
 * Created by Svyd on 07.07.2016.
 */
public interface AssignmentCache {
    Observable<Assignment> cacheAssignment(Assignment _assignment);
    Observable<Assignment> getAssignment();
    Observable<SuccessModel> clear();
    Observable<SuccessModel> updateStatus(String _status);
}
