package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Svyd on 04.03.2016.
 */
public class CodeModel implements Serializable {

    public CodeModel(String _code) {
        code = _code;
    }

    @Expose
    public String code;
}
