package com.capt.data.authorization.repository.country.data_store;

import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

import rx.Observable;

/**
 * Created by richi on 2016.03.16..
 */
public interface CountryDataStore {
    Observable<List<CountryModel>> getCountryModel();
}
