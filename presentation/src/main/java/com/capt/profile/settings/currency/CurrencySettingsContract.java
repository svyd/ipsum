package com.capt.profile.settings.currency;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.profile.settings.model.SettingsModel;

import java.util.Currency;
import java.util.List;

/**
 * Created by root on 29.04.16.
 */
public interface CurrencySettingsContract {

	interface View extends BaseView {
		void showCurrencies(List<CurrencyModel> currencyModels);
		void showMessage(String _message);
		void navigateToSettingsScreen(SettingsModel _settings);
	}

	abstract class Presenter extends BasePresenter<CurrencySettingsContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void onCurrencySelected(CurrencyModel currencyModel);
	}
}
