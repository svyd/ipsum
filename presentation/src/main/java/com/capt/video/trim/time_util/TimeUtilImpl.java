package com.capt.video.trim.time_util;


import com.capt.data.annotation.PerFragment;
import com.capt.data.base.TimeUtil;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.29..
 */
public class TimeUtilImpl implements TimeUtil {

    @Inject
    public TimeUtilImpl() {
    }

    @Override
    public String getTimeFromInteger(long _time) {
        long hours = _time / 3600;
        long minutes = (_time % 3600) / 60;
        long seconds = _time % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    @Override
    public String getTimeFromIntegerMinutes(long _time) {
        long minutes = (_time % 3600) / 60;
        long seconds = _time % 60;

        return String.format("%02d:%02d", minutes, seconds);
    }

    @Override
    public long getTimeFromPercentage(int _percentage, long _duration) {
        return (_percentage * _duration) / 100;
    }

    @Override
    public String getTimeStringFromPercentage(int _percentage, long _duration) {
        return getTimeFromInteger(getTimeFromPercentage(_percentage, _duration));
    }
}
