package com.capt.video.overview.flow;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;
import com.capt.video.overview.OverviewContract;
import com.capt.video.trim.time_util.TimeUtilImpl;
import com.capt.video.upload_manager.VideoUploadManager;

import java.io.File;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 10.04.2016.
 */
public class VideoUploadPresenter extends AbstractPresenter {


    private static final String TAG = VideoUploadPresenter.class.getSimpleName();
    private Video mData;
    private TimeUtilImpl mTimeUtil;
    private BasePostInteractor<Video> mDraftInteractor;

    @Inject
    public VideoUploadPresenter(TimeUtilImpl _timeUtil,
                                VideoUploadManager _manager,
                                SharedPreferences _preferences,
                                OverviewContract.View _view,
                                BasePostInteractor<Video> _interactor,
                                BaseExceptionDelegate _delegate,
                                @Named(Constants.PendingVideoConstants.PAY_PAL_INTERACTOR)
                                BasePostInteractor<PayPalVideoModel> _payPalInteractor) {
        super(_preferences, _manager, _view, _delegate, _payPalInteractor);
        mTimeUtil = _timeUtil;
        mDraftInteractor = _interactor;
    }

    @Override
    protected Video getVideo() {
        return mData;
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().setEditableTitle(false);
        getView().setCategories(mData.categories);
        getView().setVideoDescription(mData.getDescription());
        getView().setVideoProperties(getDraftProperties(Long.parseLong(mData.duration), mData.path));
        getView().setVideoTitle(mData.getTitle());
        getView().setVideoView(mData.path);
        getView().showDescription(true);
        getView().setCompletedVisibility(false);
        getView().setTextBtnTitle("SAVE AS DRAFT");
        getView().setPrice(getFormattedPrice(Double.parseDouble(mData.price), mData.currency));
        getView().setLocation(mData.formattedLocation);
        getView().setButtonsVisibility(true);
        getView().setDetailsBtnVisibility(false);
        getView().setListVisibility(false);
        getView().setTitle("Overview");
    }


    private String getDraftProperties(long _duration, String _path) {
        File file = new File(_path);
        long size = file.length();

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    private String getFormattedPrice(double _price, CurrencyModel _currency) {
        return _currency.getSymbol() + _price;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mDraftInteractor.unSubscribe();
    }

    @Override
    public void onTextBtnClick() {
        mData.date_upload = (new Date());
        mData.license = ("license");
        mData.setUploading(false);
        mData.setDraft(true);
        mData.draftId = System.currentTimeMillis();
        mDraftInteractor.execute(mData, new DraftObserver(this));
    }

    @Override
    public void onPlayClick() {
        getView().playVideo();
    }

    @Override
    public void setData(Bundle _model) {
        if (_model.containsKey(Constants.BUNDLE_VIDEO)) {
            mData = (Video) _model.getSerializable(Constants.BUNDLE_VIDEO);
        } else {
            throw new IllegalStateException("Arguments should contain VideoUploadModel");
        }
    }

    class DraftObserver extends BaseObserver<Long> {
        public DraftObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(Long aLong) {
            Log.d(TAG, "onNext() called with: " + "aLong = [" + aLong + "]");
            getView().navigateToDashboard();
        }
    }
}
