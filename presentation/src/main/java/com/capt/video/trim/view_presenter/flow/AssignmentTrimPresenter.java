package com.capt.video.trim.view_presenter.flow;

import android.os.Bundle;
import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.model.Video;
import com.capt.domain.global.AssignmentState;
import com.capt.video.trim.ffmpeg_manager.FFMPEGManager;
import com.capt.video.trim.view_presenter.TrimContract;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 13.05.2016.
 */
public class AssignmentTrimPresenter extends AbstractTrimPresenter {

    private static final String TAG = AssignmentTrimPresenter.class.getSimpleName();
    private Assignment mAssignment;
    private BasePostInteractor<Assignment> mUploadForAssignmentInteractor;

    @Inject
    public AssignmentTrimPresenter(TrimContract.View _view,
                                   FFMPEGManager _ffmpegManager,
                                   @Named(Constants.PendingVideoConstants.UPLOAD_FOR_ASSIGNMENT)
                                   BasePostInteractor<Assignment> _uploadInteractor,
                                   BaseExceptionDelegate _delegate) {
        super(_view, _ffmpegManager, _delegate);
        mUploadForAssignmentInteractor = _uploadInteractor;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void setData(Bundle _args) {
        if (_args.containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            mAssignment = (Assignment) _args.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        } else {
            throw new IllegalStateException("Arguments should contain a video model");
        }
    }

    @Override
    protected Video getData() {
        return mAssignment.video;
    }

    @Override
    protected void doOnNext(Video _video) {
        mAssignment.video.quality = _video.quality;
        mAssignment.video.thumbnail = _video.thumbnail;
        mAssignment.video.sourcePath = _video.sourcePath;
        mAssignment.video.state = AssignmentState.UPLOADING;
        mAssignment.video.path = _video.path;
        File file = new File(_video.path);
        long size = file.length();
        mAssignment.video.size = String.valueOf(size);
        mAssignment.video.duration = String.valueOf(((long) getTrimmedDuration() / 1000));
        mUploadForAssignmentInteractor.execute(mAssignment, new VideoObserver(this));
    }

    private class VideoObserver extends BaseObserver<SuccessModel> {

        public VideoObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            getView().finish();
        }

        @Override
        public void onNext(SuccessModel successModel) {
            Log.d(TAG, "onNext() called with: " + "successModel = [" + successModel + "]");
        }
    }
}
