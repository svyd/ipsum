package com.capt.base.dagger;

import com.capt.base.BaseActivity;


/**
 * Created by richi on 2016.02.22..
 */
public interface BaseActivityComponent {
    void inject(BaseActivity _baseActivity);

    BaseActivity activity();
}
