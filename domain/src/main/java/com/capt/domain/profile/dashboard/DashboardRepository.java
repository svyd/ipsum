package com.capt.domain.profile.dashboard;

import com.capt.domain.authorization.model.UpdateAvatarResponse;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.profile.dashboard.model.DashboardModel;
import com.capt.domain.profile.dashboard.model.DashboardParamsModel;

import java.io.File;
import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public interface DashboardRepository {
    Observable<DashboardModel> getDashboard(DashboardParamsModel _model);
    Observable<UpdateAvatarResponse> update(File _file);
}
