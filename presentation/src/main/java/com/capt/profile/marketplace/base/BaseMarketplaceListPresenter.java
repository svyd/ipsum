package com.capt.profile.marketplace.base;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.video.model.Video;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;

/**
 * Created by root on 14.04.16.
 */
public abstract class BaseMarketplaceListPresenter extends BaseMarketplaceListContract.Presenter {

    protected BaseMarketplaceListContract.View mView;
    protected BasePostInteractor<RequestParamsModel> mInteractor;
    protected List<Video> mData;
    protected RequestParamsModel mRequestModel;
    protected String mMarketplaceType;
    private boolean shouldAddItems;

    public BaseMarketplaceListPresenter(BaseMarketplaceListContract.View _view,
                                        BasePostInteractor<RequestParamsModel> _interactor,
                                        String _marketplaceType,
                                        BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
        mMarketplaceType = _marketplaceType;
    }

    @Override
    public void initialize() {
        getInitialData();
        mData = new ArrayList<>();
    }

    protected void getInitialData() {
        mRequestModel = new RequestParamsModel(mMarketplaceType, 10, 0);
        execute();
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void getMoreVideoItems() {
        mView.showProgress();
        mRequestModel.offset = mData.size();
        execute();
    }

    @Override
    public void onVideoClick(Video _video) {
        mView.navigateToVideoDetails(_video);
    }


    protected void execute() {
        mView.showProgress();
        mInteractor.execute(mRequestModel, new LoadVideoSubscriber(this));
    }

    /**
     * Defines whether to add items or to set them
     *
     * @return true if items should be added to the list
     */

    protected boolean shouldAddItems() {
        return shouldAddItems;
    }

    protected void setShouldAddItems(boolean _loaded) {
        shouldAddItems = _loaded;
    }

    private void showVideosOnView(List<Video> _videos) {
        if (shouldAddItems()) {
            onOffset(_videos);
        } else {
            onZeroOffset(_videos);
        }
    }

    private void onOffset(List<Video> _videos) {
        if (_videos.isEmpty()) {
            mView.disableLoadMore();
        } else {
            mView.addVideos(_videos);
        }
    }

    private void onZeroOffset(List<Video> _videos) {
        if (_videos.isEmpty())
            return;

        mData = _videos;
        mView.showVideos(_videos);
        setShouldAddItems(true);
    }

    protected void onVideosLoaded(List<Video> _videos) {
        showVideosOnView(_videos);
    }

    private class LoadVideoSubscriber extends BaseObserver<List<Video>> {
        public LoadVideoSubscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onNext(List<Video> _videos) {
            onVideosLoaded(_videos);
            if (_videos.isEmpty() && (mData == null || mData.isEmpty())) {
                mView.showEmptyList();
            }
        }
    }

    //region unused

    @Override
    public void onCancelClick(int _position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onRetryClick(int _position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onUploadError(int _position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onProgress(int _position, int _progress) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onStartUploading(int _position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onUploadCompleted(int _position, Video _response) {
        throw new UnsupportedOperationException();
    }

    //endregion
}
