package com.capt.video;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.application.dagger.HasComponent;
import com.capt.authorization.dagger.RestModule;
import com.capt.base.BaseActivity;
import com.capt.data.authorization.net.ApiConstants;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.model.Video;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.dagger.VideoActivityModule;
import com.capt.video.flow.AbstractVideoPresenter;
import com.capt.video.overview.OverviewFragment;
import com.capt.video.trim.view_presenter.TrimFragment;

import javax.inject.Inject;

public class VideoActivity extends BaseActivity implements VideoContract.View, HasComponent<VideoActivityComponent> {

    private VideoActivityComponent mComponent;

    @Inject
    AbstractVideoPresenter mPresenter;

    public static void startPurchasedAssignment(Assignment _assignment, Activity _activity) {
        Intent intent = new Intent(_activity, VideoActivity.class);
        intent.putExtra(Constants.FLOW, Flow.ASSIGNMENT_PURCHASED);
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _assignment);
        _activity.startActivity(intent);
    }

    public static void startRejectedAssignment(Assignment _assignment, Activity _activity) {
        Intent intent = new Intent(_activity, VideoActivity.class);
        intent.putExtra(Constants.FLOW, Flow.ASSIGNMENT_REJECTED);
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _assignment);
        _activity.startActivity(intent);
    }

    public static void startAssignmentDetails(Assignment _assignment, Activity _activity) {
        Intent intent = new Intent(_activity, VideoActivity.class);
        intent.putExtra(Constants.FLOW, Flow.ASSIGNMENT_DETAILS);
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _assignment);
        _activity.startActivityForResult(intent, Constants.RequestCode.ASSIGNMENT);
    }

    public static void startVideoFlow(String _path, Context _context) {
        Intent intent = new Intent(_context, VideoActivity.class);
        intent.putExtra(Constants.BUNDLE_VIDEO_PATH, _path);
        intent.putExtra(Constants.FLOW, Flow.VIDEO_UPLOAD);
        _context.startActivity(intent);
    }

    public static void startOverview(Video _video, Activity _context) {
        Intent intent = new Intent(_context, VideoActivity.class);
        intent.putExtra(Constants.BUNDLE_VIDEO, _video);
        intent.putExtra(Constants.FLOW, Flow.DETAILS);

        if (_context.getIntent().hasExtra(Constants.Extra.EXTRA_SERIALIZABLE)) {
            intent.putExtra(
                    Constants.Extra.EXTRA_SERIALIZABLE,
                    _context.getIntent().getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE));
        }

        _context.startActivityForResult(intent, Constants.RequestCode.SHOULD_UPDATE);
    }

    public static void startDraftOverview(Video _video, Activity _context) {
        Intent intent = new Intent(_context, VideoActivity.class);
        intent.putExtra(Constants.BUNDLE_VIDEO, _video);
        intent.putExtra(Constants.FLOW, Flow.DRAFT);

        if (_context.getIntent().hasExtra(Constants.Extra.EXTRA_SERIALIZABLE)) {
            intent.putExtra(
                    Constants.Extra.EXTRA_SERIALIZABLE,
                    _context.getIntent().getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE));
        }

        _context.startActivityForResult(intent, Constants.RequestCode.SHOULD_UPDATE);
    }

    public static void startAssignmentCapt(Assignment _assignment, Activity _context) {
        Intent intent = new Intent(_context, VideoActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _assignment);
        intent.putExtra(Constants.FLOW, Flow.CAPT_ASSIGNMENT);
        _context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        initComponent();
        inject();
        setPresenterArgs();
        if (savedInstanceState == null) {
            mPresenter.initialize();
        }
    }

    private void setPresenterArgs() {
        mPresenter.setArgs(getIntent().getExtras());
    }

    private void initComponent() {
        if (getIntent().hasExtra(Constants.FLOW)) {
            int flow = getIntent().getIntExtra(Constants.FLOW, -1);

            mComponent = CaptApplication.getApplication()
                    .getAppComponent()
                    .createVideoActivityComponent(new VideoActivityModule(this, flow), new RestModule(ApiConstants.GOOGLE_MAPS_API_ENDPOINT));
        } else {
            throw new IllegalStateException("no flow specified!");
        }
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public int getContainerId() {
        return R.id.rlContainer_AV;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AV;
    }

    @Override
    public VideoActivityComponent getComponent() {
        return mComponent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void initOverViewFragment(Bundle _args) {
        getFragmentNavigator().addFragmentWithoutBackStack(
                OverviewFragment.newInstance(_args));
    }

    @Override
    public void initTrimFragment(Bundle _args) {
        getFragmentNavigator().addFragmentWithoutBackStack(
                TrimFragment.newInstance(_args));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getFragmentNavigator().getTopFragment();
        if (fragment != null)
            fragment.onActivityResult(requestCode, resultCode, data);
    }
}
