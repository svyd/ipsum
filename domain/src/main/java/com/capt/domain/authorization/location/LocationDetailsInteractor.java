package com.capt.domain.authorization.location;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 21.03.2016.
 */
public class LocationDetailsInteractor extends BasePostInteractor<String> {

    LocationDataStore mDataStore;

    @Inject
    public LocationDetailsInteractor(LocationDataStore _store, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mDataStore = _store;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mDataStore.getPlaceDetails(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
