package com.capt.video.camera.fragment;

/**
 * Created by Svyd on 16.06.2016.
 */
public interface CameraState {
    boolean isCameraAlive();
}
