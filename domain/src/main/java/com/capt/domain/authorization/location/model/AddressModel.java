package com.capt.domain.authorization.location.model;

import javax.lang.model.element.Name;

/**
 * Created by Svyd on 16.03.2016.
 */
public class AddressModel {

    public AddressModel(String _name, double _lat, double _lon) {
        name = _name;
        lat = _lat;
        lon = _lon;
    }

    private String name;
    private double lat;
    private double lon;

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getName() {
        return name;
    }
}
