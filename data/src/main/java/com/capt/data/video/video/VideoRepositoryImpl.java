package com.capt.data.video.video;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.global.Constants;
import com.capt.domain.video.DiskDataSource;
import com.capt.domain.video.model.Video;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.video.model.VideoPatch;
import com.capt.domain.video.video.VideoRepository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;

/**
 * Created by Svyd on 29.03.2016.
 */
public class VideoRepositoryImpl implements VideoRepository {

    private VideoService mService;
    private DiskDataSource mDataSource;

    @Inject
    public VideoRepositoryImpl(VideoService _service, DiskDataSource _dataSource) {
        mService = _service;
        mDataSource = _dataSource;
    }

    @Override
    public Observable<List<Video>> getVideos(RequestParamsModel _model) {
        return mService.getVideos(_model.limit, _model.offset, _model.type)
                .map(videoModelWrapper -> videoModelWrapper.videos);
    }

    @Override
    public Observable<List<Video>> getPendingVideos() {
        return mDataSource.getAllUploading();
    }

    @Override
    public Observable<Video> patchVideo(Video _video) {
        return mService.patchVideo(_video.id, new VideoPatch(_video));
    }

    @Override
    public Observable<SuccessModel> deleteVideo(String _id) {
        return mService.deleteVideo(_id);
    }
}
