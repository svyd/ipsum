package com.capt.profile.assignments;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

/**
 * Created by root on 26.04.16.
 */
public interface AssignmentsTabContract {

	interface View extends BaseView {
		void setViewPagerItem(int position);
	}

	abstract class Presenter extends BasePresenter<AssignmentsTabContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void setType(String _type);
	}
}
