package com.capt.domain.profile.dashboard.model;

import com.capt.domain.profile.dashboard.model.base.BaseItems;

/**
 * Created by Svyd on 25.03.2016.
 */
public class Marketplace extends BaseItems {
    @Override
    public String getHeaderTitle() {
        return "MarketPlace";
    }

    @Override
    public String getHeaderDescription() {
        return "";
    }
}
