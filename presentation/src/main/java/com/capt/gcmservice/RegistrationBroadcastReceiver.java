package com.capt.gcmservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import javax.inject.Inject;

/**
 * Created by richi on 2016.03.31..
 */
public class RegistrationBroadcastReceiver extends BroadcastReceiver {

    @Inject
    public RegistrationBroadcastReceiver() {}

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        boolean sentToken = sharedPreferences
                .getBoolean(RegistrationIntentService.SENT_TOKEN_TO_SERVER, false);
        if (sentToken) {
            Log.d("MyLog", "Token retrieved and sent to server");
        } else {
            Log.d("MyLog", "An error occurred while either fetching the InstanceID token");
        }
    }
}
