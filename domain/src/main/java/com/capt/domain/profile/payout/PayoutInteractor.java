package com.capt.domain.profile.payout;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by rMozes on 8/3/16.
 */
public class PayoutInteractor extends BaseInteractor {

    private PayOutRepository mRepository;

    @Inject
    public PayoutInteractor(PayOutRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);

        mRepository = _repository;
    }

    @Override
    protected Observable buildGetObserver() {
        return mRepository.getPayouts();
    }
}
