package com.capt.video.currency;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.capt.R;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.currency.model.CurrencyModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by root on 31.03.16.
 */
@PerFragment
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>{

	private Context context;
	private List<CurrencyModel> currencies = new ArrayList<>();
	private CurrencyModel selectedCurrency;
	private OnCurrencyClickListener currencyClickListener;

	@Inject
	public CurrencyAdapter(Context context) {
		this.context = context;
	}

	public void setData(List<CurrencyModel> currencies, CurrencyModel selectedCurrency) {
		this.currencies = currencies;
		this.selectedCurrency = selectedCurrency;
	}

	@Override public CurrencyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(context)
				.inflate(R.layout.list_item_currency, parent, false);
		return new CurrencyViewHolder(view);
	}

	@Override public void onBindViewHolder(CurrencyViewHolder holder, int position) {
		CurrencyModel currencyModel = currencies.get(position);
		holder.tvTitle.setText(currencyModel.getCurrencyISO());

		if ((selectedCurrency != null) && (selectedCurrency.getCurrencyId() != null)) {

			if (selectedCurrency.getCurrencyId().equals(currencyModel.getCurrencyId())) {
				holder.ivFilter.setImageResource(R.drawable.circle_selected);
			} else {
				holder.ivFilter.setImageResource(R.drawable.circle_unselect);
			}
		}

		holder.view.setOnClickListener(v -> currencyClickListener.onCurrencyClicked(currencyModel));
	}

	@Override public int getItemCount() {
		return currencies.size();
	}

	public void setOnCurrencyClickListener(OnCurrencyClickListener listener) {
		this.currencyClickListener = listener;
	}

	public interface OnCurrencyClickListener {
		void onCurrencyClicked(CurrencyModel currencyModel);
	}

	public static class CurrencyViewHolder extends RecyclerView.ViewHolder {

		public View view;
		@Bind(R.id.tv_currency_LIC) TextView tvTitle;
		@Bind(R.id.iv_filter_LIC)   ImageView ivFilter;

		public CurrencyViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
			view = itemView;
		}
	}
}
