package com.capt.profile.settings.currency;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.dashboard.model.Progress;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.profile.dagger.ProfileActivityComponent;
import com.capt.profile.settings.currency.dagger.CurrencySettingsComponent;
import com.capt.profile.settings.currency.dagger.CurrencySettingsModule;
import com.capt.profile.settings.dagger.AccountComponent;
import com.capt.video.currency.CurrencyAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Provides;

/**
 * Created by root on 29.04.16.
 */
public class CurrencySettingsFragment extends BaseToolbarFragment implements CurrencySettingsContract.View {

    @Inject
    CurrencySettingsContract.Presenter mPresenter;

    @Inject
    CurrencyAdapter currencyAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Bind(R.id.rv_currencies_FC)
    RecyclerView rvCurrencies;

    @Bind(R.id.vTopDivider_FC)
    View vDivider;

    @Bind(R.id.pbCurrency_FC)
    ProgressBar pbCurrency;

    private CurrencySettingsComponent mComponent;
    private CurrencyModel selectedCurrency;

    public static CurrencySettingsFragment newInstance(SettingsModel _model) {

        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, _model);
        CurrencySettingsFragment fragment = new CurrencySettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_currency);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponent();
        inject();
        showToolbar();
        checkExtra();
        setUpView();
        mPresenter.initialize();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(AccountComponent.class)
                .createCurrencySettingsComponent(new CurrencySettingsModule(this));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void showToolbar() {
        getToolbarManager().showToolbar();
        getActivity().setTitle(getString(R.string.currency));
    }

    private void checkExtra() {
        if (getArguments().containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            SettingsModel settings = (SettingsModel) getArguments().getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
            assert settings != null;
            selectedCurrency = settings.defaultCurrency;
        } else {
            throw new IllegalStateException("Fragment should be instantiated with a SettingsModel as an argument");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void setUpView() {
        rvCurrencies.setLayoutManager(mLayoutManager);
    }

    @Override
    public void showProgress() {
        pbCurrency.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbCurrency.setVisibility(View.GONE);
    }

    @Override
    public void showCurrencies(List<CurrencyModel> currencyModels) {
        vDivider.setVisibility(View.VISIBLE);
        currencyAdapter.setData(currencyModels, selectedCurrency);
        rvCurrencies.setAdapter(currencyAdapter);
        currencyAdapter.setOnCurrencyClickListener(mPresenter::onCurrencySelected);
    }

    @Override
    public void showMessage(String _message) {
        super.showMessage(_message);
    }

    @Override
    public void navigateToSettingsScreen(SettingsModel _settings) {
        Intent intent = new Intent();
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _settings);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

}
