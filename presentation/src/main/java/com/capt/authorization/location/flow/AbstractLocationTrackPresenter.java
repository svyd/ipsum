package com.capt.authorization.location.flow;

import com.capt.authorization.location.LocationContract;
import com.capt.authorization.location.LocationInteractorManager;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;

import rx.Observer;

/**
 * Created by Svyd on 5/18/16.
 */
public abstract class AbstractLocationTrackPresenter extends AbstractLocationPresenter {

    protected LocationModel mLocationModel;

    public AbstractLocationTrackPresenter(LocationContract.LocationView _view,
                                          LocationInteractorManager _manager,
                                          BaseExceptionDelegate _delegate) {
        super(_view, _manager, _delegate);
    }

    @Override
    public void onLocationChanged(double _lat, double _lon) {
        mLocationModel = new LocationModel(_lat, _lon);
        mInteractorManager.execute(mLocationModel, new LocationObserver(this));
    }

    @Override
    public void onHeaderClicked() {
        getView().showUpdateLocation();
        getView().doLocationUpdate();
    }

    private class LocationObserver extends BaseObserver<PredictionsModel> {
        public LocationObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            mLocationModel.mCity = predictionsModel.predictions[0].city;
            mLocationModel.mCountry = predictionsModel.predictions[0].country;
            doOnNext(mLocationModel);
        }
    }
}
