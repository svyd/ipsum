package com.capt.data.assignment.accept;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.profile.assignment.accept.AcceptAssignmentRepository;
import com.capt.domain.profile.assignment.accept.model.AcceptAssignmentModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by root on 22.04.16.
 */
public class AcceptAssignmentRepositoryImpl implements AcceptAssignmentRepository {

	private AcceptAssignmentService mService;

	@Inject
	public AcceptAssignmentRepositoryImpl(AcceptAssignmentService mService) {
		this.mService = mService;
	}

	@Override public Observable<SuccessModel> acceptAssignment(AcceptAssignmentModel _model) {
		return mService.acceptAssignment(_model.id, _model);
	}
}
