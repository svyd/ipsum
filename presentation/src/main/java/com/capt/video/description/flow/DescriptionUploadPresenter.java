package com.capt.video.description.flow;

import com.capt.authorization.location.LocationInteractorManager;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.video.description.DescriptionContract;
import com.capt.video.trim.time_util.TimeUtilImpl;

import javax.inject.Inject;

/**
 * Created by Svyd on 01.05.2016.
 */
public class DescriptionUploadPresenter extends AbstractDescriptionPresenter {

    @Inject
    public DescriptionUploadPresenter(DescriptionContract.View _view,
                                      LocationInteractorManager _manager,
                                      TimeUtilImpl _timeUtil, BaseExceptionDelegate _delegate) {
        super(_view, _manager, _timeUtil, _delegate);
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().setVideoVisibility(true);
        getView().setVideoView(getData().path);
        getView().showToolbar(true);
        getView().setToolbarTitle("Edit");
        getView().setVideoProperties(getFormattedProperties());
    }

    private String getFormattedProperties() {
        String duration = getTimeUtil().getTimeFromIntegerMinutes(Long.parseLong(getData().duration));
        String size = getData().formattedSize;
        return duration + " / " + size + "MB";
    }
}
