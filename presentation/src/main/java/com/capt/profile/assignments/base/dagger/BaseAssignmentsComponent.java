package com.capt.profile.assignments.base.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.assignments.base.BaseAssignmentsFragment;

import dagger.Subcomponent;

/**
 * Created by root on 15.04.16.
 */
@PerFragment
@Subcomponent( modules = BaseAssignmentsModule.class)
public interface BaseAssignmentsComponent {
	void inject(BaseAssignmentsFragment _fragment);
}
