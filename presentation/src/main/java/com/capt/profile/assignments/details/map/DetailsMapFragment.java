package com.capt.profile.assignments.details.map;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.capt.R;
import com.capt.base.BaseLocationFragment;
import com.capt.domain.global.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by root on 21.04.16.
 */
public class DetailsMapFragment extends BaseLocationFragment implements OnMapReadyCallback {

	private SupportMapFragment fragment;
	private GoogleMap mMap;

	public static DetailsMapFragment newInstance(double lat, double lon) {

		Bundle args = new Bundle();
		args.putDouble(Constants.Extra.EXTRA_LAT, lat);
		args.putDouble(Constants.Extra.EXTRA_LON, lon);
		DetailsMapFragment fragment = new DetailsMapFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_map);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		fragment = SupportMapFragment.newInstance();
		FragmentTransaction fragmentTransaction =
				getChildFragmentManager().beginTransaction();
		fragmentTransaction.add(R.id.map_root, fragment);
		fragmentTransaction.commit();
		fragment.getMapAsync(DetailsMapFragment.this);
	}
	private void checkExtra() {
		if ((getArguments().containsKey(Constants.Extra.EXTRA_LAT)) &&
				(getArguments().containsKey(Constants.Extra.EXTRA_LON))) {
			double lat = getArguments().getDouble(Constants.Extra.EXTRA_LAT);
			double lon = getArguments().getDouble(Constants.Extra.EXTRA_LON);
			addMarkerToMap(new LatLng(lat, lon));
		}
	}

	@Override public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		setUpMap();
	}

	private void setUpMap() {
		// Hide the zoom controls as the button panel will cover it.
		mMap.getUiSettings().setZoomControlsEnabled(false);
//		mPresenter.setLocation();
		checkExtra();
	}

	public void addMarkerToMap(final LatLng location) {
		mMap.clear();
		mMap.addMarker(new MarkerOptions()
				.position(location)
				.title("Location")
				.snippet("CAPT")
				.draggable(false));

		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14.0f));
	}
}
