package com.capt.profile.dashboard.dagger;

import android.support.v7.widget.RecyclerView;

import com.capt.data.annotation.PerFragment;
import com.capt.data.assignment.AssignmentsMapper;
import com.capt.data.assignment.AssignmentsRepositoryImpl;
import com.capt.data.assignment.AssignmentsService;
import com.capt.data.base.TypeMapper;
import com.capt.data.dashboard.DashboardRepositoryImpl;
import com.capt.data.dashboard.UserService;
import com.capt.data.marketplace.MarketplaceMapper;
import com.capt.data.marketplace.MarketplaceRepositoryImpl;
import com.capt.data.marketplace.MarketplaceService;
import com.capt.data.video.video.VideoService;
import com.capt.data.video.video.VideoRepositoryImpl;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.profile.assignment.AssignmentsRepository;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.DashboardInteractor;
import com.capt.domain.profile.dashboard.DashboardRepository;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.profile.dashboard.model.DashboardParamsModel;
import com.capt.domain.profile.dashboard.model.Tutorial;
import com.capt.domain.profile.dashboard.UpdatePhotoInteractor;
import com.capt.domain.profile.marketplace.MarketplaceRepository;
import com.capt.domain.profile.marketplace.model.MarketplaceItem;
import com.capt.domain.profile.marketplace.model.MarketplaceModel;
import com.capt.domain.video.video.VideoRepository;
import com.capt.domain.global.Constants;
import com.capt.profile.dashboard.DashboardContract;
import com.capt.profile.dashboard.DashboardPresenter;
import com.capt.profile.dashboard.ui.adapter.DashboardAdapter;
import com.capt.profile.dashboard.ui.adapter.StickyHeaderLayoutManager;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Svyd on 28.03.2016.
 */
@Module
public class DashboardModule {

    private DashboardContract.DashboardView mView;

    public DashboardModule(DashboardContract.DashboardView _view) {
        mView = _view;
    }

    @Provides
    @PerFragment
    protected DashboardContract.DashboardView provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    protected DashboardContract.DashboardPresenter providePresenter(DashboardPresenter _presenter) {
        return _presenter;
    }

    @Provides
    @PerFragment
    Tutorial provideTutorial(Tutorial _tutorial) {
        return _tutorial;
    }

    @Provides
    @PerFragment
    MarketplaceRepository provideMarketplaceRepository(MarketplaceRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    AssignmentsService provideAssignmentsService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                 Retrofit _retrofit) {
        return _retrofit.create(AssignmentsService.class);
    }

    @Provides
    RecyclerView.ItemDecoration provideStickyDecoration(DashboardAdapter _adapter) {
        return new StickyRecyclerHeadersDecoration(_adapter);
    }

    @Provides
    RecyclerView.LayoutManager provideLinearLayoutManager() {
        return new StickyHeaderLayoutManager();
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Mapper.ASSIGNMENTS_MAPPER)
    TypeMapper<List<Assignment>, List<Assignment>> provideAssignmentsMapper(AssignmentsMapper _mapper) {
        return _mapper;
    }

    @Provides
    @PerFragment
    AssignmentsRepository provideAssignmentsRepository(AssignmentsRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Mapper.MARKETPLACE_MAPPER)
    TypeMapper<List<MarketplaceItem>, MarketplaceModel> provideMarketplaceMapper(MarketplaceMapper _mapper) {
        return _mapper;
    }

    @Provides
    @PerFragment
    MarketplaceService provideMarketplaceService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                 Retrofit _retrofit) {
        return _retrofit.create(MarketplaceService.class);
    }

    @Provides
    @PerFragment
    UserService provideUpdatePhotoService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                 Retrofit _retrofit) {
        return _retrofit.create(UserService.class);
    }

    @Provides
    @PerFragment
    VideoService provideVideoDownloadService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                     Retrofit _retrofit) {
        return _retrofit.create(VideoService.class);
    }

    @Provides
    @PerFragment
    VideoRepository provideVideoDownloadRepository(VideoRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.UPDATE_PHOTOT_INTERACTOR)
    BasePostInteractor<String> provideUploadInteractor(UpdatePhotoInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.DASHBOARD_INTERACTOR)
    BasePostInteractor<DashboardParamsModel> provideDashboardInteractor(DashboardInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    DashboardRepository provideDashboardRepository(DashboardRepositoryImpl _repository) {
        return _repository;
    }

}
