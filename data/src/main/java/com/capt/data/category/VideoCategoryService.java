package com.capt.data.category;

import com.capt.domain.category.model.CategoryModel;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Svyd on 22.04.2016.
 */
public interface VideoCategoryService {

    @GET("mobile/video/categories")
    Observable<List<CategoryModel>> getCategories();
}
