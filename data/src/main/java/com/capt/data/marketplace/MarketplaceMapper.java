package com.capt.data.marketplace;

import com.capt.data.base.TypeMapper;
import com.capt.domain.profile.dashboard.model.Marketplace;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.profile.marketplace.model.MarketplaceItem;
import com.capt.domain.profile.marketplace.model.MarketplaceModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 04.04.2016.
 */
public class MarketplaceMapper implements TypeMapper<List<MarketplaceItem>, MarketplaceModel> {

    @Inject
    public MarketplaceMapper() {}

    @Override
    public List<MarketplaceItem> convert(MarketplaceModel _value) {
        List<MarketplaceItem> items = new ArrayList<>();
        items.add(new MarketplaceItem("Recent", "" + _value.recent));
        items.add(new MarketplaceItem("Drafts", "" + _value.drafts));
        items.add(new MarketplaceItem("Sold", "" + _value.sold));
        items.add(new MarketplaceItem("My payouts", "" + _value.payouts));

        return items;
    }
}
