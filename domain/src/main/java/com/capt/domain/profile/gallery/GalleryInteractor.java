package com.capt.domain.profile.gallery;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 4/23/16.
 */
public class GalleryInteractor extends BaseInteractor {

    private GalleryRepository mRepository;

    @Inject
    public GalleryInteractor(GalleryRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildGetObserver() {
        return mRepository.getVideoList();
    }
}
