package com.capt.data.authorization.repository.datasource;


import android.content.SharedPreferences;

import com.capt.data.authorization.net.retrofit.AuthService;
import com.google.gson.Gson;

import java.net.CookieStore;

import javax.inject.Inject;

/**
 * Created by Svyd on 02.03.2016.
 */
public class AuthStoreFactory {

    private AuthDataStore mCloudDataStore;

    private AuthService mCloudDataSource;
    private CookieStore mCookieStore;
    private SharedPreferences mPreferences;
    private Gson mGson;

    @Inject
    public AuthStoreFactory(SharedPreferences _preferences,
                            Gson _gson,
                            AuthService _cloudDataStore,
                            CookieStore _store) {
        mCloudDataSource = _cloudDataStore;
        mCookieStore = _store;
        mPreferences = _preferences;
        mGson = _gson;
    }

    public AuthDataStore create() {
        if (mCloudDataStore == null)
            mCloudDataStore = new CloudAuthDataStore(mGson, mPreferences, mCloudDataSource, mCookieStore);

        return mCloudDataStore;
    }
}
