package com.capt.profile.marketplace;

import com.capt.domain.video.model.Video;

/**
 * Created by Svyd on 02.05.2016.
 */
public interface OnVideoClickListener {
    void onVideoClick(Video _video);
}
