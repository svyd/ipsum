package com.capt.video.category.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.category.CategoryFragment;

import dagger.Subcomponent;

/**
 * Created by root on 07.04.16.
 */
@PerFragment
@Subcomponent(modules = CategoryModule.class)
public interface CategoryComponent {
	void inject(CategoryFragment _fragment);
}