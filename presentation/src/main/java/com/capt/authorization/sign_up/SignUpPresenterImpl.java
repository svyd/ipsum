package com.capt.authorization.sign_up;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.capt.authorization.join_fragment.DeviceIdProvider;
import com.capt.authorization.location.LocationInteractorManager;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.base.EmailValidator;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.SignUpModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observer;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

/**
 * Created by richi on 2016.03.18..
 */
public class SignUpPresenterImpl extends SignUpContract.Presenter {

    private static final String TAG = "SignUpPresenterImpl";

    public static final String RESTORE_PHOTO_PATH = "restore_photo_path";
    public static final String RESTORE_LOCATION_NAME = "restore_location_name";
    public static final String RESTORE_LOCATION = "restore_location_coordinate";
    public static final String RESTORE_SIGN_UP_MODEL = "restore_sign_up_model";

    private BasePostInteractor<String> mEmailChecker;
    private PublishSubject<String> mEmailSubject;
    private PublishSubject<SignUpModel> mCredentialsSubject;

    private SignUpContract.View mView;
    private LocationInteractorManager mLocationManager;
    private DeviceIdProvider mDeviceIdProvider;
    private EmailValidator mEmailValidator;

    private SignUpModel mSignUpModel;
    private String mLocationName;
    private String mPhotoPath;
    private String[] mLocation;
    private long mBirthday = -1;
    private int mGender = -1;
    private boolean mIsEmailValid, mIsEmailValidationInProgress;
    private boolean mIsPasswordVisible = true;
    private boolean mIsConfirmPasswordVisible = true;

    @Inject
    public SignUpPresenterImpl(@Named(Constants.NamedAnnotation.CHECK_EMAIL_INTERACTOR)
                                   BasePostInteractor<String> _emailChecker,
                               SignUpContract.View _view,
                               LocationInteractorManager _manager,
                               DeviceIdProvider _provider,
                               EmailValidator _emailValidaotr, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        createPublishSubject();
        mView = _view;
        mLocationManager = _manager;
        mDeviceIdProvider = _provider;
        mEmailValidator = _emailValidaotr;
        mEmailChecker = _emailChecker;
    }

    private void createPublishSubject() {
        mEmailSubject = PublishSubject.create();
        mEmailSubject
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(mEmailDebounce);

        mCredentialsSubject = PublishSubject.create();
        mCredentialsSubject.skip(1)
                .subscribe(mValidateCredentials);
    }

    @Override
    public void onSaveInstanceState(Bundle _bundle) {
        _bundle.putString(RESTORE_PHOTO_PATH, mPhotoPath);
        _bundle.putString(RESTORE_LOCATION_NAME, mLocationName);
        _bundle.putStringArray(RESTORE_LOCATION, mLocation);
        _bundle.putSerializable(RESTORE_SIGN_UP_MODEL, mSignUpModel);
    }

    @Override
    public void onRestoreInstanceState(Bundle _bundle) {
        if (_bundle == null)
            return;

        mPhotoPath = _bundle.getString(RESTORE_PHOTO_PATH);
        mLocationName = _bundle.getString(RESTORE_LOCATION_NAME);
        mLocation = _bundle.getStringArray(RESTORE_LOCATION);
        mSignUpModel = (SignUpModel) _bundle.getSerializable(RESTORE_SIGN_UP_MODEL);
    }

    @Override
    public void initialize() {
        if (mLocationName != null)
            mView.setLocationName(mLocationName);

        mView.loadAvatarFrom(mPhotoPath);
    }

    @Override
    public void onStop() {
        mLocationManager.unSubscribe();
        mEmailChecker.unSubscribe();
    }

    @Override
    public void setLatLng(String[] latLng) {
        mLocation = latLng;
        mView.needToUpdateLocation(false);
    }

    @Override
    public void onNextClick(SignUpModel.Builder _signUpModel) {
        mSignUpModel = _signUpModel
                .setAvatar(mPhotoPath)
                .setLocation(mLocation)
                .setDeviceToken("test_token")
                .setDeviceId(mDeviceIdProvider.getDeviceId())
                .setGender(mGender)
                .setBirthday(mBirthday)
                .build();

        startPhoneNumberFragmentIfUserValid();
    }

    @Override
    public void onValidateCredentials(SignUpModel _model) {
        mCredentialsSubject.onNext(_model);
    }

    private Action1<SignUpModel> mValidateCredentials = new Action1<SignUpModel>() {
        @Override
        public void call(SignUpModel signUpModel) {
            if (mIsEmailValidationInProgress || !mIsEmailValid ||
                    signUpModel == null || signUpModel.getFullName().length() == 0)
                return;

            mSignUpModel = signUpModel;
            mView.enableNextBtn(isPasswordValid() && isLocationValid() && isUserNameValid());
        }
    };

    @Override
    public void onEmailTextChanged(String _email) {
        mView.showEmailIsUsed(false);
        if (!mEmailValidator.isEmailValid(_email))
            return;

        mView.enableNextBtn(false);
        mView.showProgress();
        mIsEmailValidationInProgress = true;
        mEmailSubject.onNext(_email);
    }

    private Action1<String> mEmailDebounce = new Action1<String>() {
        @Override
        public void call(String _email) {
            mEmailChecker.unSubscribe();
            mEmailChecker.execute(_email, new EmailValidObserver(SignUpPresenterImpl.this));
        }
    };

    @Override
    public void onBirthdayClicked() {
        DateTime dateTime = DateTime.now().withTimeAtStartOfDay();
        if (mBirthday != -1) {
            dateTime = dateTime.withMillis(mBirthday);
        }

        mView.showDatePickerDialog(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth());
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBirthdayChanged(int _year, int _month, int _day) {
        DateTime dateTime = DateTime.now().withTimeAtStartOfDay()
                .withYear(_year)
                .withMonthOfYear(_month)
                .withDayOfMonth(_day);

        mBirthday = dateTime.getMillis();
        String year = String.valueOf(_year);
        String month = String.format("%02d", _month);
        String day = String.format("%02d", _day);

        mView.renderBirthday(year, month, day);
    }

    @Override
    public void onGenderChanged(int _status) {
        mGender = _status;
    }

    private boolean isEmailValid() {
        return mEmailValidator.isEmailValid(mSignUpModel.getEmail());
    }

    private boolean isPasswordValid() {
        return mSignUpModel.getPassword() != null && mSignUpModel.getPassword().trim().length() > 5;
    }

    private boolean isUserNameValid() {
        return mSignUpModel.getFullName() != null && mSignUpModel.getFullName().trim().length() != 0;
    }

    private boolean isLocationValid() {
        return mLocationName != null && mLocationName.trim().length() != 0;
    }

    private boolean passwordMatches () {
        return mView.getPassword().equals(mView.getConfirmedPassword());
    }

    private void startPhoneNumberFragmentIfUserValid() {
        if (!isEmailValid()) {
            mView.showMessage("Email is not valid");
            return;
        }

        if (!isUserNameValid()) {
            mView.showMessage("Full title is not valid");
            return;
        }

        if (!isPasswordValid()) {
            mView.showMessage("Password is not valid");
            return;
        }

        if (!isLocationValid()) {
            mView.showMessage("Please choose location");
            return;
        }

        if (!passwordMatches()) {
            mView.showMessage("Password does not match");
            return;
        }

        mSignUpModel.setCountry(mLocationName);
        CodeSignUpModel model = new CodeSignUpModel();
        model.setSignUpModel(mSignUpModel);

        mView.startPhoneNumberFragment(model);
    }

    @Override
    public void onLocationChooseClick() {
        mView.startLocationFragment();
    }

    @Override
    public void locationNameSelected(String _countryName) {
        if (_countryName == null)
            return;

        mLocationName = _countryName;
        mView.setLocationName(_countryName);
    }

    @Override
    public void setPhotoPath(String _photoPath) {
        if (_photoPath == null)
            return;

        mPhotoPath = _photoPath;
        mView.loadAvatarFrom(_photoPath);
    }

    @Override
    public void onLocationChanged(Location _location) {
        if (mLocationName != null && mLocationName.trim().length() != 0)
            return;

        mLocation = new String[] {String.valueOf(_location.getLongitude()),
                String.valueOf(_location.getLatitude())};
        mLocationManager.execute(new LocationModel(_location.getLatitude(),
                _location.getLongitude()), new LocationObserver(this));
    }

    @Override
    public void onCameraClick() {
        mView.takePhoto();
    }

    @Override
    public void onEyeClick() {
        mIsPasswordVisible = !mIsPasswordVisible;
        if (mIsPasswordVisible)
            mView.showPassword();
        else
            mView.hidePassword();
    }

    @Override
    public void onConfirmEyeClick() {
        mIsConfirmPasswordVisible = !mIsConfirmPasswordVisible;
        if (mIsConfirmPasswordVisible)
            mView.showConfirmPassword();
        else
            mView.hideConfirmPassword();
    }

    private class EmailValidObserver extends BaseObserver<SuccessModel> {
        public EmailValidObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onError(Throwable e) {
            if (e instanceof HttpException) {
                HttpException error = (HttpException) e;
                if (error.code() == 409) {
                    mView.showEmailIsUsed(true);
                }
            }

            mView.hideProgress();
            mIsEmailValid = mIsEmailValidationInProgress = false;
        }

        @Override
        public void onNext(SuccessModel successModel) {
            mView.hideProgress();
            mIsEmailValidationInProgress = false;
            mIsEmailValid = true;
            onValidateCredentials(mSignUpModel);
        }
    }

    private class LocationObserver extends BaseObserver<PredictionsModel> {
        public LocationObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            Log.d(TAG, "onNext: " + predictionsModel.predictions[0].description);
            if (mLocationName == null || mLocationName.trim().length() == 0) {
                mLocationName = predictionsModel.predictions[0].description;
                locationNameSelected(mLocationName);
            }
        }
    }
}
