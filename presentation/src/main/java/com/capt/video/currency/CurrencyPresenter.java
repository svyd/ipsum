package com.capt.video.currency;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.currency.model.CurrencyModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by root on 25.03.16.
 */
public class CurrencyPresenter extends CurrencyContract.Presenter {

    private CurrencyContract.View mView;
    private BaseInteractor mInteractor;

    @Inject
    public CurrencyPresenter(CurrencyContract.View _view,
                             @Named(Constants.NamedAnnotation.CURRENCY_INTERACTOR)
                             BaseInteractor _interactor, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
    }


    @Override
    public void initialize() {
        mView.showProgress();
        mInteractor.execute(new Subscriber(this));
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void onCurrencySelected(CurrencyModel currencyModel) {
        mView.navigateToSetPriceScreen(currencyModel);
    }

    private void showCurrenciesOnView(List<CurrencyModel> currencyModels) {
        mView.showCurrencies(currencyModels);
    }

    private class Subscriber extends BaseObserver<List<CurrencyModel>> {
        public Subscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onNext(List<CurrencyModel> currencyModels) {
            showCurrenciesOnView(currencyModels);
        }
    }
}
