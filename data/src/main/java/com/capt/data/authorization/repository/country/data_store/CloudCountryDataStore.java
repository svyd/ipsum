package com.capt.data.authorization.repository.country.data_store;

import com.capt.data.authorization.net.retrofit.CountryService;
import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

import rx.Observable;

/**
 * Created by richi on 2016.03.16..
 */
public class CloudCountryDataStore implements CountryDataStore {

    private CountryService mService;

    public CloudCountryDataStore(CountryService _service) {
        mService = _service;
    }

    @Override
    public Observable<List<CountryModel>> getCountryModel() {
        return mService.getCountries();
    }
}
