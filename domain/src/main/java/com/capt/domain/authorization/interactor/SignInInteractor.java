package com.capt.domain.authorization.interactor;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 03.03.2016.
 */
public class SignInInteractor extends BasePostInteractor<SignInModel> {

    private AuthRepository mRepository;

    @Inject
    public SignInInteractor(AuthRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(SignInModel _data) {
        return mRepository.signIn(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
