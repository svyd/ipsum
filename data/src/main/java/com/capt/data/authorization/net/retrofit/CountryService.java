package com.capt.data.authorization.net.retrofit;

import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by richi on 2016.03.17..
 */
public interface CountryService {

    @GET("/mobile/countries")
    Observable<List<CountryModel>> getCountries();
}
