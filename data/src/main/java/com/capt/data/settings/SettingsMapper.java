package com.capt.data.settings;

import android.media.audiofx.BassBoost;

import com.capt.data.base.TypeMapper;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.capt.domain.profile.settings.model.Account;
import com.capt.domain.profile.settings.model.Profile;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.profile.settings.model.Support;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 12.05.2016.
 */
public class SettingsMapper implements TypeMapper<SettingsModel, SettingsModel> {

    @Inject
    public SettingsMapper(){}

    private boolean isSocial;

    private List<Section<Item>> getItems(String _currency) {
        List<Section<Item>> items = new ArrayList<>();

        List<Item> account = new ArrayList<>();
        List<Item> profile = new ArrayList<>();
        List<Item> support = new ArrayList<>();

        Section<Item> accountSection = new Section<>(account, true);
        Section<Item> profileSection = new Section<>(profile, true);
        Section<Item> supportSection = new Section<>(support, true);

        accountSection.setHeaderTitle("Account");
        profileSection.setHeaderTitle("Profile");
        supportSection.setHeaderTitle("Support");

        account.add(new Item(Constants.Settings.Account.NOTIFICATIONS, ""));
        account.add(new Item(Constants.Settings.Account.PERMISSIONS, ""));
        account.add(new Item(Constants.Settings.Account.CURRENCY, _currency));
        account.add(new Item(Constants.Settings.Account.PAYMENT, "PayPal"));

        profile.add(new Item(Constants.Settings.Profile.EDIT_PROFILE, ""));
        if (!isSocial) {
            profile.add(new Item(Constants.Settings.Profile.CHANGE_PASSWORD, ""));
        }
        profile.add(new Item(Constants.Settings.Profile.LOG_OUT, ""));

        support.add(new Item(Constants.Settings.Support.REPORT, ""));
        support.add(new Item(Constants.Settings.Support.PRIVACY, ""));
        support.add(new Item(Constants.Settings.Support.TERMS, ""));

        items.add(accountSection);
        items.add(profileSection);
        items.add(supportSection);

        return items;
    }

    @Override
    public SettingsModel convert(SettingsModel _value) {
        isSocial = _value.isSocial();
        _value.setSettingsItems(getItems(_value.defaultCurrency.getCurrencyISO()));
        return _value;
    }
}
