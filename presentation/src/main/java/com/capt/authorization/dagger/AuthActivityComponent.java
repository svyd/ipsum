package com.capt.authorization.dagger;

import com.capt.authorization.AuthorizationActivity;
import com.capt.authorization.code_verification.dagger.CodeVerifyComponent;
import com.capt.authorization.code_verification.dagger.CodeVerifyModule;
import com.capt.authorization.forgot_password.dagger.ForgotPasswordComponent;
import com.capt.authorization.forgot_password.dagger.ForgotPasswordModule;
import com.capt.authorization.join_fragment.dagger.JoinComponent;
import com.capt.authorization.join_fragment.dagger.JoinModule;
import com.capt.authorization.join_fragment.dagger.StrategyModule;
import com.capt.authorization.country_code.dagger.CountryComponent;
import com.capt.authorization.country_code.dagger.CountryModule;
import com.capt.authorization.location.dagger.LocationComponent;
import com.capt.authorization.location.dagger.LocationModule;
import com.capt.authorization.login_credentials.dagger.LogInComponent;
import com.capt.authorization.login_credentials.dagger.LogInModule;
import com.capt.authorization.phone_number.dagger.PhoneNumberComponent;
import com.capt.authorization.phone_number.dagger.PhoneNumberModule;
import com.capt.authorization.sign_up.dagger.SignUpComponent;
import com.capt.authorization.sign_up.dagger.SignUpModule;
import com.capt.authorization.sign_up.image_picker.dagger.ImagePickerComponent;
import com.capt.authorization.sign_up.image_picker.dagger.ImagePickerModule;
import com.capt.base.dagger.BaseActivityComponent;
import com.capt.data.annotation.PerActivity;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;


import javax.inject.Named;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.03.16..
 */
@PerActivity
@Subcomponent(modules = {AuthActivityModule.class, RestModule.class})
public interface AuthActivityComponent extends BaseActivityComponent, RestComponent {
    void inject(AuthorizationActivity _activity);

    JoinComponent createAuthorizationComponent(JoinModule _authModule,
                                                        StrategyModule _strategyModule);

    PhoneNumberComponent createPhoneNumberComponent(PhoneNumberModule _module);
    SignUpComponent createSignUpComponent(SignUpModule _module);
    CodeVerifyComponent createCodeVerifyComponent(CodeVerifyModule _module);
    LogInComponent createLogInComponent(LogInModule _module);
    ForgotPasswordComponent createForgotPasswordComponent(ForgotPasswordModule _module);
}
