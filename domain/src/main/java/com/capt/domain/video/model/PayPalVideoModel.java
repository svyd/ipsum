package com.capt.domain.video.model;

/**
 * Created by rMozes on 8/2/16.
 */
public class PayPalVideoModel {

    private Video video;
    private String authCode;

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public static class Builder {

        private PayPalVideoModel mModel;

        public Builder() {
            mModel = new PayPalVideoModel();
        }

        public Builder setVideo(Video _video) {
            mModel.setVideo(_video);
            return this;
        }

        public Builder setAuthCode(String _code) {
            mModel.setAuthCode(_code);
            return this;
        }

        public PayPalVideoModel build() {
            return mModel;
        }
    }
}
