package com.capt.application.dagger;

import android.view.LayoutInflater;

import com.capt.application.CaptApplication;
import com.capt.application.UploadReceiver;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.dagger.AuthActivityModule;
import com.capt.authorization.dagger.RestModule;
import com.capt.authorization.sign_up.image_picker.dagger.ImagePickerComponent;
import com.capt.authorization.sign_up.image_picker.dagger.ImagePickerModule;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.payout.PayoutModel;
import com.capt.gcmservice.RegistrationIntentService;
import com.capt.profile.assignments.details.dagger.DetailsActivityComponent;
import com.capt.profile.assignments.details.dagger.DetailsActivityModule;
import com.capt.profile.dagger.ProfileActivityComponent;
import com.capt.profile.dagger.ProfileActivityModule;

import com.capt.profile.edit_profile.dagger.EditProfileActivityComponent;
import com.capt.profile.edit_profile.dagger.EditProfileActivityModule;
import com.capt.profile.gallery.dagger.GalleryComponent;
import com.capt.profile.gallery.dagger.GalleryModule;
import com.capt.profile.marketplace.dagger.MarketPlaceModule;
import com.capt.profile.marketplace.dagger.MarketplaceActivityComponent;
import com.capt.profile.marketplace.dagger.MarketplaceActivityModule;
import com.capt.profile.payout.activity.di.PayoutActivityComponent;
import com.capt.profile.payout.activity.di.PayoutActivityModule;
import com.capt.profile.payout.di.PayoutComponent;
import com.capt.profile.payout.di.PayoutsModule;
import com.capt.profile.settings.dagger.AccountComponent;
import com.capt.profile.settings.dagger.AccountModule;
import com.capt.splash.SplashActivity;
import com.capt.video.camera.activity.dagger.CameraActivityComponent;
import com.capt.video.camera.activity.dagger.CameraActivityModule;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.dagger.VideoActivityModule;
import com.capt.video.trim.dagger.FFMpegComponent;
import com.capt.video.trim.dagger.FFMpegModule;


import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by richi on 2016.02.22..
 */
@Singleton
@Component(modules = {AppModule.class, AppRestModule.class, VideoUploadModule.class})
public interface AppComponent extends AppComponentExposeGraph {
    void inject(SplashActivity _activity);
    void inject(UploadReceiver _receiver);
    void inject(CaptApplication _application);
    void inject(RegistrationIntentService _service);

    AuthActivityComponent createAuthActivityComponent(AuthActivityModule _module, RestModule _rest);
    ProfileActivityComponent createProfileActivityComponent(ProfileActivityModule _module, RestModule _rest);
    CameraActivityComponent createCameraActivityComponent(CameraActivityModule _module);
    VideoActivityComponent createVideoActivityComponent(VideoActivityModule _module, RestModule _restModule);
    ImagePickerComponent createImagePickerComponent(ImagePickerModule _module);
    EditProfileActivityComponent createEditProfileComponent(EditProfileActivityModule _module, RestModule _restModule);
    DetailsActivityComponent createDetailsActivityComponent(DetailsActivityModule _details, RestModule _rest);
    GalleryComponent createGalleryComponent(GalleryModule _module);
    MarketplaceActivityComponent createMarketplaceComponent(MarketplaceActivityModule _module);
    AccountComponent createAccountComponent(AccountModule _module);
    PayoutComponent createPayoutComponent(PayoutsModule _module);
    PayoutActivityComponent createPayoutActivityComponent(PayoutActivityModule _module);

    BaseExceptionDelegate exceptionDelegate();
}
