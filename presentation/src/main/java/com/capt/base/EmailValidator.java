package com.capt.base;

import android.util.Patterns;

import com.capt.data.annotation.PerActivity;

import javax.inject.Inject;

/**
 * Created by richi on 2016.03.21..
 */
@PerActivity
public class EmailValidator {

    @Inject
    public EmailValidator() {}

    public boolean isEmailValid(String _email) {
        return _email != null && Patterns.EMAIL_ADDRESS.matcher(_email).matches() ;
    }
}
