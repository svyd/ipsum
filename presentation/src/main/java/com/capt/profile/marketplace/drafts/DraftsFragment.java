package com.capt.profile.marketplace.drafts;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.profile.marketplace.base.BaseMarketPlaceListFragment;

/**
 * Created by root on 08.04.16.
 */
public class DraftsFragment extends BaseMarketPlaceListFragment implements DraftListener {

    @Override
    protected String getType() {
        return Constants.MarketplaceTypes.DRAFTS;
    }

    @Override
    protected Constants.MarketplaceItems getItemType() {
        return Constants.MarketplaceItems.Drafts;
    }

    @Override
    public void addDraft(Video _draft) {
        rvVideos.setVisibility(View.VISIBLE);
        tvEmptyList.setVisibility(View.GONE);
        mAdapter.add(_draft, mAdapter.getItemCount());
        rvVideos.scrollToPosition(mAdapter.getItemCount() - 1);
        getActivity().setResult(Activity.RESULT_OK);
    }
}
