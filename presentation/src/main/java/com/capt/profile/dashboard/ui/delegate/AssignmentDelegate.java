package com.capt.profile.dashboard.ui.delegate;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.profile.dashboard.ui.adapter.ViewTypeConstants;
import com.capt.profile.dashboard.ui.delegate.base.BaseSimpleDelegate;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 26.06.2016.
 */
@SuppressWarnings("ALL")
public class AssignmentDelegate extends BaseSimpleDelegate<Assignment> {

    @Inject
    public AssignmentDelegate(LayoutInflater _layoutInflater) {
        super(_layoutInflater, ViewTypeConstants.VIEW_TYPE_ASSIGNMENTS, Assignment.class);
    }

    @Override
    protected int getType() {
        return ViewTypeConstants.VIEW_TYPE_ASSIGNMENTS;
    }

    @Override
    public void onBindViewHolder(@NonNull List<DisplayableItem> items, int position, @NonNull RecyclerView.ViewHolder holder) {
        SimpleViewHolder simpleHolder = (SimpleViewHolder) holder;
        Assignment assignment = (Assignment) items.get(position);
        if (assignment.stub) {
            simpleHolder.tvLeft.setText("No active assignments. Tap here to check for new opportunities.");
        } else {
            simpleHolder.tvLeft.setText(assignment.creator.company);
            simpleHolder.tvRight.setText(assignment.when);
        }
    }
}
