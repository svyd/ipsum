package com.capt.video.category;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.global.Constants;
import com.capt.video.category.dagger.CategoryComponent;
import com.capt.video.category.dagger.CategoryModule;
import com.capt.video.category.flow.AbstractCategoryPresenter;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.overview.OverviewFragment;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.Set;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by root on 07.04.16.
 */
public class CategoryFragment extends BaseToolbarFragment implements CategoryContract.View {

    @Bind(R.id.flow_layout_FC)
    TagFlowLayout mFlowLayout;

    @Inject
    AbstractCategoryPresenter mPresenter;

    @Inject
    ToolbarManager mToolbar;

    @Inject
    LayoutInflater mInflater;

    private CategoryComponent mComponent;
    private TagAdapter<String> mAdapter;

    public static CategoryFragment newInstance(Bundle _data) {

        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(_data);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_category);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        showToolbar();
        checkExtra();
        mPresenter.initialize();
    }

    private void checkExtra() {
        mPresenter.setData(getArguments());
    }


    private void initComponent() {
        if (mComponent != null)
            return;

        if (getArguments().containsKey(Constants.FLOW)) {
            int flow = getArguments().getInt(Constants.FLOW);

            mComponent = getComponent(VideoActivityComponent.class)
                    .createCategoryComponent(new CategoryModule(this, flow));
        } else {
            throw new IllegalStateException(CategoryFragment.class.getSimpleName()
                    + " should contain flow");
        }
    }

    private void inject() {
        if (mToolbar != null)
            return;

        mComponent.inject(this);
    }

    private void showToolbar() {
        mToolbar.showToolbar();
        getActivity().setTitle(getString(R.string.category_title));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
    }

    @Override
    public void selectCategories(int[] positions) {
        mAdapter.setSelectedList(positions);
    }

    @Override
    public void navigateBack() {
        if (getActivity() != null) {
            getActivity().setResult(Activity.RESULT_OK, getActivity().getIntent());
            getActivity().finish();
        }
    }

    @Override
    public void navigateToVideoViewScreen(Bundle _args) {
        getFragmentNavigator()
                .replaceFragmentWithBackStack(OverviewFragment.newInstance(_args));
    }

    @Override
    public void setData(String[] _data) {
        mAdapter = new TagAdapter<String>(_data) {
            @Override
            public View getView(FlowLayout parent, int position, String title) {
                TextView tv = (TextView) mInflater.inflate(R.layout.item_category,
                        mFlowLayout, false);
                tv.setText(title);
                return tv;
            }
        };

        mFlowLayout.setAdapter(mAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void showToast(String _text) {
        Toast.makeText(getActivity(), _text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Set<Integer> getSelectedList() {
        return mFlowLayout.getSelectedList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;

            case R.id.action_next:
                mPresenter.onNextClick();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
