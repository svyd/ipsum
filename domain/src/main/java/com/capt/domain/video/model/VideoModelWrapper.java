package com.capt.domain.video.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Svyd on 20.04.2016.
 */
public class VideoModelWrapper {

    @Expose
    @SerializedName("data")
    public List<Video> videos;

    @Expose
    @SerializedName("total")
    public int count;

}
