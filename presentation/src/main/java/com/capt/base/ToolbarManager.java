package com.capt.base;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.capt.data.annotation.PerActivity;

import javax.inject.Inject;

/**
 * Created by richi on 2016.03.20..
 */
@PerActivity
public class ToolbarManager {

    private Toolbar mToolbar;

    @Inject
    public ToolbarManager(BaseActivity _activity) {
        mToolbar = (Toolbar) _activity.findViewById(_activity.getToolbarId());
    }

    public void hideToolbar() {
        mToolbar.setVisibility(View.GONE);
    }

    public void showToolbar() {
        mToolbar.setVisibility(View.VISIBLE);
    }
}
