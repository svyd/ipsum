//package com.capt.authorization.test;
//
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.widget.Toast;
//
//import com.capt.R;
//import com.capt.application.CaptApplication;
//import com.capt.authorization.AuthorizationActivity;
//import com.capt.authorization.dagger.AuthActivityComponent;
//import com.capt.authorization.dagger.AuthActivityModule;
//import com.capt.authorization.dagger.RestModule;
//import com.capt.base.BaseActivity;
//import com.capt.data.authorization.net.ApiConstants;
//import com.capt.domain.authorization.interactor.SignOutInteractor;
//import com.capt.domain.authorization.model.SuccessModel;
//import com.facebook.login.LoginManager;
//
//import javax.inject.Inject;
//
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import rx.Observer;
//import rx.observers.TestSubscriber;
//
///**
// * Created by richi on 2016.03.22..
// */
//public class ProfileActivity extends BaseActivity {
//
//    @Inject SignOutInteractor mInteractor;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_profile);
//
//        AuthActivityComponent component = CaptApplication.getApplication()
//                .getAppComponent()
//                .createAuthActivityComponent(new AuthActivityModule(this), new RestModule(ApiConstants.API_ENDPOINT));
//        component.inject(this);
//
//        ButterKnife.bind(this);
//    }
//
////    @OnClick(R.id.btnLogOut)
////    protected void onLogOut() {
////        mInteractor.execute(new Observer<SuccessModel>() {
////            @Override
////            public void onCompleted() {
////
////            }
////
////            @Override
////            public void onError(Throwable e) {
////                Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT)
////                        .show();
////            }
////
////            @Override
////            public void onNext(SuccessModel o) {
////                SharedPreferences preferences = CaptApplication.getApplication().getAppComponent().sharedPreferences();
////                preferences.edit().clear().apply();
////                LoginManager.getInstance().logOut();
////                startActivity(new Intent(ProfileActivity.this, AuthorizationActivity.class));
////                finishWithResult();
////            }
////        });
////    }
//
//    @Override
//    public int getContainerId() {
//        return 0;
//    }
//
//    @Override
//    public int getToolbarId() {
//        return 0;
//    }
//}
