package com.capt.domain.authorization.country_code;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by richi on 2016.03.16..
 */
public class CountryCodeInteractor extends BaseInteractor {

    private CountryRepository mRepository;

    @Inject
    public CountryCodeInteractor(CountryRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildGetObserver() {
        return mRepository.getCountryModels();
    }
}
