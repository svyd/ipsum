package com.capt.profile.assignments.details.capt_dialog.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.assignments.details.capt_dialog.CaptDialogContract;
import com.capt.profile.assignments.details.capt_dialog.CaptDialogPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 22.04.16.
 */
@Module
public class CaptDialogModule {

	private CaptDialogContract.View mView;

	public CaptDialogModule(CaptDialogContract.View _view) {
		mView = _view;
	}

	@Provides @PerFragment
	protected CaptDialogContract.View provideView() {
		return mView;
	}

	@Provides @PerFragment
	protected CaptDialogContract.Presenter providePresenter(CaptDialogPresenter _presenter) {
		return _presenter;
	}
}
