package com.capt.data.authorization.net.retrofit;

import com.capt.domain.authorization.model.CodeModel;
import com.capt.domain.authorization.model.PasswordModel;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.PushToken;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UpdateAvatarResponse;
import com.capt.domain.authorization.model.UserModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import rx.Observer;

/**
 * Created by Svyd on 04.03.2016.
 */
public interface AuthService {

    @POST("/mobile/user/auth/{network}")
    Observable<UserModel> signInWithSocNet(@Path("network") String _network, @Body SocNetAuthModel _body);

    @POST("/mobile/user/sendAuthenticationCode")
    Observable<SuccessModel> getAuthenticationCode(@Body PhoneNumberModel _body);

    @POST("/mobile/user/signIn")
    Observable<UserModel> signIn(@Body SignInModel _model);

    @POST("/mobile/user/checkAuthenticationCode")
    Observable<SuccessModel> verifyCode(@Body CodeModel _body);

    @Multipart
    @POST("/mobile/user/signUp")
    Observable<SuccessModel> signUp(@Part("avatar\"; filename=\"image.jpg") RequestBody _file,
                                    @Part("email") RequestBody _email,
                                    @Part("password") RequestBody _password,
                                    @Part("fullName") RequestBody _fullName,
                                    @Part("deviceToken") RequestBody _deviceToken,
                                    @Part("location") RequestBody _location,
                                    @Part("provider") RequestBody _provider,
                                    @Part("deviceId") RequestBody _deviceId);

    @GET("mobile/user/profile")
    Observable<UserModel> getProfile();

    @GET("mobile/user/signOut")
    Observable<SuccessModel> signOut();

    @GET("mobile/user/check")
    Observable<SuccessModel> checkEmail(@Query("email") String _email);

    @PUT("mobile/user/device")
    Observable<SuccessModel> updatePushToken(@Body PushToken token);

    @POST("mobile/user/changePassword")
    Observable<SuccessModel> changePassword(@Body PasswordModel _password);

    @Multipart
    @PUT("/mobile/user/profile")
    Observable<UserModel> updateUser(@Part("avatar\"; filename=\"image.jpg") RequestBody _file,
                                     @Part("email") RequestBody _email,
                                     @Part("fullName") RequestBody _fullName,
                                     @Part("location") RequestBody _location,
                                     @Part("country") RequestBody _country,
                                     @Part("city") RequestBody _city,
                                     @Part("phone") RequestBody _phone);
}
