package com.capt.video.camera.custom_view;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Svyd on 13.04.2016.
 */

@SuppressWarnings("deprecation")
public class CaptCameraView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "CaptCameraView";

    private static final String CAMERA_THREAD_NAME = CaptCameraView.class.getSimpleName() + " THREAD";

    private boolean mIsAudioAllow = true;

    private HandlerThread mCameraThread;
    private Handler mCameraHandler;
    private Handler mUIHandler;

    private Camera mCamera;
    private List<Camera.Size> mSupportSize = new ArrayList<>();
    private Camera.Size mCurrentVideoSize;

    private CameraListener mListener;
    private MediaRecorder mMediaRecorder;
    private RectF mPreviewSize;

    private boolean mIsFocusing = false;

    private boolean mIsPrepared = false;

    private Matrix mMatrix;

    private final int FOCUS_AREA_SIZE = 50;

    public CaptCameraView(Context context) {
        this(context, null);
    }

    public CaptCameraView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CaptCameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mMatrix = new Matrix();
    }

    private void initHandlers() {
        mCameraThread = new HandlerThread(CAMERA_THREAD_NAME);
        mCameraThread.setPriority(Thread.NORM_PRIORITY);
        mCameraThread.start();

        mCameraHandler = new Handler(mCameraThread.getLooper());
        mUIHandler = new Handler();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        getHolder().addCallback(this);
        mIsFocusing = false;
    }

    @Override
    protected void onDetachedFromWindow() {
        getHolder().removeCallback(this);
        super.onDetachedFromWindow();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        initHandlers();
        mCameraHandler.post(() -> {
            try {
                mCamera = Camera.open();
                mCamera.setDisplayOrientation(getCorrectCameraOrientation());
            } catch (RuntimeException e) {
                e.printStackTrace();
                stopAndRelease();
            }
        });
    }

    public int getCorrectCameraOrientation() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, cameraInfo);
        WindowManager windowManager = (WindowManager) getContext()
                .getSystemService(Context.WINDOW_SERVICE);
        int rotation = windowManager.getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;

            case Surface.ROTATION_90:
                degrees = 90;
                break;

            case Surface.ROTATION_180:
                degrees = 180;
                break;

            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (cameraInfo.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else {
            result = (cameraInfo.orientation - degrees + 360) % 360;
        }

        return result;
    }

    private void setPreviewSize(int _width, int _height) {
        Camera.Size previewSize = mCamera.getParameters().getPreviewSize();
        if (previewSize.width == _width && previewSize.height == _height)
            return;

        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setPreviewSize(_width, _height);
        mCamera.setParameters(parameters);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, final int format, int width, int height) {
        mCameraHandler.post(() -> {
            try {
                if (mCamera == null) {
                    return;
                }

                if (mPreviewSize != null && ((int) mPreviewSize.width()) == width
                        && ((int) mPreviewSize.height()) == height) {
                    setPreviewSize(mCurrentVideoSize.width, mCurrentVideoSize.height);
                    startPreview();
                    return;
                }

                Log.d(TAG, "surfaceChanged: " + width + "x" + height);
                initMatrix(width, height);
                sortVideoSizesAndSetMaxSize();
                startPreview();
                checkFlash();
                Log.d(TAG, "surfaceChanged: ViewSize: " + getWidth() + "x" + getHeight());
            } catch (Exception _e) {
                _e.printStackTrace();
            }
        });
    }

    public boolean isAlive() {
        return mIsPrepared;
    }

    private void initMatrix(int _width, int _height) {
        Matrix matrix = new Matrix();
        matrix.postScale(_width / 2000f, _height / 2000f);
        matrix.postTranslate(_width / 2f, _height / 2f);
        matrix.invert(mMatrix);
    }

    private void sortVideoSizesAndSetMaxSize() {
        if (mCurrentVideoSize != null) {
            setUpPreviewSize(false);
            return;
        }

        if (!mSupportSize.isEmpty())
            return;

        List<Camera.Size> supportSize = mCamera.getParameters().getSupportedVideoSizes();
        if (supportSize == null)
            supportSize = mCamera.getParameters().getSupportedPreviewSizes();

        Collections.sort(supportSize, mCameraComparator);

        for (Camera.Size size : supportSize) {
            float ratio = (float) size.width / size.height;
            if (ratio > 1.5 && size.height % 10 == 0)
                mSupportSize.add(size);
        }

        if (mSupportSize == null || mSupportSize.size() == 0)
            mSupportSize = supportSize;

        if (mListener != null)
            mUIHandler.post(() -> mListener.onPreviewSizeList(mSupportSize));
        setVideoSize(mSupportSize.get(0));
    }

    private void setUpPreviewSize(boolean fullScreen) {
        WindowManager windowManager = ((WindowManager) getContext()
                .getSystemService(Context.WINDOW_SERVICE));
        Display display = windowManager.getDefaultDisplay();
        Point screenSize = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            display.getRealSize(screenSize);
        else
            display.getSize(screenSize);

        boolean widthIsMax = screenSize.x > screenSize.y;

        Camera.Size size = mCurrentVideoSize;
        Log.d(TAG, "setUpPreviewSize: " + size.width + "x" + size.height);
        setPreviewSize(size.width, size.height);

        RectF rectDisplay = new RectF();
        RectF rectPreview = new RectF();

        rectDisplay.set(0, 0, screenSize.x, screenSize.y);

        if (widthIsMax) {
            rectPreview.set(0, 0, size.width, size.height);
        } else {
            rectPreview.set(0, 0, size.height, size.width);
        }

        Matrix matrix = new Matrix();
        if (!fullScreen) {
            matrix.setRectToRect(rectPreview, rectDisplay,
                    Matrix.ScaleToFit.START);
        } else {
            matrix.setRectToRect(rectDisplay, rectPreview,
                    Matrix.ScaleToFit.START);
            matrix.invert(matrix);
        }

        matrix.mapRect(rectPreview);
        if (rectPreview.equals(mPreviewSize))
            return;

        mPreviewSize = rectPreview;
        Log.d(TAG, "setUpPreviewSize: " + rectPreview);

        mUIHandler.post(this::requestLayout);
    }

    private void startPreview() {
        try {
            mCamera.stopPreview();
        } catch (Exception _e) {
            _e.printStackTrace();
        }

        try {
            mCamera.setPreviewDisplay(getHolder());
            mCamera.startPreview();
            if (!isAlive())
                mCamera.setOneShotPreviewCallback((data, camera) -> {
                    mIsPrepared = true;
                    if (mListener != null) {
                        mListener.onCameraPrepared();
                    }
                });

        } catch (IOException e) {
            e.printStackTrace();
            releaseCamera();
        }
    }

    private void checkFlash() {
        Camera.Parameters parameters = mCamera.getParameters();
        if (mListener != null)
            mUIHandler.post(() -> mListener.onFlashAvailable(parameters.getFlashMode() != null));
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopAndRelease();
    }

    private void stopAndRelease() {
        mIsFocusing = false;
        mIsPrepared = false;
        stop();
    }

    private void stop() {
        stopRecord();
        releaseMediaRecorder();
        releaseCamera();
    }

    private void releaseCamera() {
        if (mCameraHandler != null) {
            mCameraHandler.post(() -> {
                if (mCamera != null) {
                    mCamera.release();
                    mCamera = null;
                }

                quitThread();
                releaseHandlers();
            });
        }
    }

    private void quitThread() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mCameraThread.quitSafely();
        } else {
            mCameraThread.quit();
        }
    }

    private void releaseHandlers() {
        mCameraHandler = null;
        mCameraThread = null;
    }

    public void setAudioAllow(boolean _isAudioAllow) {
        mIsAudioAllow = _isAudioAllow;
    }

    public void recordVideo(final String _path) {
        mCameraHandler.post(() -> startRecord(_path));
    }

    private void startRecord(String _path) {
        try {
            mMediaRecorder = new MediaRecorder();

            mCamera.stopPreview();
            mMediaRecorder.setPreviewDisplay(getHolder().getSurface());

            mCamera.unlock();

            mMediaRecorder.setCamera(mCamera);

            if (mIsAudioAllow)
                mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

            CamcorderProfile camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);

            mMediaRecorder.setOutputFormat(camcorderProfile.fileFormat);
            mMediaRecorder.setVideoEncoder(camcorderProfile.videoCodec);
            mMediaRecorder.setVideoEncodingBitRate(camcorderProfile.videoBitRate);
            mMediaRecorder.setVideoFrameRate(camcorderProfile.videoFrameRate);
            mMediaRecorder.setVideoSize(mCurrentVideoSize.width, mCurrentVideoSize.height);

            if (mIsAudioAllow) {
                mMediaRecorder.setAudioEncodingBitRate(camcorderProfile.audioBitRate);
                mMediaRecorder.setAudioChannels(camcorderProfile.audioChannels);
                mMediaRecorder.setAudioSamplingRate(camcorderProfile.audioSampleRate);
                mMediaRecorder.setAudioEncoder(camcorderProfile.audioCodec);
            }

            mMediaRecorder.setOutputFile(_path);

            mMediaRecorder.prepare();
            mMediaRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
            releaseMediaRecorder();
        }
    }

    public void stopRecord() {
        if (mMediaRecorder != null) {
            try {
                mMediaRecorder.stop();
            } catch (RuntimeException e) {
                e.printStackTrace();
            } finally {
                releaseMediaRecorder();
            }
        }
//        if (mCameraHandler != null) {
//            mCameraHandler.post(() -> {
//                if (mMediaRecorder != null) {
//                    try {
//                        mMediaRecorder.stop();
//                    } catch (RuntimeException e) {
//                        e.printStackTrace();
//                    } finally {
//                        releaseMediaRecorder();
//                    }
//                }
//            });
//        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mPreviewSize == null)
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        else {
            setMeasuredDimension(((int) mPreviewSize.width()), ((int) mPreviewSize.height()));
        }
    }

    private void releaseMediaRecorder() {
        if (mCameraHandler != null)
            mCameraHandler.post(() -> {
                if (mMediaRecorder != null) {
                    mMediaRecorder.reset();
                    mMediaRecorder.release();
                    mMediaRecorder = null;
                }
                if (mCamera != null) {
                    mCamera.lock();
                }
            });
    }

    public void setFlashMode(String _flashMode) {
        if (mCamera == null)
            return;

        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setFlashMode(_flashMode);
        mCamera.setParameters(parameters);
    }

    public void setVideoSize(final Camera.Size _size) {
        if (!mSupportSize.contains(_size))
            throw new IllegalArgumentException("Unsupported camera size");

        try {
            mCamera.stopPreview();
            mCurrentVideoSize = _size;
            setPreviewSize(mCurrentVideoSize.width, mCurrentVideoSize.height);
            setUpPreviewSize(false);
            mCamera.startPreview();
        } catch (Exception _e) {
            _e.printStackTrace();
        }
    }

    public void setCameraSizeListener(CameraListener _listener) {
        mListener = _listener;
    }

    private Comparator<Camera.Size> mCameraComparator = (lhs, rhs) -> {
        if (lhs.width > rhs.width)
            return -1;
        else if (lhs.width < rhs.width)
            return 1;
        else
            return 0;
    };

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            focusOnTouch(event);
            return true;
        } catch (Exception _e) {
            _e.printStackTrace();
            return false;
        }
    }

    protected void focusOnTouch(MotionEvent event) {
        if (mCamera != null && !mIsFocusing) {
            mIsFocusing = true;
            mCamera.cancelAutoFocus();
            Rect focusRect = calculateTapArea(event.getX(), event.getY(), 1f);
            Rect meteringRect = calculateTapArea(event.getX(), event.getY(), 1.5f);

            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            List<Camera.Area> areas = new ArrayList<>();
            areas.add(new Camera.Area(focusRect, 1000));
            parameters.setFocusAreas(areas);

            if (meteringAreaSupported()) {
                parameters.setMeteringAreas(Collections.singletonList(new Camera.Area(meteringRect, 1000)));
            }

            mCamera.setParameters(parameters);
            mCamera.autoFocus((success, camera) -> mIsFocusing = false);
        }
    }

    private Rect calculateTapArea(float x, float y, float coefficient) {
        int areaSize = Float.valueOf(getPixel(FOCUS_AREA_SIZE) * coefficient).intValue();

        int left = clamp((int) x - areaSize / 2, 0, getWidth() - areaSize);
        int top = clamp((int) y - areaSize / 2, 0, getHeight() - areaSize);

        RectF rectF = new RectF(left, top, left + areaSize, top + areaSize);
        mMatrix.mapRect(rectF);

        return new Rect(Math.round(rectF.left), Math.round(rectF.top), Math.round(rectF.right), Math.round(rectF.bottom));
    }

    private float getPixel(float _dimen) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                _dimen, getResources().getDisplayMetrics());
    }

    private int clamp(int x, int min, int max) {
        if (x > max) {
            return max;
        }
        if (x < min) {
            return min;
        }
        return x;
    }

    private boolean meteringAreaSupported() {
        return mCamera.getParameters().getMaxNumMeteringAreas() > 0;
    }

    public interface CameraListener {

        void onPreviewSizeList(List<Camera.Size> _sizeList);

        void onFlashAvailable(boolean _available);

        void onCameraPrepared();
    }
}