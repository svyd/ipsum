package com.capt.profile.dashboard.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.domain.global.Constants;
import com.capt.profile.dashboard.ui.adapter.DashboardAdapter;
import com.capt.profile.dashboard.ui.adapter.DashboardDelegateManager;
import com.capt.profile.dashboard.ui.delegate.AssignmentDelegate;
import com.capt.profile.dashboard.ui.delegate.AssignmentsNonRecycleDelegate;
import com.capt.profile.dashboard.ui.delegate.MarketplaceDelegate;
import com.capt.profile.dashboard.ui.delegate.MarketplaceNonRecycleDelegate;
import com.capt.profile.dashboard.ui.delegate.ProgressDelegate;
import com.capt.profile.dashboard.ui.delegate.TutorialNonRecycleDelegate;
import com.capt.profile.dashboard.ui.delegate.ReminderDelegate;
import com.capt.profile.dashboard.ui.delegate.VideoDelegate;
import com.capt.profile.dashboard.ui.delegate.base.BaseDelegate;
import com.hannesdorfmann.adapterdelegates.AdapterDelegatesManager;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 28.03.2016.
 */
@Module
public class DashboardListModule {

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Delegate.ASSIGNMENTS_DELEGATE)
    BaseDelegate provideAssignmentsDelegate(AssignmentDelegate _delegate) {
        return _delegate;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Delegate.MARKETPLACE_DELEGATE)
    BaseDelegate provideMarketplaceDelegate(MarketplaceDelegate _delegate) {
        return _delegate;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Delegate.REMINDER_DELEGATE)
    BaseDelegate provideReminderDelegate(ReminderDelegate _delegate) {
        return _delegate;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Delegate.PROGRESS_DELEGATE)
    BaseDelegate provideProgressDelegate(ProgressDelegate _delegate) {
        return _delegate;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Delegate.VIDEO_DELEGATE)
    BaseDelegate provideVideoDelegate(VideoDelegate _delegate) {
        return _delegate;
    }

    @Provides
    @PerFragment
    AdapterDelegatesManager provideDelegateManager(DashboardDelegateManager _manager) {
        return _manager;
    }

    //needs to be provided explicitly due to generics
    @Provides
    @PerFragment
    DashboardAdapter provideDashboardAdapter(DashboardDelegateManager _manager) {
        return new DashboardAdapter(_manager);
    }
}
