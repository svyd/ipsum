package com.capt.domain.profile.assignment;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 06.05.2016.
 */
public class DeleteRejectedAssignmentInteractor extends BasePostInteractor<String> {

    AssignmentsRepository mRepository;

    @Inject
    public DeleteRejectedAssignmentInteractor(AssignmentsRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.delete(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
