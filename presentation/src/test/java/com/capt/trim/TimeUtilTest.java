//package com.capt.trim;
//
//import com.capt.BuildConfig;
//import com.capt.TestCaptApplication;
//import com.capt.video.trim.dagger.DaggerFFMpegComponent;
//import com.capt.video.trim.dagger.FFMpegComponent;
//import com.capt.video.trim.time_util.TimeUtil;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.robolectric.RobolectricGradleTestRunner;
//import org.robolectric.RuntimeEnvironment;
//import org.robolectric.annotation.Config;
//
//import static org.junit.Assert.*;
//
///**
// * Created by richi on 2016.02.29..
// */
//@RunWith(RobolectricGradleTestRunner.class)
//@Config(constants = BuildConfig.class,
//        application = TestCaptApplication.class)
//public class TimeUtilTest {
//
//    private static final String sSecondFiled = "Seconds test is failed";
//    private static final String sMinuteFailed = "Minute test is failed";
//    private static final String sHoursFailed = "Hours test is failed";
//
//    protected TimeUtil mTimeUtil;
//
//    @Before
//    public void setUp() {
//        TestCaptApplication app = (TestCaptApplication) RuntimeEnvironment.application;
//
//        FFMpegComponent component = DaggerFFMpegComponent.builder()
//                .appComponent(app.getAppComponent())
//                .build();
//
//        mTimeUtil = component.timeUtil();
//    }
//
//    @Test
//    public void testTimeFromInt() {
//        assertEquals(sSecondFiled, "00:00:50", mTimeUtil.getTimeFromInteger(50));
//        assertEquals(sSecondFiled, "00:00:27", mTimeUtil.getTimeFromInteger(27));
//        assertEquals(sSecondFiled, "00:00:38", mTimeUtil.getTimeFromInteger(38));
//
//        assertEquals(sMinuteFailed, "00:01:27", mTimeUtil.getTimeFromInteger(87));
//        assertEquals(sMinuteFailed, "00:10:36", mTimeUtil.getTimeFromInteger(636));
//        assertEquals(sMinuteFailed, "00:20:58", mTimeUtil.getTimeFromInteger(1258));
//
//        assertEquals(sHoursFailed, "01:01:58", mTimeUtil.getTimeFromInteger(3718));
//        assertEquals(sHoursFailed, "10:01:58", mTimeUtil.getTimeFromInteger(36118));
//        assertEquals(sHoursFailed, "26:01:58", mTimeUtil.getTimeFromInteger(93718));
//    }
//
//    @Test
//    public void testTimeFromPercentage() {
//        assertEquals("Time from percentage is failed", 7, mTimeUtil.getTimeFromPercentage(7, 100));
//        assertEquals("Time from percentage is failed", 14, mTimeUtil.getTimeFromPercentage(7, 200));
//        assertEquals("Time from percentage is failed", 1, mTimeUtil.getTimeFromPercentage(2, 50));
//    }
//}
