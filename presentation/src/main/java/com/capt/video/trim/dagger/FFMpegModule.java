package com.capt.video.trim.dagger;

import android.media.MediaMetadataRetriever;

import com.capt.data.annotation.PerFragment;
import com.capt.video.trim.ffmpeg_manager.FFMPEGManagerImpl;
import com.capt.video.trim.ffmpeg_command_builder.CommandBuilderImpl;
import com.capt.video.trim.ffmpeg_command_builder.FFMPEGCommandBuilder;
import com.capt.video.trim.ffmpeg_manager.FFMPEGManager;
import com.netcompss.loader.LoadJNI;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.02.29..
 */
@Module
public class FFMpegModule {

    public FFMpegModule() {}

    //TODO: PerActivity
    @Provides @PerFragment
    protected LoadJNI provideLoadJNI() {
        return new LoadJNI();
    }

    @Provides @PerFragment
    protected FFMPEGManager provideFFMpegManager(FFMPEGManagerImpl _manager) {
        return _manager;
    }

    @Provides @PerFragment
    protected FFMPEGCommandBuilder provideCommandBuilder(CommandBuilderImpl _builder) {
        return _builder;
    }

    @Provides
    protected MediaMetadataRetriever provideDataRetriever() {
        return new MediaMetadataRetriever();
    }

}
