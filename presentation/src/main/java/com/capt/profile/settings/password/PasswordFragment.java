package com.capt.profile.settings.password;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.profile.edit_profile.dagger.EditProfileActivityComponent;
import com.capt.profile.settings.password.dagger.PasswordComponent;
import com.capt.profile.settings.password.dagger.PasswordModule;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 23.06.2016.
 */
public class PasswordFragment extends BaseToolbarFragment implements PasswordContract.View {

    private PasswordComponent mComponent;
    private ProgressDialog mProgress;

    @Bind(R.id.etCurrentPassword_FCP)
    protected EditText etCurrentPassword;

    @Bind(R.id.etNewPassword_FCP)
    protected EditText etNewPassword;

    @Bind(R.id.etConfirmPassword_FCP)
    protected EditText etConfirmPassword;

    @Bind(R.id.ivCurrentEye_FCP)
    protected ImageView ivCurrent;

    @Bind(R.id.ivNewEye_FCP)
    protected ImageView ivNew;

    @Bind(R.id.ivEyeConfirm_FCP)
    protected ImageView ivConfirm;

    @Bind(R.id.btnChangePassword_FCP)
    Button btnChange;

    @Inject
    protected PasswordContract.Presenter mPresenter;

    public static PasswordFragment newInstance() {

        Bundle args = new Bundle();

        PasswordFragment fragment = new PasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_password);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        initProgress();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    private void initProgress() {
        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle("Loading");
        mProgress.setMessage("Please wait");
        mProgress.setCancelable(false);
    }

    private void initComponent() {
        mComponent = getComponent(EditProfileActivityComponent.class)
                .createPasswordComponent(new PasswordModule(this));
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @OnClick(R.id.ivCurrentEye_FCP)
    void onCurrentEyeClick() {
        mPresenter.onCurrentEyeClick();
    }

    @OnClick(R.id.ivNewEye_FCP)
    void onNewEyeClick() {
        mPresenter.onNewEyeClick();
    }

    @OnClick(R.id.ivEyeConfirm_FCP)
    void onConfirmEyeClick() {
        mPresenter.onConfirmEyeClick();
    }

    @OnClick(R.id.btnChangePassword_FCP)
    void onChangeClick() {
        mPresenter.onChangeClick();
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public void showProgress() {
        mProgress.show();
    }

    @Override
    public void hideProgress() {
        mProgress.cancel();
    }

    @Override
    public void hideCurrentPassword() {
        etCurrentPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        ivCurrent.setImageResource(R.drawable.ic_eye);
    }

    @Override
    public void showCurrentPassword() {
        etCurrentPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        ivCurrent.setImageResource(R.drawable.ic_eye2);
    }

    @Override
    public void hideNewPassword() {
        etNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        ivNew.setImageResource(R.drawable.ic_eye);
    }

    @Override
    public void showNewPassword() {
        etNewPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        ivNew.setImageResource(R.drawable.ic_eye2);
    }

    @Override
    public void hideConfirmPassword() {
        etConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        ivConfirm.setImageResource(R.drawable.ic_eye);
    }

    @Override
    public void showConfirmPassword() {
        etConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        ivConfirm.setImageResource(R.drawable.ic_eye2);
    }

    @Override
    public void showToast(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getCurrentPassword() {
        return etCurrentPassword.getText().toString();
    }

    @Override
    public String getNewPassword() {
        return etNewPassword.getText().toString();
    }

    @Override
    public String getConfirmPassword() {
        return etConfirmPassword.getText().toString();
    }

    @Override
    public void navigateBack() {
        getActivity().finish();
    }
}
