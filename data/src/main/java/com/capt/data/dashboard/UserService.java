package com.capt.data.dashboard;

import com.capt.domain.authorization.model.UpdateAvatarResponse;
import com.capt.domain.authorization.model.UserModel;

import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import rx.Observable;

/**
 * Created by Svyd on 4/22/16.
 */
public interface UserService {
    @Multipart
    @PUT("/mobile/user/avatar")
    Observable<UpdateAvatarResponse> updateAvatar(@Part("avatar\"; filename=\"image.jpg") RequestBody _file);

    @GET("mobile/user/profile")
    Observable<UserModel> getUser();

}
