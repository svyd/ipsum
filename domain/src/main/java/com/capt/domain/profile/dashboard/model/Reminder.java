package com.capt.domain.profile.dashboard.model;

import com.capt.domain.profile.dashboard.model.base.DisplayableItem;

import java.util.List;

/**
 * Created by Svyd on 10.05.2016.
 */
public class Reminder implements DisplayableItem {

    private String mReminder;

    private int mId;

    public Reminder(String _reminder, int _id) {
        mReminder = _reminder;
        mId = _id;
    }

    public int getId() {
        return mId;
    }

    public String getReminder() {
        return mReminder;
    }

    @Override
    public String getHeaderTitle() {
        return "Reminder";
    }

    @Override
    public String getHeaderDescription() {
        return "";
    }
}