package com.capt.profile.dashboard.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.dashboard.DashboardFragment;

import dagger.Subcomponent;

/**
 * Created by Svyd on 28.03.2016.
 */
@PerFragment
@Subcomponent(modules = {DashboardModule.class, DashboardListModule.class})
public interface DashboardComponent {
    void inject(DashboardFragment _fragment);
}
