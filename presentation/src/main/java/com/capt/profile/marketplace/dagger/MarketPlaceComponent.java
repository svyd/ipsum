package com.capt.profile.marketplace.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.marketplace.MarketPlaceFragment;

import dagger.Subcomponent;

/**
 * Created by root on 08.04.16.
 */
@PerFragment
@Subcomponent(modules = MarketPlaceModule.class)
public interface MarketPlaceComponent {
	void inject(MarketPlaceFragment _fragment);
}
