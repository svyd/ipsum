package com.capt.data.authorization.repository.country.data_store;

import com.capt.data.authorization.repository.country.cache.CountryCache;
import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

import rx.Observable;

/**
 * Created by richi on 2016.03.16..
 */
public class MemoryCountryDataStore implements CountryDataStore {

    private CountryCache mCache;

    public MemoryCountryDataStore(CountryCache _cache) {
        mCache = _cache;
    }

    @Override
    public Observable<List<CountryModel>> getCountryModel() {
        return Observable.just(mCache.get());
    }
}
