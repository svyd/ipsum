package com.capt.domain.video.draft;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.model.Video;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 26.05.2016.
 */
public class UpdatePendingInteractor extends BasePostInteractor<Video> {

    private DraftRepository mRepository;

    @Inject
    public UpdatePendingInteractor(DraftRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(Video _data) {
        return mRepository.updatePending(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
