package com.capt.data.video.upload.cache;

import android.content.SharedPreferences;

import com.capt.data.base.BaseDAO;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.upload.PendingUploadCache;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by richi on 2016.03.14..
 */
@Singleton
public class PendingUploadCacheImpl implements PendingUploadCache {

    private static final String LIST_STRING = "pending_video_list_string";

    private SharedPreferences mSharedPreferences;
    private Gson mGson;

    @Inject
    public PendingUploadCacheImpl(SharedPreferences _sharedPreferences, Gson _gson) {
        mSharedPreferences = _sharedPreferences;
        mGson = _gson;
    }

    @Override
    public Observable<String> cachePendingVideo(Video _video) {
        return getPendingUploads()
                .flatMap(listId ->
                        Observable.create(subscriber -> {

                            listId.add(_video);
                            saveToCache(listId);

                            subscriber.onNext(_video.id);
                            subscriber.onCompleted();
                        }));
    }

    @Override
    public Observable<String> cancelPendingUpload(String _id) {
        return complete(_id, false);
    }

    @Override
    public Observable<String> completePendingUpload(String _id) {
        return complete(_id, true);
    }

    @SuppressWarnings({"ResultOfMethodCallIgnored", "Convert2streamapi"})
    private Observable<String> complete(String _id, boolean _delete) {
        return getPendingUploads()
                .flatMap(videos ->
                        Observable.create(subscriber -> {
                            for (Video video: videos) {
                                if (video.id.equals(_id)) {
                                    videos.remove(video);
                                    if (_delete) {
                                        File file = new File(video.path);
                                        file.delete();
                                    }
                                    break;
                                }
                            }
                            saveToCache(videos);

                            subscriber.onNext(_id);
                            subscriber.onCompleted();
                        }));
    }

    private void saveToCache(List<Video> _listId) {


        String listString = mGson.toJson(_listId);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(LIST_STRING, listString)
                .apply();
    }

    @Override
    public Observable<List<Video>> getPendingUploads() {
        return Observable.create(subscriber -> {
            String listString = mSharedPreferences.getString(LIST_STRING, "[]");
            if (listString.equals("[]")) {
                subscriber.onNext(new ArrayList<Video>());
            } else {
                List<Video> listId = mGson.fromJson(listString, new TypeToken<List<Video>>() {
                }.getType());
                subscriber.onNext(sortQueue(listId));
            }

            subscriber.onCompleted();
        });
    }

    private List<Video> sortQueue(List<Video> _videos) {
        Collections.sort(_videos);
        for (Video video: _videos) {
            if (video.isCurrentlyUploading()) {
                _videos.remove(video);
                _videos.add(0, video);
                break;
            }
        }
        return _videos;
    }

}
