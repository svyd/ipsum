package com.capt.video.view;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by Svyd on 11.04.2016.
 */
public class ListenableVideoView extends VideoView implements MediaPlayer.OnCompletionListener {

    private VideoListener mListener;

    public ListenableVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public ListenableVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public ListenableVideoView(Context context) {
        super(context);
    }

    public void setVideoListener(VideoListener _listener) {
        setOnCompletionListener(this);
        mListener = _listener;
    }

    @Override
    public void start() {
        super.start();
        if (mListener != null) {
            mListener.onVideoPlay();
        }
    }

    @Override
    public void pause() {
        super.pause();
        if (mListener != null) {
            mListener.onVideoPause();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mListener.onVideoPause();
    }

    public interface VideoListener {
        void onVideoPlay();
        void onVideoPause();
    }
}
