package com.capt.domain.profile.dashboard.model;

import com.capt.domain.profile.dashboard.model.base.DisplayableItem;

import java.util.List;

/**
 * Created by Svyd on 25.03.2016.
 */
public class Progress implements DisplayableItem {

    @Override
    public String getHeaderTitle() {
        return "Progress...";
    }

    @Override
    public String getHeaderDescription() {
        return "";
    }
}
