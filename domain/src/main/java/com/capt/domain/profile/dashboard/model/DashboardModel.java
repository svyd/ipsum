package com.capt.domain.profile.dashboard.model;

import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.capt.domain.profile.marketplace.model.MarketplaceItem;
import com.capt.domain.video.model.Video;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 28.03.2016.
 */
public class DashboardModel implements Serializable{

    private Section<Assignment> assignments;
    private Section<MarketplaceItem> marketplace;
    private Section<Video> videos;
    private UserModel user;

    public void setUser(UserModel user) {
        this.user = user;
    }

    public UserModel getUser() {
        return user;
    }

    public void setAssignments(Section<Assignment> _assignments) {
        assignments = _assignments;
    }

    public void setMarketplace(Section<MarketplaceItem> _marketplace) {
        marketplace = _marketplace;
    }

    public void setVideos(Section<Video> _videos) {
        videos = _videos;
    }

    public Section<Assignment> getAssignments() {
        return assignments;
    }

    public Section<Video> getVideos() {
        return videos;
    }

    public Section<MarketplaceItem> getMarketplace() {
        return marketplace;
    }

    public List<Section> getAsSections() {
        List<Section> items = new ArrayList<>();
        items.add(assignments);
        items.add(marketplace);
        items.add(videos);
        return items;
    }
}
