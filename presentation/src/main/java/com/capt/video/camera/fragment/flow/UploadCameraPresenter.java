package com.capt.video.camera.fragment.flow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.OrientationEventListener;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.camera.BaseAdapter;
import com.capt.video.camera.custom_view.morphing_btn.MorphingButton;
import com.capt.video.camera.fragment.CameraContract;
import com.capt.video.trim.file_manager.FileManager;
import com.capt.video.trim.view_presenter.flow.AbstractTrimPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by richi on 2016.03.28..
 */
public class UploadCameraPresenter extends AbstractCameraPresenter {

    @Inject
    public UploadCameraPresenter(Handler _handler,
                                 FileManager _manager,
                                 CameraContract.View _view,
                                 @Named(Constants.NamedAnnotation.TOP_ADAPTER)
                                 BaseAdapter _topAdapter,
                                 @Named(Constants.NamedAnnotation.BOTTOM_ADAPTER)
                                 BaseAdapter _bottomAdapter, BaseExceptionDelegate _delegate) {
        super(_handler, _manager, _view, _topAdapter, _bottomAdapter, _delegate);
    }

    @Override
    protected void stopRecording() {
        getView().showRightBar();
        getView().stopRecording();
        getView().startTrimFragment(getPath());
    }
}
