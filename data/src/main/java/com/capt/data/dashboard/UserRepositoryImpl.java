package com.capt.data.dashboard;

import com.capt.data.authorization.repository.datasource.UserCache;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.profile.dashboard.UserRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 4/23/16.
 */
public class UserRepositoryImpl implements UserRepository {

    private UserCache mCache;

    @Inject
    public UserRepositoryImpl(UserCache _userCache) {
        mCache = _userCache;
    }

    @Override
    public Observable<UserModel> getUser() {
        return mCache.getUser();
    }
}
