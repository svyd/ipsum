package com.capt.data.base;

import java.util.List;

import rx.Observable;

/**
 * Created by richi on 2016.03.14..
 */
public interface BaseDAO<T> {
    List<T> getAll();
    T get(String _id);
    long add(T _model);
    long updateDraft(T _model);
    int deleteByDraftId(String _id);
    int deleteById(String _id);
    int getDraftCount();
    long updatePending(T _model);
}
