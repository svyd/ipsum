package com.capt.authorization.forgot_password;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.capt.R;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.forgot_password.dagger.ForgotPasswordComponent;
import com.capt.authorization.forgot_password.dagger.ForgotPasswordModule;
import com.capt.base.BaseFragment;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Svyd on 18.03.2016.
 */
public class ForgotPasswordFragment extends BaseFragment implements ForgotPasswordContract.ForgotPasswordView {

    public static ForgotPasswordFragment newInstance() {

        Bundle args = new Bundle();

        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }



    @Inject
    ForgotPasswordContract.ForgotPasswordPresenter mPresenter;

    private ForgotPasswordComponent mComponent;

    @Bind(R.id.ivCheck_FFP)
    ImageView mIvCheck;

    @Bind(R.id.btnRequest_FFP)
    Button btnRequest;

    @Bind(R.id.progressBar_FFP)
    ProgressBar mProgress;

    @Bind(R.id.etEmail_FFP)
    EditText etEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_forgot_password);

        initializeInjector();
        inject();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @OnTextChanged(R.id.etEmail_FFP)
    void onTextChanged(CharSequence _text) {
        mPresenter.checkEmail(_text.toString());
    }

    @OnClick(R.id.btnRequest_FFP)
    void onRequestClicked() {
        mPresenter.passEmail(etEmail.getText().toString());
    }

    private void inject() {
        if (mPresenter != null)
            return;
        mComponent.inject(this);
    }

    private void initializeInjector() {
        if (mComponent != null)
            return;
        mComponent = getComponent(AuthActivityComponent.class)
                .createForgotPasswordComponent(new ForgotPasswordModule(this));
    }

    @Override
    public void showProgress() {
        mIvCheck.setVisibility(View.GONE);
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onEmailChecked(boolean _checked) {
        if (_checked) {
            mIvCheck.setVisibility(View.VISIBLE);
        } else {
            mIvCheck.setVisibility(View.GONE);
        }
    }

    @Override
    public void enableButton(boolean _enable) {
        btnRequest.setEnabled(_enable);
    }

    @Override
    public void setEmailError(String error) {
        etEmail.setError(error);
    }

    @Override
    public void onEmailPassed() {
        Toast.makeText(getActivity(), "New password has been sent to your email", Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }
}
