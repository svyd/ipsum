package com.capt.domain.video.model;

import com.capt.domain.category.model.CategoryModel;
import com.capt.domain.currency.model.CurrencyModel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by richi on 2016.03.11..
 */
public class VideoUploadModel implements Serializable {

    private String mId;
    private String mTitle;
    private String mDescription;
    private double mPrice;
    private String mLicense;
    private Date mDate;
    private double[] mLocation;
    private String[] mTags;
    private String mFilePath;
    private long mDuration;
    private CurrencyModel currency;
    private String formattedLocation;
    private String formattedSize;
    private List<CategoryModel> categories;
    private int isDraft;
    private int isUploading;
    private Quality mQuality;
    private String city;
    private String country;

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setQuality(Quality _quality) {
        mQuality = _quality;
    }

    public Quality getQuality() {
        return mQuality;
    }

    public boolean isDraft() {
        return isDraft == 1;
    }

    public boolean isUploading() {
        return isUploading == 1;
    }

    public int isUploadingInt() {
        return isUploading;
    }

    public int isDraftInt() {
        return isDraft;
    }

    public void setDraft(boolean draft) {
        isDraft = draft ? 1: 0;
    }

    public void setUploading(boolean uploading) {
        isUploading = uploading ? 1 :0;
    }

    public void setCategories(List<CategoryModel> categories) {
        this.categories = categories;
    }

    public List<CategoryModel> getCategories() {
        return categories;
    }

    public void setFormattedLocation(String formattedLocation) {
        this.formattedLocation = formattedLocation;
    }

    public String getFormattedLocation() {
        return formattedLocation;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setDuration(long _duration) {
        mDuration = _duration;
    }

    public long getDuration() {
        return mDuration;
    }

    public void setTags(String[] _tags) {
        mTags = _tags;
    }

    public String[] getTags() {
        return mTags;
    }

    public String getId() {
        return mId;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date mDate) {
        this.mDate = mDate;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double mPrice) {
        this.mPrice = mPrice;
    }

    public String getLicense() {
        return mLicense;
    }

    public void setLicense(String mLicense) {
        this.mLicense = mLicense;
    }

    public double[] getLocation() {
        return mLocation;
    }

    public void setLocation(double[] mLocation) {
        this.mLocation = mLocation;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setmFilePath(String mFilePath) {
        this.mFilePath = mFilePath;
    }

    public String getFormattedSize() {
        return formattedSize;
    }

    public void setFormattedSize(String formattedSize) {
        this.formattedSize = formattedSize;
    }

    public static class Builder {
        private String mId;
        private String mTitle;
        private String mDescription;
        private double mPrice;
        private Date mDate;
        private String mLicense;
        private double[] mLocation;
        private String mFilePath;
        private String[] mTags;
        private long mDuration;
        private CurrencyModel currency;
        private String formattedLocation;
        private String formattedSize;
        private List<CategoryModel> categories;
        private int isDraft;
        private int isUploading;
        private Quality mQuality;
        private String city;
        private String country;

        public Builder setCountry(String country) {
            this.country = country;
            return this;
        }

        public Builder setCity(String city) {
            this.city = city;
            return this;
        }

        public Builder setQuality(Quality _quality) {
            mQuality = _quality;
            return this;
        }


        public void setCategories(List<CategoryModel> categories) {
            this.categories = categories;
        }

        public List<CategoryModel> getCategories() {
            return categories;
        }

        public Builder setFormattedLocation(String formattedLocation) {
            this.formattedLocation = formattedLocation;
            return this;
        }

        public String getFormattedLocation() {
            return formattedLocation;
        }

        boolean isUploading() {
            return isUploading == 1;
        }

        boolean isDraft() {
            return isDraft == 1;
        }

        public Builder setDraft(int draft) {
            isDraft = draft;
            return this;
        }

        public Builder setUploading(int uploading) {
            isUploading = uploading;
            return this;
        }

        public CurrencyModel getCurrency() {
            return currency;
        }

        public Builder setCurrency(CurrencyModel currency) {
            this.currency = currency;
            return this;
        }

        public Builder setDuration(long _duration) {
            mDuration = _duration;
            return this;
        }

        public long getDuration() {
            return mDuration;
        }

        public Builder setId(String mId) {
            this.mId = mId;
            return this;
        }

        public Builder setDate(Date mDate) {
            this.mDate = mDate;
            return this;
        }

        public Builder setTitle(String mTitle) {
            this.mTitle = mTitle;
            return this;
        }

        public Builder setDescription(String mDescription) {
            this.mDescription = mDescription;
            return this;
        }

        public Builder setPrice(double mPrice) {
            this.mPrice = mPrice;
            return this;
        }

        public Builder setLicense(String mLicense) {
            this.mLicense = mLicense;
            return this;
        }

        public Builder setLocation(double[] mLocation) {
            this.mLocation = mLocation;
            return this;
        }

        public Builder setFilePath(String mFilePath) {
            this.mFilePath = mFilePath;
            return this;
        }

        public Builder setFormattedSize(String mFormattedSize) {
            this.formattedSize = mFormattedSize;
            return this;
        }

        public Builder setTags(String[] _tags) {
            mTags = _tags;
            return this;
        }

        public VideoUploadModel build() {
            VideoUploadModel videoUploadModel = new VideoUploadModel();
            videoUploadModel.setId(mId);
            videoUploadModel.setDescription(mDescription);
            videoUploadModel.setPrice(mPrice);
            videoUploadModel.setLicense(mLicense);
            videoUploadModel.setLocation(mLocation);
            videoUploadModel.setTitle(mTitle);
            videoUploadModel.setmFilePath(mFilePath);
            videoUploadModel.setDate(mDate);
            videoUploadModel.setTags(mTags);
            videoUploadModel.setDuration(mDuration);
            videoUploadModel.setCurrency(currency);
            videoUploadModel.setFormattedLocation(formattedLocation);
            videoUploadModel.setFormattedSize(formattedSize);
            videoUploadModel.setCategories(categories);
            videoUploadModel.setUploading(isUploading());
            videoUploadModel.setDraft(isDraft());
            videoUploadModel.setQuality(mQuality);
            videoUploadModel.setCity(city);
            videoUploadModel.setCountry(country);

            return videoUploadModel;
        }
    }
}
