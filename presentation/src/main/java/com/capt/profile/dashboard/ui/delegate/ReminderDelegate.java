package com.capt.profile.dashboard.ui.delegate;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capt.R;
import com.capt.domain.profile.dashboard.model.Reminder;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.profile.dashboard.ui.adapter.SectioningAdapter;
import com.capt.profile.dashboard.ui.adapter.ViewTypeConstants;
import com.capt.profile.dashboard.ui.delegate.base.BaseDelegate;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 10.05.2016.
 */
public class ReminderDelegate extends BaseDelegate {

    private LayoutInflater mInflater;

    @Inject
    public ReminderDelegate(LayoutInflater _inflater) {
        super(ViewTypeConstants.VIEW_TYPE_REMINDER);
        mInflater = _inflater;
    }

    @Override
    public boolean isForViewType(@NonNull List<DisplayableItem> items, int position) {
        return items.get(position) instanceof Reminder;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ReminderViewHolder(mInflater.inflate(R.layout.list_item_reminder, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull List<DisplayableItem> items, int position, @NonNull RecyclerView.ViewHolder holder) {
        ReminderViewHolder reminderHolder = (ReminderViewHolder) holder;
        Reminder reminder = (Reminder) items.get(position);

        reminderHolder.tvText.setText(reminder.getReminder());
    }

    public class ReminderViewHolder extends SectioningAdapter.ViewHolder implements View.OnClickListener{

        @Bind(R.id.tvReminder_LIR)
        public TextView tvText;

        public ReminderViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onDelegateClick(ViewTypeConstants.VIEW_TYPE_REMINDER, getLayoutPosition());
        }
    }
}
