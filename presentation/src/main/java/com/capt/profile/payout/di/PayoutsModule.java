package com.capt.profile.payout.di;

import com.capt.data.annotation.PerFragment;
import com.capt.data.base.TypeMapper;
import com.capt.data.payout.DateConverter;
import com.capt.data.payout.PayoutRepositoryImpl;
import com.capt.data.payout.PayoutService;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.payout.PayoutModel;
import com.capt.domain.profile.payout.PayOutRepository;
import com.capt.domain.profile.payout.PayoutInteractor;
import com.capt.profile.payout.BasePayoutsAdapter;
import com.capt.profile.payout.PayoutPresenter;
import com.capt.profile.payout.PayoutsAdapter;
import com.capt.profile.payout.PayoutsContract;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by rMozes on 8/3/16.
 */
@Module
public class PayoutsModule {

    PayoutsContract.View mView;

    public PayoutsModule(PayoutsContract.View _view) {
        mView = _view;
    }

    @PerFragment
    @Provides
    protected PayoutsContract.View provideView() {
        return mView;
    }

    @PerFragment
    @Provides
    protected PayoutsContract.PayoutsPresenter providePresenter(PayoutPresenter _presenter) {
        return _presenter;
    }

    @PerFragment
    @Provides
    protected BasePayoutsAdapter<PayoutsAdapter.PayOutViewHolder, PayoutModel> provideAdapter(PayoutsAdapter _adapter) {
        return _adapter;
    }

    @Provides
    @PerFragment
    protected PayoutService providePayoutService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                 Retrofit _retrofit) {
        return _retrofit.create(PayoutService.class);
    }

    @Provides
    @PerFragment
    protected TypeMapper<String, String> provideDateConverter(DateConverter _converter) {
        return _converter;
    }

    @Provides
    @PerFragment
    protected PayOutRepository provideRepository(PayoutRepositoryImpl _repository) {
        return _repository;
    }

    @PerFragment
    @Provides
    @Named(Constants.NamedAnnotation.PAYOUT)
    protected BaseInteractor provideInteractor(PayoutInteractor _interactor) {
        return _interactor;
    }
}
