package com.capt.profile.assignments.for_grabs;

import android.content.Intent;

import com.capt.domain.global.Constants;
import com.capt.profile.assignments.base.BaseAssignmentsFragment;

/**
 * Created by root on 15.04.16.
 */
public class ForGrabsFragment extends BaseAssignmentsFragment {

    @Override
    protected String getType() {
        return Constants.AssignmentTypes.FOR_GRABS;
    }

}
