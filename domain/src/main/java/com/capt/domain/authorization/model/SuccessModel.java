package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuccessModel<T> {

    public T mModel;

    public SuccessModel(String _success) {
        success = _success;
    }

    public SuccessModel() {

    }

    @Expose @SerializedName("success")
    public String success;

    @Expose @SerializedName("error")
    public String error;
}