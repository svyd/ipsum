package com.capt.profile.settings.password;

import android.os.Handler;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.PasswordModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;


import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 23.06.2016.
 */
public class PasswordPresenter extends PasswordContract.Presenter {

    private boolean isCurrentVisible;
    private boolean isNewVisible;
    private boolean isConfirmVisible;

    private PasswordContract.View mView;
    private Handler mHandler;
    private BasePostInteractor<PasswordModel> mInteractor;

    @Inject
    public PasswordPresenter(Handler _handler,
                             PasswordContract.View _view,
                             @Named(Constants.NamedAnnotation.CHANGE_PASSWORD_INTERACTOR)
                             BasePostInteractor<PasswordModel> _interactor,
                             BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mHandler = _handler;
        mInteractor = _interactor;
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void onCurrentEyeClick() {
        if (isCurrentVisible) {
            mView.hideCurrentPassword();
        } else {
            mView.showCurrentPassword();
        }
        isCurrentVisible = !isCurrentVisible;
    }

    @Override
    public void onNewEyeClick() {
        if (isNewVisible) {
            mView.hideNewPassword();
        } else {
            mView.showNewPassword();
        }
        isNewVisible = !isNewVisible;
    }

    @Override
    public void onConfirmEyeClick() {
        if (isConfirmVisible) {
            mView.hideConfirmPassword();
        } else {
            mView.showConfirmPassword();
        }
        isConfirmVisible = !isConfirmVisible;
    }

    private boolean checkMatch() {
        return mView.getNewPassword().equals(mView.getConfirmPassword());
    }

    private boolean validateCurrent() {
        return !mView.getCurrentPassword().equals("")
                && mView.getCurrentPassword().trim().length() > 5;
    }

    @Override
    public void onChangeClick() {
        if (!validateCurrent()) {
            mView.showToast("Current password isn't valid");
            return;
        }
        if (!checkMatch()) {
            mView.showToast("Password doesn't match");
            return;
        }
        if (!checkConfirm()) {
            mView.showToast("New password isn't valid");
            return;
        }
        mView.showProgress();

        mHandler.postDelayed(() -> {
            mView.hideProgress();
            mView.navigateBack();
        }, 2000);
    }

    private boolean checkConfirm() {
        return !mView.getNewPassword().equals("")
                && mView.getNewPassword().trim().length() > 5;
    }

    private class PasswordObserver extends BaseObserver<SuccessModel> {
        public PasswordObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.navigateBack();
        }

        @Override
        public void onNext(SuccessModel successModel) {
            mView.hideProgress();
        }
    }
}
