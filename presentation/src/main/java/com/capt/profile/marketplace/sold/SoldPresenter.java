package com.capt.profile.marketplace.sold;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.model.RequestParamsModel;
import com.capt.profile.marketplace.base.BaseMarketplaceListContract;
import com.capt.profile.marketplace.base.BaseMarketplaceListPresenter;

import javax.inject.Inject;

/**
 * Created by Svyd on 25.05.2016.
 */
public class SoldPresenter extends BaseMarketplaceListPresenter {

    @Inject
    public SoldPresenter(BaseMarketplaceListContract.View _view,
                         BasePostInteractor<RequestParamsModel> _interactor,
                         String _marketplaceType, BaseExceptionDelegate _delegate) {
        super(_view, _interactor, _marketplaceType, _delegate);
    }
}
