package com.capt.authorization.login_credentials.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.login_credentials.LogInContract;
import com.capt.authorization.login_credentials.LogInPresenter;
import com.capt.domain.authorization.interactor.SignInInteractor;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.base.BasePostInteractor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.03.20..
 */
@Module
public class LogInModule {

    private LogInContract.View mView;

    public LogInModule(LogInContract.View _view) {
        mView = _view;
    }

    @Provides @PerFragment
    protected LogInContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    protected LogInContract.Presenter providePresenter(LogInPresenter _presenter) {
        return _presenter;
    }

    @Provides @PerFragment
    protected BasePostInteractor<SignInModel> provideInteractor(SignInInteractor _interactor) {
        return _interactor;
    }
}
