package com.capt.data.assignment;

import com.capt.data.base.TypeMapper;
import com.capt.data.dashboard.DashboardRepositoryImpl;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.profile.assignment.AssignmentsRepository;
import com.capt.domain.profile.assignment.cache.AssignmentCache;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.profile.dashboard.model.DashboardModel;
import com.capt.domain.profile.marketplace.model.MarketplaceItem;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.model.VideoPatch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.functions.Func2;

/**
 * Created by Svyd on 29.03.2016.
 */
public class AssignmentsRepositoryImpl implements AssignmentsRepository {

    AssignmentCache mCache;
    AssignmentsService mService;
    TypeMapper<List<Assignment>, List<Assignment>> mMapper;

    @Inject
    public AssignmentsRepositoryImpl(AssignmentCache _cache,
                                     @Named(Constants.NamedAnnotation.Mapper.ASSIGNMENTS_MAPPER)
                                     TypeMapper<List<Assignment>, List<Assignment>> _mapper,
                                     AssignmentsService _service) {
        mService = _service;
        mMapper = _mapper;
        mCache = _cache;
    }

    @Override
    public Observable<Assignments> getAssignments(RequestParamsModel _model) {
//        return mService.getAssignments(_model.limit, _model.offset, _model.type).map(mMapper::convert);
        return null;
    }

    @Override
    public Observable<List<Assignment>> getAssignmentModels(RequestParamsModel _model) {
        if (_model.offset == 0 &&
                _model.type.equals(Constants.AssignmentTypes.FINISHED)) {
            return getFinished(_model).map(mMapper::convert);
        } else if (_model.type.equals(Constants.AssignmentTypes.IN_PROGRESS)) {
            return getInProgress(_model);
        }
        return mService.getAssignments(_model.limit, _model.offset, _model.type).map(mMapper::convert);
    }

    private Observable<List<Assignment>> getInProgress(RequestParamsModel _model) {
        return Observable.zip(mCache.getAssignment(),
                mService.getAssignments(_model.limit, _model.offset, _model.type),
                (assignment, assignments) -> {
                    if (assignment != null) {
                        Iterator<Assignment> iterator = assignments.iterator();
                        while (iterator.hasNext()) {
                            Assignment a = iterator.next();
                            if(a.id.equals(assignment.id)) {
                                iterator.remove();
                            }
                        }
                    }
                    return assignments;
                });
    }

    private Observable<List<Assignment>> getFinished(RequestParamsModel _model) {
        return Observable.zip(mCache.getAssignment(),
                mService.getAssignments(_model.limit, _model.offset, _model.type),
                (assignment, assignments) -> {
                    if (assignment != null) {
                        assignments.add(0, assignment);
                    }
                    return assignments;
                });
    }

    @Override
    public Observable<Video> uploadRejectedVideo(Video _video) {
        return mService.uploadRejectedVideo(_video.id, new VideoPatch(_video));
    }

    @Override
    public Observable<SuccessModel> delete(String _id) {
        return mService.deleteRejectedAssignment(_id);
    }
}
