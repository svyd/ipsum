package com.capt.profile.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.capt.R;
import com.capt.authorization.AuthorizationActivity;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.profile.HeaderElevationUtil;
import com.capt.profile.dagger.ProfileActivityComponent;
import com.capt.profile.dashboard.ui.adapter.StickyHeaderLayoutManager;
import com.capt.profile.dashboard.ui.listener.OnDelegateItemClickListener;
import com.capt.profile.edit_profile.EditProfileActivity;
import com.capt.profile.settings.currency.CurrencySettingsFragment;
import com.capt.profile.settings.dagger.SettingsComponent;
import com.capt.profile.settings.dagger.SettingsModule;
import com.capt.profile.settings.notification.NotificationSettingsFragment;
import com.capt.profile.settings.ui.NewSettingsAdapter;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 20.04.2016.
 */
public class SettingsFragment extends BaseToolbarFragment implements SettingsContract.View, OnDelegateItemClickListener {

    private boolean isItemSelected = false;

    @Bind(R.id.rvSettings_FS)
    RecyclerView rvSettings;

    @Bind(R.id.pbSettings_FS)
    ProgressBar pbSettings;

    @Bind(R.id.llRetry_FS)
    LinearLayout llRetry;

    @Inject
    SettingsContract.Presenter mPresenter;

    @Inject
    RecyclerView.LayoutManager mLayoutManager;

    @Inject
    StickyRecyclerHeadersDecoration mHeadersDecor;

    @Inject
    HeaderElevationUtil mElevationUtil;

    NewSettingsAdapter mAdapter;

    private SettingsComponent mComponent;

    public static SettingsFragment newInstance() {

        Bundle args = new Bundle();

        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponent();
        inject();
        getToolbarManager().showToolbar();
        getActivity().setTitle("Settings");
        initList();
        mPresenter.initialize();
    }

    private void initComponent() {
        mComponent = getComponent(ProfileActivityComponent.class)
                .createSettingsComponent(new SettingsModule(this));
    }

    @Override
    protected void setUpActionBar(ActionBar actionBar) {
        super.setUpActionBar(actionBar);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onStart() {
        super.onStart();
        isItemSelected = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void initList() {
        mAdapter = new NewSettingsAdapter(getActivity());
        rvSettings.setAdapter(mAdapter);
        rvSettings.setLayoutManager(mLayoutManager);
        ((StickyHeaderLayoutManager) mLayoutManager)
                .setHeaderPositionChangedCallback((sectionIndex, header, oldPosition, newPosition)
                        -> mElevationUtil.onHeaderPositionChanged(sectionIndex, header, oldPosition, newPosition));
    }

    @Override
    public void showProgress() {
        pbSettings.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbSettings.setVisibility(View.GONE);
    }

    @Override
    public void showNoInternetConnectionError() {
        llRetry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoInternetConnectionError() {
        llRetry.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void onDelegateClick(int delegateType, int position) {
            switch (delegateType) {
                case 0:
                    mPresenter.onAccountSettingsClick(position);
                    break;
                case 1:
                    mPresenter.onProfileSettingsClick(position);
                    break;
                case 2:
                    mPresenter.onSupportSettingsClick(position);
                    break;
            }
    }

    @Override
    public void onResume() {
        super.onResume();
        isItemSelected = false;
    }

    @Override
    public void navigateToNotificationSettings(SettingsModel _model) {
        AccountSettingsActivity.startNotificationSettings(getActivity(), _model);
    }

    @Override
    public void navigateToPermissionSettings() {
        //temporary kostyl due to not working settings item yet
        isItemSelected = false;
    }

    @Override
    public void navigateToSupportSettings() {
        //temporary kostyl due to not working settings item yet
        isItemSelected = false;
    }

    @Override
    public void navigateToCurrencySettings(SettingsModel _model) {
        AccountSettingsActivity.startCurrencySettings(getActivity(), _model);
    }

    @Override
    public void navigateToPayPalSettings() {
        //temporary kostyl due to not working settings item yet
        isItemSelected = false;
    }

    @Override
    public void navigateToPasswordSettings() {
        EditProfileActivity.startPasswordFlow(getActivity());
        isItemSelected = false;
    }

    @Override
    public void logOut() {
        Intent intent = new Intent(getActivity(), AuthorizationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void navigateToEditProfile() {
        EditProfileActivity.startEditProfileFlow(getActivity());
    }

    @Override
    public void showMessage(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showSettings(SettingsModel _model) {
        mAdapter.setItems(_model.getItems());
        mAdapter.setOnItemClickListener(this);
    }

    @OnClick(R.id.btnTryAgain_IR)
    void onRetryClick() {
        mPresenter.onRetryClick();
    }
}
