package com.capt.video.map.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.map.MapFragment;

import dagger.Subcomponent;

/**
 * Created by root on 05.04.16.
 */
@PerFragment
@Subcomponent(modules = MapModule.class)
public interface MapComponent {
	void inject(MapFragment _fragment);
}
