package com.capt.video.trim.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.trim.view_presenter.TrimFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.02.29..
 */
@PerFragment
@Subcomponent(modules = {FFMpegModule.class, TrimModule.class})
public interface FFMpegComponent {
    void inject(TrimFragment _fragment);

}
