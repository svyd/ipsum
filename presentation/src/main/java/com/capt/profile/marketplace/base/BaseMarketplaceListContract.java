package com.capt.profile.marketplace.base;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.video.model.Video;

import java.util.List;

/**
 * Created by root on 14.04.16.
 */
public interface BaseMarketplaceListContract {

	interface View extends BaseView {
		void navigateToDraftDetails(Video _video);
		void navigateToVideoDetails(Video _video);
		void showVideos(List<Video> videoModels);
		void showEmptyList();
		void showMessage(String _message);
		void addVideos(List<Video> newVideoModels);
        void removeItem(int _position);
        void replaceItem(int _position, Video _new);
		void disableLoadMore();
        void onSaveDraft(Video _draft);
		void setPending(int _position);
		void scrollTo(int _position);
        void setProgress(int _position, int _progress);
        void setError(int _position);
        void swap(int _first, int _second);
		void notifyChanges();
        void setResult(int _result);
	}

	abstract class Presenter extends BasePresenter<BaseMarketplaceListContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void getMoreVideoItems();
		public abstract void onVideoClick(Video _video);
		public abstract void onUploadCompleted(int _position, Video _response);
		public abstract void onUploadError(int _position);
		public abstract void onProgress(int _position, int _progress);
		public abstract void onRetryClick(int _position);
		public abstract void onCancelClick(int _position);
		public abstract void onStartUploading(int _position);
	}
}
