package com.capt.authorization.phone_number.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.phone_number.PhoneNumberFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.03.17..
 */
@PerFragment
@Subcomponent(modules = PhoneNumberModule.class)
public interface PhoneNumberComponent {
    void inject(PhoneNumberFragment _fragment);
}
