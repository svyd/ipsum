package com.capt.authorization.login_credentials;

import android.content.SharedPreferences;

import com.capt.authorization.join_fragment.DeviceIdProvider;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.base.EmailValidator;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.google.gson.Gson;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by richi on 2016.03.20..
 */
public class LogInPresenter extends LogInContract.Presenter {

    private LogInContract.View mView;
    private BasePostInteractor<SignInModel> mInteractor;
    private DeviceIdProvider mIdProvider;
    private EmailValidator mEmailValidator;

    private boolean mIsPasswordVisible = true;

    @Inject
    public LogInPresenter(LogInContract.View _view, BasePostInteractor<SignInModel> _interactor,
                          DeviceIdProvider _provider, EmailValidator _emailValidator,
                          BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
        mIdProvider = _provider;
        mEmailValidator = _emailValidator;
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void onJoinClick(SignInModel.Builder _builder) {
        _builder.setDeviceId(mIdProvider.getDeviceId())
                .setToken("test_token");

        signInIfCredentialsValid(_builder.build());
    }

    @Override
    public void onTextChanged() {
        if (isEmailValid(mView.getEmail()) && isPasswordValid(mView.getPassword()))
            mView.setEnableJoin(true);
        else
            mView.setEnableJoin(false);
    }

    @Override
    public void onEyeClick() {
        mIsPasswordVisible = !mIsPasswordVisible;
        if (mIsPasswordVisible)
            mView.showPassword();
        else
            mView.hidePassword();
    }

    private boolean isEmailValid(String _email) {
        return mEmailValidator.isEmailValid(_email);
    }

    private boolean isPasswordValid(String _password) {
        return _password.trim().length() > 4;
    }

    private void signInIfCredentialsValid(SignInModel _model) {
        if (!isEmailValid(_model.getEmail())) {
            mView.showMessage("Email is not valid");
            return;
        }

        if (!isPasswordValid(_model.getPassword())) {
            mView.showMessage("Password is not correct");
            return;
        }

        mInteractor.execute(_model, new SignInObserver(this));
    }

    private class SignInObserver extends BaseObserver<UserModel> {
        public SignInObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(UserModel userModel) {
            mView.startProfile(userModel);
        }
    }
}
