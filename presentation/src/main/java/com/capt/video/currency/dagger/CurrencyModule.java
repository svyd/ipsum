package com.capt.video.currency.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.domain.global.Constants;
import com.capt.video.currency.CurrencyContract;
import com.capt.video.currency.CurrencyPresenter;
import com.capt.data.currency.CurrencyRepositoryImpl;
import com.capt.data.currency.CurrencyService;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.currency.CurrencyInteractor;
import com.capt.domain.currency.CurrencyRepository;
import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by root on 25.03.16.
 */
@Module
public class CurrencyModule {

	private CurrencyContract.View mView;

	public CurrencyModule(CurrencyContract.View _view) {
		mView = _view;
	}

	@Provides @PerFragment
	protected CurrencyContract.View provideView() {
		return mView;
	}

	@Provides @PerFragment
	protected CurrencyContract.Presenter providePresenter(CurrencyPresenter _presenter) {
		return _presenter;
	}

	@Provides
	@PerFragment
	CurrencyService provideCurrencyService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
	                                       Retrofit _retrofit) {
		return _retrofit.create(CurrencyService.class);
	}

	@Provides
	@PerFragment
	 CurrencyRepository provideCurrencyRepository(CurrencyRepositoryImpl _repository) {
		return _repository;
	}

	@Provides
	@PerFragment
	@Named(Constants.NamedAnnotation.CURRENCY_INTERACTOR)
	BaseInteractor provideCurrencyInteractor(CurrencyInteractor _interactor) {
		return _interactor;
	}
}
