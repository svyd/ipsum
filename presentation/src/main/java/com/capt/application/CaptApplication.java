package com.capt.application;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.capt.BuildConfig;
import com.capt.R;
import com.capt.application.dagger.AppComponent;
import com.capt.application.dagger.AppModule;
import com.capt.application.dagger.AppRestModule;
import com.capt.application.dagger.DaggerAppComponent;
import com.capt.application.dagger.VideoUploadModule;
import com.capt.data.authorization.net.ApiConstants;
import com.capt.video.upload_manager.VideoUploadManager;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GoogleApiAvailability;
//import com.squareup.leakcanary.LeakCanary;
//import com.squareup.leakcanary.RefWatcher;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import net.gotev.uploadservice.Logger;
import net.gotev.uploadservice.UploadService;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;


import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by richi on 2016.02.22..
 */
public class CaptApplication extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "LxmBiwDMSlRYhETJKpmjwPCKN";
    private static final String TWITTER_SECRET = "MpYR9MJ2232tGKSfwHpd2yGQ6v0vglZlPei9ZFa7AmqurzzoIK";
    private static final String TAG = CaptApplication.class.getSimpleName();

    private static CaptApplication sINSTANCE;

//    public static RefWatcher getRefWatcher(Context context) {
//        CaptApplication application = (CaptApplication) context.getApplicationContext();
//        return application.refWatcher;
//    }
//
//    private RefWatcher refWatcher;

    @Inject
    VideoUploadManager mUploadManager;
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sINSTANCE = this;

//        refWatcher = LeakCanary.install(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("roboto/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        //UploadService
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        Logger.setLogLevel(Logger.LogLevel.DEBUG);


        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig), new Crashlytics());

        FacebookSdk.sdkInitialize(this);
        initializeInjector();

        uploadPendingVideos();
    }

    private void uploadPendingVideos() {
        mUploadManager.startPendingUploading();
    }

    private void initializeInjector() {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .appRestModule(new AppRestModule(ApiConstants.API_ENDPOINT))
                .videoUploadModule(new VideoUploadModule(ApiConstants.UPLOAD_VIDEO))
                .build();

        CookieHandler.setDefault(new CookieManager(mAppComponent.cookieStore(),
                CookiePolicy.ACCEPT_ALL));
        mAppComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public static CaptApplication getApplication() {
        return sINSTANCE;
    }

}
