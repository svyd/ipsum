package com.capt.data.assignment;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.model.VideoPatch;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Svyd on 04.04.2016.
 */
public interface AssignmentsService {

    @GET("mobile/assignments")
    Observable<List<Assignment>> getAssignments(
            @Query("limit") int _limit,
            @Query("offset") int _offset,
            @Query("type") String _type
    );

    @DELETE("/mobile/assignments/{id}")
    Observable<SuccessModel> deleteRejectedAssignment(@Path("id") String _id);

    @PATCH("/mobile/video/assignment/{id}")
    Observable<Video> uploadRejectedVideo(@Path("id") String _id, @Body VideoPatch _patch);
}
