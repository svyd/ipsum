package com.capt.video.trim.view_presenter.flow;

import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.trim.ffmpeg_manager.FFMPEGManager;
import com.capt.video.trim.view_presenter.TrimContract;

import java.io.File;

import rx.Observer;

/**
 * Created by Svyd on 13.05.2016.
 */
public abstract class AbstractTrimPresenter extends TrimContract.Presenter {
    private TrimContract.View mView;
    private FFMPEGManager mFFmpeg;

//    protected Video mData;
    private float mTrimmedDuration;

    public AbstractTrimPresenter(TrimContract.View _view, FFMPEGManager _ffmpegManager,
                                 BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mFFmpeg = _ffmpegManager;
    }

    @Override
    public void initialize() {
        Log.d("TestLog", "TrimPresenter.initialize()");
        if (getData().sourcePath != null) {
            mView.initVideoView(getData().sourcePath);
        } else {
            mView.initVideoView(getData().path);
        }
    }

    @Override
    public void onStart() {
    }

    protected abstract Video getData();

    protected TrimContract.View getView() {
        return mView;
    }

    protected float getTrimmedDuration() {
        return mTrimmedDuration;
    }

    @Override
    public void onStop() {
        mView.hideProgress();
        mFFmpeg.unBind();
    }

    @Override
    public void onTrimClick(long _left, long _right) {
        mTrimmedDuration = _right - _left;
        mView.showProgress();
        mFFmpeg.trimVideo(_left / 1000, _right / 1000, getData().path, new FFMPEGCallback(this));
    }

    protected abstract void doOnNext(Video _video);

    protected String formatSizeOfFile(String _path) {
        File file = new File(_path);
        long size = file.length();

        // convert Bites to MB
        return String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
    }

    private class FFMPEGCallback extends BaseObserver<Video> {

        public FFMPEGCallback(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(Video _model) {
            mView.hideProgress();
            doOnNext(_model);
        }
    }
}
