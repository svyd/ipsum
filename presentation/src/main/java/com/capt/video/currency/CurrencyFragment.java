package com.capt.video.currency;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.global.Constants;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.video.currency.dagger.CurrencyComponent;
import com.capt.video.currency.dagger.CurrencyModule;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.price.SetPriceFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by root on 25.03.16.
 */
public class CurrencyFragment extends BaseToolbarFragment implements CurrencyContract.View {

    @Inject
    CurrencyContract.Presenter mPresenter;
    @Inject
    ToolbarManager mToolbar;
    @Inject
    CurrencyAdapter currencyAdapter;

    @Bind(R.id.rv_currencies_FC)
    RecyclerView rvCurrencies;

    @Bind(R.id.pbCurrency_FC)
    ProgressBar pbCurrency;

    @Bind(R.id.vTopDivider_FC)
    View vDivider;

    private CurrencyComponent mComponent;
    private CurrencyModel selectedCurrency;

    public static BaseFragment newInstance(@NonNull CurrencyModel currency) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.Extra.EXTRA_CURRENCY, currency);
        CurrencyFragment currencyFragment = new CurrencyFragment();
        currencyFragment.setArguments(bundle);
        return currencyFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_currency);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        showToolbar();
        mPresenter.initialize();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(VideoActivityComponent.class)
                .createCurrencyComponent(new CurrencyModule(this));
    }

    private void showToolbar() {
        mToolbar.showToolbar();
        getActivity().setTitle(getString(R.string.currency));
    }

    private void setUpView() {
        selectedCurrency = (CurrencyModel) getArguments().getSerializable(Constants.Extra.EXTRA_CURRENCY);

        rvCurrencies.setLayoutManager(new LinearLayoutManager(getActivity(),
                android.support.v7.widget.LinearLayoutManager.VERTICAL, false));
    }

    private void inject() {
        if (mToolbar != null)
            return;

        mComponent.inject(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void showProgress() {
        pbCurrency.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbCurrency.setVisibility(View.GONE);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setUpView();
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void navigateToSetPriceScreen(CurrencyModel currencyModel) {

        selectedCurrency = currencyModel;
        Intent data = new Intent();
        data.putExtra(Constants.Extra.EXTRA_CURRENCY, currencyModel);

        getTargetFragment().onActivityResult(SetPriceFragment.CURRENCY_REQUEST_CODE, Activity.RESULT_OK, data);
        getFragmentNavigator().popBackStack();
    }

    @Override
    public void showCurrencies(List<CurrencyModel> currencyModels) {
        vDivider.setVisibility(View.VISIBLE);
        currencyAdapter.setData(currencyModels, selectedCurrency);
        rvCurrencies.setAdapter(currencyAdapter);
        currencyAdapter.setOnCurrencyClickListener(mPresenter::onCurrencySelected);
    }

    @Override
    public void showMessage(String _message) {
        super.showMessage(_message);
    }
}
