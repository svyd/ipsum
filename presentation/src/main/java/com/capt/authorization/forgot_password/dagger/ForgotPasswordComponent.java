package com.capt.authorization.forgot_password.dagger;

import com.capt.authorization.forgot_password.ForgotPasswordFragment;
import com.capt.data.annotation.PerFragment;

import dagger.Subcomponent;

/**
 * Created by Svyd on 22.03.2016.
 */
@PerFragment
@Subcomponent(modules = {ForgotPasswordModule.class})
public interface ForgotPasswordComponent {
    void inject(ForgotPasswordFragment _fragment);
}
