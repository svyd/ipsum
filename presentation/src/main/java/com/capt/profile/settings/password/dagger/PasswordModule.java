package com.capt.profile.settings.password.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.domain.authorization.interactor.ChangePasswordInteractor;
import com.capt.domain.authorization.model.PasswordModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.profile.settings.password.PasswordContract;
import com.capt.profile.settings.password.PasswordPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 23.06.2016.
 */
@Module
public class PasswordModule {

    private PasswordContract.View mView;

    public PasswordModule(PasswordContract.View _view) {
        mView = _view;
    }

    @Provides
    @PerFragment
    protected PasswordContract.View provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.CHANGE_PASSWORD_INTERACTOR)
    protected BasePostInteractor<PasswordModel> providePasswordInteractor(ChangePasswordInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment PasswordContract.Presenter providePresenter(PasswordPresenter _presenter) {
        return _presenter;
    }
}
