package com.capt.profile.edit_profile;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.base.EmailValidator;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 4/29/16.
 */
public class EditProfilePresenter extends EditProfileFragmentContract.Presenter {

    private static final String RESTORE_OLD_EMAIL = "old_email_key";
    private static final String RESTORE_OLD_PHONE = "old_phone_key";

    private EditProfileFragmentContract.View mView;
    private EmailValidator mEmailValidator;
    private boolean isEmailValid = false;

    private BasePostInteractor<UserModel> mUpdateUserInteractor;
    private PublishSubject<String> mEmailDebouncer;
    private BasePostInteractor<String> mEmailCheckInteractor;
    private BasePostInteractor<PhoneNumberModel> mCodeInteractor;
    private BaseInteractor mUserInteractor;

    private String mOldEmail;
    private PhoneNumberModel mOldPhone;
    private String mOriginalEmail;
    private UserModel mUserModel;

    @Inject
    public EditProfilePresenter(EditProfileFragmentContract.View _view,
                                EmailValidator _validator,
                                BaseInteractor _userInteractor,
                                @Named(Constants.NamedAnnotation.CHECK_EMAIL_INTERACTOR)
                                BasePostInteractor<String> _emailChecker,
                                @Named(Constants.NamedAnnotation.UPDATE_PROFILE_INTERACTOR)
                                BasePostInteractor<UserModel> _updateProfile,
                                BaseExceptionDelegate _delegate,
                                @Named(Constants.NamedAnnotation.VERIFICATION_INTERACTOR)
                                BasePostInteractor<PhoneNumberModel> codeInteractor) {
        super(_view, _delegate);
        initEmailSubject();
        mView = _view;
        mEmailValidator = _validator;
        mEmailCheckInteractor = _emailChecker;
        mUpdateUserInteractor = _updateProfile;
        mUserInteractor = _userInteractor;
        mCodeInteractor = codeInteractor;
    }

    private void initEmailSubject() {
        mEmailDebouncer = PublishSubject.create();
        mEmailDebouncer
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(s -> {
                    if (!isEmailOriginal(s)) {
                        if (validateEmail(s)) {
                            mView.hideRightEmail();
                            mEmailCheckInteractor.execute(s, new EmailCheckerObserver(this));
                        } else {
                            mView.onShowEmailError();
                            isEmailValid = false;
                            mView.hideEmailProgress();
                            mView.hideRightEmail();
                        }
                    } else {
                        isEmailValid = true;
                        mUserModel.setEmail(s);
                        mView.hideEmailProgress();
                        mView.showRightEmail();
                    }
                });
    }

    private boolean validateEmail(String _email) {
        if (isEmailSame(_email)) {
            mUserModel.setEmail(_email);
            return false;
        }

        return mEmailValidator.isEmailValid(_email);
    }

    private boolean isEmailOriginal(String _email) {
        return mOriginalEmail != null && mOriginalEmail.equals(_email);
    }

    @Override
    public void initialize() {
        if (mUserModel == null)
            mUserInteractor.execute(new UserCacheObserver(this));
        else
            renderUser();
    }

    private class UserCacheObserver extends BaseObserver<UserModel> {
        public UserCacheObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            renderUser();
        }

        @Override
        public void onNext(UserModel userModel) {
            mUserModel = userModel;
            mOldPhone = new PhoneNumberModel(mUserModel.getPhone().getCode(), mUserModel.getPhone().getPhone());
            mOldEmail = mUserModel.getEmail();
            if (mOriginalEmail == null) {
                mOriginalEmail = mOldEmail;
            }
        }
    }

    class CodeObserver extends BaseObserver<SuccessModel> {

        public CodeObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(SuccessModel userModel) {
            mView.hideProgress();
            mView.confirmPhoneAndUpdate();
        }
    }

    private void renderUser() {
        mView.renderEmail(mUserModel.getEmail());
        mView.renderImage(mUserModel.getAvatarUrl());

        if (!TextUtils.isEmpty(mUserModel.getCity()))
            mView.renderCity(mUserModel.getCity() + ", ");

        if (!TextUtils.isEmpty(mUserModel.getCountry()))
            mView.renderCountry(mUserModel.getCountry());

        mView.renderFullName(mUserModel.getName());
        mView.renderCountryCode(mUserModel.getPhone().getCode());
        mView.renderPhoneNumber(mUserModel.getPhone().getPhone());
    }

    @Override
    public void onEmailChanged(CharSequence _email) {
        mView.showEmailProgress();
        mView.hideRightEmail();
        mEmailDebouncer.onNext(_email.toString());
    }

    private class EmailCheckerObserver extends BaseObserver<SuccessModel<String>> {
        public EmailCheckerObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onError(Throwable e) {
            isEmailValid = false;
            mView.hideRightEmail();
            mView.hideEmailProgress();
            mView.onShowEmailError();
        }

        @Override
        public void onNext(SuccessModel<String> successModel) {
            isEmailValid = true;
            mUserModel.setEmail(successModel.mModel);
            mView.hideEmailProgress();
            mView.showRightEmail();
        }
    }

    private boolean isEmailSame(String _email) {
        return _email.equals(mUserModel.getEmail());
    }

    @Override
    public void onCountryCodeClicked() {
        mView.navigateToCountryCodePicker();
    }

    @Override
    public void onLocationClicked() {
        mView.navigateToLocationPicker();
    }

    private boolean isFullNameValid(String _fullName) {
        return _fullName != null && _fullName.trim().length() != 0;
    }

    @Override
    public void onDoneClicked() {

        if (!isEmailValid) {
            mView.showError("Invalid email");
            return;
        }

        if (!isFullNameValid(mUserModel.getName())) {
            mView.showFullNameError();
            return;
        }


        mView.showProgress();
        mUpdateUserInteractor.execute(mUserModel, new UpdateUserObserver(this));

    }

    private void executeCode() {
        mCodeInteractor.execute(mUserModel.getPhone(), new CodeObserver(this));
    }

    private class UpdateUserObserver extends BaseObserver<UserModel> {
        public UpdateUserObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(UserModel userModel) {
            if (!mOldPhone.getPhone().equals(mUserModel.getPhone().getPhone())
                    || !mOldPhone.getPhone().equals(mUserModel.getPhone().getPhone())) {
                executeCode();
            } else {

                mView.hideProgress();
                Intent intent = new Intent();
                intent.putExtra(Constants.BUNDLE_USER_MODEL, userModel);
                mView.onGoBack(intent);
            }
        }
    }

    @Override
    public void fullNameChanged(String _fullName) {
        mUserModel.setName(_fullName);
    }

    @Override
    public void onPhoneChanged(String _phone) {
        mUserModel.getPhone().setPhone(_phone);
    }

    @Override
    public void onPhotoClicked() {
        mView.showImagePickerSheet();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mView.hideProgress();
        mEmailDebouncer.onCompleted();
        mUpdateUserInteractor.unSubscribe();
        mEmailCheckInteractor.unSubscribe();
        mUserInteractor.unSubscribe();
    }

    @Override
    public void setLocation(LocationModel _location) {
        if (_location == null)
            return;

        mUserModel.setLocation(new double[]{_location.getLon(), _location.getLat()});
        mUserModel.setCity(_location.mCity);
        mUserModel.setCountry(_location.mCountry);
    }

    @Override
    public void setCountryCode(String _code) {
        if (TextUtils.isEmpty(_code))
            return;

        mUserModel.getPhone().setCode(_code);
    }

    @Override
    public void setAvatar(String _avatar) {
        if (TextUtils.isEmpty(_avatar))
            return;

        mUserModel.setAvatarUrl(_avatar);
        mView.renderImage(_avatar);
    }

    @Override
    public void savedInstanceState(Bundle _bundle) {
        _bundle.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, mUserModel);

        if (mOldEmail != null) {
            _bundle.putString(RESTORE_OLD_EMAIL, mOldEmail);
            _bundle.putSerializable(RESTORE_OLD_PHONE, mOldPhone);
        }
    }

    @Override
    public void restoreState(Bundle _bundle) {
        if (_bundle == null)
            return;

        mUserModel = ((UserModel) _bundle.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE));
        if (mUserModel != null) {
            renderUser();
        }

        if (_bundle.containsKey(RESTORE_OLD_EMAIL))
            mOldEmail = _bundle.getString(RESTORE_OLD_EMAIL);

        if (_bundle.containsKey(RESTORE_OLD_PHONE)) {
            mOldPhone = (PhoneNumberModel) _bundle.getSerializable(RESTORE_OLD_PHONE);
        }
    }

}
