package com.capt.profile.dagger;

import com.capt.base.utility.DateTimeUtilityImpl;
import com.capt.data.annotation.PerActivity;
import com.capt.base.BaseActivity;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.data.annotation.PerFragment;
import com.capt.data.base.DateTimeUtility;
import com.capt.data.base.TypeMapper;
import com.capt.data.settings.SettingsMapper;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.model.SettingsModel;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 25.03.2016.
 */
@Module
public class ProfileActivityModule {
    private BaseActivity mActivity;

    public ProfileActivityModule(BaseActivity _activity) {
        mActivity = _activity;
    }

    @Provides
    @PerActivity
    protected BaseActivity provideBaseActivity() {
        return mActivity;
    }

    @Provides @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }

    @Provides
    @PerActivity
    DateTimeUtility provideDateTimeUtility(DateTimeUtilityImpl _utility) {
        return _utility;
    }


}
