package com.capt.video.trim.ffmpeg_manager;

import rx.Observer;

/**
 * Created by richi on 2016.02.29..
 */
public interface FFMPEGManager {
    void trimVideo(long _startTime, long _endTime, String _input, Observer _observer);
    void unBind();
}
