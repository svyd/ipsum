package com.capt.video.camera.custom_view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.capt.R;
import com.capt.video.camera.BaseAdapter;

/**
 * Created by richi on 2016.03.25..
 */
public class AdapterLayout extends RelativeLayout {

    private BaseAdapter mAdapter;
    private OnItemClickListener mClick;

    private LinearLayout llContainer;

    private boolean mIsBottom;

    public AdapterLayout(Context context) {
        this(context, null);
    }

    public AdapterLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdapterLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater.from(context).inflate(R.layout.camera_setting_picker, this, true);

        llContainer = (LinearLayout) findViewById(R.id.llContainer);
    }

    public void setAdapter(BaseAdapter _adapter) {
        mAdapter = _adapter;
    }

    public void setBottom(boolean _isBottom) {
        mIsBottom = _isBottom;
    }

    public void setOnItemClickListener(OnItemClickListener _listener) {
        mClick = _listener;
    }

    public void notifyData() {
        if (llContainer.getChildCount() != 0)
            return;

        llContainer.setVisibility(GONE);
        if (mIsBottom) {
            addView(mAdapter.getMainView(this));
            LayoutParams layoutParams = ((LayoutParams) mAdapter.getMainView(this).getLayoutParams());
            layoutParams.addRule(ALIGN_PARENT_BOTTOM);
            mAdapter.getMainView(this).setLayoutParams(layoutParams);

            layoutParams = ((LayoutParams) llContainer.getLayoutParams());
            layoutParams.addRule(ALIGN_PARENT_BOTTOM);
            llContainer.setLayoutParams(layoutParams);
        } else {
            addView(mAdapter.getMainView(this));
        }

        mAdapter.getMainView(this).setOnClickListener(v -> {
            v.setVisibility(v.getVisibility() == GONE ? VISIBLE : GONE);
            llContainer.setVisibility(llContainer.getVisibility() == GONE ? VISIBLE : GONE);
            if (mClick != null)
                mClick.onMainClick();
        });
        for (int i = 0; i < mAdapter.getCount(); i++) {
            View view = mAdapter.getView(i, llContainer);
            view.setTag(i);
            view.setOnClickListener(v -> {
                int position = (int) v.getTag();
                if (mClick != null)
                    mClick.onItemClick(position);

                llContainer.setVisibility(llContainer.getVisibility() == GONE ? VISIBLE : GONE);
                mAdapter.getMainView(AdapterLayout.this).setVisibility(mAdapter.getMainView(AdapterLayout.this).getVisibility() == GONE ? VISIBLE : GONE);

                ((TextView) mAdapter.getMainView(AdapterLayout.this)).setText(((TextView) v).getText());
            });
            llContainer.addView(view);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int _position);
        void onMainClick();
    }

}
