package com.capt.profile;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by root on 13.04.16.
 */
public class ScrollForLoadMoreListener extends RecyclerView.OnScrollListener {
	private boolean handleScrolls = true;
	private OnLoadMoreListener loadMoreListener;
	public static final int ROW_COUNT = 10;


	public ScrollForLoadMoreListener(OnLoadMoreListener loadMoreListener) {
		this.loadMoreListener = loadMoreListener;
	}

	public void enable() {
		this.handleScrolls = true;
	}

	@Override
	public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
		super.onScrolled(recyclerView, dx, dy);
		LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
		if (layoutManager.getItemCount() >= (ROW_COUNT - 3) &&
				layoutManager.findFirstVisibleItemPosition() ==
						layoutManager.getItemCount() - layoutManager.getChildCount()) {

			if (handleScrolls) {
				recyclerView.removeOnScrollListener(this);
				if (loadMoreListener != null) loadMoreListener.onLoadMore();
			}
			handleScrolls = false;
		}
	}

	public interface OnLoadMoreListener {
		void onLoadMore();
	}
}
