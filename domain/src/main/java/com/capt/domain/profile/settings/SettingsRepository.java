package com.capt.domain.profile.settings;

import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.profile.settings.model.UpdateSettingsModel;

import rx.Observable;

/**
 * Created by root on 04.05.16.
 */
public interface SettingsRepository {
	Observable<SettingsModel> getSettings();
	Observable<SettingsModel> updateSettings (UpdateSettingsModel _model);
}
