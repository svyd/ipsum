package com.capt.authorization.sign_up.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.sign_up.SignUpFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.03.18..
 */
@PerFragment
@Subcomponent(modules = SignUpModule.class)
public interface SignUpComponent {
    void inject(SignUpFragment _fragment);
}
