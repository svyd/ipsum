package com.capt.video.map;

import com.capt.authorization.location.LocationInteractorManager;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by root on 05.04.16.
 */
public class MapPresenter extends MapContract.Presenter {

    private MapContract.View mView;
    private LocationInteractorManager mLocationManager;
    private LocationModel locationModel;

    @Inject
    public MapPresenter(MapContract.View _view, LocationInteractorManager _manager,
                        BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mLocationManager = _manager;
    }

    @Override
    public void initialize() {
        locationModel = mView.getCurrentLocation();
    }

    @Override
    public void onStop() {
        mLocationManager.unSubscribe();
    }

    @Override
    public void getAddress() {
        locationModel = mView.getCurrentLocation();
        locationModel.result.address = "No address available";
        mLocationManager.execute(locationModel, new LocationObserver(this));
    }

    @Override
    public void navigateToDescription() {
        mView.navigateToDescriptionScreen(locationModel);
    }

    private class LocationObserver extends BaseObserver<PredictionsModel> {
        public LocationObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            locationModel.result.address = predictionsModel.predictions[0].address;
            mView.setTitle(locationModel.result.address);
        }

    }

}
