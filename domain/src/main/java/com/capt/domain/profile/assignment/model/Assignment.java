package com.capt.domain.profile.assignment.model;

import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.domain.video.model.Quality;
import com.capt.domain.video.model.Video;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Svyd on 04.04.2016.
 */
public class Assignment implements DisplayableItem {
    @Expose
    @SerializedName("_id")
    public String id;

    @Expose
    @SerializedName("date")
    public String date;

    @Expose
    @SerializedName("city")
    public String city;

    @Expose
    @SerializedName("country")
    public String country;

    @Expose
    @SerializedName("title")
    public String title;

    @Expose
    @SerializedName("description")
    public String description;

    @Expose
    @SerializedName("length")
    public String length;

    @Expose
    @SerializedName("quality")
    public Quality quality;

    @Expose
    @SerializedName("price")
    public String price;

    @Expose
    @SerializedName("location")
    public double[] location;

    @Expose
    @SerializedName("creator")
    public Creator creator;

    @Expose
    @SerializedName("currency")
    public CurrencyModel currency;

    @Expose
    public String who;

    @Expose
    public String where;

    @Expose
    public String when;

    @Expose
    public String formattedQuality;

    public boolean stub;

    @Expose
    @SerializedName("video")
    public Video video;

    @Override
    public String getHeaderTitle() {
        return "Active Assignments";
    }

    @Override
    public String getHeaderDescription() {
        return "";
    }

    public static class Creator implements Serializable {
        @Expose
        @SerializedName("company")
        public String company;

        @Expose
        @SerializedName("logo")
        public String logo;

        public String getCompany() {
            return company;
        }

        public String getLogo() {
            return logo;
        }

    }

}
