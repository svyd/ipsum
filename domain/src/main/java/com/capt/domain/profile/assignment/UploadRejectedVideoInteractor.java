package com.capt.domain.profile.assignment;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.model.Video;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 06.05.2016.
 */
public class UploadRejectedVideoInteractor extends BasePostInteractor<Video> {

    private AssignmentsRepository mRepository;

    @Inject
    public UploadRejectedVideoInteractor(AssignmentsRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;

    }

    @Override
    protected Observable buildPostObservable(Video _data) {
        return mRepository.uploadRejectedVideo(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
