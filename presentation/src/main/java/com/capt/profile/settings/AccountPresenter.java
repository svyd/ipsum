package com.capt.profile.settings;

import android.os.Bundle;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.model.SettingsModel;

import javax.inject.Inject;

/**
 * Created by Svyd on 30.06.2016.
 */
public class AccountPresenter extends AccountContract.Presenter {

    private AccountContract.View mView;

    private SettingsModel mSettings;
    private int mCode;

    @Inject
    public AccountPresenter(AccountContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
    }

    @Override
    public void setArgs(Bundle _args) {
        mSettings = (SettingsModel) _args.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        mCode = _args.getInt(AccountSettingsActivity.CODE);
    }

    @Override
    public void initialize() {
        switch (mCode) {
            case AccountSettingsActivity.CODE_NOTIFICATIONS:
                mView.navigateToNotifications(mSettings);
                break;

            case AccountSettingsActivity.CODE_CURRENCY:
                mView.navigateToCurrency(mSettings);
                break;
        }
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
