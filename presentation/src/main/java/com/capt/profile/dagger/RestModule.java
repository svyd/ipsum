package com.capt.profile.dagger;

import com.capt.data.annotation.PerActivity;
import com.capt.data.video.video.VideoService;
import com.capt.data.video.video.VideoRepositoryImpl;
import com.capt.domain.video.video.VideoRepository;
import com.capt.domain.global.Constants;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Svyd on 29.03.2016.
 */
@Module
public class RestModule {

    @Provides
    @PerActivity
    VideoService provideVideoDownloadService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                     Retrofit _retrofit) {
        return _retrofit.create(VideoService.class);
    }

    @Provides
    @PerActivity
    VideoRepository provideVideoRepository(VideoRepositoryImpl _repository) {
        return _repository;
    }
}
