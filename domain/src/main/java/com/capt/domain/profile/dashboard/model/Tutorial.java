package com.capt.domain.profile.dashboard.model;

import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.Item;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 25.03.2016.
 */
public class Tutorial extends BaseItems {

    @Inject
    public Tutorial(){
        List<Item> tutorialItems = new ArrayList<>();
        tutorialItems.add(new Item("Welcome to CAPT", ""));
        tutorialItems.add(new Item("How assignments work", ""));
        tutorialItems.add(new Item("How rating works", ""));
        tutorialItems.add(new Item("How the marketplace works", ""));
        tutorialItems.add(new Item("Recording tips & tricks", ""));

    }

    @Override
    public String getHeaderTitle() {
        return "Tutorial";
    }

    @Override
    public String getHeaderDescription() {
        return "";
    }
}
