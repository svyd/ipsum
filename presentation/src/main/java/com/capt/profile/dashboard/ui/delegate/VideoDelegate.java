package com.capt.profile.dashboard.ui.delegate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.capt.R;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.profile.dashboard.ui.adapter.SectioningAdapter;
import com.capt.profile.dashboard.ui.adapter.ViewTypeConstants;
import com.capt.profile.dashboard.ui.delegate.base.BaseDelegate;
import com.capt.video.trim.time_util.TimeUtilImpl;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 25.03.2016.
 */
public class VideoDelegate extends BaseDelegate {

    private LayoutInflater mInflater;
    private Context mContext;
    private TimeUtilImpl mTimeUtil;

    @Inject
    public VideoDelegate(TimeUtilImpl _timeUtil, Context _context) {
        super(ViewTypeConstants.VIEW_TYPE_VIDEO);
        mContext = _context;
        mInflater = LayoutInflater.from(_context);
        mTimeUtil = _timeUtil;
    }

    @Override
    public boolean isForViewType(@NonNull List<DisplayableItem> items, int position) {
        return items.size() != position && items.get(position) instanceof Video;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.list_item_video, parent, false);
        return new VideoViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull List<DisplayableItem> items, int position, @NonNull RecyclerView.ViewHolder holder) {
        VideoViewHolder videoHolder = (VideoViewHolder) holder;
        Video video = (Video) items.get(position);

        videoHolder.tvPrice.setText(video.currency.getSymbol() + video.price);
        videoHolder.tvDescription.setText(video.description);
        videoHolder.tvName.setText(video.title);
        String duration;
        if (video.isDraft() || video.isUploading()){
            duration = getDraftProperties(Math.round(Double.parseDouble(video.duration)), video.path);
        } else {
            duration = getFormattedProperties(Math.round(Double.parseDouble(video.duration)), video.size);
        }

        videoHolder.tvDuration.setText(duration);
        Glide.with(mContext)
                .load(video.thumbnail)
                .fitCenter()
                .centerCrop()
                .into(videoHolder.ivThumbNail);
    }

    private String getDraftProperties(long _duration, String _path) {
        File file = new File(_path);
        long size = file.length();

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    private String getFormattedProperties(long _duration, String _size) {
        long size = Long.parseLong(_size);

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    public class VideoViewHolder extends SectioningAdapter.ViewHolder implements View.OnClickListener{

        @Bind(R.id.cardView_LR)
        CardView cvVideo;

        @Bind(R.id.ivThumbnail_LIV)
        public ImageView ivThumbNail;

        @Bind(R.id.tvDescription_LIV)
        public TextView tvDescription;

        @Bind(R.id.tvName_LIV)
        public TextView tvName;

        @Bind(R.id.tvPrice_LIV)
        public TextView tvPrice;

        @Bind(R.id.tvDuration_LIV)
        public TextView tvDuration;

        public VideoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cvVideo.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onDelegateClick(ViewTypeConstants.VIEW_TYPE_VIDEO, getLayoutPosition());
        }
    }
}
