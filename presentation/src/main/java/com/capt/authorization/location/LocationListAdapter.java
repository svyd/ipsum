package com.capt.authorization.location;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.capt.R;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.authorization.location.model.PredictionsModel;

import javax.inject.Inject;

/**
 * Created by Svyd on 17.03.2016.
 */
@PerFragment
public class LocationListAdapter extends BaseAdapter {

    private PredictionsModel.Prediction[] mData;
    private Context mContext;

    @Inject
    public LocationListAdapter(Context _context) {
        mContext = _context;
    }

    public void setData(PredictionsModel.Prediction[] _data) {
        mData = _data;
    }

    @Override
    public int getCount() {
        if (mData != null) {
            return mData.length;
        }
        return 0;
    }

    @Override
    public PredictionsModel.Prediction getItem(int position) {
        return mData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.location_list_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvLocation = (TextView) convertView.findViewById(R.id.tvLocation_LLR);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvLocation.setText(getItem(position).description);

        return convertView;
    }

    private class ViewHolder {
        private TextView tvLocation;
    }
}
