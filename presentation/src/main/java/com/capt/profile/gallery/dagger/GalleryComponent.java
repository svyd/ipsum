package com.capt.profile.gallery.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.gallery.GalleryFragment;
import com.capt.video.trim.dagger.FFMpegModule;

import dagger.Subcomponent;

/**
 * Created by Svyd on 4/23/16.
 */
@PerFragment
@Subcomponent(modules = {GalleryModule.class})
public interface GalleryComponent {
    void inject(GalleryFragment _fragment);
}
