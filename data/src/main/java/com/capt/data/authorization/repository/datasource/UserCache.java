package com.capt.data.authorization.repository.datasource;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UserModel;

import rx.Observable;


/**
 * Created by Svyd on 4/22/16.
 */
public interface UserCache {
    Observable<UserModel> cacheUser(UserModel _model);
    Observable<UserModel> getUser();
    Observable<SuccessModel> clear();
}
