package com.capt.domain.profile.payout;

import com.capt.domain.currency.model.CurrencyModel;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by rMozes on 8/3/16.
 */
public class PayoutModel implements Serializable {

    @Expose
    private String title;

    @Expose
    private String description;

    @Expose
    private String price;

    @Expose
    private String updatedAt;

    @Expose
    private String type;

    @Expose
    private CurrencyModel currency;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CurrencyModel getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyModel currency) {
        this.currency = currency;
    }
}
