package com.capt.profile.marketplace.recent;

import android.os.Handler;
import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.video.model.Video;
import com.capt.profile.marketplace.base.BaseMarketplaceListContract;
import com.capt.profile.marketplace.base.BaseMarketplaceListPresenter;
import com.capt.video.upload_manager.VideoUploadManager;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 25.05.2016.
 */
public class RecentPresenter extends BaseMarketplaceListPresenter {

    private static final String TAG = RecentPresenter.class.getSimpleName();
    private BaseInteractor mPendingInteractor;
    private VideoUploadManager mVideoUploadManager;
    private BasePostInteractor<Video> mSaveDraftInteractor;

    @Inject
    public RecentPresenter(BaseMarketplaceListContract.View _view,
                           BasePostInteractor<RequestParamsModel> _interactor,
                           String _marketplaceType,
                           @Named(Constants.NamedAnnotation.SAVE_DRAFT_INTERACTOR)
                           BasePostInteractor<Video> _saveDraftInteractor,
                           @Named(Constants.NamedAnnotation.PENDING_VIDEO_INTERACTOR)
                           BaseInteractor _pendingInteractor,
                           VideoUploadManager _uploadManager, BaseExceptionDelegate _delegate) {
        super(_view, _interactor, _marketplaceType, _delegate);
        mPendingInteractor = _pendingInteractor;
        mVideoUploadManager = _uploadManager;
        mSaveDraftInteractor = _saveDraftInteractor;
    }

    @Override
    public void onUploadCompleted(int _position, Video _response) {
        mView.replaceItem(_position, _response);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPendingInteractor.unSubscribe();
        mSaveDraftInteractor.unSubscribe();
    }

    @Override
    public void onUploadError(int _position) {
        mData.get(_position).setError(true);
        mData.get(_position).setCurrentlyUploading(false);
        mView.setError(_position);
        mView.notifyChanges();
    }

    @Override
    public void onProgress(int _position, int _progress) {
        if (mData == null)
            return;
        mData.get(_position).progress = _progress;
        mView.setProgress(_position, _progress);
    }

    @Override
    public void initialize() {
        mRequestModel = new RequestParamsModel(mMarketplaceType, 10, 0);
        mPendingInteractor.execute(new PendingObserver(this));
    }

    @Override
    public void onCancelClick(int _position) {
        Video video = mData.get(_position);
        video.setDraft(true);
        video.setUploading(false);
        video.setError(false);
        video.draftId = System.currentTimeMillis();
        mSaveDraftInteractor.execute(video, new SaveDraftObserver(this));
        mView.removeItem(_position);
        mView.onSaveDraft(video);
        mVideoUploadManager.cancelUpload(video.id);
    }

    @Override
    public void onRetryClick(int _position) {
        mVideoUploadManager.retry(mData.get(_position).id);
        mData.get(_position).setError(false);
        mView.setPending(_position);
        final int newPosition;
        if (_position != 0) {
            if (mData.get(0).isCurrentlyUploading()) {
                mView.swap(_position, 1);
                newPosition = 1;
            } else {
                mView.swap(_position, 0);
                newPosition = 0;
            }
        } else {
            newPosition = -1;
        }
        mView.scrollTo(newPosition);
    }

    @Override
    public void onStartUploading(int _position) {
        mData.get(_position).setCurrentlyUploading(true);
        mData.get(_position).setError(false);
        mView.swap(_position, 0);
    }

    class SaveDraftObserver extends BaseObserver<Long> {
        public SaveDraftObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(Long aLong) {
            Log.d(TAG, "onNext() called with: " + "aLong = [" + aLong + "]");
        }
    }

    class PendingObserver extends BaseObserver<List<Video>> {
        public PendingObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onNext(List<Video> _videos) {
            if(!_videos.isEmpty()) {
                onVideosLoaded(_videos);
                setShouldAddItems(true);
            }
            getInitialData();
        }
    }
}
