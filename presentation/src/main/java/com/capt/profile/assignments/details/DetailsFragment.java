package com.capt.profile.assignments.details;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.capt.R;
import com.capt.base.BaseToolbarFragment;
import com.capt.data.base.DateTimeUtility;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.profile.assignments.details.capt_dialog.CaptDialogFragment;
import com.capt.profile.assignments.details.dagger.DetailsActivityComponent;
import com.capt.profile.assignments.details.dagger.DetailsComponent;
import com.capt.profile.assignments.details.dagger.DetailsModule;
import com.capt.profile.assignments.details.map.DetailsMapFragment;
import com.capt.profile.assignments.details.overview.OverviewFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by root on 20.04.16.
 */
public class DetailsFragment extends BaseToolbarFragment implements DetailsContract.View, OnMapReadyCallback, GoogleMap.OnMapClickListener {

    @Inject
    DetailsContract.Presenter mPresenter;

    @Inject
    DateTimeUtility mDateUtility;

    @Bind(R.id.tv_location_AH)
    TextView tvLocation;
    @Bind(R.id.rl_location_AH)
    RelativeLayout rlLocation;
    @Bind(R.id.tv_title_AH)
    TextView tvTitle;
    @Bind(R.id.tv_description_AH)
    TextView tvDescription;
    @Bind(R.id.tv_price_AH)
    TextView tvPrice;
    @Bind(R.id.tv_who_AH)
    TextView tvWho;
    @Bind(R.id.tv_when_AH)
    TextView tvWhen;
    @Bind(R.id.tv_where_AH)
    TextView tvWhere;
    @Bind(R.id.tv_what_AH)
    TextView tvWhat;
    @Bind(R.id.tv_how_much_AH)
    TextView tvHowMuch;
    @Bind(R.id.tv_min_quality_AH)
    TextView tvMinQuality;
    @Bind(R.id.btnRed_FAD)
    Button btnRed;
    @Bind(R.id.tvNo_FAD)
    TextView tvNo;

    public static final int REQUEST_ACCEPT_ASSIGNMENT = 9875;

    private DetailsComponent mComponent;
    private GoogleMap mMap;

    public static DetailsFragment newInstance(Assignment _data, String _type) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, _data);
        args.putString(Constants.Extra.EXTRA_STRING, _type);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_assignment_details);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponent();
        inject();
        showToolbar();
        checkExtra();
        addMapFragment();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        if (!getArguments().containsKey(Constants.Extra.EXTRA_STRING))
            throw new RuntimeException("You have to provide a flow!");

        mComponent = getComponent(DetailsActivityComponent.class)
                .createAssignmentDetailsComponent(new DetailsModule(this,
                        getArguments().getString(Constants.Extra.EXTRA_STRING)));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void checkExtra() {
        if (getArguments().containsKey(Constants.Extra.EXTRA_STRING)) {
            String type = getArguments().getString(Constants.Extra.EXTRA_STRING);
            mPresenter.setType(type);
        }

        if (getArguments().containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            Assignment model = (Assignment) getArguments().getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
            mPresenter.setAssignmentModel(model);
        }
    }

    private void showToolbar() {
        getToolbarManager().showToolbar();
    }


    private void addMapFragment() {
        SupportMapFragment fragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_frame_AH, fragment);
        fragmentTransaction.commit();
        fragment.getMapAsync(DetailsFragment.this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setUpView(Assignment _model) {
        getActivity().setTitle(_model.creator.getCompany());
        tvTitle.setText(_model.title);
        tvDescription.setText(_model.description);
        tvPrice.setText(_model.currency.getSymbol() + _model.price);
        tvWho.setText(_model.creator.getCompany());
        tvWhat.setText(_model.title);
        tvWhen.setText(mDateUtility.formatDate(_model.date, true));
        tvHowMuch.setText(_model.currency.getSymbol() + _model.price);
        tvMinQuality.setText(String.valueOf(_model.quality.getWidth()) + "p, " + String.valueOf(_model.quality.getFps()) + "fps");
    }

    @Override
    public void setForGrabs() {
        btnRed.setText(R.string.accept);
        tvNo.setText(R.string.no_thanks);
    }

    @Override
    public void setInProgress() {
        btnRed.setText(R.string.capt_title_FCL);
        tvNo.setText(R.string.cancel_DFPI);
    }

    @Override
    public void navigateToMapScreen(double lat, double lon) {

        getFragmentNavigator().replaceFragmentWithBackStack(DetailsMapFragment.newInstance(lat, lon));
    }

    @Override
    public void showMessage(String _message) {
        super.showMessage(_message);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.rl_location_AH, R.id.btnRed_FAD, R.id.tvNo_FAD})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_location_AH:
                mPresenter.onLocationClick();
                break;
            case R.id.btnRed_FAD:
                mPresenter.onRedBtnClicked();
                break;
            case R.id.tvNo_FAD:
                mPresenter.onNoThanksClick();
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();
    }

    private void setUpMap() {
        // Hide the zoom controls as the button panel will cover it.
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setOnMapClickListener(this);
        mPresenter.setLocation();
    }

    @Override
    public void addMarkerToMap(final LatLng location) {
        mMap.clear();

        mMap.addMarker(new MarkerOptions()
                .position(location)
                .title("Location")
                .snippet("CAPT")
                .draggable(false));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 10.0f));
    }

    @Override
    public void setAddress(String _address) {
        tvLocation.setText(_address);
        tvWhere.setText(_address);
    }

    @Override
    public void showCaptDialog(Assignment _assignment) {
        CaptDialogFragment.showWithTarget(this, REQUEST_ACCEPT_ASSIGNMENT, _assignment);
    }

    @Override
    public void navigateBackToAssignments() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().onBackPressed();

//        getFragmentNavigator().replaceFragmentWithoutBackStack(AssignmentsTabFragment.newInstance(Constants.AssignmentTypes.FOR_GRABS));
//        Intent intent = new Intent(getActivity(), ProfileActivity.class);
//        intent.putExtra(Constants.Extra.EXTRA_STRING, Constants.Profile.ASSIGNMENTS);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        getActivity().finishWithResult();
    }

    @Override
    public void navigateToOverviewScreen(Assignment _model) {
        getActivity().setResult(Activity.RESULT_OK);
        getFragmentNavigator().replaceFragmentWithoutBackStack(OverviewFragment.newInstance(_model));
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mPresenter.onLocationClick();
    }
}
