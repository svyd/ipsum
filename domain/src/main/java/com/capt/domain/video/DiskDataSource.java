package com.capt.domain.video;

import com.capt.domain.video.model.Video;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 14.04.2016.
 */
public interface DiskDataSource {
    Observable<List<Video>> getAll();
    Observable<List<Video>> getAllUploading();
    Observable<Video> get(String _id);
    Observable<Long> add(Video _model);
    Observable<Long> update(Video _model);
    Observable<Video> updatePending(Video _model);
    Observable<Integer> deleteByDraftId(String _id);
    Observable<Integer> deleteById(String _id);
}
