package com.capt.domain.profile.dashboard.model.base;

import java.io.Serializable;

/**
 * Created by Svyd on 25.03.2016.
 */
public class Item implements Serializable {
    public Item(String _left, String _right) {
        left = _left;
        right = _right;
    }

    public String left;
    public String right;
}
