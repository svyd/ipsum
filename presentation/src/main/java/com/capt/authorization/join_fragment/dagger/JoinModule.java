package com.capt.authorization.join_fragment.dagger;

import com.capt.authorization.join_fragment.JoinContract;
import com.capt.authorization.join_fragment.JoinPresenter;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.authorization.interactor.CodeInteractor;
import com.capt.domain.authorization.interactor.SignInInteractor;
import com.capt.domain.authorization.interactor.SocNetAuthInteractor;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.facebook.CallbackManager;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.02.23..
 */
@Module
public class JoinModule {

    private JoinContract.View mView;

    public JoinModule(JoinContract.View _view) {
        mView = _view;
    }

    @Provides @PerFragment
    protected JoinContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    JoinContract.Presenter providePresenter(JoinPresenter _presenter) {
        return _presenter;
    }

    @Provides @PerFragment
    @Named(Constants.NamedAnnotation.SOC_NET_AUTH_INTERACTOR)
    BasePostInteractor<SocNetAuthModel> provideSocNetAuthInteractor(SocNetAuthInteractor _interactor) {
        return _interactor;
    }



    @Provides @PerFragment
    @Named(Constants.NamedAnnotation.SIGN_IN_INTERACTOR)
    BasePostInteractor<SignInModel> provideSignInInteractor(SignInInteractor _interactor) {
        return _interactor;
    }

    @Provides @PerFragment
    CallbackManager provideCallbackManager() {
        return CallbackManager.Factory.create();
    }

    @Provides @PerFragment
    TwitterAuthClient provideTwitterClient() {
        return new TwitterAuthClient();
    }
}
