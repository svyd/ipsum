package com.capt.authorization.auth_strategy;

import android.content.Intent;

import com.capt.base.BaseFragment;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.global.Constants;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.24..
 */
public class FacebookStrategy implements AuthorizationStrategy {

    protected CallbackManager mCallbackManager;
    protected WeakReference<CaptCallback> mLogInCallback;

    @Inject
    public FacebookStrategy(CallbackManager _manager) {
        mCallbackManager = _manager;
        LoginManager.getInstance().registerCallback(mCallbackManager, mCallback);
    }

    @Override
    public void logIn(BaseFragment _fragment, CaptCallback _callback) {
        mLogInCallback = new WeakReference<>(_callback);
        List<String> permissions = new ArrayList<>();
        LoginManager.getInstance().logInWithPublishPermissions(_fragment, permissions);
    }

    @Override
    public void onActivityResult(int _resultCode, int _requestCode, Intent _intent) {
        mCallbackManager.onActivityResult(_requestCode, _resultCode, _intent);
    }

    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            if (mLogInCallback.get() == null)
                return;

            SocNetAuthModel.Builder model = new SocNetAuthModel.Builder()
                    .setAccessToken(loginResult.getAccessToken().getToken())
                    .setNetwork(Constants.AuthType.FACEBOOK);

            mLogInCallback.get().onSuccess(model);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {
            if (mLogInCallback.get() == null)
                return;

            mLogInCallback.get().onError(error);
        }
    };
}
