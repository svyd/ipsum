package com.capt.authorization.sign_up.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.sign_up.SignUpContract;
import com.capt.authorization.sign_up.SignUpPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.03.18..
 */
@Module
public class SignUpModule {

    private SignUpContract.View mView;

    public SignUpModule(SignUpContract.View _view) {
        mView = _view;
    }

    @Provides @PerFragment
    protected SignUpContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    protected SignUpContract.Presenter providePresenter(SignUpPresenterImpl _presenter) {
        return _presenter;
    }
}
