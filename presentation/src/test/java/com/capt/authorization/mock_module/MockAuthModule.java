//package com.capt.authorization.mock_module;
//
//import com.capt.authorization.AuthorizationContract;
//import com.capt.authorization.dagger.AuthorizationModule;
//import com.capt.domain.interactors.AuthorizationInteractor;
//import com.capt.domain.interactors.base.BaseInteractor;
//
//import org.mockito.Mockito;
//
///**
// * Created by richi on 2016.02.27..
// */
//public class MockAuthModule extends AuthorizationModule {
//
//    public MockAuthModule(AuthorizationContract.View _view) {
//        super(_view);
//    }
//
//    @Override
//    protected BaseInteractor provideAuthorizationInteractor(AuthorizationInteractor _interactor) {
//        return Mockito.mock(BaseInteractor.class);
//    }
//}
