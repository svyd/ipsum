package com.capt.domain.category;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 22.04.2016.
 */
public class VideoCategoryInteractor extends BaseInteractor {

    private VideoCategoryRepository mRepository;

    @Inject
    public VideoCategoryInteractor(VideoCategoryRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildGetObserver() {
        return mRepository.getCategories();
    }
}
