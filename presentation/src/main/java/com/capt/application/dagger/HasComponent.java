package com.capt.application.dagger;

/**
 * Created by richi on 2016.03.16..
 */
public interface HasComponent<C> {
    C getComponent();
}
