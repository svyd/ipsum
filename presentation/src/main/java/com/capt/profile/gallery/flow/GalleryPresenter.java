package com.capt.profile.gallery.flow;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.gallery.model.GalleryModel;
import com.capt.profile.gallery.GalleryContract;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 4/23/16.
 */
public class GalleryPresenter extends AbstractGalleryPresenter {

    @Inject
    public GalleryPresenter(GalleryContract.View _view,
                            @Named(Constants.NamedAnnotation.GALLERY_INTERACTOR)
                            BaseInteractor _interactor,
                            BaseExceptionDelegate _delegate) {
        super(_view, _interactor, _delegate);
    }

    @Override
    public void onItemClick(GalleryModel _model) {
        getView().navigateToTrimFragment(_model.getPath());
    }

}
