package com.capt.video.trim.view_presenter.flow;

import android.os.Bundle;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.trim.ffmpeg_manager.FFMPEGManager;
import com.capt.video.trim.view_presenter.TrimContract;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.29..
 */
public class TrimPresenter extends AbstractTrimPresenter {

    private Video mData;

    @Inject
    public TrimPresenter(TrimContract.View _view, FFMPEGManager _ffmpegManager,
                         BaseExceptionDelegate _delegate) {
        super(_view, _ffmpegManager, _delegate);
    }

    @Override
    protected Video getData() {
        return mData;
    }

    @Override
    public void setData(Bundle _args) {
        if (_args.containsKey(Constants.BUNDLE_VIDEO)) {
            mData = (Video) _args.getSerializable(Constants.BUNDLE_VIDEO);
        } else {
            throw new IllegalStateException("Arguments should contain a video model");
        }
    }

    @Override
    protected void doOnNext(Video _video) {
        getData().sourcePath = _video.sourcePath;
        getData().path = _video.path;
        getData().thumbnail = _video.thumbnail;
        getData().quality = _video.quality;
        getData().duration = String.valueOf(((long) getTrimmedDuration() / 1000));
        getData().formattedSize = (formatSizeOfFile(_video.path));
        getView().navigateToDescription(getData());
    }

}
