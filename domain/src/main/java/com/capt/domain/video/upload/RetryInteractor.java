package com.capt.domain.video.upload;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 31.05.2016.
 */
public class RetryInteractor extends BasePostInteractor<String> {

    private VideoUploadRepository mRepository;

    @Inject
    public RetryInteractor(VideoUploadRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);

        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.retry(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
