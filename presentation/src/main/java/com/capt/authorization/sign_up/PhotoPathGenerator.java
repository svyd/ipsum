package com.capt.authorization.sign_up;

import android.content.Context;

import com.bumptech.glide.Glide;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by richi on 2016.03.19..
 */
@Singleton
public class PhotoPathGenerator {

    private Context mContext;

    private String mPhotoPath;

    @Inject
    public PhotoPathGenerator(Context _context) {
        mContext = _context;
        generatePhotoPath();
    }

    private void generatePhotoPath() {
        File externalDir = mContext.getExternalCacheDir();

        File photoPath = new File(externalDir, "avatar.jpg");
        mPhotoPath = photoPath.getAbsolutePath();
    }

    public String getAvatarPath() {
        return mPhotoPath;
    }
}
