package com.capt.video.overview.dagger;

import android.content.SharedPreferences;

import com.capt.base.utility.DateTimeUtilityImpl;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.annotation.PerFragment;
import com.capt.data.assignment.AssignmentsMapper;
import com.capt.data.assignment.AssignmentsRepositoryImpl;
import com.capt.data.assignment.AssignmentsService;
import com.capt.data.base.DateTimeUtility;
import com.capt.data.base.TypeMapper;
import com.capt.data.video.video.VideoRepositoryImpl;
import com.capt.data.video.video.VideoService;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.AssignmentsRepository;
import com.capt.domain.profile.assignment.DeleteRejectedAssignmentInteractor;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.video.draft.DeleteDraftInteractor;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.upload.PayPalInteractor;
import com.capt.domain.video.upload.UploadForAssignmentInteractor;
import com.capt.domain.video.video.DeleteVideoInteractor;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.draft.SaveDraftInteractor;
import com.capt.domain.video.video.VideoRepository;
import com.capt.video.overview.OverviewContract;
import com.capt.video.overview.flow.AbstractPresenter;
import com.capt.video.overview.flow.AssignmentsPresenter;
import com.capt.video.overview.flow.DetailsPresenter;
import com.capt.video.Flow;
import com.capt.video.overview.flow.DraftDetailsPresenter;
import com.capt.video.overview.flow.PurchasedPresenter;
import com.capt.video.overview.flow.RejectedPresenter;
import com.capt.video.overview.flow.VideoUploadPresenter;
import com.capt.data.base.TimeUtil;
import com.capt.video.trim.time_util.TimeUtilImpl;
import com.capt.video.upload_manager.VideoUploadManager;


import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Svyd on 10.04.2016.
 */
@Module
public class OverviewModule {

    private OverviewContract.View mView;
    private int mFlow;

    public OverviewModule(OverviewContract.View _view, int _flow) {
        mView = _view;
        mFlow = _flow;
    }

    @Provides
    @PerFragment
    OverviewContract.View provideView() {
        return mView;
    }


    @Provides
    @PerFragment
    VideoService provideDeleteVideoService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                           Retrofit _retrofit) {
        return _retrofit.create(VideoService.class);
    }

    @Provides
    @PerFragment
    VideoRepository provideDeleteVideoRepository(VideoRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.VIDEO_DELETE_INTERACTOR)
    BasePostInteractor<String> provideDeleteVideoInteractor(DeleteVideoInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    protected BasePostInteractor<Video> provideDraftInteractor(SaveDraftInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.DRAFT_DELETE_INTERACTOR)
    protected BasePostInteractor<String> provideDeleteDraftInteractor(DeleteDraftInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    AssignmentsService provideAssignmentsService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                 Retrofit _retrofit) {
        return _retrofit.create(AssignmentsService.class);
    }


    @Provides
    @PerFragment
    DateTimeUtility provideDateTimeUtility(DateTimeUtilityImpl _utility) {
        return _utility;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Mapper.ASSIGNMENTS_MAPPER)
    TypeMapper<List<Assignment>, List<Assignment>> provideAssignmentsMapper(AssignmentsMapper _mapper) {
        return _mapper;
    }

    @Provides
    @PerFragment
    AssignmentsRepository provideAssignmentsRepository(AssignmentsRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.DELETE_REJECTED_INTERACTOR)
    BasePostInteractor<String> provideDeleteRejectedInteractor(DeleteRejectedAssignmentInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    @Named(Constants.PendingVideoConstants.PAY_PAL_INTERACTOR)
    protected BasePostInteractor<PayPalVideoModel> providePayPalInteractor(PayPalInteractor _interactor) {
        return _interactor;
    }


    @Provides
    @PerFragment
    AbstractPresenter providePresenter(@Named(Constants.NamedAnnotation.ASSIGNMENT_CACHE_CLEAR)
                                       BaseInteractor _clearAssignmentInteractor,
                                       @Named(Constants.PendingVideoConstants.UPLOAD_FOR_ASSIGNMENT)
                                       BasePostInteractor<Assignment> _assignmentVideoInteractor,
                                       @Named(Constants.NamedAnnotation.DRAFT_DELETE_INTERACTOR)
                                       BasePostInteractor<String> _draftInteractor,
                                       @Named(Constants.NamedAnnotation.VIDEO_DELETE_INTERACTOR)
                                       BasePostInteractor<String> _deleteInteractor,
                                       @Named(Constants.NamedAnnotation.DELETE_REJECTED_INTERACTOR)
                                       BasePostInteractor<String> _deleteRejectedInteractor,
                                       @Named(Constants.PendingVideoConstants.PAY_PAL_INTERACTOR)
                                       BasePostInteractor<PayPalVideoModel> _payPalInteractor,
                                       TimeUtilImpl _timeUtil,
                                       BaseInteractor _userInteractor,
                                       VideoUploadManager _manager,
                                       SharedPreferences _preferences,
                                       DateTimeUtility _dateUtility,
                                       BasePostInteractor<Video> _interactor,
                                       BaseExceptionDelegate _delegate) {
        switch (mFlow) {
            case Flow.VIDEO_UPLOAD:
                return new VideoUploadPresenter(_timeUtil, _manager, _preferences, mView,
                        _interactor, _delegate, _payPalInteractor);
            case Flow.ASSIGNMENT_DETAILS:
                return new AssignmentsPresenter(_clearAssignmentInteractor, _assignmentVideoInteractor,
                        _dateUtility, _timeUtil, mView, _delegate, _payPalInteractor);
            case Flow.DETAILS:
                return new DetailsPresenter(_deleteInteractor, _timeUtil, mView, _delegate, _payPalInteractor);
            case Flow.DRAFT:
                return new DraftDetailsPresenter(_draftInteractor, _manager, _preferences,
                        _timeUtil, mView, _delegate, _payPalInteractor);
            case Flow.ASSIGNMENT_REJECTED:
                return new RejectedPresenter(_deleteRejectedInteractor, _userInteractor, mView,
                        _timeUtil, _delegate, _payPalInteractor);
            case Flow.ASSIGNMENT_PURCHASED:
                return new PurchasedPresenter(_userInteractor, mView, _timeUtil, _delegate, _payPalInteractor);
            default:
                throw new IllegalArgumentException("No such flow: " + mFlow);
        }
    }

    @Provides
    @PerFragment
    TimeUtil provideTimeUtil(TimeUtilImpl _util) {
        return _util;
    }

    @Provides
    @PerFragment
    SharedPreferences provideSharedPreferences(SharedPreferences _settings) {
        return _settings;
    }
}
