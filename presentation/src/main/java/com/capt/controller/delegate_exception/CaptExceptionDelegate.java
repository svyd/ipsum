package com.capt.controller.delegate_exception;


import javax.inject.Inject;


/**
 * Created by Svyd on 7/28/16.
 */
public class CaptExceptionDelegate extends BaseExceptionDelegate<Throwable> {

    @Inject
    public CaptExceptionDelegate(BaseExceptionDelegate _delegate) {
        mDelegate = _delegate;
    }

    @Override
    public void handleException(Throwable _exception) {
        mDelegate.onError(_exception);
    }
}
