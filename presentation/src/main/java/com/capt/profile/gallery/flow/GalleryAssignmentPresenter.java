package com.capt.profile.gallery.flow;

import android.os.Bundle;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.gallery.model.GalleryModel;
import com.capt.domain.video.model.Video;
import com.capt.profile.gallery.GalleryContract;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Svyd on 17.05.2016.
 */
public class GalleryAssignmentPresenter extends AbstractGalleryPresenter {

    private Assignment mAssignment;

    @Inject
    public GalleryAssignmentPresenter(GalleryContract.View _view,
                                      @Named(Constants.NamedAnnotation.GALLERY_INTERACTOR)
                                      BaseInteractor _interactor,
                                      BaseExceptionDelegate _delegate) {
        super(_view, _interactor, _delegate);
    }

    @Override
    public void setArgs(Bundle _args) {
        if (_args.containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            mAssignment = (Assignment) _args.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        } else {
            throw new IllegalArgumentException(GalleryAssignmentPresenter.class.getSimpleName() + " should be provided with an assignment");
        }
    }

    @Override
    public void onItemClick(GalleryModel _model) {
        Video video = new Video();
        video.path = _model.getPath();
        mAssignment.video = video;
        getView().startAssignmentFlow(mAssignment);
    }
}
