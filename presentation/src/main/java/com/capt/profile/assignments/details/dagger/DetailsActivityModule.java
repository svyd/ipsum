package com.capt.profile.assignments.details.dagger;

import com.capt.base.BaseActivity;
import com.capt.base.utility.DateTimeUtilityImpl;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.data.annotation.PerActivity;
import com.capt.data.base.DateTimeUtility;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 28.04.2016.
 */
@Module
public class DetailsActivityModule {

    private BaseActivity mActivity;

    public DetailsActivityModule(BaseActivity _activity) {
        mActivity = _activity;
    }

    @Provides
    @PerActivity
    protected BaseActivity provideBaseActivity() {
        return mActivity;
    }

    @Provides @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }

    @Provides
    @PerActivity
    DateTimeUtility provideDateTimeUtility(DateTimeUtilityImpl _utility) {
        return _utility;
    }
}
