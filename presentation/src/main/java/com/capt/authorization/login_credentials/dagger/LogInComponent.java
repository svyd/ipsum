package com.capt.authorization.login_credentials.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.login_credentials.LogInFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.03.20..
 */
@PerFragment
@Subcomponent(modules = LogInModule.class)
public interface LogInComponent {
    void inject(LogInFragment _fragment);
}
