package com.capt.profile.gallery;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.CaptRecycleViewItemListener;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.gallery.model.GalleryModel;
import com.capt.profile.ProfileActivity;
import com.capt.profile.gallery.dagger.GalleryComponent;
import com.capt.profile.gallery.dagger.GalleryModule;
import com.capt.video.VideoActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by Svyd on 4/23/16.
 */
@RuntimePermissions
public class GalleryFragment extends BaseToolbarFragment
        implements GalleryContract.View,
        CaptRecycleViewItemListener<GalleryModel>,
        SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.rvGallery_FG)
    RecyclerView rvGallery;

    @Bind(R.id.tvEmptyView_FG)
    TextView tvEmptyView;

    @Bind(R.id.srlGallery_FG)
    SwipeRefreshLayout srlGallery;

    @Inject
    GalleryContract.Presenter mPresenter;

    @Inject
    GalleryAdapter mAdapter;

    @Inject
    RecyclerView.LayoutManager mLayoutManager;

    GalleryComponent mComponent;

    private boolean isItemSelected;

    public static BaseFragment newInstance(Bundle _args) {

        GalleryFragment fragment = new GalleryFragment();

        fragment.setArguments(_args);
        return fragment;
    }


    @Override
    protected void setUpActionBar(ActionBar actionBar) {
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_gallery);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        isItemSelected = false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponent();
        inject();
        initUi();
        setUpToolbar();
        GalleryFragmentPermissionsDispatcher.showWritePermissionWithCheck(this);
        mPresenter.setArgs(getArguments());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        GalleryFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void startGallery() {
        mPresenter.initialize();
    }

    //------------------------------Write external storage---------------------------------------
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void showWritePermission() {
        startGallery();
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void showWritePermissionRational(final PermissionRequest request) {
        new AlertDialog.Builder(getActivity(), R.style.CaptAlertDialogStyle)
                .setTitle(R.string.permission)
                .setMessage(R.string.gallery_rational)
                .setPositiveButton(R.string.allow, (dialog, which) -> {
                    request.proceed();
                })
                .setNegativeButton(R.string.deny, (dialog, which) -> {
                    request.cancel();
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void showWritePermissionDenied() {
        tvEmptyView.setVisibility(View.VISIBLE);
        tvEmptyView.setText(R.string.gallery_turn_on_permission);
        Toast.makeText(getActivity(), R.string.gallery_deny, Toast.LENGTH_SHORT)
                .show();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void showWritePermissionNeverAsk() {
        tvEmptyView.setVisibility(View.VISIBLE);
        tvEmptyView.setText(R.string.gallery_turn_on_permission);
        Toast.makeText(getActivity(), R.string.gallery_never_ask_again, Toast.LENGTH_SHORT)
                .show();
    }
    //------------------------------Write external storage---------------------------------------

    private void initComponent() {
        if (mComponent != null)
            return;

        if (getArguments().containsKey(Constants.FLOW)) {
            int flow = getArguments().getInt(Constants.FLOW);

            mComponent = getAppComponent()
                    .createGalleryComponent(new GalleryModule(this, this, this, flow));
        } else {
            throw new IllegalArgumentException(GalleryFragment.class.getSimpleName() + " should contain flow");
        }
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void initUi() {
        rvGallery.setLayoutManager(mLayoutManager);
        rvGallery.setAdapter(mAdapter);
        srlGallery.setOnRefreshListener(this);
        srlGallery.setColorSchemeColors(R.color.red_capture);
    }

    private void setUpToolbar() {
        getToolbarManager().showToolbar();
        getActivity().setTitle(R.string.gallery_title);
    }

    @Override
    public void finish() {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Constants.Extra.EXTRA_STRING, Constants.Profile.ASSIGNMENTS);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void showGalleryData(List<GalleryModel> _data) {
        mAdapter.setData(_data);
    }

    @Override
    public void showEmptyView() {
        tvEmptyView.setVisibility(View.VISIBLE);
        tvEmptyView.setText(R.string.gallery_empty_view);
    }

    @Override
    public void hideEmptyView() {
        if (tvEmptyView != null) {
            tvEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void startAssignmentFlow(Assignment _assignment) {
        VideoActivity.startAssignmentCapt(_assignment, getActivity());
    }

    @Override
    public void hideRefresh() {
        if (srlGallery.isRefreshing()) {
            srlGallery.setRefreshing(false);
        }
    }

    @Override
    public void showMessage(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(GalleryModel _data, int _position) {
        if (!isItemSelected) {
            isItemSelected = true;
            mPresenter.onItemClick(_data);
        }
    }

    @Override
    public void navigateToTrimFragment(String _path) {
        VideoActivity.startVideoFlow(_path, getActivity());
    }

    @Override
    public void onRefresh() {
        mPresenter.onRefresh();
    }
}
