package com.capt.video.flow;

import android.os.Bundle;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.VideoContract;

import javax.inject.Inject;

/**
 * Created by Svyd on 15.05.2016.
 */
public class VideoFlowPresenter extends AbstractVideoPresenter {

    @Inject
    public VideoFlowPresenter(VideoContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
    }

    @Override
    public void setArgs(Bundle _args) {
        Video video = new Video();
        video.path = _args.getString(Constants.BUNDLE_VIDEO_PATH);
        getData().putSerializable(Constants.BUNDLE_VIDEO, video);
        getData().putInt(Constants.FLOW, _args.getInt(Constants.FLOW, -1));
    }

    @Override
    public void initialize() {
        getView().initTrimFragment(getData());
    }
}
