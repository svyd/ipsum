package com.capt.profile.settings.currency.dagger;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.capt.data.annotation.PerFragment;
import com.capt.data.base.TypeMapper;
import com.capt.data.settings.SettingsMapper;
import com.capt.data.settings.SettingsRepositoryImpl;
import com.capt.data.settings.SettingsService;
import com.capt.data.currency.CurrencyRepositoryImpl;
import com.capt.data.currency.CurrencyService;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.SettingsRepository;
import com.capt.domain.profile.settings.UpdateSettingsInteractor;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.profile.settings.model.UpdateSettingsModel;
import com.capt.domain.currency.CurrencyInteractor;
import com.capt.domain.currency.CurrencyRepository;
import com.capt.profile.settings.currency.CurrencySettingsContract;
import com.capt.profile.settings.currency.CurrencySettingsPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by root on 29.04.16.
 */
@Module
public class CurrencySettingsModule {
    CurrencySettingsContract.View mView;

    public CurrencySettingsModule(CurrencySettingsContract.View _view) {
        mView = _view;
    }

    @Provides
    @PerFragment
    protected CurrencySettingsContract.Presenter providePresenter(CurrencySettingsPresenter _presenter) {
        return _presenter;
    }

    @Provides
    @PerFragment
    protected CurrencySettingsContract.View provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    protected LinearLayoutManager provideLayoutManager(Context _context) {
        return new LinearLayoutManager(_context, android.support.v7.widget.LinearLayoutManager.VERTICAL, false);
    }

    @Provides
    @PerFragment
    CurrencyService provideCurrencyService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                           Retrofit _retrofit) {
        return _retrofit.create(CurrencyService.class);
    }

    @Provides
    @PerFragment
    CurrencyRepository provideCurrencyRepository(CurrencyRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.CURRENCY_INTERACTOR)
    BaseInteractor provideCurrencyInteractor(CurrencyInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    SettingsService provideSettingsService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                           Retrofit _retrofit) {
        return _retrofit.create(SettingsService.class);
    }

    @Provides
    @PerFragment
    SettingsRepository provideSettingsRepository(SettingsRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.SETTINGS_INTERACTOR)
    BasePostInteractor<UpdateSettingsModel> provideUpdateSettingsInteractor(UpdateSettingsInteractor _interactor) {
        return _interactor;
    }
}
