package com.capt.profile.settings;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.video.model.Video;

/**
 * Created by Svyd on 30.06.2016.
 */
public interface AccountContract {

    interface View extends BaseView {
        void navigateToCurrency(SettingsModel _model);
        void navigateToNotifications(SettingsModel _model);
    }

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setArgs(Bundle _args);
    }
}
