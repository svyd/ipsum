package com.capt.domain.profile.dashboard.model;

import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.Item;

import java.util.List;

/**
 * Created by Svyd on 25.03.2016.
 */
public class Assignments extends BaseItems {

    List<Assignment> mAssignments;

    public List<Assignment> getAssignments() {
        return mAssignments;
    }

    public void setAssignments(List<Assignment> mAssignments) {
        this.mAssignments = mAssignments;
    }

    public void setItems(List<Item> _items) {
        mItems = _items;
    }

    @Override
    public String getHeaderTitle() {
        return "Active Assignments";
    }

    @Override
    public String getHeaderDescription() {
        return "";
    }
}
