package com.capt.profile.gallery;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.gallery.model.GalleryModel;

import java.util.List;

/**
 * Created by Svyd on 4/23/16.
 */
public interface GalleryContract {

    interface View extends BaseView {
        void finish();
        void showGalleryData(List<GalleryModel> _data);
        void showEmptyView();
        void hideEmptyView();
        void showMessage(String _message);
        void navigateToTrimFragment(String _path);
        void startAssignmentFlow(Assignment _model);
        void hideRefresh();
    }

    abstract class Presenter extends BasePresenter<GalleryContract.View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onItemClick(GalleryModel _model);
        public abstract void setArgs(Bundle _args);

        public abstract void onRefresh();
    }
}
