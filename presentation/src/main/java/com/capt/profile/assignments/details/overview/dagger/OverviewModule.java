package com.capt.profile.assignments.details.overview.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.assignments.details.overview.OverviewContract;
import com.capt.profile.assignments.details.overview.OverviewPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 25.04.16.
 */
@Module
public class OverviewModule {
	private OverviewContract.View mView;

	public OverviewModule(OverviewContract.View mView) {
		this.mView = mView;
	}

	@Provides @PerFragment
	protected OverviewContract.View provideView() {
		return mView;
	}

	@Provides @PerFragment
	protected OverviewContract.Presenter providePresenter(OverviewPresenter _presenter) {
		return _presenter;
	}
}
