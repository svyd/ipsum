package com.capt.video.description;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.BaseLocationFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.global.Constants;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.description.dagger.DescriptionComponent;
import com.capt.video.description.dagger.DescriptionModule;
import com.capt.video.description.flow.AbstractDescriptionPresenter;
import com.capt.video.map.MapFragment;
import com.capt.video.price.SetPriceFragment;
import com.capt.video.view.ListenableVideoView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by root on 03.04.16.
 */


public class DescriptionFragment extends BaseLocationFragment implements DescriptionContract.View, ListenableVideoView.VideoListener {

    @Inject
    AbstractDescriptionPresenter mPresenter;

    @Inject
    ToolbarManager mToolbar;

    @Inject
    MediaController mController;

    @Bind(R.id.vv_video_IVH)
    ListenableVideoView vvNewVideo;
    @Bind(R.id.iv_play_IVH)
    ImageView ivPlay;
    @Bind(R.id.tv_video_properties_IVH)
    TextView tvVideoProperties;
    @Bind(R.id.et_title_IVH)
    EditText etTitle;
    @Bind(R.id.tv_location_IVH)
    TextView tvLocation;
    @Bind(R.id.rl_location_IVH)
    RelativeLayout rlLocation;
    @Bind(R.id.et_description_FD)
    EditText etDescription;
    @Bind(R.id.tv_title_IVH)
    TextView tvTitle;


    @Bind(R.id.ivThumb_IVH)
    ImageView ivThumb;

    public static final int LOCATION_REQUEST_CODE = 1122;
    private DescriptionComponent mComponent;
    private String mLocationName;
    private LocationModel locationModel;
    private Location currentLocation;

    public static DescriptionFragment newInstance(Bundle _args) {

        DescriptionFragment fragment = new DescriptionFragment();
        fragment.setArguments(_args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_description);
        setHasOptionsMenu(true);
    }

    private void checkExtra() {
        mPresenter.setData(getArguments());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        checkExtra();
        mPresenter.initialize();
        mPresenter.locationNameSelected(mLocationName);
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        if (getArguments().containsKey(Constants.FLOW)) {
            int flow = getArguments().getInt(Constants.FLOW);
            mComponent = getComponent(VideoActivityComponent.class)
                    .createDescriptionComponent(new DescriptionModule(this, flow));
        } else {
            throw new IllegalStateException(DescriptionFragment.class.getSimpleName() +
                    "should be instantiated with specified flow");
        }
    }

    private void inject() {
        if (mToolbar != null)
            return;

        mComponent.inject(this);
    }

    @Override
    protected void setUpActionBar(ActionBar actionBar) {
        super.setUpActionBar(actionBar);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close);
    }

    @Override
    public void setEditableTitle(boolean _editable) {
        etTitle.setVisibility(_editable ? View.VISIBLE : View.GONE);
        tvTitle.setVisibility(_editable ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showToolbar(boolean _show) {
        if (_show) {
            mToolbar.showToolbar();
        } else {
            mToolbar.hideToolbar();
        }
    }

    @Override
    public void setVideoVisibility(boolean _visible) {
        if (_visible) {
            vvNewVideo.setVisibility(View.VISIBLE);
            ivPlay.setVisibility(View.VISIBLE);
            ivThumb.setVisibility(View.GONE);
        } else {
            vvNewVideo.setVisibility(View.GONE);
            ivPlay.setVisibility(View.GONE);
            ivThumb.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setToolbarTitle(String _title) {
        getActivity().setTitle(_title);
    }

    @Override
    public void setThumbnail(String _url) {
        Glide.with(getActivity())
                .load(_url)
                .asBitmap()
                .centerCrop()
                .into(ivThumb);
    }

    @Override
    public void showMessage(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showValidateMessage() {
        showMessage(getString(R.string.error_empty_fields));
    }

    @Override
    public void navigateToSetPriceScreen(Bundle _args) {
        _args.putInt(Constants.FLOW, getArguments().getInt(Constants.FLOW));
        getFragmentNavigator().replaceFragmentWithBackStack(SetPriceFragment.newInstance(_args));
    }

    @OnClick(R.id.rl_location_IVH)
    protected void onLocationClick() {
        mPresenter.navigateToMap();
    }

    @Override
    public void navigateToMapScreen() {

        if (locationModel == null) {
            if (currentLocation != null) {
                locationModel = new LocationModel(currentLocation.getLatitude(), currentLocation.getLongitude());
            } else {
                locationModel = new LocationModel(0, 0);
            }
            locationModel.result.address = mLocationName;
        }

        BaseFragment fragment = MapFragment.newInstance(locationModel);
        fragment.setTargetFragment(this, LOCATION_REQUEST_CODE);

        getFragmentNavigator().replaceFragmentWithBackStack(fragment);
    }

    @Override
    public void setVideoProperties(String _properties) {
        tvVideoProperties.setText(_properties);
    }

    @Override
    public void needToUpdateLocation(boolean _isNeeded) {
        this.needUpdateLocation(_isNeeded);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent _data) {
        if (requestCode == LOCATION_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            locationResult(_data);
        }
        if (requestCode == LOCATION_REQUEST_CODE && resultCode == Activity.RESULT_CANCELED) {
            locationResult(_data);
        }
        super.onActivityResult(requestCode, resultCode, _data);
    }

    private void locationResult(Intent _data) {
        LocationModel model = (LocationModel) _data
                .getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE);
        if (model != null) {

            locationModel = model;
            mLocationName = model.result.address;
            mPresenter.setLatLng(model.getLatLngArray());
        } else {
            mLocationName = null;
            mPresenter.locationNameSelected("");
            needUpdateLocation(true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public String getTitle() {
        return etTitle.getText().toString();
    }

    @Override
    public String getDescription() {
        return etDescription.getText().toString();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        mPresenter.onLocationChanged(location);
    }

    @Override
    public void setLocationName(String _name) {
        mLocationName = _name;
        if (tvLocation != null)
            tvLocation.setText(_name);
    }

    @Override
    public void setVideoView(String _path) {
        vvNewVideo.setVideoPath(_path);
        vvNewVideo.setMediaController(mController);
        vvNewVideo.setVideoListener(this);
        vvNewVideo.setOnPreparedListener(mp -> initMediaPlayer());
    }

    private void initMediaPlayer() {
        if (vvNewVideo != null) {
            vvNewVideo.start();
            vvNewVideo.pause();
        }
    }

    @Override
    public void playVideo() {
        vvNewVideo.start();
    }

    @Override
    public void setTitle(String _title) {
        etTitle.setText(_title);
    }

    @Override
    public void setDescription(String _description) {
        etDescription.setText(_description);
    }

    @Override
    public void onVideoPlay() {
        ivPlay.setVisibility(View.GONE);
    }

    @Override
    public void onVideoPause() {
        ivPlay.setVisibility(View.VISIBLE);
    }



    @OnClick(R.id.iv_play_IVH)
    protected void OnPlayClick() {
        mPresenter.onPlayClick();
        mController.hide();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;

            case R.id.action_next:
                mPresenter.onNextClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
