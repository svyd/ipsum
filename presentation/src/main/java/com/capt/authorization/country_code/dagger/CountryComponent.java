package com.capt.authorization.country_code.dagger;

import com.capt.authorization.country_code.CountryFragment;
import com.capt.data.annotation.PerFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.03.16..
 */
@PerFragment
@Subcomponent(modules = CountryModule.class)
public interface CountryComponent {
    void inject(CountryFragment _fragment);
}
