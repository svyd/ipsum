package com.capt.domain.currency;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by root on 31.03.16.
 */
public class CurrencyInteractor extends BaseInteractor {

	CurrencyRepository mRepository;

	@Inject
	public CurrencyInteractor(CurrencyRepository _repository, PostExecutionThread postExecutionThread) {
		super(postExecutionThread);
		mRepository = _repository;
	}

	@Override protected Observable buildGetObserver() {
		return mRepository.getCurrencies();
	}
}
