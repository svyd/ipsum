package com.capt.authorization.location.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.location.LocationFragment;

import dagger.Subcomponent;

/**
 * Created by Svyd on 16.03.2016.
 */

@PerFragment
@Subcomponent(modules = {LocationModule.class})
public interface LocationComponent {
    void inject(LocationFragment _fragment);
}
