package com.capt.video.trim.ffmpeg_command_builder;

/**
 * Created by richi on 2016.02.29..
 */
public interface FFMPEGCommandBuilder {
    String getTrimVideoCommand(long _startTime, long _endTime, String _input, String _outPut);
}
