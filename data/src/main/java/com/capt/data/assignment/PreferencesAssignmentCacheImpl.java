package com.capt.data.assignment;

import android.content.SharedPreferences;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.cache.AssignmentCache;
import com.capt.domain.profile.assignment.model.Assignment;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;

/**
 * Created by Svyd on 07.07.2016.
 */
public class PreferencesAssignmentCacheImpl implements AssignmentCache {

    private final String ASSIGNMENT_KEY = "assignment_key";

    private SharedPreferences mPreferences;
    private Gson mGson;


    @Inject
    public PreferencesAssignmentCacheImpl(SharedPreferences _preferences,
                                          @Named(Constants.NamedAnnotation.NON_EXCLUDED_GSON)
                                          Gson _gson) {
        mGson = _gson;
        mPreferences = _preferences;
    }

    @Override
    public Observable<Assignment> cacheAssignment(Assignment _assignment) {
        return Observable.create(subscriber -> {
            try {
                String json = mGson.toJson(_assignment);
                mPreferences.edit()
                        .putString(ASSIGNMENT_KEY, json)
                        .apply();
                subscriber.onNext(_assignment);
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<Assignment> getAssignment() {
        return Observable.create(subscriber -> {
            try {
                String json = mPreferences.getString(ASSIGNMENT_KEY, null);
                Assignment assignment = mGson.fromJson(json, Assignment.class);
                subscriber.onNext(assignment);
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<SuccessModel> clear() {
        return Observable.create(subscriber -> {
            try {
                mPreferences.edit().remove(ASSIGNMENT_KEY).apply();
                subscriber.onNext(new SuccessModel("success"));
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<SuccessModel> updateStatus(String _status) {
        return Observable.create(subscriber -> {
            try {
                String source = mPreferences.getString(ASSIGNMENT_KEY, "");
                Assignment assignment = mGson.fromJson(source, Assignment.class);
                assignment.video.state = _status;

                String target = mGson.toJson(assignment);
                mPreferences.edit()
                        .putString(ASSIGNMENT_KEY, target)
                        .apply();
                subscriber.onNext(new SuccessModel("success"));
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }
}
