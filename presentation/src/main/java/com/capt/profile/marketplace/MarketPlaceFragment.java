package com.capt.profile.marketplace;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.profile.ViewPagerAdapter;
import com.capt.profile.dagger.ProfileActivityComponent;
import com.capt.profile.marketplace.dagger.MarketPlaceComponent;
import com.capt.profile.marketplace.dagger.MarketPlaceModule;
import com.capt.profile.marketplace.dagger.MarketplaceActivityComponent;
import com.capt.profile.marketplace.drafts.DraftListener;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.capt.domain.global.Constants.*;

/**
 * Created by root on 08.04.16.
 */
public class MarketPlaceFragment extends BaseToolbarFragment implements MarketplaceTabListener {

    @Inject
    ViewPagerAdapter mAdapter;

    @Bind(R.id.tab_layout_FM)
    TabLayout mTabLayout;

    @Bind(R.id.view_pager_FM)
    ViewPager mViewPager;

    private MarketPlaceComponent mComponent;

    public static BaseFragment newInstance(MarketplaceItems item) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, item);
        MarketPlaceFragment marketPlaceFragment = new MarketPlaceFragment();
        marketPlaceFragment.setArguments(bundle);
        return marketPlaceFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_marketplace);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        setUpTabs();
        showToolbar();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(MarketplaceActivityComponent.class)
                .createMarketPlaceComponent(new MarketPlaceModule(this));
    }

    private void showToolbar() {
        getToolbarManager().showToolbar();
        getActivity().setTitle(getString(R.string.title_marketplace));
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void setUpTabs() {
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);

        Bundle bundle = getArguments();
        if (bundle != null) {
            MarketplaceItems marketplaceItem = (MarketplaceItems) bundle.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
            if (marketplaceItem != null) {
                if (marketplaceItem.equals(MarketplaceItems.Recent)) {
                    mViewPager.setCurrentItem(0);
                } else if (marketplaceItem.equals(MarketplaceItems.Drafts)) {
                    mViewPager.setCurrentItem(1);
                } else if (marketplaceItem.equals(MarketplaceItems.Sold)) {
                    mViewPager.setCurrentItem(2);
                }
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(2);
    }

    @Override
    public void onAddDraft(Video _draft) {
        mViewPager.setCurrentItem(1);
        if (mAdapter.getItem(1) instanceof DraftListener) {
            ((DraftListener) mAdapter.getItem(1)).addDraft(_draft);
        } else {
            throw new RuntimeException("DraftsFragment should implement DraftListener");
        }
    }
}
