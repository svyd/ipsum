//package com.capt;
//
//import com.capt.application.CaptApplication;
//import com.capt.application.dagger.AppModule;
//import com.capt.application.dagger.TrimModule;
//
///**
// * Created by richi on 2016.02.27..
// */
//public class TestCaptApplication extends CaptApplication {
//
//    private AppModule mAppModule;
//    private TrimModule mTrimModule;
//
//    @Override
//    protected AppModule getAppModule() {
//        if (mAppModule == null)
//            return super.getAppModule();
//        else
//            return mAppModule;
//    }
//
//    @Override
//    protected TrimModule getTrimModule() {
//        if (mTrimModule == null)
//            return super.getTrimModule();
//        else
//            return mTrimModule;
//    }
//
//    public void setAppModule(AppModule _module) {
//        mAppModule = _module;
//        initializeInjector();
//    }
//
//    public void setTrimModule(TrimModule _module) {
//        mTrimModule = _module;
//        initializeInjector();
//    }
//}
