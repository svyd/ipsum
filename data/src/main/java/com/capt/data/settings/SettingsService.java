package com.capt.data.settings;

import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.profile.settings.model.UpdateSettingsModel;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import rx.Observable;

/**
 * Created by root on 04.05.16.
 */
public interface SettingsService {

	@GET("/mobile/user/settings")
	Observable<SettingsModel> getSettings();

	@PUT("/mobile/user/settings")
	Observable<SettingsModel> updateSettings(@Body UpdateSettingsModel _model);
}
