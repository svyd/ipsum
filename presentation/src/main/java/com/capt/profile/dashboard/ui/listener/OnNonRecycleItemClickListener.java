package com.capt.profile.dashboard.ui.listener;

import com.capt.profile.dashboard.ui.view.NonRecycleList;

/**
 * Created by Svyd on 28.03.2016.
 */
public interface OnNonRecycleItemClickListener {
    void onClick(int position, NonRecycleList _list);
}
