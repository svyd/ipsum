package com.capt.video.trim.view_presenter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.profile.ProfileActivity;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.description.DescriptionFragment;
import com.capt.video.trim.dagger.FFMpegComponent;
import com.capt.video.trim.dagger.FFMpegModule;
import com.capt.video.trim.dagger.TrimModule;
import com.capt.video.trim.design.adapter.BunchAdapter;
import com.capt.video.trim.design.view.TrimmingFrameView;
import com.capt.video.trim.time_util.TimeUtilImpl;
import com.capt.video.view.ListenableVideoView;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subjects.PublishSubject;


/**
 * Created by richi on 2016.02.29..
 */
public class TrimFragment extends BaseToolbarFragment implements TrimContract.View, TrimmingFrameView.OnTrimListener, ListenableVideoView.VideoListener {

    private static final String TAG = TrimFragment.class.getSimpleName();
    private final int MINUTE = 60000;
    private final int PADDING_SUM = 40;
    private final int VISIBLE_ITEM_COUNT = 4;

    @Inject
    protected TrimContract.Presenter mPresenter;

    ToolbarManager mToolbar;

    @Inject
    TimeUtilImpl mTimeUtil;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    BunchAdapter mAdapter;

    @Bind(R.id.trimmer_FT)
    TrimmingFrameView mTrimmer;

    @Bind(R.id.tvLeft)
    TextView tvLeft;

    @Bind(R.id.tvRight)
    TextView tvRight;

    @Bind(R.id.video_FT)
    ListenableVideoView mVideo;

    @Bind(R.id.rvRange_FT)
    RecyclerView rvRange;

    @Bind(R.id.ivPlay_FT)
    ImageView ivPlay;

    protected FFMpegComponent mComponent;

    private float mDuration;
    private float mFullLength;
    private float leftVisibleBound;
    private float rightTrimBound;
    private float leftTrimBound;
    private boolean rightBoundScrolled;
    private boolean pendingToPlay = false;

    private ProgressDialog mProgress;
    private PublishSubject<Float> mTrimSubject;

    public static BaseFragment newInstance(Bundle _args) {
        BaseFragment fragment = new TrimFragment();
        fragment.setArguments(_args);

        return fragment;
    }

    @Override
    protected void setUpActionBar(ActionBar actionBar) {
        super.setUpActionBar(actionBar);
        mToolbar = getToolbarManager();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit, menu);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_trim);
        setHasOptionsMenu(true);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponent();
        inject();
        initRangeBar();
        checkExtra();
        mPresenter.initialize();
        showToolbar();
        mVideo.setVideoListener(this);
        initProgress();
        initSubject();
    }

    private void initSubject() {
        mTrimSubject = PublishSubject.create();
        mTrimSubject.debounce(30, TimeUnit.MILLISECONDS)
                .subscribe(this::seek);
    }

    private void initProgress() {
        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Trim...");
        mProgress.setCancelable(false);
    }

    private void showToolbar() {
        mToolbar.showToolbar();
        getActivity().setTitle(getString(R.string.trim));
    }

    private void initTrimmer() {
        mTrimmer.setTrimListener(this);
        mTrimmer.setMaxWidth(getMaxTrimLength());
        mTrimmer.setMinWidth(getMinTrimLength());
    }

    private void checkExtra() {
        mPresenter.setData(getArguments());
    }

    private float getMaxTrimLength() {
        return (mFullLength * 3 * MINUTE) / mDuration;
    }

    private float getMinTrimLength() {
        return (mFullLength * (MINUTE / 12)) / mDuration;
    }

    private void initRangeBar() {
        rvRange.setLayoutManager(mLayoutManager);
        rvRange.addOnScrollListener(new TrimListener());
    }

    private void initAdapter(Uri _uri) {
        if (rvRange == null) {
            return;
        }
        mAdapter.setDuration((int) mDuration);
        setFullLength();
        mAdapter.setItemWidth((int) ((getWidth() - getPixel(40)) / 4));
        mAdapter.setData(_uri);
        rvRange.setAdapter(mAdapter);
        initTrimmer();
    }

    @Override
    public void initVideoView(String _path) {
        mVideo.setVideoPath(_path);
        mVideo.setOnPreparedListener(mp -> {
            mp.start();
            mp.pause();

            mDuration = mp.getDuration();
            initAdapter(Uri.fromFile(new File(_path)));
            initTrimmer();
        });
    }

    private void inject() {
        mComponent.inject(this);
    }

    private float millisToPixels(float _millis) {
        return ((mFullLength * _millis) / mDuration) - rvRange.computeHorizontalScrollOffset();
    }

    private int getWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    private void setFullLength() {
        mFullLength = mAdapter.getItemCount() * ((getWidth() - getPixel(PADDING_SUM)) / VISIBLE_ITEM_COUNT);
    }

    private void initComponent() {
        if (getArguments().containsKey(Constants.FLOW)) {

            int flow = getArguments().getInt(Constants.FLOW);

            mComponent = getComponent(VideoActivityComponent.class)
                    .createFFMpegComponent(new FFMpegModule(), new TrimModule(this, flow));
        } else {
            throw new IllegalStateException(TrimFragment.class.getSimpleName() + " should contain specified flow");
        }
    }



    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        mProgress.show();
    }

    @Override
    public void hideProgress() {
        mProgress.cancel();
    }

    @Override
    public void navigateToDescription(Video _data) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.BUNDLE_VIDEO, _data);
        int flow = getArguments().getInt(Constants.FLOW);
        args.putInt(Constants.FLOW, flow);
        getFragmentNavigator().replaceFragmentWithBackStack(DescriptionFragment.newInstance(args));
    }

    @Override
    public void navigateBack() {
        getActivity().finish();
    }

    @Override
    public void finish() {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Constants.Extra.EXTRA_STRING, Constants.Profile.ASSIGNMENTS);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;

            case R.id.action_next:
                mPresenter.onTrimClick(
                        (long) (leftVisibleBound + leftTrimBound),
                        (long) (leftVisibleBound + rightTrimBound)
                );
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.flClick_FT)
    void onVideoClick() {
        if (mVideo.isPlaying()) {
            mVideo.pause();
            mTrimmer.stopPlaying();
        } else {
            if (!mTrimmer.isScrolling()) {
                play();
            } else {
                pendingToPlay = true;
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void onLeftBoundChanged(float _leftBound) {
        if (mVideo.isPlaying()) {
            mVideo.pause();
        }
        rightBoundScrolled = false;
        leftTrimBound = getBound(_leftBound - getPixel(20));
        setTime(leftTrimBound);
    }

    private float getPixel(float _dimen) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                _dimen, getResources().getDisplayMetrics());
    }

    @Override
    public void onRightBoundChanged(float _rightBound) {
        if (mVideo.isPlaying()) {
            mVideo.pause();
        }
        rightBoundScrolled = true;
        rightTrimBound = getBound(_rightBound - getPixel(20));
        setTime(rightTrimBound);
    }

    @Override
    public void onVideoEnd() {
        if (mVideo != null) {
            mVideo.pause();
            mVideo.seekTo((int) leftTrimBound);
        }
    }

    private void play() {
        if (rightBoundScrolled) {
            mVideo.seekTo((int) (leftTrimBound + leftVisibleBound));
            rightBoundScrolled = false;
        }
        mTrimmer.onPlayClick(
                millisToPixels(mVideo.getCurrentPosition()),
                (int) (rightTrimBound + leftVisibleBound - mVideo.getCurrentPosition())
        );
        mVideo.start();
    }

    @Override
    public void onScrollFinished() {
        if (pendingToPlay) {
            play();
            pendingToPlay = false;
        }
    }

    private void setTime(float _time) {
        updateTimeBounds();
        seek(_time);
//        mTrimSubject.onNext(_time);
    }

    private void updateTimeBounds() {
        tvLeft.setText(mTimeUtil.getTimeFromInteger((long) (leftVisibleBound + leftTrimBound) / 1000));
        tvRight.setText(mTimeUtil.getTimeFromInteger((long) (leftVisibleBound + rightTrimBound) / 1000));
    }

    private void seek(float _seek) {
        if (_seek + leftVisibleBound >= mDuration) {
            getActivity().runOnUiThread(() -> mVideo.seekTo((int) mDuration - 400));
        } else {
            getActivity().runOnUiThread(() -> mVideo.seekTo((int) (_seek + leftVisibleBound)));
        }
    }

    private float getBound(float _currentLength) {
        return ((mDuration * _currentLength) / mFullLength);
    }

    @Override
    public void onVideoPlay() {
        ivPlay.setVisibility(View.GONE);
    }

    @Override
    public void onVideoPause() {
        ivPlay.setVisibility(View.VISIBLE);
    }

    private class TrimListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            mVideo.pause();
            mTrimmer.stopPlaying();
            leftVisibleBound = getBound((float) recyclerView.computeHorizontalScrollOffset());
            setTime(leftTrimBound);
        }
    }
}
