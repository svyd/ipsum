package com.capt.profile.edit_profile;

import android.content.Intent;
import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.UserModel;

/**
 * Created by Svyd on 4/29/16.
 */
public interface EditProfileFragmentContract {

    interface View extends BaseView {
        void onShowEmailError();
        void onGoBack(Intent _intent);
        void showFullNameError();
        void navigateToCountryCodePicker();
        void navigateToLocationPicker();
        void showRightEmail();
        void hideRightEmail();
        void showImagePickerSheet();
        void renderPhoneNumber(String _number);
        void renderCountry(String _country);
        void renderCity(String _location);
        void renderImage(String _path);
        void renderCountryCode(String _countryCode);
        void renderFullName(String _fullName);
        void renderEmail(String _email);
        void showEmailProgress();
        void hideEmailProgress();
        void confirmPhoneAndUpdate();
    }

    abstract class Presenter extends BasePresenter<EditProfileFragmentContract.View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onEmailChanged(CharSequence _email);
        public abstract void onCountryCodeClicked();
        public abstract void onLocationClicked();
        public abstract void onDoneClicked();
        public abstract void onPhotoClicked();
        public abstract void setLocation(LocationModel _location);
        public abstract void setCountryCode(String _code);
        public abstract void setAvatar(String _avatar);
        public abstract void savedInstanceState(Bundle _bundle);
        public abstract void restoreState(Bundle _bundle);
        public abstract void fullNameChanged(String _fullName);

        public abstract void onPhoneChanged(String _phone);
    }
}
