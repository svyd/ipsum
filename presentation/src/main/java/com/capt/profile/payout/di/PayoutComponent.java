package com.capt.profile.payout.di;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.payout.PayoutsFragment;

import dagger.Subcomponent;

/**
 * Created by rMozes on 8/3/16.
 */
@PerFragment
@Subcomponent(modules = PayoutsModule.class)
public interface PayoutComponent {
    void inject(PayoutsFragment _fragment);
}
