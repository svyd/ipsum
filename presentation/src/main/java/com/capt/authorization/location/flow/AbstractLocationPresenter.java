package com.capt.authorization.location.flow;

import android.util.Log;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.authorization.location.LocationContract;
import com.capt.authorization.location.LocationInteractorManager;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observer;
import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 18.05.2016.
 */
public abstract class AbstractLocationPresenter extends LocationContract.LocationPresenter {
    final String TAG = LocationPresenter.class.getSimpleName();
    private PublishSubject<String> mTextSubject;
    protected LocationInteractorManager mInteractorManager;

    private LocationContract.LocationView mView;

    public AbstractLocationPresenter(LocationContract.LocationView _view,
                                     LocationInteractorManager _manager, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractorManager = _manager;
        initSubject();
    }

    private void initSubject() {
        mTextSubject = PublishSubject.create();
        mTextSubject
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(s -> {
                    mInteractorManager.execute(s, new LocationObserver(this));
                });
    }

    @Override
    public void getAddressList(String _query) {
        mTextSubject.onNext(_query);
    }

    @Override
    public void getAddressDetails(String _reference) {
        mInteractorManager.executeDetails(_reference, new DetailsObserver(this));
    }

    @Override
    public void onLocationChanged(double _lat, double _lon) {
        throw new RuntimeException("This operation is unsupported here");
    }

    protected LocationContract.LocationView getView() {
        return mView;
    }

    @Override
    public void initialize() {
        getView().hideUpdateLocation();
    }

    @Override
    public void onStop() {
        mInteractorManager.unSubscribe();
    }

    protected abstract void doOnNext(LocationModel locationModel);

    private class DetailsObserver extends BaseObserver<LocationModel> {
        public DetailsObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(LocationModel locationModel) {
            doOnNext(locationModel);
        }
    }

    private class LocationObserver extends BaseObserver<PredictionsModel> {
        public LocationObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            for (PredictionsModel.Prediction prediction : predictionsModel.predictions) {
                Log.d(TAG, prediction.description);
            }
            mView.setAddressList(predictionsModel);
        }
    }
}
