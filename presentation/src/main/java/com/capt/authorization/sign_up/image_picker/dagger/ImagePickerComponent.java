package com.capt.authorization.sign_up.image_picker.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.sign_up.image_picker.ImagePickerFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.03.20..
 */
@PerFragment
@Subcomponent(modules = ImagePickerModule.class)
public interface ImagePickerComponent {
    void inject(ImagePickerFragment _fragment);
}
