package com.capt.profile.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.UpdateAvatarResponse;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.DashboardModel;
import com.capt.domain.profile.dashboard.model.DashboardParamsModel;
import com.capt.domain.profile.dashboard.model.Reminder;
import com.capt.domain.profile.dashboard.model.ReminderId;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.capt.domain.video.model.Video;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 28.03.2016.
 */
public class DashboardPresenter extends DashboardContract.DashboardPresenter {

    private static final String TAG = "DashboardPresenter";

    BasePostInteractor<DashboardParamsModel> mInteractor;
    BasePostInteractor<String> mUploadInteractor;
    BaseInteractor mUserInteractor;
    UserModel mUserModel;
    DashboardContract.DashboardView mView;
    DashboardModel mData;
    Section<Reminder> mReminders;
    List<Section> mItems;

    @Inject
    public DashboardPresenter(DashboardContract.DashboardView _view,
                              @Named(Constants.NamedAnnotation.DASHBOARD_INTERACTOR)
                              BasePostInteractor<DashboardParamsModel> _interactor,
                              @Named(Constants.NamedAnnotation.UPDATE_PHOTOT_INTERACTOR)
                              BasePostInteractor<String> _uploadInteractor,
                              BaseInteractor _userInteractor, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mInteractor = _interactor;
        mUploadInteractor = _uploadInteractor;
        mUserInteractor = _userInteractor;
        mView = _view;
    }

    @Override
    public void initialize() {
        mUserInteractor.execute(new UserObserver(this));
        getContent();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mView.hideRefresh();
        mInteractor.unSubscribe();
    }

    @Override
    public void setArgs(Bundle _args) {
        if (_args != null && _args.containsKey(Constants.FLAG_TO_RECENT)) {
            if (_args.getBoolean(Constants.FLAG_TO_RECENT)) {
                _args.putBoolean(Constants.FLAG_TO_RECENT, false);
                mView.navigateToMarketPlace(Constants.MarketplaceItems.Recent);
            } else {
                initialize();
            }
        } else {
            initialize();
        }
    }

    @Override
    public void onEditReminderResult(Intent _intent) {
        mUserModel = (UserModel) _intent.getSerializableExtra(Constants.BUNDLE_USER_MODEL);
//        mView.setContent(setReminders(mUserModel, mItems));
    }

    @Override
    public void onRetryClick() {
        mView.hideNoInternetConnectionError();
        mView.showProgress();
        mUserInteractor.execute(new UserObserver(this));
        getContent();
    }

    @Override
    public void getContent() {
        mView.showProgress();
        DashboardParamsModel params = new DashboardParamsModel(
                new RequestParamsModel(Constants.AssignmentTypes.IN_PROGRESS, 2, 0),
                new RequestParamsModel("recent", 5, 0));
        mInteractor.execute(params, new DashboardObserver(this));
    }

    @Override
    public void onRefresh() {
        DashboardParamsModel params = new DashboardParamsModel(
                new RequestParamsModel(Constants.AssignmentTypes.IN_PROGRESS, 2, 0),
                new RequestParamsModel("recent", 5, 0));
        mInteractor.execute(params, new DashboardObserver(this));
    }

    @Override
    public void avatarClicked() {
        mView.navigateToImagePicker();
    }

    private void downloadAvatar() {
        Log.d(TAG, "downloadAvatar() called with: " + "");
        if (mUserModel != null) {
            mView.downloadAvatar(mUserModel.getAvatarUrl());
        }
    }

    @Override
    public void setLocalPath(String _url) {
        if (TextUtils.isEmpty(_url))
            return;

        mView.showAvatarProgress();
        mUploadInteractor.execute(_url, new UpdateObserver(this));
    }

    @Override
    public void onReminderClick(int _position) {
        switch (mReminders.getItems().get(_position).getId()) {
            case ReminderId.LOCATION:
                mView.navigateToLocation();
                break;
            default:
                mView.navigateToEditProfile();
        }
    }

    @Override
    public void onAssignmentsClick(int _position) {
        Assignment assignment = mData.getAssignments().getItems().get(_position);

        if (!assignment.stub) {
            mView.navigateToAssignment(assignment, Constants.AssignmentTypes.IN_PROGRESS);
        } else {
            mView.navigateToPendingAssignments();
        }
    }

    @Override
    public void onMarketplaceClick(int _position) {
        switch (_position) {
            case 0:
                mView.navigateToMarketPlace(Constants.MarketplaceItems.Recent);
                break;
            case 1:
                mView.navigateToMarketPlace(Constants.MarketplaceItems.Drafts);
                break;
            case 2:
                mView.navigateToMarketPlace(Constants.MarketplaceItems.Sold);
                break;
            case 3:
                mView.navigateToPayouts();
                break;
        }
    }

    private List<Section> setReminders(UserModel _model, List<Section> _items) {
        List<Section> items = new ArrayList<>();

        List<Reminder> reminders = new ArrayList<>();
        mReminders = new Section<>(reminders, true);
        mReminders.setHeaderTitle("Reminder");

        if (_model.getLocation()[0] == 0 &&
                _model.getLocation()[1] == 0) {
            Reminder location = new Reminder("Please, complete your registration by filling out your location.", ReminderId.LOCATION);
            reminders.add(location);
        }
        if (_model.getEmail().equals("")) {
            Reminder email = new Reminder("Please, complete your registration by filling out your E-mail address.", ReminderId.EMAIL);
            reminders.add(email);
        }
        if (_model.getName().equals("")) {
            Reminder name = new Reminder("Please, complete your registration by filling out your full name.", ReminderId.NAME);
            reminders.add(name);
        }

        if (mReminders.hasItems()) {
            items.add(mReminders);
        }
        items.addAll(_items);

        return items;
    }

    @Override
    public void onTutorialClick(int _position) {

    }

    @Override
    public void onVideoClick(int _position) {
        Video video = mData.getVideos().getItems().get(_position);
        mView.navigateToVideoDetails(video);
    }

    private class UserObserver extends BaseObserver<UserModel> {
        public UserObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.setHeader(mUserModel);
        }


        @Override
        public void onNext(UserModel userModel) {
            mUserModel = userModel;
        }
    }

    private class UpdateObserver extends BaseObserver<UpdateAvatarResponse> {
        public UpdateObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            downloadAvatar();
        }

        @Override
        public void onNext(UpdateAvatarResponse updateAvatarResponse) {
            Log.d(TAG, "onNext() called with: " + "updateAvatarResponse = [" + updateAvatarResponse.url + "]");
            mUserModel.setAvatarUrl(updateAvatarResponse.url);
        }
    }

    private class DashboardObserver extends BaseObserver<DashboardModel> {
        public DashboardObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
            mView.hideRefresh();
            mView.setHeader(mUserModel);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            mView.hideRefresh();
        }

        @Override
        public void onNext(DashboardModel dashboardModel) {
            mUserModel = dashboardModel.getUser();
            mItems = dashboardModel.getAsSections();
            mView.setContent(mItems);
            mView.setContent(setReminders(mUserModel, mItems));
            mData = dashboardModel;
        }
    }
}
