package com.capt.base.utility;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.inject.Inject;

/**
 * Created by root on 26.04.16.
 */
public class DateTimeUtilityImpl implements com.capt.data.base.DateTimeUtility {

    @Inject
    public DateTimeUtilityImpl() {

    }

    public String formatDate(String _date, boolean withTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateTime dt = formatter.parseDateTime(validateString(_date));
        DateTime currentDate = new DateTime(DateTimeZone.UTC);

        String pattern = "dd.MM.yyyy";
        if (withTime) {
            pattern = "dd.MM.yyyy', 'HH:mm";
        }
        String formattedDate = dt.toString(pattern);
        if ((dt.year() == currentDate.year()) && (dt.monthOfYear() == currentDate.monthOfYear())) {
            if (dt.dayOfMonth() == currentDate.dayOfMonth()) {
                formattedDate = "Today";
            } else if (currentDate.plusDays(1).dayOfMonth() == dt.dayOfMonth()) {
                formattedDate = "Tomorrow";
            }
        }
        return formattedDate;
    }

    public static String validateString(String _field) {
        return (_field == null) ? "" : _field;
    }
}
