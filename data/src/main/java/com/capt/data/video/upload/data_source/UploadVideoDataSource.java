package com.capt.data.video.upload.data_source;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;

import rx.Observable;

/**
 * Created by richi on 2016.03.11..
 */
public interface UploadVideoDataSource {
    Observable<SuccessModel> uploadAssignmentVideo(Assignment _model);
    Observable<SuccessModel> uploadToCloud(Video _model);
    Observable<SuccessModel> uploadPendingVideos();
    Observable<SuccessModel> uploadPayPalCode(String _model);
}
