package com.capt.domain.profile.dashboard;

import com.capt.domain.authorization.model.UserModel;

import rx.Observable;

/**
 * Created by Svyd on 4/23/16.
 */
public interface UserRepository {
    Observable<UserModel> getUser();
}
