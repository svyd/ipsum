package com.capt.authorization.location;


import android.content.Intent;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;

/**
 * Created by Svyd on 16.03.2016.
 */
public interface LocationContract {

    interface LocationView extends BaseView {
        void setAddressList(PredictionsModel _addresses);
        void showToast(String _message);
        void passActivityResult(LocationModel _model);
        void finishWithResult(Intent _intent);
        void navigateToBack();
        void doLocationUpdate();
        void showUpdateLocation();
        void hideUpdateLocation();
    }

    abstract class LocationPresenter extends BasePresenter<LocationView> {
        public LocationPresenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void getAddressList(String _query);
        public abstract void getAddressDetails(String _reference);
        public abstract void onLocationChanged(double _lat, double _lon);
        public abstract void onHeaderClicked();
    }
}
