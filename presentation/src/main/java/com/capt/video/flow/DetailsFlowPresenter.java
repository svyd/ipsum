package com.capt.video.flow;

import android.os.Bundle;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.video.VideoContract;

import javax.inject.Inject;

/**
 * Created by Svyd on 15.05.2016.
 */
public class DetailsFlowPresenter extends AbstractVideoPresenter {

    @Inject
    public DetailsFlowPresenter(VideoContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
    }

    @Override
    public void setArgs(Bundle _args) {
        getData().putSerializable(
                Constants.BUNDLE_VIDEO,
                _args.getSerializable(Constants.BUNDLE_VIDEO));

        int flow = _args.getInt(Constants.FLOW, -1);

        getData().putInt(Constants.FLOW, flow);
    }

    @Override
    public void initialize() {
        getView().initOverViewFragment(getData());
    }
}
