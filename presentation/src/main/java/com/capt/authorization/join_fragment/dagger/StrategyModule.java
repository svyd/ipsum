package com.capt.authorization.join_fragment.dagger;

import com.capt.authorization.auth_strategy.AuthorizationStrategy;
import com.capt.authorization.auth_strategy.FacebookStrategy;
import com.capt.authorization.auth_strategy.InstagramStrategy;
import com.capt.authorization.auth_strategy.TwitterStrategy;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.global.Constants;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.02.25..
 */
@Module
public class StrategyModule {

    @Provides @PerFragment
    @Named(Constants.AuthType.FACEBOOK)
    AuthorizationStrategy provideFacebookStrategy(FacebookStrategy _strategy) {
        return _strategy;
    }

    @Provides @PerFragment @Named(Constants.AuthType.INSTAGRAM)
    AuthorizationStrategy provideInstaStrategy(InstagramStrategy _strategy) {
        return _strategy;
    }

    @Provides @PerFragment @Named(Constants.AuthType.TWITTER)
    AuthorizationStrategy provideTwitterStrategy(TwitterStrategy _strategy) {
        return _strategy;
    }
}
