package com.capt.profile.payout.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.base.BaseActivity;
import com.capt.profile.payout.PayoutsFragment;
import com.capt.profile.payout.activity.di.PayoutActivityComponent;
import com.capt.profile.payout.activity.di.PayoutActivityModule;

/**
 * Created by rMozes on 8/3/16.
 */
public class PayOutActivity extends BaseActivity {

    private PayoutActivityComponent mComponent;

    public static void startPayout(Context _context) {
        Intent intent = new Intent(_context, PayOutActivity.class);
        _context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payout);

        createInjector();
        inject();
        setTitle(R.string.payout);

        if (savedInstanceState == null)
            getFragmentNavigator().addFragmentWithoutBackStack(PayoutsFragment.newInstance());
    }

    private void createInjector() {
        mComponent = CaptApplication.getApplication().getAppComponent()
                .createPayoutActivityComponent(new PayoutActivityModule(this));
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public int getContainerId() {
        return R.id.flContainer_AP;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AP;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
