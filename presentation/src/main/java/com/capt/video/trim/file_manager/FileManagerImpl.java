package com.capt.video.trim.file_manager;


import com.capt.application.CaptApplication;

import java.io.File;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.29..
 */
public class FileManagerImpl extends FileManager {

    public static final String TRIM_DIR = "trim";
    public static final String VIDEO_DIR = "video";

    @Inject
    public FileManagerImpl() {

    }

    @Override
    public File getTrimPath(String _name) {
        return getFilePath(_name, TRIM_DIR);
    }

    @Override
    public File getVideoPath(String _name) {
        return getFilePath(_name, VIDEO_DIR);
    }

    @Override
    public boolean renameFile(File _file, String _name) {
        return false;
    }

    private File getFilePath(String _name, String _directory) {
        File createdFile = null;
        File file = CaptApplication.getApplication().getExternalFilesDir(null);
        if (file != null && file.exists()) {
            File trimDir = new File(file, _directory);
            if (!trimDir.exists()) {
                trimDir.mkdir();
            }

            createdFile = new File(trimDir, _name);
        }

        return createdFile;
    }
}
