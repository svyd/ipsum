package com.capt.video;

/**
 * Created by Svyd on 21.04.2016.
 */
public class Flow {
    public static final int VIDEO_UPLOAD = 0;
    public static final int ASSIGNMENT_DETAILS = 1;
    public static final int DETAILS = 2;
    public static final int EDIT = 3;
    public static final int DRAFT = 4;
    public static final int ASSIGNMENT_REJECTED = 5;
    public static final int EDIT_PROFILE = 6;
    public static final int ADD_LOCATION = 7;
    public static final int UPDATE_PROFILE = 8;
    public static final int CAPT_ASSIGNMENT = 9;
    public static final int ASSIGNMENT_PURCHASED = 10;
    public static final int CHANGE_PASSWORD = 11;
}
