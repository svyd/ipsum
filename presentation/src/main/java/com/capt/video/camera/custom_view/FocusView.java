package com.capt.video.camera.custom_view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.capt.R;

/**
 * Created by Svyd on 13.04.2016.
 */
public class FocusView extends RelativeLayout {

    private ImageView ivFocus;

    private ValueAnimator mScaleUpAnimator;
    private ValueAnimator mScaleDownAnimator;
    private ValueAnimator mFadeInAnimator;
    private ValueAnimator mFadeOutAnimator;
    private AnimatorSet mStartSet;
    private AnimatorSet mEndSet;

    public FocusView(Context context) {
        this(context, null);
    }

    public FocusView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FocusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initImage();
        initAnimators();
    }

    private void initImage() {
        ivFocus = new ImageView(getContext());
        ivFocus.setImageResource(R.drawable.ic_shapes);
//        ivFocus.setAlpha(0);
        ivFocus.setVisibility(INVISIBLE);

        addView(ivFocus, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        );
    }

    private void initAnimators() {
        mScaleUpAnimator = ValueAnimator.ofFloat(1.0f, 1.1f);
        mScaleUpAnimator.addUpdateListener(animation -> {
            Log.d("TAG", "initAnimators: " + (float) animation.getAnimatedValue());
            ivFocus.setScaleX((float) animation.getAnimatedValue());
            ivFocus.setScaleY((float) animation.getAnimatedValue());
        });

        mScaleDownAnimator = ValueAnimator.ofFloat(1.1f, 1.0f);
        mScaleDownAnimator.addUpdateListener(animation -> {
            Log.d("TAG", "initAnimators: " + (float) animation.getAnimatedValue());
            ivFocus.setScaleX((float) animation.getAnimatedValue());
            ivFocus.setScaleY((float) animation.getAnimatedValue());
        });

        mFadeOutAnimator = ValueAnimator.ofFloat(1f, 0f);
//        mFadeOutAnimator.setDuration(75);
        mFadeOutAnimator.addUpdateListener(animation ->
                ivFocus.setAlpha((float) animation.getAnimatedValue()));

        mFadeInAnimator = ValueAnimator.ofFloat(0f, 1f);
//        mFadeInAnimator.setDuration(100);
        mFadeInAnimator.addUpdateListener(animation ->
                ivFocus.setAlpha((float) animation.getAnimatedValue()));

        mStartSet = new AnimatorSet();
        mStartSet.setDuration(200);
        mStartSet.setInterpolator(new LinearInterpolator());
        mStartSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mEndSet.start();
            }
        });
        mStartSet.play(mFadeInAnimator).with(mScaleUpAnimator);

        mEndSet = new AnimatorSet();
        mEndSet.setDuration(200);
        mEndSet.setInterpolator(new LinearInterpolator());
        mEndSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                ivFocus.setVisibility(INVISIBLE);
            }
        });
        mEndSet.play(mScaleDownAnimator)
                .with(mFadeOutAnimator);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        ivFocus.setX(event.getX() - ivFocus.getWidth() / 2);
        ivFocus.setY(event.getY() - ivFocus.getWidth() / 2);
        startAnimation();
        return false;
    }


    private void startAnimation() {
        if (mStartSet.isRunning()) {
            mStartSet.cancel();
        }

        ivFocus.setVisibility(VISIBLE);
        mStartSet.start();
    }

}
