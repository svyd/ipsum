package com.capt.data.authorization.location;

import android.location.Address;
import android.location.Geocoder;

import com.capt.data.authorization.net.ApiConstants;
import com.capt.data.authorization.net.retrofit.PlacesService;
import com.capt.domain.authorization.location.LocationDataStore;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PlaceModel;
import com.capt.domain.authorization.location.model.PredictionsModel;
import com.capt.domain.global.Constants;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Svyd on 16.03.2016.
 */
public class LocationDataStoreImpl implements LocationDataStore {

    private final int MAX_RESULTS = 15;

    LocationMapper mMapper;
    Geocoder mGeocoder;
    PlacesService mService;

    @Inject
    public LocationDataStoreImpl(
            LocationMapper _mapper, PlacesService _service, Geocoder _geocoder) {
        mMapper = _mapper;
        mService = _service;
        mGeocoder = _geocoder;
    }

    @Override
    public Observable<PredictionsModel> getAddressList(LocationModel _model) {
        return getByLatLng(_model).map(mMapper::convert);
    }

    @Override
    public Observable<PredictionsModel> getAddressList(final String _query) {
        return mService.getPlacesPredictions(_query, ApiConstants.GOOGLE_PLACES_API_KEY);
    }

    @Override
    public Observable<LocationModel> getPlaceDetails(String _placeReference) {
        return mService.getPlaceDetails(_placeReference, ApiConstants.GOOGLE_PLACES_API_KEY)
                .map(locationModel -> {
                    for (LocationModel.Result.AddressComponent component : locationModel.result.mListAdressComponent) {
                        for (String type : component.mTypes) {
                            if (type.equals("country")) {
                                locationModel.mCountry = component.shortName;
                            }

                            if (type.equals("locality")) {
                                locationModel.mCity = component.longName;
                            }
                        }
                    }

                    return locationModel;
                });
    }

    private Observable<List<Address>> getByLatLng(final LocationModel _model) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(mGeocoder.getFromLocation(_model.getLat(), _model.getLon(), MAX_RESULTS));
                subscriber.onCompleted();
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }
}
