package com.capt.video.overview;

import android.content.Intent;
import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.category.model.CategoryModel;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.video.model.Video;
import com.paypal.android.sdk.payments.PayPalAuthorization;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Svyd on 10.04.2016.
 */
public interface OverviewContract {

    interface View extends BaseView {
        void setResult(Intent _intent, int _result);
        void navigateToOverview(Bundle _args);
        void setFrameBtnTitle(String _text);
        void setDetailsTextBtnTitle(String _text);
        void setEditableTitle(boolean _editable);
        void navigateToDescription(Bundle _args);
        void setCategories(List<CategoryModel> _categories);
        void setLocation(String _location);
        void playVideo();
        void setPrice(String _price);
        void setVideoDescription(String _description);
        void setVideoView(String _path);
        void setVideoProperties(String _properties);
        void setVideoTitle(String _title);
        void setVideoPlayability(boolean _visible);
        void setButtonsVisibility(boolean _visible);
        void setListVisibility(boolean _visible);
        void setWarningVisibility(boolean _visible);
        void setTextBtnTitle(String _title);
        void setListData(List<Item> _items);
        void setThumbnail(String _url);
        void setTitle(String _title);
        void onFuturePayment();
        void navigateToDashboard();
        void setDetailsBtnVisibility(boolean visibility);
        void navigateBack();
        void navigateBack(Intent _intent);
        void navigateToDraftEdit(Bundle _args);
        void showLocation(boolean _show);
        void setRedBtnText(String _text);
        void showDescription(boolean _show);
        void setCompletedText(String _name);
        void setCompletedVisibility(boolean _visibility);
        void finishWithResult(boolean _toDrafts);
        void showErrorOptions();
        void hideErrorOptions();
        void showVideoProgress();
        void hideVideoProgress();
        void setVideoProgress(int _progress);

        void showDenial();
    }

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onActionEdit();
        public abstract void onFrameBtnClick();
        public abstract void onDetailsTextBtnClick();
        public abstract void onPlayClick();
        public abstract void uploadVideo();
        public abstract void onTextBtnClick();
        public abstract void setData(Bundle _model);
        public abstract void onRedBtnClick();
        public abstract void sendAuthorizationToServer(PayPalAuthorization authorization);
    }
}
