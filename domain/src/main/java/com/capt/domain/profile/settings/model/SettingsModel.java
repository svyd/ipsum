package com.capt.domain.profile.settings.model;

import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by root on 04.05.16.
 */
public class SettingsModel implements Serializable {

	@Expose
	@SerializedName("defaultCurrency")
	public CurrencyModel defaultCurrency;

	@Expose
	@SerializedName("notifications")
	public NotificationsModel notifications;

    private boolean isSocial;

    public void setSocial(boolean _social) {
        isSocial = _social;
    }

    public boolean isSocial() {
        return isSocial;
    }

	private List<Section<Item>> mSettingsItems;

    public List<Section<Item>> getItems() {
        return mSettingsItems;
    }

    public void setSettingsItems(List<Section<Item>> _items) {
        mSettingsItems = _items;
    }
}
