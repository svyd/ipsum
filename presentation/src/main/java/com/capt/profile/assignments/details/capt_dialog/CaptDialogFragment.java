package com.capt.profile.assignments.details.capt_dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.application.dagger.HasComponent;
import com.capt.base.BaseActivity;
import com.capt.base.BaseFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.profile.assignments.details.capt_dialog.dagger.CaptDialogComponent;
import com.capt.profile.assignments.details.capt_dialog.dagger.CaptDialogModule;
import com.capt.profile.assignments.details.dagger.DetailsActivityComponent;
import com.capt.profile.gallery.GalleryFragment;
import com.capt.video.Flow;
import com.capt.video.camera.activity.CameraActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by root on 22.04.16.
 */
public class CaptDialogFragment extends BottomSheetDialogFragment implements CaptDialogContract.View {

    @Inject
    CaptDialogContract.Presenter mPresenter;

    @Bind(R.id.tvCamera_DFAS)
    TextView tvCamera;

    private CaptDialogComponent mComponent;

    public static void showWithTarget(BaseFragment _fragment, int _requestCode, Assignment _assignment) {
        DialogFragment fragment = new CaptDialogFragment();
        fragment.setTargetFragment(_fragment, _requestCode);

        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, _assignment);
        fragment.setArguments(args);
        fragment.show(_fragment.getFragmentManager(), fragment.getClass().getSimpleName());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_capt_assignment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(DetailsActivityComponent.class)
                .createCaptDialogComponent(new CaptDialogModule(this));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        if (!(getActivity() instanceof HasComponent))
            return null;

        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    @OnClick(R.id.tvCamera_DFAS)
    protected void onCameraClick() {
        mPresenter.onCameraClick();
    }

    @OnClick(R.id.tvGallery_DFAS)
    protected void onGalleryClick() {
        mPresenter.onGalleryClick();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showNoInternetConnectionError() {

    }

    @Override
    public void hideNoInternetConnectionError() {

    }

    @Override
    public void showError(String _error) {

    }

    @Override
    public void navigateToCamera() {
        Intent intent = new Intent(getActivity(), CameraActivity.class);
        intent.putExtra(Constants.FLOW, Flow.CAPT_ASSIGNMENT);
        intent.putExtra("x", tvCamera.getX() + tvCamera.getWidth() / 2);
        intent.putExtra("y", tvCamera.getY() + tvCamera.getHeight() / 2);
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, getArguments().getSerializable(Constants.Extra.EXTRA_SERIALIZABLE));
        getActivity().startActivity(intent);
        dismiss();
    }

    @Override
    public void navigateToGallery() {
        Bundle args = new Bundle();
        args.putInt(Constants.FLOW, Flow.CAPT_ASSIGNMENT);
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, getArguments().getSerializable(Constants.Extra.EXTRA_SERIALIZABLE));
        ((BaseActivity) getActivity()).getFragmentNavigator().replaceFragmentWithBackStack(GalleryFragment.newInstance(args));
        dismiss();
    }

    @Override
    public void showDenial() {
        Toast.makeText(getActivity(), "Please wait until the last uploading is done", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToJoinScreen() {

    }
}
