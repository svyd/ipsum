package com.capt.domain.profile.payout;

import java.util.List;

/**
 * Created by rMozes on 8/3/16.
 */
public class PayoutScreenModel {

    private String mTotalPayout;
    private List<PayoutModel> mModelList;

    public String getTotalPayout() {
        return mTotalPayout;
    }

    public void setTotalPayout(String mTotalPayout) {
        this.mTotalPayout = mTotalPayout;
    }

    public List<PayoutModel> getModelList() {
        return mModelList;
    }

    public void setModelList(List<PayoutModel> mModelList) {
        this.mModelList = mModelList;
    }
}
