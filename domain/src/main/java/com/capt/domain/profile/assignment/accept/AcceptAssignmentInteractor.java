package com.capt.domain.profile.assignment.accept;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.profile.assignment.accept.model.AcceptAssignmentModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by root on 22.04.16.
 */
public class AcceptAssignmentInteractor extends BasePostInteractor<AcceptAssignmentModel> {

	AcceptAssignmentRepository mRepository;

	@Inject
	public AcceptAssignmentInteractor(PostExecutionThread postExecutionThread, AcceptAssignmentRepository _repository) {
		super(postExecutionThread);
		mRepository = _repository;
	}

	@Override protected Observable buildPostObservable(AcceptAssignmentModel _data) {
		return mRepository.acceptAssignment(_data);
	}

	@Override protected Observable buildGetObserver() {
		return null;
	}
}
