package com.capt.profile.dashboard;

import android.content.Intent;
import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.capt.domain.video.model.Video;

import java.util.List;

/**
 * Created by Svyd on 25.03.2016.
 */
public interface DashboardContract {

    interface DashboardView extends BaseView {
        void setContent(List<Section> _model);
        void navigateToImagePicker();
        void navigateToAssignment(Assignment _model, String _type);
        void setHeader(UserModel _userModel);
        void downloadAvatar(String _url);
        void showAvatarProgress();
        void hideAvatarProgress();
        void navigateToMarketPlace(Constants.MarketplaceItems _item);
        void navigateToVideoDetails(Video _video);
        void navigateToPayouts();
        void navigateToPendingAssignments();
        int getItemTypeCount();
        void hideRefresh();
        void navigateToEditProfile();
        void navigateToLocation();
        int getLocalPositionIndex(int _index);
    }

    abstract class DashboardPresenter extends BasePresenter<DashboardView> {
        public DashboardPresenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setArgs(Bundle _args);
        public abstract void onEditReminderResult(Intent _intent);
        public abstract void onRetryClick();
        public abstract void getContent();
        public abstract void onRefresh();
        public abstract void avatarClicked();
        public abstract void setLocalPath(String _url);
        public abstract void onReminderClick(int _position);
        public abstract void onAssignmentsClick(int _position);
        public abstract void onMarketplaceClick(int _position);
        public abstract void onTutorialClick(int _position);
        public abstract void onVideoClick(int _position);
    }
}
