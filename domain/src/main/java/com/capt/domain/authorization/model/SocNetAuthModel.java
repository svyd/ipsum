package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 02.03.2016.
 */
public class SocNetAuthModel {

    @SerializedName("oauth_token")
    @Expose()
    private String oauthToken;

    @SerializedName("oauth_token_secret")
    @Expose()
    private String oauthTokenSecret;

    @SerializedName("user_id")
    @Expose()
    private String userId;

    @SerializedName("access_token")
    @Expose()
    private String accessToken;

    @Expose()
    private String deviceId;

    @Expose()
    private String deviceToken;

    @Expose()
    private String provider = "GOOGLE";

    private String networkType;

    public String getOauthToken() {
        return oauthToken;
    }

    public void setOauthToken(String oauthToken) {
        this.oauthToken = oauthToken;
    }

    public String getOauthTokenSecret() {
        return oauthTokenSecret;
    }

    public void setOauthTokenSecret(String oauthTokenSecret) {
        this.oauthTokenSecret = oauthTokenSecret;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public static class Builder {

        private SocNetAuthModel mBuildModel;

        public Builder() {
            mBuildModel = new SocNetAuthModel();
        }

        public Builder setOathToken(String _token) {
            mBuildModel.setOauthToken(_token);
            return this;
        }

        public Builder setOauthTokenSecret(String _tokenSecret) {
            mBuildModel.setOauthTokenSecret(_tokenSecret);
            return this;
        }

        public Builder setUserId(String _userId) {
            mBuildModel.setUserId(_userId);
            return this;
        }

        public Builder setAccessToken(String _accessToken) {
            mBuildModel.setAccessToken(_accessToken);
            return this;
        }

        public Builder setDeviceId(String _deviceId) {
            mBuildModel.setDeviceId(_deviceId);
            return this;
        }

        public Builder setDeviceToken(String _deviceToken) {
            mBuildModel.setDeviceToken(_deviceToken);
            return this;
        }

        public Builder setNetwork(String _network) {
            mBuildModel.setNetworkType(_network);
            return this;
        }

        public SocNetAuthModel build() {
            return mBuildModel;
        }
    }
}
