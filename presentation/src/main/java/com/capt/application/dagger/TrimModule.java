package com.capt.application.dagger;


import com.netcompss.loader.LoadJNI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.02.29..
 */
@Module
public class TrimModule {

    @Provides
    @Singleton
    protected LoadJNI provideFFMPEG() {
        return new LoadJNI();
    }
}
