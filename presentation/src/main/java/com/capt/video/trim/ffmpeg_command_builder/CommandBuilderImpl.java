package com.capt.video.trim.ffmpeg_command_builder;

import com.capt.data.annotation.PerFragment;
import com.capt.data.base.TimeUtil;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.29..
 */
@PerFragment
public class CommandBuilderImpl implements FFMPEGCommandBuilder {

    private TimeUtil mTimeUtil;

    @Inject
    public CommandBuilderImpl(TimeUtil _timeUtil) {
        mTimeUtil = _timeUtil;
    }

    @Override
    public String getTrimVideoCommand(long _startTime, long _endTime,
                                      String _input, String _outPut) {
        String startTime = mTimeUtil.getTimeFromInteger(_startTime);
        String endTime = mTimeUtil.getTimeFromInteger(_endTime);

        StringBuilder command = new StringBuilder();
        command.append("ffmpeg -y -i ")
                .append(_input)
                .append(" -ss ")
                .append(startTime)
                .append(" -to ")
                .append(endTime)
                .append(" -acodec copy -vcodec copy ")
                .append(_outPut);

        return command.toString();
    }
}
