package com.capt.domain.authorization.interactor;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 21.06.2016.
 */
public class UpdatePushInteractor extends BasePostInteractor<String> {

    private AuthRepository mRepository;

    @Inject
    public UpdatePushInteractor(AuthRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);

        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.updatePushToken(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
