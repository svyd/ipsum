package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;

/**
 * Created by richi on 2016.03.22..
 */
public class UpdateAvatarResponse {

    @Expose
    public String url;
}
