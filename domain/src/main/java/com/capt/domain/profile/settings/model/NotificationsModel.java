package com.capt.domain.profile.settings.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by root on 04.05.16.
 */
public class NotificationsModel implements Serializable {

	@Expose
	@SerializedName("levelUp")
	public boolean levelUp;

	@Expose
	@SerializedName("assignmentIsNotSold")
	public boolean assignmentIsNotSold;

	@Expose
	@SerializedName("newAssignment")
	public boolean newAssignment;

	@Expose
	@SerializedName("assignmentIsSold")
	public boolean assignmentIsSold;

	@Expose
	@SerializedName("videoIsSold")
	public boolean videoIsSold;
}
