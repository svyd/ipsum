package com.capt.domain.authorization.model;

import java.io.Serializable;

/**
 * Created by Svyd on 04.03.2016.
 */
public class CodeSignUpModel implements Serializable {

    private CodeModel codeModel;
    private SignUpModel signUpModel;
    private String phoneNumber;

    public CodeSignUpModel() {}

    public CodeSignUpModel(CodeModel _code, SignUpModel _signUp) {
        codeModel = _code;
        signUpModel = _signUp;
    }

    public CodeModel getCodeModel() {
        return codeModel;
    }

    public SignUpModel getSignUpModel() {
        return signUpModel;
    }

    public void setCodeModel(CodeModel codeModel) {
        this.codeModel = codeModel;
    }

    public void setSignUpModel(SignUpModel signUpModel) {
        this.signUpModel = signUpModel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
