package com.capt.authorization.phone_number.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.phone_number.PhoneNumberContract;
import com.capt.authorization.phone_number.PhoneNumberPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.03.17..
 */
@Module
public class PhoneNumberModule {

    private PhoneNumberContract.View mView;

    public PhoneNumberModule(PhoneNumberContract.View _view) {
        mView = _view;
    }

    @Provides @PerFragment
    protected PhoneNumberContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    protected PhoneNumberContract.Presenter providePresenter(PhoneNumberPresenter _presenter) {
        return _presenter;
    }
}
