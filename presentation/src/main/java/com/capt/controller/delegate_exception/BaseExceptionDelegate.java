package com.capt.controller.delegate_exception;

import com.capt.base.BaseView;

import java.lang.reflect.ParameterizedType;

/**
 * Created by Svyd on 7/28/16.
 */
public abstract class BaseExceptionDelegate<T extends Throwable> {

    protected BaseView mBaseView;
    protected BaseExceptionDelegate mDelegate;
    private Class<T> clazz;

    public BaseExceptionDelegate() {
        clazz = getGenericClass();
    }

    @SuppressWarnings("unchecked")
    public Class<T> getGenericClass() {
        if (clazz == null) {
            ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
            clazz = (Class<T>) pt.getActualTypeArguments()[0];
        }

        return clazz;
    }

    public void setView(BaseView _view) {
        mBaseView = _view;
        if (mDelegate != null)
            mDelegate.setView(_view);
    }

    @SuppressWarnings("unchecked")
    public void onError(Throwable _exception) {
        if (isRightException(_exception))
            handleException(((T) _exception));
        else if (mDelegate != null)
            mDelegate.onError(_exception);
    }

    public boolean isRightException(Throwable _exception) {
        return clazz.isAssignableFrom(_exception.getClass());
    }

    public abstract void handleException(T _exception);
}
