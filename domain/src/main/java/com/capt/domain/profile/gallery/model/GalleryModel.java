package com.capt.domain.profile.gallery.model;

/**
 * Created by Svyd on 4/23/16.
 */
public class GalleryModel {
    private String mPath;
    private boolean mIsRightDuration;

    public String getPath() {
        return mPath;
    }

    public void setPath(String _path) {
        mPath = _path;
    }

    public boolean isRightDuration() {
        return mIsRightDuration;
    }

    public void setRightDuration(boolean _isRightVideo) {
        mIsRightDuration = _isRightVideo;
    }

}
