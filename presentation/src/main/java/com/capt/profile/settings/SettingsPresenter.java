package com.capt.profile.settings;

import android.app.Activity;
import android.content.Intent;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.model.SettingsModel;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 20.04.2016.
 */
public class SettingsPresenter extends SettingsContract.Presenter {

    private SettingsContract.View mView;
    private BaseInteractor mInteractor;
    private BaseInteractor mSignOutInteractor;
    private SettingsModel mSettings;

    @Inject
    public SettingsPresenter(SettingsContract.View _view,
                             @Named(Constants.NamedAnnotation.SIGN_OUT_INTERACTOR)
                             BaseInteractor _signOutInteractor,
                             @Named(Constants.NamedAnnotation.GET_SETTINGS_INTERACTOR)
                             BaseInteractor _interactor,
                             BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
        mSignOutInteractor = _signOutInteractor;
    }

    @Override
    public void initialize() {
        mInteractor.execute(new Subscriber(this));
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void onAccountSettingsClick(int position) {
        switch (position) {
            case 0:
                mView.navigateToNotificationSettings(mSettings);
                break;
            case 1:
                mView.navigateToPermissionSettings();
                break;
            case 2:
                mView.navigateToCurrencySettings(mSettings);
                break;
            case 3:
                mView.navigateToPayPalSettings();
                break;
        }
    }


    private void showSettingOnView(SettingsModel settingsModel) {
        mSettings = settingsModel;
        mView.showSettings(settingsModel);
    }

    @Override
    public void onProfileSettingsClick(int position) {

        String setting = mSettings
                .getItems()
                .get(1)
                .getItems()
                .get(position)
                .left;

        switch (setting) {
            case Constants.Settings.Profile.EDIT_PROFILE:
                mView.navigateToEditProfile();
                break;
            case Constants.Settings.Profile.CHANGE_PASSWORD:
                mView.navigateToPasswordSettings();
                break;
            case Constants.Settings.Profile.LOG_OUT:
                mSignOutInteractor.execute(new SignOutObserver(this));
                break;
        }
    }

    @Override
    public void onSupportSettingsClick(int position) {
        mView.navigateToSupportSettings();
    }

    @Override
    public void onRetryClick() {
        mView.hideNoInternetConnectionError();
        mView.showProgress();
        mInteractor.execute(new Subscriber(this));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case AccountSettingsActivity.CODE_CURRENCY:
                    onCurrencyResult(data);
                    break;
                case AccountSettingsActivity.CODE_NOTIFICATIONS:
                    onNotificationsResult(data);
                    break;
            }
        }
    }

    private void onNotificationsResult(Intent _data) {
        mSettings = (SettingsModel) _data.getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE);
    }

    void onCurrencyResult(Intent _data) {
        SettingsModel setting = (SettingsModel) _data.getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE);
        mSettings = setting;
        mView.showSettings(setting);
    }

    private class SignOutObserver extends BaseObserver {
        public SignOutObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(Object o) {
            mView.logOut();
        }
    }

    private class Subscriber extends BaseObserver<SettingsModel> {
        public Subscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onNext(SettingsModel settingsModel) {
            showSettingOnView(settingsModel);
        }
    }
}
