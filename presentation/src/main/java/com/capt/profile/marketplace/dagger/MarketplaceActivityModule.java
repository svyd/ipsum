package com.capt.profile.marketplace.dagger;

import com.capt.base.BaseActivity;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.data.annotation.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 03.06.2016.
 */
@Module
public class MarketplaceActivityModule {

    private BaseActivity mActivity;

    public MarketplaceActivityModule(BaseActivity _activity) {
        mActivity = _activity;
    }

    @Provides
    @PerActivity
    protected BaseActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }

}
