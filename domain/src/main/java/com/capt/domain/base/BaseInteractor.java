package com.capt.domain.base;


import com.capt.domain.executor.PostExecutionThread;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by richi on 2015.10.19..
 */
public abstract class BaseInteractor {

    protected final PostExecutionThread mPostExecutionThread;

    private Subscription mGetSubscription = Subscriptions.empty();

    public BaseInteractor(PostExecutionThread postExecutionThread) {
        mPostExecutionThread = postExecutionThread;
    }

    @SuppressWarnings("unchecked")
    public void execute(Observer _subscriber) {
        mGetSubscription = buildGetObserver()
                .subscribeOn(Schedulers.newThread())
                .observeOn(mPostExecutionThread.getScheduler())
                .subscribe(_subscriber);
    }

    public void unSubscribe() {
        if (!mGetSubscription.isUnsubscribed())
            mGetSubscription.unsubscribe();
    }

    protected abstract Observable buildGetObserver();

    public static class SimpleObserver<T> implements Observer<T> {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(T t) {

        }
    }
}
