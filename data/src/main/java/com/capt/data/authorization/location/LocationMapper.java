package com.capt.data.authorization.location;

import android.location.Address;

import com.capt.data.base.TypeMapper;
import com.capt.domain.authorization.location.model.PredictionsModel;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 16.03.2016.
 */
public class LocationMapper implements TypeMapper<PredictionsModel, List<Address>> {

    @Inject
    public LocationMapper() {}

    @Override
    public PredictionsModel convert(List<Address> _value) {
        PredictionsModel.Prediction[] predictions = new PredictionsModel.Prediction[_value.size()];
        for (int i = 0; i < predictions.length; i++) {
            predictions[i] = new PredictionsModel.Prediction();
            predictions[i].country = _value.get(i).getCountryCode();
            predictions[i].city = _value.get(i).getLocality();
            predictions[i].description = _value.get(i).getCountryName();
            predictions[i].address = _value.get(i).getAddressLine(0) + ", " + _value.get(i).getAddressLine(1)  + ", " + _value.get(i).getCountryName();
        }
        return new PredictionsModel(predictions);
    }
}
