package com.capt.data.assignment;

import com.capt.data.base.DateTimeUtility;
import com.capt.data.base.TypeMapper;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.profile.dashboard.model.base.Item;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 04.04.2016.
 */
public class AssignmentsMapper implements TypeMapper<List<Assignment>, List<Assignment>> {

    private DateTimeUtility mDateUtility;

    @Inject
    public AssignmentsMapper(DateTimeUtility _utility) {
        mDateUtility = _utility;
    }

    @Override
    public List<Assignment> convert(List<Assignment> _value) {
        for (Assignment assignment : _value) {
            assignment.when = mDateUtility.formatDate(assignment.date, false);
        }

        return _value;
    }
}
