package com.capt.domain.profile.settings;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.profile.settings.model.UpdateSettingsModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by root on 04.05.16.
 */
public class UpdateSettingsInteractor extends BasePostInteractor<UpdateSettingsModel> {

	SettingsRepository mRepository;

	@Inject
	public UpdateSettingsInteractor(SettingsRepository _repository, PostExecutionThread postExecutionThread) {
		super(postExecutionThread);
		mRepository = _repository;
	}

	@Override protected Observable buildPostObservable(UpdateSettingsModel _data) {
		return mRepository.updateSettings(_data);
	}

	@Override protected Observable buildGetObserver() {
		return null;
	}
}
