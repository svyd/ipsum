package com.capt.domain.video.upload;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.model.PayPalVideoModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by rMozes on 8/2/16.
 */
public class PayPalInteractor extends BasePostInteractor<PayPalVideoModel> {

    private VideoUploadRepository mRespository;

    @Inject
    public PayPalInteractor(VideoUploadRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRespository = _repository;
    }

    @Override
    protected Observable buildPostObservable(PayPalVideoModel _data) {
        return mRespository.uploadWithPayPalCode(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
