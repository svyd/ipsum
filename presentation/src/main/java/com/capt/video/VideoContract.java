package com.capt.video;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

/**
 * Created by richi on 2016.03.11..
 */
public interface VideoContract {

    interface View extends BaseView {
        void initOverViewFragment(Bundle _args);
        void initTrimFragment(Bundle _args);
    }

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setArgs(Bundle _args);
    }
}
