package com.capt.domain.profile.payout;

import rx.Observable;

/**
 * Created by rMozes on 8/3/16.
 */
public interface PayOutRepository {
    Observable<PayoutScreenModel> getPayouts();
}
