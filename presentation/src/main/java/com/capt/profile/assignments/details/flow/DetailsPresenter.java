package com.capt.profile.assignments.details.flow;

import com.capt.authorization.location.LocationInteractorManager;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.base.DateTimeUtility;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.accept.model.AcceptAssignmentModel;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.profile.assignments.details.DetailsContract;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by root on 20.04.16.
 */
public abstract class DetailsPresenter extends DetailsContract.Presenter {

    protected DetailsContract.View mView;
    protected Assignment mAssignment;

    private BasePostInteractor<AcceptAssignmentModel> mInteractor;
    private LocationInteractorManager mLocationManager;
    private AcceptAssignmentModel acceptAssignmentModel;
    private DateTimeUtility mDateUtility;
    private boolean mIsRequestInProgress;

    public DetailsPresenter(DateTimeUtility _utility,
                            DetailsContract.View _view,
                            BasePostInteractor<AcceptAssignmentModel> _interactor,
                            LocationInteractorManager _manager,
                            BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
        mLocationManager = _manager;
        mDateUtility = _utility;
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
        mLocationManager.unSubscribe();
    }

    @Override
    public void setAssignmentModel(Assignment _model) {
        mAssignment = _model;
//        mAssignment.creator = _model.creator;
//        mAssignment.id = _model.id;
//        mAssignment.title = validate(_model.title);
//        mAssignment.description = validate(_model.description);
//        String price = validate(_model.currency.getSymbol()) + validate(_model.price);
//        mAssignment.currency = _model.currency;
//        mAssignment.price = price;
//        if ((_model.location != null)) {
//            mAssignment.location = _model.location;
//        }
//        mAssignment.date = validate(_model.date);
//        mAssignment.when = mDateUtility.formatDate(_model.date, true);
//        mAssignment.who = validate(_model.creator.getCompany());
//        mAssignment.formattedQuality = "";
//        if ((_model.quality != null)) {
//            mAssignment.quality = _model.quality;
//            mAssignment.formattedQuality = String.valueOf(_model.quality.getWidth()) + "p, " + String.valueOf(_model.quality.getFps()) + "fps";
//        }
        mView.setUpView(mAssignment);
    }

    @Override
    public void setType(String _type) {
        if (_type.equals(Constants.AssignmentTypes.FOR_GRABS)) {
            mView.setForGrabs();
        } else if (_type.equals(Constants.AssignmentTypes.IN_PROGRESS)) {
            mView.setInProgress();
        }
    }

    @Override
    public void setLocation() {
        mView.addMarkerToMap(new LatLng(mAssignment.location[1], mAssignment.location[0]));
        mLocationManager.execute(new LocationModel(mAssignment.location[1], mAssignment.location[0]), new LocationObserver(this));
    }

    @Override
    public void onLocationClick() {
        mView.navigateToMapScreen(mAssignment.location[1], mAssignment.location[0]);
    }

    @Override
    public void onCaptClick() {
        mView.showCaptDialog(mAssignment);
    }

    @Override
    public void onAcceptClick() {
        sendAcceptRequest(true);
    }

    @Override
    public void onNoThanksClick() {
        sendAcceptRequest(false);
    }

    protected void sendAcceptRequest(boolean accept) {
        if (mIsRequestInProgress)
            return;

        mIsRequestInProgress = true;
        acceptAssignmentModel = new AcceptAssignmentModel(mAssignment.id, accept);
        mInteractor.execute(acceptAssignmentModel, new AcceptAssignmentSubscriber(this));
    }

    private String validate(String _field) {
        return (_field == null) ? "" : _field;
    }

    private class LocationObserver extends BaseObserver<PredictionsModel> {
        public LocationObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            String address = predictionsModel.predictions[0].address;
            mAssignment.where = address;
            mView.setAddress(address);
        }
    }

    private class AcceptAssignmentSubscriber extends BaseObserver<SuccessModel> {
        public AcceptAssignmentSubscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            if (acceptAssignmentModel.accept) {
                mView.navigateToOverviewScreen(mAssignment);
            } else {
                mView.navigateBackToAssignments();
            }

            mIsRequestInProgress = false;
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            mIsRequestInProgress = false;
        }

        @Override
        public void onNext(SuccessModel successModel) {
            mView.hideProgress();
        }
    }
}
