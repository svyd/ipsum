package com.capt.profile.assignments.details.overview.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.assignments.details.overview.OverviewFragment;

import dagger.Subcomponent;

/**
 * Created by root on 25.04.16.
 */
@PerFragment
@Subcomponent(modules = OverviewModule.class)
public interface OverviewComponent {
	void inject(OverviewFragment _fragment);
}
