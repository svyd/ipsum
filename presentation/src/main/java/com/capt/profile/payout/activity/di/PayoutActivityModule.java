package com.capt.profile.payout.activity.di;

import android.support.v4.app.FragmentManager;

import com.capt.base.BaseActivity;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.data.annotation.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by rMozes on 8/3/16.
 */
@Module
public class PayoutActivityModule {

    private BaseActivity mBaseActivity;

    public PayoutActivityModule(BaseActivity _activity) {
        mBaseActivity = _activity;
    }

    @PerActivity
    @Provides
    protected BaseActivity provideBaseActivity() {
        return mBaseActivity;
    }

    @PerActivity
    @Provides
    protected FragmentManager provideFragmentManager(BaseActivity _activity) {
        return _activity.getSupportFragmentManager();
    }

    @Provides
    @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }
}
