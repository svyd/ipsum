package com.capt.video.currency.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.currency.CurrencyFragment;

import dagger.Subcomponent;

/**
 * Created by root on 25.03.16.
 */
@PerFragment
@Subcomponent(modules = CurrencyModule.class)
public interface CurrencyComponent {
	void inject(CurrencyFragment _fragment);
}
