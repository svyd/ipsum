package com.capt.data.authorization.repository.country;

import com.capt.data.authorization.repository.country.cache.CountryCache;
import com.capt.domain.authorization.country_code.CountryRepository;
import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by richi on 2016.03.16..
 */
public class CountryRepositoryImpl implements CountryRepository {

    private CountryCache mCache;
    private CountryDataStoreFactory mFactory;

    @Inject
    public CountryRepositoryImpl(CountryCache _cache, CountryDataStoreFactory _factory) {
        mCache = _cache;
        mFactory = _factory;
    }

    @Override
    public Observable<List<CountryModel>> getCountryModels() {
        return mFactory.createStore()
                .getCountryModel()
                .doOnNext(mCache::setToCache);
    }
}
