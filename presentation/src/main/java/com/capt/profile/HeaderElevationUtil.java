package com.capt.profile;

import android.animation.ValueAnimator;
import android.os.Build;
import android.view.View;

import com.capt.profile.dashboard.ui.adapter.StickyHeaderLayoutManager;

import javax.inject.Inject;

/**
 * Created by Svyd on 27.06.2016.
 */
@SuppressWarnings("ALL")
public class HeaderElevationUtil {

    private ValueAnimator mElevationAnimator;

    @Inject
    public HeaderElevationUtil() {
    }

    public  void onHeaderPositionChanged(int sectionIndex, View header, StickyHeaderLayoutManager.HeaderPosition oldPosition, StickyHeaderLayoutManager.HeaderPosition newPosition) {
        boolean e = newPosition == StickyHeaderLayoutManager.HeaderPosition.STICKY;
        if (oldPosition != StickyHeaderLayoutManager.HeaderPosition.NONE) {
            animate(header, e ? 0 : 1, e ? 1 : 0);
        }
    }


    // TODO: 8/4/16 Leak:
    private void animate(final View _view, float _from, float _to) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mElevationAnimator = ValueAnimator.ofFloat(_from * 12, _to * 12);
            mElevationAnimator.setDuration(75);
            mElevationAnimator.addUpdateListener(animation ->
                    _view.setElevation((float) animation.getAnimatedValue()));
            mElevationAnimator.start();
        }
    }
}