package com.capt.domain.profile.settings.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 04.05.16.
 */
public class UpdateSettingsModel {

	@Expose
	@SerializedName("defaultCurrency")
	public String defaultCurrencyId;

	@Expose
	@SerializedName("notifications")
	public NotificationsModel notifications;
}
