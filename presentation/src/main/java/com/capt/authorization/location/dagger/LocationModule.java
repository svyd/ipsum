package com.capt.authorization.location.dagger;

import com.capt.authorization.location.flow.AddLocationPresenter;
import com.capt.authorization.location.flow.UpdateProfileLocationPresenter;
import com.capt.data.annotation.PerFragment;
import com.capt.authorization.location.LocationContract;
import com.capt.authorization.location.flow.LocationPresenter;
import com.capt.video.Flow;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 16.03.2016.
 */
@Module
public class LocationModule {

    private LocationContract.LocationView mView;
    private int mFlow;

    public LocationModule(LocationContract.LocationView _view, int _flow) {
        mView = _view;
        mFlow = _flow;
    }

    @Provides @PerFragment
    protected LocationContract.LocationView provideLocationView() {
        return mView;
    }

    @Provides @PerFragment
    protected LocationContract.LocationPresenter provideLocationPresenter(LocationPresenter _presenter,
                                                                          AddLocationPresenter _addPresenter,
                                                                          UpdateProfileLocationPresenter _updateProfile) {
        switch (mFlow) {
            case Flow.ADD_LOCATION:
                return _addPresenter;
            case Flow.UPDATE_PROFILE:
                return _updateProfile;
            default:
                return _presenter;
        }
    }
}
