package com.capt.profile.gallery.flow;

import android.os.Bundle;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.gallery.model.GalleryModel;
import com.capt.profile.gallery.GalleryContract;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 17.05.2016.
 */
public abstract class AbstractGalleryPresenter extends GalleryContract.Presenter {

    private GalleryContract.View mView;
    private BaseInteractor mInteractor;
    private List<GalleryModel> mData;

    public AbstractGalleryPresenter(GalleryContract.View _view,
                                    @Named(Constants.NamedAnnotation.GALLERY_INTERACTOR)
                                    BaseInteractor _interactor, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
    }

    @Override
    public void initialize() {
        if (mData == null)
            mInteractor.execute(new GalleryObserver(this));
    }

    protected GalleryContract.View getView() {
        return mView;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void setArgs(Bundle _args) {

    }

    @Override
    public void onRefresh() {
        mInteractor.execute(new GalleryObserver(this));
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    private class GalleryObserver extends BaseObserver<List<GalleryModel>> {
        public GalleryObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideRefresh();
            if (mData.size() != 0) {
                mView.hideEmptyView();
                mView.showGalleryData(mData);
            } else {
                mView.showEmptyView();
            }
        }

        @Override
        public void onNext(List<GalleryModel> galleryModels) {
            mData = galleryModels;
        }
    }
}
