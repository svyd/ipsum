package com.capt.domain.profile.dashboard.model;

/**
 * Created by Svyd on 19.05.2016.
 */
public abstract class ReminderId {
    public static final int NAME = 0;
    public static final int EMAIL = 1;
    public static final int LOCATION = 2;

}
