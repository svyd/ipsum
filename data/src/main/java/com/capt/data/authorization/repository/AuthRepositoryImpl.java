package com.capt.data.authorization.repository;

import android.content.SharedPreferences;

import com.capt.data.authorization.repository.datasource.AuthStoreFactory;
import com.capt.data.authorization.repository.datasource.UserCache;
import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.PasswordModel;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;

/**
 * Created by Svyd on 02.03.2016.
 */
public class AuthRepositoryImpl implements AuthRepository {

    SharedPreferences mCookies;
    AuthStoreFactory mFactory;
    UserCache mCache;
    SharedPreferences mPreferences;

    @Inject
    public AuthRepositoryImpl(SharedPreferences _preferences,
                              @Named(Constants.NamedAnnotation.PREFERENCES_COOKIES)
                              SharedPreferences _cookies,
                              AuthStoreFactory _factory,
                              UserCache _cache) {
        mFactory = _factory;
        mCache = _cache;
        mCookies = _cookies;
        mPreferences = _preferences;
    }

    @Override
    public Observable<UserModel> signInWithSocNet(SocNetAuthModel _socNetAuthModel) {
        String token = mPreferences.getString(Constants.DEVICE_GCM_TOKEN, "");

        _socNetAuthModel.setDeviceToken(token);
        return mFactory.create().signInWithSocNet(_socNetAuthModel)
                .flatMap(userModel -> mCache.cacheUser(userModel));
    }

    @Override
    public Observable<SuccessModel> getAuthCode(PhoneNumberModel _model) {
        return mFactory.create().getAuthCode(_model);
    }

    @Override
    public Observable<UserModel> verifyCodeAndSignUp(CodeSignUpModel _data) {
        String token = mPreferences.getString(Constants.DEVICE_GCM_TOKEN, "");

        _data.getSignUpModel().setDeviceToken(token);
        return mFactory.create().verifyCodeAndSignUp(_data)
                .flatMap(userModel -> mCache.cacheUser(userModel));
    }

    @Override
    public Observable<UserModel> verifyCodeAndGetProfile(CodeSignUpModel _data) {
        return mFactory.create().verifyCodeAndGetProfile(_data)
                .flatMap(userModel -> mCache.cacheUser(userModel));
    }

    @Override
    public Observable<UserModel> signIn(SignInModel _model) {
        String token = mPreferences.getString(Constants.DEVICE_GCM_TOKEN, "");

        _model.setDeviceToken(token);
        return mFactory.create().signIn(_model)
                .flatMap(userModel -> mCache.cacheUser(userModel));
    }

    @Override
    public Observable<SuccessModel> signOut() {
        return mFactory.create()
                .signOut()
                .flatMap(this::clearCookies)
                .flatMap(s -> mCache.clear());
    }

    private Observable<SuccessModel> clearCookies(SuccessModel _success) {
        return Observable.create(subscriber -> {
            try {
                mCookies.edit().clear().commit();
                subscriber.onNext(_success);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<SuccessModel<String>> checkEmail(String _email) {
        return mFactory.create().checkEmail(_email);
    }

    @Override
    public Observable<UserModel> updateUserModel(UserModel _data) {
        return mFactory.create().updateUserModel(_data)
                .flatMap(userModel -> mCache.cacheUser(userModel));
    }

    @Override
    public Observable<UserModel> updateUserModel(LocationModel _data) {
        return mCache.getUser()
                .flatMap(userModel -> {
                    userModel.setCountry(_data.mCountry);
                    userModel.setCity(_data.mCity);
                    userModel.setLocation(_data.getAsLonLat());

                    return mFactory.create().updateUserModel(userModel);
                })
                .flatMap(serverUser -> mCache.cacheUser(serverUser));
    }

    @Override
    public Observable<SuccessModel> updatePushToken(String _data) {
        return mFactory.create().updatePushToken(_data);
    }

    @Override
    public Observable<SuccessModel> changePassword(PasswordModel _password) {
        return mFactory.create().changePassword(_password);
    }
}
