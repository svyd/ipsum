package com.capt.application.dagger;

import android.content.Context;
import android.content.SharedPreferences;

import com.capt.data.annotation.PerActivity;
import com.capt.data.annotation.PerFragment;
import com.capt.data.authorization.net.ApiConstants;
import com.capt.data.authorization.net.retrofit.AuthService;
import com.capt.data.authorization.repository.AuthRepositoryImpl;
import com.capt.data.cookies.AddCookiesInterceptor;
import com.capt.data.cookies.PersistentCookieStore;
import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.interactor.CodeInteractor;
import com.capt.domain.authorization.interactor.EmailCheckInteractor;
import com.capt.domain.authorization.interactor.UpdatePushInteractor;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.CookieJar;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by richi on 2016.03.05..
 */
@Module
public class AppRestModule {

    private String mBaseUrl;

    public AppRestModule(String _baseUrl) {
        mBaseUrl = _baseUrl;
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.HEADER_INTERCEPTOR)
    protected HttpLoggingInterceptor provideHeaderInterceptor() {
        HttpLoggingInterceptor headerInterceptor = new HttpLoggingInterceptor();
        headerInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        return headerInterceptor;
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.BODY_INTERCEPTOR)
    protected HttpLoggingInterceptor provideBodyInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return interceptor;
    }

    @Provides
    @Singleton
    protected CookieStore provideCookieStore(@Named(Constants.NamedAnnotation.PREFERENCES_COOKIES)
                                             SharedPreferences _preferences) {
        return new PersistentCookieStore(_preferences);
    }

    @Provides
    @Singleton
    protected CookieHandler provideCookieHandler(CookieStore _store) {
        return new CookieManager(_store, CookiePolicy.ACCEPT_ALL);
    }

    @Provides
    @Singleton
    protected CookieJar provideCookieJar(CookieHandler _handler) {
        return new JavaNetCookieJar(_handler);
    }

    @Provides
    @Singleton
    protected Interceptor provideCookieInterceptor(AddCookiesInterceptor _interceptor) {
        return _interceptor;
    }

    @Provides
    @Singleton
    protected OkHttpClient provideOkHttpClient(
            CookieJar _cookieJar,
            @Named(Constants.NamedAnnotation.HEADER_INTERCEPTOR) HttpLoggingInterceptor _headerInterceptor,
            @Named(Constants.NamedAnnotation.BODY_INTERCEPTOR) HttpLoggingInterceptor _bodyInterceptor,
            Interceptor _cookieInterceptor) {
        return new OkHttpClient.Builder()
                .cookieJar(_cookieJar)
                .addInterceptor(_cookieInterceptor)
                .addInterceptor(_bodyInterceptor)
                .addInterceptor(_headerInterceptor)
                .connectTimeout(ApiConstants.CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(ApiConstants.READ_TIME_OUT, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    protected Gson provideGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.NON_EXCLUDED_GSON)
    protected Gson provideNonExcludedGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    protected Converter.Factory provideConverterFactory(Gson _gson) {
        return GsonConverterFactory.create(_gson);
    }

    @Provides
    @Singleton
    protected CallAdapter.Factory provideCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.MAIN_RETROFIT)
    protected Retrofit provideRetrofit(OkHttpClient _client,
                                       CallAdapter.Factory _factory,
                                       Converter.Factory _converterFactory) {
        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addCallAdapterFactory(_factory)
                .addConverterFactory(_converterFactory)
                .client(_client)
                .build();
    }

    @Provides
    @Singleton
    protected AuthService provideAuthService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                             Retrofit _retrofit) {
        return _retrofit.create(AuthService.class);
    }

    @Provides
    @Singleton
    protected AuthRepository provideAuthRep(AuthRepositoryImpl _authRep) {
        return _authRep;
    }

    @Singleton
    @Provides
    @Named(Constants.NamedAnnotation.PENDING_UPDATE_TOKEN_INTERACTOR)
    protected BasePostInteractor<String> provideUpdatePushInteractor(UpdatePushInteractor _interactor) {
        return _interactor;
    }

    @Provides @Singleton
    @Named(Constants.NamedAnnotation.VERIFICATION_INTERACTOR)
    BasePostInteractor<PhoneNumberModel> provideVerificationInteractor(CodeInteractor _interactor) {
        return _interactor;
    }
}
