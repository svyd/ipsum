package com.capt.domain.video.video;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 02.05.2016.
 */
public class DeleteVideoInteractor extends BasePostInteractor<String>{

    private VideoRepository mRepository;

    @Inject
    public DeleteVideoInteractor(VideoRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.deleteVideo(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
