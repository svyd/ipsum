package com.capt.video.overview.flow;

import android.os.Bundle;
import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;
import com.capt.video.Flow;
import com.capt.video.overview.OverviewContract;
import com.capt.video.trim.time_util.TimeUtilImpl;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 29.04.2016.
 */
public class DetailsPresenter extends AbstractPresenter {

    private static final String TAG = DetailsPresenter.class.getSimpleName();
    private Video mData;
    private TimeUtilImpl mTimeUtil;
    private BasePostInteractor<String> mDeleteInteractor;

    @Inject
    public DetailsPresenter(BasePostInteractor<String> _deleteInteractor,
                            TimeUtilImpl _timeUtil,
                            OverviewContract.View _view, BaseExceptionDelegate _delegate,
                            @Named(Constants.PendingVideoConstants.PAY_PAL_INTERACTOR)
                            BasePostInteractor<PayPalVideoModel> _payPalInteractor) {
        super(_view, _delegate, _payPalInteractor);
        mTimeUtil = _timeUtil;
        mDeleteInteractor = _deleteInteractor;
    }

    @Override
    public void setData(Bundle _model) {
        if (_model.containsKey(Constants.BUNDLE_VIDEO)) {
            mData = (Video) _model.getSerializable(Constants.BUNDLE_VIDEO);
        } else {
            throw new IllegalStateException("Arguments should contain Video");
        }
    }

    @Override
    public void initialize() {
        getView().setEditableTitle(false);
        getView().setDetailsBtnVisibility(true);
        getView().setVideoPlayability(false);
        getView().setButtonsVisibility(false);
        getView().setListVisibility(false);
        getView().setTitle("Recent");
        getView().showDescription(true);
        getView().setCompletedVisibility(false);
        getView().setPrice(mData.currency.getSymbol() + mData.price);
        getView().setVideoDescription(mData.description);
        getView().setLocation(mData.country + ", " + mData.city);
        getView().setVideoTitle(mData.title);
        getView().setVideoProperties(getFormattedProperties(Math.round(Double.parseDouble(mData.duration)), mData.size));

        getView().setCategories(mData.categories);
        getView().setThumbnail(mData.thumbnail);
    }

    @Override
    public void onFrameBtnClick() {
        Bundle args = new Bundle();
        args.putSerializable(Constants.BUNDLE_VIDEO, mData);
        args.putInt(Constants.FLOW, Flow.EDIT);
        getView().navigateToDescription(args);
    }

    @Override
    public void onDetailsTextBtnClick() {
        mDeleteInteractor.execute(mData.id, new DeleteObserver(this));
    }

    protected String getFormattedProperties(long _duration, String _size) {
        long size = Long.parseLong(_size);

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    @Override
    public void onStop() {
        mDeleteInteractor.unSubscribe();
    }

    class DeleteObserver extends BaseObserver<SuccessModel> {
        public DeleteObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(SuccessModel successModel) {
            getView().navigateBack();
            Log.d(TAG, "onNext() called with: " + "successModel = [" + successModel + "]");
        }
    }
}
