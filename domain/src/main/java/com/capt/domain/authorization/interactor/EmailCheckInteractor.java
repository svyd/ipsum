package com.capt.domain.authorization.interactor;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 09.03.2016.
 */
public class EmailCheckInteractor extends BasePostInteractor<String> {

    private AuthRepository mRepository;

    @Inject
    public EmailCheckInteractor(AuthRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.checkEmail(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
