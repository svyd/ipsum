package com.capt.profile.assignments.in_progress;

import android.content.Intent;

import com.capt.domain.global.Constants;
import com.capt.profile.assignments.base.BaseAssignmentsFragment;

/**
 * Created by root on 15.04.16.
 */
public class InProgressFragment extends BaseAssignmentsFragment {

    @Override
    protected String getType() {
        return Constants.AssignmentTypes.IN_PROGRESS;
    }


}
