package com.capt.video.category;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

import java.util.Set;

/**
 * Created by root on 07.04.16.
 */
public interface CategoryContract {

	interface View extends BaseView {
		void selectCategories(int[] positions);
		void navigateBack();
		void navigateToVideoViewScreen(Bundle _args);
		void setData(String[] _data);
		void showToast(String _text);
		Set<Integer> getSelectedList();
	}

	abstract class Presenter extends BasePresenter<CategoryContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void onNextClick();
		public abstract void setData(Bundle _args);
	}
}
