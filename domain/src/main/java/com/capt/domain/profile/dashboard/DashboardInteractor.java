package com.capt.domain.profile.dashboard;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.profile.dashboard.model.DashboardParamsModel;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public class DashboardInteractor extends BasePostInteractor<DashboardParamsModel> {

    DashboardRepository mRepository;

    @Inject
    public DashboardInteractor(DashboardRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(DashboardParamsModel _data) {
        return mRepository.getDashboard(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
