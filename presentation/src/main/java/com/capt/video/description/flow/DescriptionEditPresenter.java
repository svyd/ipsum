package com.capt.video.description.flow;

import com.capt.authorization.location.LocationInteractorManager;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.video.description.DescriptionContract;
import com.capt.video.trim.time_util.TimeUtilImpl;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by Svyd on 01.05.2016.
 */
public class DescriptionEditPresenter extends AbstractDescriptionPresenter {


    @Inject
    public DescriptionEditPresenter(DescriptionContract.View _view, LocationInteractorManager _manager,
                                    TimeUtilImpl _timeUtil, BaseExceptionDelegate _delegate) {
        super(_view, _manager, _timeUtil, _delegate);
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().setVideoVisibility(false);
        getView().setToolbarTitle("Edit");
        getView().showToolbar(true);
        getView().setDescription(getData().description);
        getView().setVideoProperties(getFormattedProperties(Long.parseLong(getData().duration), getData().size));
        getView().setThumbnail(getData().thumbnail);
        getView().setTitle(getData().getTitle());
        getView().setDescription(getData().getDescription());
    }

    private String getFormattedProperties(long _duration, String _size) {
        long size = Long.parseLong(_size);

        // convert Bytes to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = getTimeUtil().getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }


}
