package com.capt.domain.video.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Svyd on 26.04.2016.
 */
public class Quality implements Serializable {
    @Expose
    @SerializedName("width")
    public String width;

    @Expose
    @SerializedName("height")
    public String height;

    @Expose
    @SerializedName("fps")
    public String fps;

    public int duration;

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    public String getFps() {
        return fps;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public static class Builder {
        public String width;
        public String height;
        public String fps;
        public int duration;

        public Builder setDuration(int duration) {
            this.duration = duration;
            return this;
        }

        public Builder setFps(String fps) {
            this.fps = fps;
            return this;
        }

        public Builder setHeight(String height) {
            this.height = height;
            return this;
        }

        public Builder setWidth(String width) {
            this.width = width;
            return this;
        }

        public Quality build() {
            Quality quality = new Quality();

            quality.width = width;
            quality.height = height;
            quality.fps = fps;

            return quality;
        }
    }

}
