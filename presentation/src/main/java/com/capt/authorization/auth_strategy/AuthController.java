package com.capt.authorization.auth_strategy;

import android.content.Intent;

import com.capt.data.annotation.PerFragment;
import com.capt.base.BaseFragment;
import com.capt.domain.global.Constants;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by richi on 2016.02.24..
 */
@PerFragment
public class AuthController implements AuthorizationStrategy {

    private AuthorizationStrategy mDelegateStrategy;

    //The FacebookStrategy is the default strategy
    @Inject
    public AuthController(@Named(Constants.AuthType.FACEBOOK) AuthorizationStrategy _delegate) {
        mDelegateStrategy = _delegate;
    }

    public void setDelegateStrategy(AuthorizationStrategy _delegate) {
        mDelegateStrategy = _delegate;
    }

    @Override
    public void logIn(BaseFragment _fragment, CaptCallback _callback) {
        mDelegateStrategy.logIn(_fragment, _callback);
    }

    @Override
    public void onActivityResult(int _resultCode, int _requestCode, Intent _intent) {
        mDelegateStrategy.onActivityResult(_resultCode, _requestCode, _intent);
    }
}
