package com.capt.authorization.join_fragment;

import android.util.Log;

import com.capt.authorization.auth_strategy.AuthController;
import com.capt.authorization.auth_strategy.AuthStrategyProvider;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.facebook.login.LoginManager;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Subscriber;

/**
 * Created by richi on 2016.02.23..
 */
public class JoinPresenter extends JoinContract.Presenter {

    private JoinContract.View mView;

    private AuthController mAuthController;
    private AuthStrategyProvider mStrategyFactory;
    private BasePostInteractor<SocNetAuthModel> mSocNetInteractor;
    private DeviceIdProvider mDeviceIdHelper;

    @Inject
    public JoinPresenter(JoinContract.View _view,
                         DeviceIdProvider _idProvider,
                         @Named(Constants.NamedAnnotation.SOC_NET_AUTH_INTERACTOR)
                            BasePostInteractor<SocNetAuthModel> _socNetInteractor,
                         AuthController _controller,
                         AuthStrategyProvider _factory, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mAuthController = _controller;
        mStrategyFactory = _factory;
        mSocNetInteractor = _socNetInteractor;
        mDeviceIdHelper = _idProvider;
        mView = _view;
    }

    @Override
    public void onStart() {
        LoginManager.getInstance().logOut();
    }

    @Override
    public void onStop() {
        mSocNetInteractor.unSubscribe();
    }

    @Override
    public void onFacebookClick() {
        mAuthController.setDelegateStrategy(mStrategyFactory.provideFacebookStrategy());
        performLogIn();
    }

    @Override
    public void onTwitterClick() {
        mAuthController.setDelegateStrategy(mStrategyFactory.provideTwitterStrategy());
        performLogIn();
    }

    @Override
    public void onInstagramClick() {
        mAuthController.setDelegateStrategy(mStrategyFactory.provideInstagramStrategy());
        performLogIn();
    }

    @Override
    public void onActivityResult() {
        mView.setEnabledSocBtn(true);
    }

    private void performLogIn() {
        mView.setEnabledSocBtn(false);
        mView.performLogIn();
    }

    @Override
    public void onSuccess(SocNetAuthModel.Builder _builder) {
        _builder.setDeviceId(mDeviceIdHelper.getDeviceId());
        mSocNetInteractor.execute(_builder.build(), new AuthSubscriber(this));
    }

    @Override
    public void onError(Throwable _error) {
        mView.showToast(_error.getMessage());
    }

    private final class AuthSubscriber extends BaseObserver<UserModel> {
        public AuthSubscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            Log.d("OkHttp", "#onCompleted");
        }

        @Override
        public void onNext(UserModel success) {
            if (success.getPhone() == null || success.getPhone().getPhone().isEmpty())
                mView.startPhoneNumberScreen();
            else
                mView.startDashboard(success);
        }
    }
}
