package com.capt.profile.dashboard.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capt.R;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.profile.dashboard.ui.listener.OnDelegateItemClickListener;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 25.03.2016.
 */
public class DashboardAdapter extends RecyclerView.Adapter implements StickyRecyclerHeadersAdapter {

    private DashboardDelegateManager mDelegateManager;
    private List<DisplayableItem> mItems;

    @Inject
    public DashboardAdapter(DashboardDelegateManager _manager) {
        mItems = new ArrayList<>();
        mDelegateManager = _manager;

    }

    public void setOnDelegateItemClickListener(OnDelegateItemClickListener _listener) {
        mDelegateManager.setOnDelegateItemClickListener(_listener);
    }

    public void setItems(List<DisplayableItem> _items) {
        mItems = _items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mDelegateManager.getItemViewType(mItems, position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return mDelegateManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mDelegateManager.onBindViewHolder(mItems, position, holder);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int getItemTypeCount() {
        return mDelegateManager.getTypeCount();
    }

    @Override
    public long getHeaderId(int position) {
        return Math.abs(mItems.get(position).getHeaderTitle().hashCode());
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_sticky_header, parent, false);
        return new RecyclerView.ViewHolder(view) {
        };
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        String headerTitle = mItems.get(position).getHeaderTitle();
        TextView left = (TextView) holder.itemView.findViewById(R.id.tvLeft_LISH);
        TextView right = (TextView) holder.itemView.findViewById(R.id.tvRightLISH);
        left.setText(headerTitle);
        right.setText(mItems.get(position).getHeaderDescription());
    }
}
