package com.capt.video.category.flow;

import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.video.model.Video;
import com.capt.video.category.CategoryContract;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by Svyd on 03.05.2016.
 */
public class CategoryEditDraftPresenter extends AbstractCategoryPresenter {

    private static final String TAG = CategoryEditPresenter.class.getSimpleName();
    private BasePostInteractor<Video> mUpdateInteractor;

    @Inject
    public CategoryEditDraftPresenter(BasePostInteractor<Video> _patchInteractor,
                                      BaseInteractor _interactor,
                                      CategoryContract.View _view,
                                      BaseExceptionDelegate _delegate) {
        super(_interactor, _view, _delegate);
        mUpdateInteractor = _patchInteractor;
    }

    @Override
    protected void doNext() {
        getView().showProgress();
        mUpdateInteractor.execute(getData(), new PatchDraftObserver(this));
    }

    @Override
    public void onStop() {
        super.onStop();
        mUpdateInteractor.unSubscribe();
    }

    class PatchDraftObserver extends BaseObserver<Long> {
        public PatchDraftObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            getView().hideProgress();
            getView().navigateBack();
        }

        @Override
        public void onNext(Long video) {
            Log.d(TAG, "onNext() called with: " + "video = [" + video + "]");
        }
    }

}
