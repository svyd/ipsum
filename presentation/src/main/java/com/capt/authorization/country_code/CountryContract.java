package com.capt.authorization.country_code;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

/**
 * Created by richi on 2016.03.16..
 */
public interface CountryContract {

    interface View extends BaseView {
        void setData(List<CountryModel> _data);
        void showMessage(String _message);
    }

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onSearchTextChanged(String _text);
    }
}
