package com.capt.profile.assignments.details.capt_dialog;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

/**
 * Created by root on 22.04.16.
 */
public interface CaptDialogContract {

	interface View extends BaseView {
		void navigateToCamera();
		void navigateToGallery();
		void showDenial();
	}

	abstract class Presenter extends BasePresenter<CaptDialogContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void onCameraClick();
		public abstract void onGalleryClick();
	}
}
