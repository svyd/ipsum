package com.capt.domain.authorization.forgot_password;

import com.capt.domain.authorization.forgot_password.model.ForgotPasswordModel;
import com.capt.domain.authorization.model.SuccessModel;

import rx.Observable;

/**
 * Created by Svyd on 22.03.2016.
 */
public interface ForgotPasswordDataStore {
    Observable<SuccessModel> passEmail(ForgotPasswordModel _email);
}
