package com.capt.video.category.dagger;

import com.capt.base.utility.DateTimeUtilityImpl;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.annotation.PerFragment;
import com.capt.data.assignment.AssignmentsMapper;
import com.capt.data.assignment.AssignmentsRepositoryImpl;
import com.capt.data.assignment.AssignmentsService;
import com.capt.data.base.DateTimeUtility;
import com.capt.data.base.TypeMapper;
import com.capt.data.category.VideoCategoryRepositoryImpl;
import com.capt.data.category.VideoCategoryService;
import com.capt.data.video.video.VideoRepositoryImpl;
import com.capt.data.video.video.VideoService;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.AssignmentsRepository;
import com.capt.domain.profile.assignment.UploadRejectedVideoInteractor;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.category.VideoCategoryInteractor;
import com.capt.domain.category.VideoCategoryRepository;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.draft.DraftUpdateInteractor;
import com.capt.domain.video.video.VideoPatchInteractor;
import com.capt.domain.video.video.VideoRepository;
import com.capt.video.Flow;
import com.capt.video.category.CategoryContract;
import com.capt.video.category.flow.AbstractCategoryPresenter;
import com.capt.video.category.flow.CategoryEditDraftPresenter;
import com.capt.video.category.flow.CategoryEditPresenter;
import com.capt.video.category.flow.CategoryUploadPresenter;
import com.capt.video.category.flow.UploadRejectedCategoryPresenter;

import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by root on 07.04.16.
 */
@Module
public class CategoryModule {

    private CategoryContract.View mView;
    private int mFlow;

    public CategoryModule(CategoryContract.View _view, int _flow) {
        mView = _view;
        mFlow = _flow;
    }

    @Provides
    @PerFragment
    protected CategoryContract.View provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    VideoService provideVideoPatchService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                          Retrofit _retrofit) {
        return _retrofit.create(VideoService.class);
    }

    @Provides
    @PerFragment
    VideoRepository provideVideoPatchRepository(VideoRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.VIDEO_PATCH_INTERACTOR)
    BasePostInteractor<Video> provideVideoPatchInteractor(VideoPatchInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    protected VideoCategoryService provideCategoryService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                          Retrofit _retrofit) {
        return _retrofit.create(VideoCategoryService.class);
    }

    @Provides
    @PerFragment
    protected VideoCategoryRepository provideCategoryRepo(VideoCategoryRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.CATEGORY_INTERACTOR)
    protected BaseInteractor provideCategoryInteractor(VideoCategoryInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.DRAFT_UPDATE_INTERACTOR)
    BasePostInteractor<Video> provideDraftUpdateInteractor(DraftUpdateInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    AssignmentsService provideAssignmentsService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                 Retrofit _retrofit) {
        return _retrofit.create(AssignmentsService.class);
    }


    @Provides
    @PerFragment
    DateTimeUtility provideDateTimeUtility(DateTimeUtilityImpl _utility) {
        return _utility;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Mapper.ASSIGNMENTS_MAPPER)
    TypeMapper<List<Assignment>, List<Assignment>> provideAssignmentsMapper(AssignmentsMapper _mapper) {
        return _mapper;
    }

    @Provides
    @PerFragment
    AssignmentsRepository provideAssignmentsRepository(AssignmentsRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.UPLOAD_REJECTED_INTERACTOR)
    BasePostInteractor<Video> provideUploadRejectedInteractor(UploadRejectedVideoInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    protected AbstractCategoryPresenter providePresenter(@Named(Constants.NamedAnnotation.DRAFT_UPDATE_INTERACTOR)
                                                         BasePostInteractor<Video> _draftUpdateInteractor,
                                                         @Named(Constants.NamedAnnotation.VIDEO_PATCH_INTERACTOR)
                                                         BasePostInteractor<Video> _patchInteractor,
                                                         @Named(Constants.NamedAnnotation.CATEGORY_INTERACTOR)
                                                         BaseInteractor _interactor,
                                                         @Named(Constants.NamedAnnotation.UPLOAD_REJECTED_INTERACTOR)
                                                         BasePostInteractor<Video> _rejectedInteractor,
                                                         CategoryContract.View _view,
                                                         BaseExceptionDelegate _delegate) {
        switch (mFlow) {
            case Flow.VIDEO_UPLOAD:
                return new CategoryUploadPresenter(_interactor, _view, _delegate);
            case Flow.EDIT:
                return new CategoryEditPresenter(_patchInteractor, _interactor, _view, _delegate);
            case Flow.DRAFT:
                return new CategoryEditDraftPresenter(_draftUpdateInteractor, _interactor, _view, _delegate);
            case Flow.ASSIGNMENT_REJECTED:
                return new UploadRejectedCategoryPresenter(_interactor, _rejectedInteractor, _view, _delegate);
            default:
                throw new IllegalStateException("Unsupported flow: " + mFlow);
        }
    }
}
