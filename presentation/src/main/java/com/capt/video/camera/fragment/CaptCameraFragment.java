package com.capt.video.camera.fragment;

import android.Manifest;
import android.animation.Animator;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.video.VideoActivity;
import com.capt.video.camera.BaseAdapter;
import com.capt.video.camera.activity.dagger.CameraActivityComponent;
import com.capt.video.camera.custom_view.AdapterLayout;
import com.capt.video.camera.custom_view.CaptCameraView;
import com.capt.video.camera.custom_view.OverlayView;
import com.capt.video.camera.custom_view.morphing_btn.MorphingButton;
import com.capt.video.camera.fragment.dagger.CameraFragmentComponent;
import com.capt.video.camera.fragment.dagger.CameraFragmentModule;
import com.capt.video.view.VerticalTextView;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by richi on 2016.03.28..
 */
@RuntimePermissions
public class CaptCameraFragment extends BaseFragment implements CameraContract.View, CaptCameraView.CameraListener, CameraState {

    private static final String TAG = CaptCameraFragment.class.getSimpleName();
    //Dagger2
    @Inject
    CameraContract.Presenter mPresenter;

    @Inject
    Handler mHandler;

    @Inject
    @Named(Constants.NamedAnnotation.TOP_ADAPTER)
    BaseAdapter mTopAdapter;

    @Inject
    @Named(Constants.NamedAnnotation.BOTTOM_ADAPTER)
    BaseAdapter mBottomAdapter;

    //ButterKnife
    @Bind(R.id.rlContainer_FC)
    RelativeLayout rlContainer;

    @Bind(R.id.ccvCamera_FC)
    CaptCameraView ccvCamera;

    @Bind(R.id.flPlaceholder_FC)
    FrameLayout flPlaceholder;

    @Bind(R.id.rlMorphContainer_FC)
    RelativeLayout rlMorphContainer;

    @Bind(R.id.alTop_FC)
    AdapterLayout alTop;

    @Bind(R.id.alBottom_FC)
    AdapterLayout alBottom;

    @Bind(R.id.vtvDuration_FC)
    VerticalTextView vtvDuration;

    @Bind(R.id.ovOverlay_FC)
    OverlayView ovOverlay;

    @Bind(R.id.mpButton_FC)
    MorphingButton mpButton;

    @Bind(R.id.vtvTitle_FC)
    TextView vtvTitle;

    @Bind(R.id.vtvDescription_FC)
    TextView vtvDescription;

    private CameraFragmentComponent mComponent;

    public static BaseFragment newInstance(Bundle _args) {

        BaseFragment fragment = new CaptCameraFragment();
        fragment.setArguments(_args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_camera);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        setAdapters();
        initCamera();
        mPresenter.onRestoreInstanceState(savedInstanceState);
        mPresenter.setArgs(getArguments());
        CaptCameraFragmentPermissionsDispatcher.showMicrophonePermissionWithCheck(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        CaptCameraFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void setMicrophoneEnable() {
        ccvCamera.setAudioAllow(true);
    }

    // TODO: 4/25/16 Treba prudymatu stringu
    //------------------------------Record audio permission---------------------------------------
    @NeedsPermission(Manifest.permission.RECORD_AUDIO)
    protected void showMicrophonePermission() {
        setMicrophoneEnable();
    }

    @OnShowRationale(Manifest.permission.RECORD_AUDIO)
    protected void showMicrophonePermissionRational(final PermissionRequest request) {
        new AlertDialog.Builder(getActivity(), R.style.CaptAlertDialogStyle)
                .setTitle(R.string.permission)
                .setMessage(R.string.gallery_rational)
                .setPositiveButton(R.string.allow, (dialog, which) -> {
                    request.proceed();
                })
                .setNegativeButton(R.string.deny, (dialog, which) -> {
                    request.cancel();
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.RECORD_AUDIO)
    protected void showMicrophonePermissionDenied() {
        ccvCamera.setAudioAllow(false);
        Toast.makeText(getActivity(), R.string.gallery_deny, Toast.LENGTH_SHORT)
                .show();
    }

    @OnNeverAskAgain(Manifest.permission.RECORD_AUDIO)
    protected void showMicrophonePermissionNeverAsk() {
        ccvCamera.setAudioAllow(false);
        Toast.makeText(getActivity(), R.string.gallery_never_ask_again, Toast.LENGTH_SHORT)
                .show();
    }
    //------------------------------Record audio permission---------------------------------------

    private void initCamera() {
        ccvCamera.setCameraSizeListener(this);
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        if (getArguments().containsKey(Constants.FLOW)) {

            int flow = getArguments().getInt(Constants.FLOW);

            mComponent = getComponent(CameraActivityComponent.class)
                    .createCameraComponent(new CameraFragmentModule(this, flow));
        } else {
            throw new IllegalStateException(CaptCameraFragment.class.getSimpleName() + " should contain a flow");
        }
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPresenter.onSaveInstanceState(outState);
    }

    private void setAdapters() {
        alTop.setAdapter(mTopAdapter);

        alBottom.setAdapter(mBottomAdapter);
        alBottom.setBottom(true);

        alTop.setOnItemClickListener(mTopListener);
        alBottom.setOnItemClickListener(mBottomListener);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!ccvCamera.isAlive()) {
            flPlaceholder.setVisibility(View.VISIBLE);
        }
        mPresenter.onStart();
    }

    @Override
    public void onStop() {
        mPresenter.onStop();
        super.onStop();
    }

    @Override
    public void showDurationToast(String _text) {
        vtvDuration.setVisibility(View.VISIBLE);
        mHandler.postDelayed(() -> vtvDuration.setVisibility(View.GONE), 1500);
    }

    @Override
    public void close() {
        getFragmentNavigator().popBackStack();
    }

    @Override
    public void showText() {
        if (vtvTitle != null) {
            vtvTitle.setVisibility(View.VISIBLE);
        }
        if (vtvDescription != null) {
            vtvDescription.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideText() {
        vtvTitle.setVisibility(View.GONE);
        vtvDescription.setVisibility(View.GONE);
    }

    @Override
    public void morphingTo(MorphingButton.Params _params) {
        mpButton.morph(_params);
    }

    @Override
    public void record(String _path) {
        try {
            ccvCamera.recordVideo(_path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stopRecording() {
        ccvCamera.stopRecord();
    }

    @Override
    public void showRightBar() {
        if (rlContainer != null)
            rlContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRightBar() {
        rlContainer.setVisibility(View.GONE);
    }

    @Override
    public void animateOverlayViewToPortrait(Animator.AnimatorListener _listener) {
        if (ovOverlay != null)
            ovOverlay.animateToPortrait(_listener);
    }

    @Override
    public void animateOverlayViewToLandscape(int _orientation, Animator.AnimatorListener _listener) {
        if (ovOverlay != null)
            ovOverlay.animateToLandscape(_orientation, _listener);
    }

    @Override
    public void showMorphBtn() {
        if (rlMorphContainer != null)
            rlMorphContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMorphBtn() {
        if (rlMorphContainer != null)
            rlMorphContainer.setVisibility(View.GONE);
    }

    @Override
    public void notifyBottomSettings() {
        if (alBottom != null)
            alBottom.notifyData();
    }

    @Override
    public void notifyTopSettings() {
        if (alTop != null)
            alTop.notifyData();
    }

    @Override
    public void hideTopSettings() {
        if (alTop != null)
            alTop.setVisibility(View.GONE);
    }

    @Override
    public void hideBottomSettings() {
        if (alBottom != null)
            alBottom.setVisibility(View.GONE);
    }

    @Override
    public void showBottomSettings() {
        if (alBottom != null)
            alBottom.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTopSettings() {
        if (alTop != null)
            alTop.setVisibility(View.VISIBLE);
    }



    @Override
    public void hidePlaceholder() {
        Log.d(TAG, "hidePlaceholder() called with: " + "");
        int duration = 200;
        AlphaAnimation animation1 = new AlphaAnimation(1.0f, 0.0f);
        animation1.setDuration(duration);

        getActivity().runOnUiThread(() -> flPlaceholder.startAnimation(animation1));
        mHandler.postDelayed(() -> {
            if (flPlaceholder != null)
                flPlaceholder.setVisibility(View.GONE);
        }, duration);
    }

    @Override
    public void setVideoSize(Camera.Size _size) {
        ccvCamera.setVideoSize(_size);
    }

    @Override
    public void setFlashMode(String _mode) {
        ccvCamera.setFlashMode(_mode);
    }

    @Override
    public void startTrimFragment(String _path) {
        VideoActivity.startVideoFlow(_path, getActivity());
    }

    @Override
    public void startAssignmentFlow(Assignment _assignment) {
        VideoActivity.startAssignmentCapt(_assignment, getActivity());
    }

    private AdapterLayout.OnItemClickListener mTopListener = new AdapterLayout.OnItemClickListener() {
        @Override
        public void onItemClick(int _position) {
            mPresenter.onTopClick(_position);
        }

        @Override
        public void onMainClick() {
            mPresenter.onTopMainClick();
        }
    };

    private AdapterLayout.OnItemClickListener mBottomListener = new AdapterLayout.OnItemClickListener() {
        @Override
        public void onItemClick(int _position) {
            mPresenter.onBottomClick(_position);
        }

        @Override
        public void onMainClick() {
            mPresenter.onBottomMainClick();
        }
    };

    @OnClick(R.id.mpButton_FC)
    protected void onMorphingBtnClick() {
        mPresenter.onMorphingBtnClick();
    }

    @Override
    public void onPreviewSizeList(List<Camera.Size> _sizeList) {
        mPresenter.setCameraSizes(_sizeList);
    }

    @Override
    public void onFlashAvailable(boolean _available) {
        mPresenter.onFlashAvailable(_available);
    }

    @Override
    public void onCameraPrepared() {
        mPresenter.onCameraPrepared();
    }

    @Override
    public boolean isCameraAlive() {
        return ccvCamera.isAlive();
    }
}
