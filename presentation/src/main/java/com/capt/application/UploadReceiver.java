package com.capt.application;

import android.util.Log;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.AssignmentState;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.upload_manager.VideoUploadManager;

import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by richi on 2016.03.14..
 */
public class UploadReceiver extends UploadServiceBroadcastReceiver {

    @Inject
    VideoUploadManager mUploadManager;

    @Inject
    @Named(Constants.NamedAnnotation.PENDING_UPDATE_INTERACTOR)
    BasePostInteractor<Video> mUpdatePendingInteractor;

    @Inject
    @Named(Constants.NamedAnnotation.ASSIGNMENT_CACHE_UPDATE)
    BasePostInteractor<String> mUpdateAssignmentCacheInteractor;

    @Inject
    @Named(Constants.NamedAnnotation.ASSIGNMENT_CACHE_CLEAR)
    BaseInteractor mClearAssignmentCacheInteractor;

    private static boolean isUploading;
    private static boolean isAssignmentUploading;

    @Inject
    public UploadReceiver() {
        CaptApplication.getApplication().getAppComponent().inject(this);
    }

    public static boolean isUploading() {
        return isUploading;
    }

    public static boolean isAssignmentUploading() {
        return isAssignmentUploading;
    }

    public static void onAssignmentCacheCleared() {
        isUploading = isAssignmentUploading = false;
    }

    private void setUploading(boolean uploading, String id) {
        isUploading = uploading;
        if (id.startsWith(Constants.ASSIGNMENT_PREFIX)) {
            isAssignmentUploading = uploading;
        }
    }

    @Override
    public void onStarted(String uploadId) {
        setUploading(true, uploadId);

        Video video = new Video();
        video.id = uploadId;
        video.setError(false);
        video.setCurrentlyUploading(true);
        mUpdatePendingInteractor.execute(video, new UpdateObserver());
    }

    @Override
    public void onCompleted(String uploadId, int serverResponseCode, byte[] serverResponseBody) {
        super.onCompleted(uploadId, serverResponseCode, serverResponseBody);
        setUploading(false, uploadId);
        if (uploadId.startsWith(Constants.ASSIGNMENT_PREFIX)) {
            mClearAssignmentCacheInteractor.execute(new UpdateObserver());
        }
        mUploadManager.completePendingUploading(uploadId);
    }

    @Override
    public void onError(String uploadId, Exception exception) {
        super.onError(uploadId, exception);
        if (uploadId.startsWith(Constants.ASSIGNMENT_PREFIX)) {
            mUpdateAssignmentCacheInteractor.execute(AssignmentState.ERROR, new UpdateObserver());
            return;
        }
        setUploading(false, uploadId);

        Video video = new Video();
        video.id = uploadId;
        video.setError(true);
        video.setCurrentlyUploading(false);
        mUpdatePendingInteractor.execute(video, new UpdateObserver());
    }

    @Override
    public void onProgress(String uploadId, int progress) {
        super.onProgress(uploadId, progress);

        setUploading(true, uploadId);
    }

    class UpdateObserver implements Observer<SuccessModel> {

        private final String TAG = UpdateObserver.class.getSimpleName();

        @Override
        public void onCompleted() {
            Log.d(TAG, "onCompleted() called with: " + "");
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "onError() called with: " + "e = [" + e + "]");
        }

        @Override
        public void onNext(SuccessModel successModel) {
            Log.d(TAG, "onNext() called with: " + "successModel = [" + successModel + "]");
        }
    }
}
