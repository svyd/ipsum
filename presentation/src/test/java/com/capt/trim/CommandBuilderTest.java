//package com.capt.trim;
//
//import com.capt.BuildConfig;
//import com.capt.TestCaptApplication;
//import com.capt.trim.mock_module.MockFFMpegModule;
//import com.capt.video.trim.dagger.DaggerFFMpegComponent;
//import com.capt.video.trim.dagger.FFMpegComponent;
//import com.capt.video.trim.ffmpeg_command_builder.FFMPEGCommandBuilder;
//import com.capt.video.trim.time_util.TimeUtil;
//
//import junit.framework.Assert;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.mockito.internal.verification.Times;
//import org.robolectric.RobolectricGradleTestRunner;
//import org.robolectric.RuntimeEnvironment;
//import org.robolectric.annotation.Config;
//
///**
// * Created by richi on 2016.02.29..
// */
//@RunWith(RobolectricGradleTestRunner.class)
//@Config(constants = BuildConfig.class,
//        application = TestCaptApplication.class)
//public class CommandBuilderTest {
//
//    @Mock TimeUtil mMockTimeUtil;
//
//    private FFMPEGCommandBuilder mCommandBuilder;
//
//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//
//        TestCaptApplication app = (TestCaptApplication) RuntimeEnvironment.application;
//
//        FFMpegComponent component = DaggerFFMpegComponent.builder()
//                .appComponent(app.getAppComponent())
//                .fFMpegModule(new MockFFMpegModule(mMockTimeUtil))
//                .build();
//
//        mCommandBuilder = component.commandBuilder();
//    }
//
//    @Test
//    public void testCommandBuilder() {
//        String expectedResult = "ffmpeg -i movie.mp4 -ss 00:00:03 -t 00:00:08 -async 1 cut.mp4";
//        String command = mCommandBuilder.getTrimVideoCommand(3, 8, "movie.mp4", "cut.mp4");
//
//        Assert.assertEquals("Commands are not equal", command, expectedResult);
//    }
//
//    @Test
//    public void testInteractionWithTimeUtil() {
//        mCommandBuilder.getTrimVideoCommand(3, 8, "movie.mp4", "cut.mp4");
//
//        Mockito.verify(mMockTimeUtil, new Times(2)).getTimeFromInteger(Mockito.anyInt());
//    }
//}
