package com.capt.profile.assignments.details.capt_dialog.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.assignments.details.capt_dialog.CaptDialogFragment;

import dagger.Subcomponent;

/**
 * Created by root on 22.04.16.
 */
@PerFragment
@Subcomponent(modules = CaptDialogModule.class)
public interface CaptDialogComponent {
	void inject(CaptDialogFragment _fragment);
}
