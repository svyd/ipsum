package com.capt.video.map.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.map.MapContract;
import com.capt.video.map.MapPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 05.04.16.
 */
@Module
public class MapModule {

	private MapContract.View mView;

	public MapModule(MapContract.View _view) {
		mView = _view;
	}

	@Provides @PerFragment
	protected MapContract.View provideView() {
		return mView;
	}

	@Provides @PerFragment
	protected MapContract.Presenter providePresenter(MapPresenter _presenter) {
		return _presenter;
	}
}
