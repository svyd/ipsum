package com.capt.authorization.code_verification.dagger;

import com.capt.authorization.code_verification.CodeVerifyFragment;
import com.capt.data.annotation.PerFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.03.19..
 */
@PerFragment
@Subcomponent(modules = CodeVerifyModule.class)
public interface CodeVerifyComponent {
    void inject(CodeVerifyFragment _fragment);
}
