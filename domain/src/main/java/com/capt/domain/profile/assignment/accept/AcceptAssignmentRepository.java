package com.capt.domain.profile.assignment.accept;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.profile.assignment.accept.model.AcceptAssignmentModel;

import java.util.List;

import rx.Observable;

/**
 * Created by root on 22.04.16.
 */
public interface AcceptAssignmentRepository {
	Observable<SuccessModel> acceptAssignment (AcceptAssignmentModel _model);
}
