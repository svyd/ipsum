package com.capt.domain.profile.assignment;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.video.model.Video;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public interface AssignmentsRepository {
    Observable<Assignments> getAssignments(RequestParamsModel _model);
    Observable<List<Assignment>> getAssignmentModels(RequestParamsModel _model);
    Observable<Video> uploadRejectedVideo(Video _assignment);
    Observable<SuccessModel> delete(String _id);

}
