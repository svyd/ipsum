package com.capt.video.price.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.data.settings.SettingsRepositoryImpl;
import com.capt.data.settings.SettingsService;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.GetSettingsInteractor;
import com.capt.domain.profile.settings.SettingsRepository;
import com.capt.video.price.SetPriceContract;
import com.capt.video.price.SetPricePresenter;
import com.capt.data.currency.CurrencyRepositoryImpl;
import com.capt.data.currency.CurrencyService;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.currency.CurrencyInteractor;
import com.capt.domain.currency.CurrencyRepository;
import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by root on 25.03.16.
 */
@Module
public class SetPriceModule {

	private SetPriceContract.View mView;

	public SetPriceModule(SetPriceContract.View _view) {
		mView = _view;
	}

	@Provides @PerFragment
	protected SetPriceContract.View provideView() {
		return mView;
	}

	@Provides @PerFragment
	protected SetPriceContract.Presenter providePresenter(SetPricePresenter _presenter) {
		return _presenter;
	}

	@Provides
	@PerFragment CurrencyService provideCurrencyService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
	                                                    Retrofit _retrofit) {
		return _retrofit.create(CurrencyService.class);
	}

	@Provides
	@PerFragment CurrencyRepository provideCurrencyRepository(CurrencyRepositoryImpl _repository) {
		return _repository;
	}

	@Provides
	@PerFragment
	@Named(Constants.NamedAnnotation.CURRENCY_INTERACTOR) BaseInteractor provideCurrencyInteractor(CurrencyInteractor _interactor) {
		return _interactor;
	}

	@Provides
	@PerFragment
	@Named(Constants.NamedAnnotation.GET_SETTINGS_INTERACTOR)
	protected BaseInteractor provideGetSettingsInteractor(GetSettingsInteractor _interactor) {
		return _interactor;
	}

    @Provides
    @PerFragment
    SettingsService provideSettingsService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                           Retrofit _retrofit) {
        return _retrofit.create(SettingsService.class);
    }

    @Provides
    @PerFragment
    SettingsRepository provideSettingsRepository(SettingsRepositoryImpl _repository) {
        return _repository;
    }
}
