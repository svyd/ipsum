package com.capt.domain.video.upload;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;

import rx.Observable;
import rx.Observer;

/**
 * Created by richi on 2016.03.11..
 */
public interface VideoUploadRepository {
    Observable<SuccessModel> uploadForAssignment(Assignment _model);
    Observable<String> uploadToCloud(Video _model);
    Observable<PayPalVideoModel> uploadWithPayPalCode(PayPalVideoModel _model);
    Observable<SuccessModel> uploadPayPalCode(String _payPalCode);
    Observable<SuccessModel> retry(String _id);
    Observable<SuccessModel> uploadPendingVideos();
}
