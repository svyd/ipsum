package com.capt.data.payout;

import com.capt.domain.profile.payout.PayoutModel;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by rMozes on 8/3/16.
 */
public interface PayoutService {

    @GET("/mobile/user/payouts")
    Observable<List<PayoutModel>> getPayoutLis();
}
