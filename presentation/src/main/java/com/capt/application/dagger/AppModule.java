package com.capt.application.dagger;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;

import com.capt.UIThread;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.controller.delegate_exception.CaptExceptionDelegate;
import com.capt.controller.delegate_exception.HttpExceptionDelegate;
import com.capt.controller.delegate_exception.IOExceptionDelegate;
import com.capt.controller.delegate_exception.UnknownExceptionDelegate;
import com.capt.data.annotation.PerActivity;
import com.capt.data.authorization.repository.datasource.PreferenceUserCache;
import com.capt.data.authorization.repository.datasource.UserCache;
import com.capt.data.base.TypeMapper;
import com.capt.data.dashboard.UserRepositoryImpl;
import com.capt.data.settings.SettingsMapper;
import com.capt.domain.authorization.interactor.UpdatePushInteractor;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.dashboard.UserInteractor;
import com.capt.domain.profile.dashboard.UserRepository;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.video.trim.file_manager.FileManager;
import com.capt.video.trim.file_manager.FileManagerImpl;
import com.google.android.gms.common.api.GoogleApiClient;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by rMozes on 2016.02.22..
 */
@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application _application) {
        mApplication = _application;
    }

    @Provides
    @Singleton
    Context provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context _context) {
        return PreferenceManager.getDefaultSharedPreferences(_context);
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.PREFERENCES_COOKIES)
    SharedPreferences provideCookieStorePreferences(Context _context) {
        return _context.getSharedPreferences(Constants.PREFERENCE_NAME_COOKIES, Context.MODE_PRIVATE);
    }

    @Provides
    protected GoogleApiClient.Builder provideGoogleApiBuilder(Context _context) {
        return new GoogleApiClient.Builder(_context);
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    protected InputMethodManager provideInputManager(Context _context) {
        return (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Provides
    @Singleton
    protected LayoutInflater provideLayoutInflater(Context _context) {
        return LayoutInflater.from(_context);
    }

    @Provides
    @Singleton
    protected FileManager provideFileManager(FileManagerImpl _fileManager) {
        return _fileManager;
    }

    @Provides
    @Singleton
    protected Handler provideUiHandler() {
        return new Handler();
    }

    @Singleton
    @Provides
    protected UserCache provideUserCache(PreferenceUserCache _cache) {
        return _cache;
    }

    @Singleton
    @Provides
    protected UserRepository provideUserRepository(UserRepositoryImpl _repository) {
        return _repository;
    }

//    @Singleton - MemoryLeak, i do not know why
    @Provides
    protected BaseInteractor provideUserInteractor(UserInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.Mapper.SETTINGS_MAPPER)
    TypeMapper<SettingsModel, SettingsModel> provideSettingsMapper(SettingsMapper _mapper) {
        return _mapper;
    }

    @Provides
    protected BaseExceptionDelegate provideExceptionDelegate(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                             Retrofit _retrofit) {
        BaseExceptionDelegate unknownExceptionDelegate = new UnknownExceptionDelegate();
        BaseExceptionDelegate ioExceptionDelegate = new IOExceptionDelegate(unknownExceptionDelegate);
        BaseExceptionDelegate httpExceptionDelegateDelegate = new HttpExceptionDelegate(ioExceptionDelegate, _retrofit);

        return new CaptExceptionDelegate(httpExceptionDelegateDelegate);
    }
}
