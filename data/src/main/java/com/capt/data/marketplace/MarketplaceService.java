package com.capt.data.marketplace;

import com.capt.domain.profile.dashboard.model.Marketplace;
import com.capt.domain.profile.marketplace.model.MarketplaceModel;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Svyd on 04.04.2016.
 */
public interface MarketplaceService {

    @GET("mobile/video/marketplace")
    Observable<MarketplaceModel> getMarketplaceInfo();

}
