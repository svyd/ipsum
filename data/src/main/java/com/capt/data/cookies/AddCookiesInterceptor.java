package com.capt.data.cookies;

import java.io.IOException;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.List;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Svyd on 05.03.2016.
 */
public class AddCookiesInterceptor implements Interceptor {

    private CookieStore mCookieStore;

    @Inject
    public AddCookiesInterceptor(CookieStore _cookieStore) {
        mCookieStore = _cookieStore;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        List<HttpCookie> cookies = mCookieStore.getCookies();
        for (HttpCookie cookie : cookies) {
            builder.addHeader("Cookie", cookie.getValue());
        }

        return chain.proceed(builder.build());
    }
}