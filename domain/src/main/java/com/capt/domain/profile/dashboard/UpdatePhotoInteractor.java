package com.capt.domain.profile.dashboard;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.profile.dashboard.DashboardRepository;

import java.io.File;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 4/22/16.
 */
public class UpdatePhotoInteractor extends BasePostInteractor<String> {

    private DashboardRepository mRepository;

    @Inject
    public UpdatePhotoInteractor(PostExecutionThread postExecutionThread, DashboardRepository _repositiry) {
        super(postExecutionThread);
        mRepository = _repositiry;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.update(new File(_data));
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
