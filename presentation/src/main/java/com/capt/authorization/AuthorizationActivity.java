package com.capt.authorization;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.utility.PlayServiceUtility;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.gcmservice.RegistrationIntentService;
import com.capt.application.CaptApplication;
import com.capt.application.dagger.HasComponent;
import com.capt.authorization.dagger.RestModule;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.dagger.AuthActivityModule;
import com.capt.authorization.splash.TutorialFragment;
import com.capt.base.BaseActivity;
import com.capt.base.BaseFragment;
import com.capt.data.authorization.net.ApiConstants;
import com.capt.domain.base.BaseInteractor;
import com.capt.profile.ProfileActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.models.User;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.22..
 */
public class AuthorizationActivity extends BaseActivity implements HasComponent<AuthActivityComponent> {

    private static final String TAG = "AuthorizationActivity";
    private AuthActivityComponent mComponent;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    private boolean isReceiverRegistered;

    @Inject
    BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);

        initComponent();
        inject();
        registerReceiver();
        if (checkPlayServices()) {
            startGCMRegisterService();
        }
        downloadCountries();
        initFragment(savedInstanceState);
    }

    protected void initFragment(Bundle _savedInstanceState) {
        if (_savedInstanceState == null) {
            getFragmentNavigator().addFragmentWithoutBackStack(TutorialFragment.newInstance());
        }
    }

    private void initComponent() {
        mComponent = CaptApplication.getApplication()
                .getAppComponent()
                .createAuthActivityComponent(new AuthActivityModule(this),
                        new RestModule(ApiConstants.GOOGLE_MAPS_API_ENDPOINT));
    }

    private void inject() {
        mComponent.inject(this);
    }

    private void downloadCountries() {
        mComponent.countryCodeInteractor().execute(new BaseInteractor.SimpleObserver());
    }

    @Override
    public AuthActivityComponent getComponent() {
        return mComponent;
    }

    @Override
    public int getContainerId() {
        return R.id.rlContainer_AA;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AA;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        BaseFragment fragment = getFragmentNavigator().getTopFragment();
        if (fragment != null)
            fragment.onActivityResult(requestCode, resultCode, data);
    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(RegistrationIntentService.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
            Log.d("MyLog", "#registerReceiver");
        }
    }

    private void startGCMRegisterService() {
        if (PlayServiceUtility.checkPlayServices(this)) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }
}
