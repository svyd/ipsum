package com.capt.domain.profile.dashboard.model.base;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 25.03.2016.
 */
public abstract class BaseItems implements DisplayableItem {
    protected List<Item> mItems;

    public BaseItems() {
        mItems = new ArrayList<>();
    }

    public int getSize() {
        return mItems.size();
    }
}
