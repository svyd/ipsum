package com.capt.authorization.dagger;

import android.content.BroadcastReceiver;

import com.capt.base.BaseActivity;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.gcmservice.RegistrationBroadcastReceiver;
import com.capt.data.annotation.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.03.16..
 */
@Module
public class AuthActivityModule {

    private BaseActivity mActivity;

    public AuthActivityModule(BaseActivity _activity) {
        mActivity = _activity;
    }

    @Provides @PerActivity
    protected BaseActivity provideBaseActivity() {
        return mActivity;
    }

    @Provides @PerActivity
    protected BroadcastReceiver provideReceiver(RegistrationBroadcastReceiver _receiver) {
        return _receiver;
    }

    @Provides @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }
}
