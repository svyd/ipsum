package com.capt.profile.settings.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.settings.SettingsFragment;

import dagger.Subcomponent;

/**
 * Created by Svyd on 20.04.2016.
 */
@PerFragment
@Subcomponent(modules = {SettingsModule.class})
public interface SettingsComponent {
    void inject(SettingsFragment _fragment);
}
