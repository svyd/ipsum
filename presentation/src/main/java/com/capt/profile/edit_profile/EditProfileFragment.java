package com.capt.profile.edit_profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.authorization.CaptAuthActivity;
import com.capt.authorization.country_code.CountryFragment;
import com.capt.authorization.location.LocationFragment;
import com.capt.authorization.sign_up.CircleTransform;
import com.capt.authorization.sign_up.image_picker.ImagePickerFragment;
import com.capt.base.BaseFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.profile.edit_profile.dagger.EditProfileActivityComponent;
import com.capt.profile.edit_profile.dagger.EditProfileFragmentComponent;
import com.capt.profile.edit_profile.dagger.EditProfileFragmentModule;
import com.capt.video.Flow;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Svyd on 4/29/16.
 */
public class EditProfileFragment extends BaseToolbarFragment implements EditProfileFragmentContract.View {

    private static final int REQUEST_IMAGE = 167;
    private static final int REQUEST_COUNTRY_CODE = 168;
    private static final int REQUEST_LOCATION = 169;

    @Bind(R.id.etEmail_FEP)
    EditText etEmail;

    @Bind(R.id.pbEmail_FEP)
    ProgressBar pbEmail;

    @Bind(R.id.ivRightEmail_FEP)
    ImageView ivRight;

    @Bind(R.id.ivAvatar_FEP)
    ImageView ivAvatar;

    @Bind(R.id.etFullName_FEP)
    EditText etFullName;

    @Bind(R.id.tvCode_FEP)
    EditText tvCode;

    @Bind(R.id.etPhone_FEP)
    EditText etPhone;

    @Bind(R.id.etLocation_FEP)
    EditText etLocation;

    @Inject
    EditProfileFragmentContract.Presenter mPresenter;

    EditProfileFragmentComponent mComponent;

    String mAvatarUrl, mCountryCode;
    LocationModel mLocation;

    ProgressDialog mUserUpdateProgress;

    public static EditProfileFragment newInstance(int _flow) {

        Bundle args = new Bundle();
        args.putInt(Constants.FLOW, _flow);

        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_edit_profile);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        initToolbar();
        initUserUpdateProgress();
        mPresenter.restoreState(savedInstanceState);
        initPresenter();
        mPresenter.initialize();
        hideRightEmail();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.savedInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(EditProfileActivityComponent.class)
                .createEditProfileComponent(new EditProfileFragmentModule(this));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void initToolbar() {
        getToolbarManager().showToolbar();
        getActivity().setTitle(R.string.edit_profile);
    }

    private void initUserUpdateProgress() {
        mUserUpdateProgress = new ProgressDialog(getActivity());
        mUserUpdateProgress.setTitle(R.string.progress_update_user);
        mUserUpdateProgress.setMessage(getString(R.string.progress_update_user_message));
    }

    private void initPresenter() {
        mPresenter.setAvatar(mAvatarUrl);
        mPresenter.setCountryCode(mCountryCode);
        mPresenter.setLocation(mLocation);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onShowEmailError() {
        getActivity().runOnUiThread(() -> etEmail.setError("Incorrect e-mail"));
    }

    @Override
    public void onGoBack(Intent _intent) {
        getActivity().setResult(Activity.RESULT_OK, _intent);
        getActivity().onBackPressed();
    }

    @Override
    public void showFullNameError() {
        etFullName.setError("Full name is null");
    }

    @Override
    public void navigateToCountryCodePicker() {
        BaseFragment fragment = CountryFragment.newInstance();
        fragment.setTargetFragment(this, REQUEST_COUNTRY_CODE);
        getFragmentNavigator().replaceFragmentWithBackStack(fragment);
    }

    @Override
    public void navigateToLocationPicker() {
        BaseFragment fragment = LocationFragment.newInstance(getArguments());
        fragment.setTargetFragment(this, REQUEST_LOCATION);
        getFragmentNavigator().replaceFragmentWithBackStack(fragment);
    }

    @Override
    public void showRightEmail() {
        if (ivRight != null)
            getActivity().runOnUiThread(() -> ivRight.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideRightEmail() {
        if (ivRight != null)
            getActivity().runOnUiThread(() -> ivRight.setVisibility(View.GONE));
    }

    @Override
    public void showImagePickerSheet() {
        ImagePickerFragment.showWithTarget(this, REQUEST_IMAGE);
    }

    @Override
    public void renderPhoneNumber(String _phoneNumber) {
        if (etPhone != null)
            etPhone.setText(_phoneNumber);
    }

    @Override
    public void renderEmail(String _email) {
        if (etEmail != null)
            etEmail.setText(_email);
    }

    @Override
    public void renderCity(String _location) {
        if (etLocation != null)
            etLocation.setText(_location);
    }

    @Override
    public void renderCountry(String _country) {
        if (etLocation != null)
            etLocation.append(_country);
    }

    @Override
    public void renderImage(String _path) {
        if (ivAvatar != null)
            loadImage(_path, ivAvatar);
    }

    @Override
    public void renderCountryCode(String _countryCode) {
        if (tvCode != null)
            tvCode.setText(_countryCode);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @OnTextChanged(R.id.etPhone_FEP)
    void onPhoneChanged(CharSequence phone) {
        mPresenter.onPhoneChanged(phone.toString());
    }

    @Override
    public void renderFullName(String _fullName) {
        if (etFullName != null)
            etFullName.setText(_fullName);
    }

    private void loadImage(String _path, ImageView _image) {
        Glide.with(this)
                .load(_path)
                .centerCrop()
                .placeholder(R.drawable.ic_camera)
                .error(R.drawable.ic_camera)
                .transform(new CircleTransform(CaptApplication.getApplication()))
                .into(_image);
    }

    @OnTextChanged(R.id.etEmail_FEP)
    protected void onEmailChanged(CharSequence _email) {
        etEmail.setError(null);
        mPresenter.onEmailChanged(_email);
    }

    @OnTextChanged(R.id.etFullName_FEP)
    protected void onFullNameChanged(CharSequence _fullName) {
        etFullName.setError(null);
        mPresenter.fullNameChanged(_fullName.toString());
    }

    @Override
    public void showProgress() {
        if (mUserUpdateProgress != null && mUserUpdateProgress.isShowing())
            return;

        if (mUserUpdateProgress != null)
            mUserUpdateProgress.show();
    }

    @Override
    public void hideProgress() {
        if (mUserUpdateProgress != null)
            mUserUpdateProgress.cancel();
    }

    @Override
    public void showNoInternetConnectionError() {

    }

    @Override
    public void hideNoInternetConnectionError() {

    }

    @Override
    public void showEmailProgress() {
        pbEmail.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmailProgress() {
        if (pbEmail != null)
            getActivity().runOnUiThread(() -> pbEmail.setVisibility(View.GONE));
    }

    @Override
    public void confirmPhoneAndUpdate() {
        CaptAuthActivity.startCodeFlow(getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit_profile, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miDone:
                mPresenter.onDoneClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE) {
            avatarResult(data);
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_COUNTRY_CODE) {
            countryCodeResult(data);
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_LOCATION) {
            locationResult(data);
        }
    }

    private void avatarResult(Intent _data) {
        mAvatarUrl = ((String) _data.getSerializableExtra(Constants.Extra.EXTRA_STRING));
        mPresenter.setAvatar(mAvatarUrl);
    }

    private void locationResult(Intent _data) {
        mLocation = (LocationModel) _data.getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE);
    }

    private void countryCodeResult(Intent _data) {
        mCountryCode = _data.getStringExtra(Constants.Extra.EXTRA_STRING);
    }

    @OnClick(R.id.tvCode_FEP)
    protected void onCodeClicked() {
        mPresenter.onCountryCodeClicked();
    }

    @OnClick(R.id.etLocation_FEP)
    protected void onLocationClicked() {
        mPresenter.onLocationClicked();
    }

    @OnClick(R.id.ivAvatar_FEP)
    protected void onAvatarClicked() {
        mPresenter.onPhotoClicked();
    }
}
