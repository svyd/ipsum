package com.capt.profile.dashboard.ui.delegate.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capt.R;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.profile.dashboard.ui.adapter.SectioningAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 26.06.2016.
 */
public abstract class BaseSimpleDelegate<T> extends BaseDelegate {

    private Class<T> mClazz;
    private LayoutInflater mInflater;

    public BaseSimpleDelegate(LayoutInflater _layoutInflater,
                                  int viewType,
                                  Class<T> _t) {
        super(viewType);
        mInflater = _layoutInflater;
        mClazz = _t;
    }

    @Override
    public boolean isForViewType(@NonNull List<DisplayableItem> items, int position) {
        return mClazz.isInstance(items.get(position));
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new SimpleViewHolder(mInflater.inflate(R.layout.list_item_simple, parent, false));
    }

    protected abstract int getType();

    protected class SimpleViewHolder extends SectioningAdapter.ViewHolder implements View.OnClickListener {

        @Bind(R.id.tvLeft_LI)
        public TextView tvLeft;

        @Bind(R.id.tvRight_LI)
        public TextView tvRight;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onDelegateClick(getType(), getLayoutPosition());
        }
    }
}
