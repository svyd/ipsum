package com.capt.domain.currency.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by rMozes on 31.03.16.
 */
public class CurrencyModel implements Serializable {

    @Expose
    @SerializedName("_id")
    private String currencyId;

    @Expose
    @SerializedName("iso")
    private String currencyISO;

    @Expose
    @SerializedName("title")
    private String currencyTitle;

    @Expose
    @SerializedName("symbol")
    private Symbol symbol;

    public void setSymbol(String symbol) {
        this.symbol = new Symbol();
        this.symbol.grapheme = symbol;
    }

    public String getSymbol() {
        return symbol.grapheme;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getCurrencyISO() {
        return currencyISO;
    }

    public void setCurrencyISO(String currencyISO) {
        this.currencyISO = currencyISO;
    }

    public String getCurrencyTitle() {
        return currencyTitle;
    }

    public void setCurrencyTitle(String currencyTitle) {
        this.currencyTitle = currencyTitle;
    }

    private class Symbol implements Serializable {

        @Expose
        @SerializedName("grapheme")
        String grapheme;

    }
}
