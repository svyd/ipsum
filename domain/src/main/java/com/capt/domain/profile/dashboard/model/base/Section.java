package com.capt.domain.profile.dashboard.model.base;

import org.omg.PortableInterceptor.ServerRequestInfo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Svyd on 26.06.2016.
 */
public class Section<T> implements Serializable {

    private List<T> mItems;
    private String mHeaderTitle;
    private String mHeaderDescription;
    private boolean hasHeader;
    private boolean hasButton;

    public Section(List<T> _items, boolean _hasHeader) {
        mItems = _items;
        hasHeader = _hasHeader;
    }

    public String getHeaderTitle() {
        return mHeaderTitle;
    }

    public String getHeaderDescription() {
        if (mHeaderDescription != null) {
            return mHeaderDescription;
        } else {
            return "";
        }
    }

    public boolean hasButton() {
        return hasButton;
    }

    public boolean hasHeader() {
        return hasHeader && hasItems();
    }

    public List<T> getItems() {
        return mItems;
    }

    public void setHeaderTitle(String _title) {
        mHeaderTitle = _title;
    }

    public void setHeaderDescription(String _description) {
        mHeaderDescription = _description;
    }

    public void setHasButton(boolean _hasButton) {
        hasButton = _hasButton;
    }

    public int size() {
        return mItems.size();
    }

    public boolean hasItems() {
        return !mItems.isEmpty();
    }

}
