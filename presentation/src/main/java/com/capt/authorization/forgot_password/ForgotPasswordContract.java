package com.capt.authorization.forgot_password;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

/**
 * Created by Svyd on 18.03.2016.
 */
public interface ForgotPasswordContract {

    interface ForgotPasswordView extends BaseView {
        void onEmailChecked(boolean _checked);
        void enableButton(boolean _enable);
        void setEmailError(String error);
        void onEmailPassed();
    }

    abstract class ForgotPasswordPresenter extends BasePresenter<ForgotPasswordView> {
        public ForgotPasswordPresenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void passEmail(String _email);
        public abstract void checkEmail(String _email);
    }
}
