package com.capt.data.authorization.net;

/**
 * Created by Svyd on 04.03.2016.
 */
public abstract class ApiConstants {

    public static final int CONNECTION_TIME_OUT = 20;
    public static final int READ_TIME_OUT = 10;

    public static final String API_ENDPOINT = "endpoint";
    public static final String UPLOAD_VIDEO = API_ENDPOINT + "mobile/video/upload";
    public static final String UPLOAD_FOR_ASSIGNMENT = API_ENDPOINT + "mobile/assignments/";
    public static final String GOOGLE_PLACES_API_KEY = "key";
    public static final String GOOGLE_MAPS_API_ENDPOINT = "https://maps.googleapis.com/";
}
