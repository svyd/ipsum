package com.capt.data.authorization.repository.country.cache;

import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

/**
 * Created by richi on 2016.03.16..
 */
public interface CountryCache {
    void setToCache(List<CountryModel> _model);
    List<CountryModel> get();
    boolean isCachedInMemory();
}
