package com.capt.profile.settings.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capt.R;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.capt.profile.dashboard.ui.adapter.SectioningAdapter;
import com.capt.profile.dashboard.ui.listener.OnDelegateItemClickListener;
import com.capt.profile.settings.SettingsFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 27.06.2016.
 */
public class NewSettingsAdapter extends SectioningAdapter {

    protected OnDelegateItemClickListener mListener;
    private LayoutInflater mInflater;
    private List<Section<Item>> mSections;

    public NewSettingsAdapter(Context _context) {
        mInflater = LayoutInflater.from(_context);
        mSections = new ArrayList<>();
    }

    public void setItems(List<Section<Item>> _items) {
        mSections = _items;
        notifyAllSectionsDataSetChanged();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return mSections.get(sectionIndex).size();
    }

    @Override
    public int getNumberOfSections() {
        return mSections.size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return mSections.get(sectionIndex).hasHeader();
    }

    @Override
    public ViewHolder onCreateItemViewHolder(ViewGroup parent) {
        return  new ItemViewHolder(mInflater.inflate(R.layout.list_item_simple, parent, false));
    }

    @Override
    public ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_sticky_header, parent, false);
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(ViewHolder viewHolder, int sectionIndex, int itemIndex) {
        Item item = mSections.get(sectionIndex).getItems().get(itemIndex);
        ItemViewHolder holder = (ItemViewHolder) viewHolder;

        holder.tvLeft.setText(item.left);
        holder.tvRight.setText(item.right);
    }

    @Override
    public void onBindHeaderViewHolder(ViewHolder viewHolder, int sectionIndex) {
        HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
        Section section = mSections.get(sectionIndex);
        holder.tvTitle.setText(section.getHeaderTitle());
        if (section.hasButton()) {
            holder.tvOption.setVisibility(View.VISIBLE);
            holder.tvOption.setText(section.getHeaderDescription());
        } else {
            holder.tvOption.setVisibility(View.GONE);
        }
    }

    public void setOnItemClickListener(OnDelegateItemClickListener onItemClickListener) {
        mListener = onItemClickListener;
    }

    protected class ItemViewHolder extends SectioningAdapter.ViewHolder implements View.OnClickListener {

        @Bind(R.id.tvLeft_LI)
        public TextView tvLeft;

        @Bind(R.id.tvRight_LI)
        public TextView tvRight;

        public ItemViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onDelegateClick(getSectionForAdapterPosition(getLayoutPosition()), getLocalPosition(getLayoutPosition()));
        }
    }

    protected class HeaderViewHolder extends SectioningAdapter.ViewHolder {
        @Bind(R.id.tvLeft_LISH)
        TextView tvTitle;

        @Bind(R.id.tvRightLISH)
        TextView tvOption;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
