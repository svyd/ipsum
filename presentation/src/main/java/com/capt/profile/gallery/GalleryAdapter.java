package com.capt.profile.gallery;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.CaptRecycleViewItemListener;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.profile.gallery.model.GalleryModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 4/23/16.
 */
@PerFragment
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private CaptRecycleViewItemListener<GalleryModel> mListener;
    private BaseFragment mFragment;
    private Context mContext;

    private List<GalleryModel> mModelList;

    @Inject
    public GalleryAdapter(Context _context,
                          LayoutInflater _inflater,
                          CaptRecycleViewItemListener<GalleryModel> _listener,
                          BaseFragment _fragment) {
        mInflater = _inflater;
        mListener = _listener;
        mFragment = _fragment;
        mContext = _context;
        mModelList = new ArrayList<>();
    }

    public void setData(List<GalleryModel> _list) {
        mModelList = _list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GalleryModel model = mModelList.get(position);
        loadImage(holder.ivGalleryItem, model.getPath());
        holder.flError.setVisibility(model.isRightDuration() ? View.GONE : View.VISIBLE);
        if (!model.isRightDuration()) {
            holder.tvError.setText(mContext.getString(R.string.gallery_item_too_short));
        }
    }

    private void loadImage(ImageView _imageView, String _path) {
        Glide.with(mFragment)
                .load(_path)
                .centerCrop()
                .into(_imageView);
    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ivGalleryItem_IG)
        ImageView ivGalleryItem;

        @Bind(R.id.flError_IG)
        FrameLayout flError;

        @Bind(R.id.tvError_IG)
        TextView tvError;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.flContainer_IG)
        protected void onItemClick() {
            mListener.onItemClick(mModelList.get(getAdapterPosition()), getAdapterPosition());
        }
    }
}
