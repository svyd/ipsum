package com.capt.base;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.capt.application.CaptApplication;
import com.capt.application.dagger.AppComponent;
import com.capt.application.dagger.HasComponent;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.splash.SplashActivity;
//import com.squareup.leakcanary.RefWatcher;

/**
 * Created by richi on 2016.02.22..
 */
public abstract class BaseFragment extends Fragment implements BaseView {

    protected @LayoutRes int mContentId = -1;
    protected Toast mToast;

    protected void setContentView(@LayoutRes int _contentId) {
        mContentId = _contentId;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mContentId == -1)
            throw new RuntimeException("You need to call setContentView before onCreateView");
        return inflater.inflate(mContentId, container, false);
    }

    protected FragmentNavigator getFragmentNavigator() {
        return ((BaseActivity) getActivity()).getFragmentNavigator();
    }

    protected ToolbarManager getToolbarManager() {
        return ((BaseActivity) getActivity()).getToolbarManager();
    }

    @Override
    public void onDestroyView() {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).getKeyboardUtil().hideKeyboard(getView());
        super.onDestroyView();
    }

    protected AppComponent getAppComponent() {
        return ((BaseActivity) getActivity()).getApplicationComponent();
    }

    protected void showMessage(String _message) {
        if (mToast != null)
            mToast.cancel();

        mToast = Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    public void showError(String _error) {
        showMessage(_error);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showNoInternetConnectionError() {

    }

    @Override
    public void hideNoInternetConnectionError() {

    }

    @Override
    public void navigateToJoinScreen() {
        CaptApplication.getApplication().getAppComponent()
                .sharedPreferences()
                .edit()
                .clear()
                .apply();

        Intent intent = new Intent(getActivity(), SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        getActivity().startActivity(intent);
    }

    @Override public void onDestroy() {
        super.onDestroy();
//        RefWatcher refWatcher = CaptApplication.getRefWatcher(getActivity());
//        refWatcher.watch(this);
    }

    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        if (!(getActivity() instanceof HasComponent))
            return null;

        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
