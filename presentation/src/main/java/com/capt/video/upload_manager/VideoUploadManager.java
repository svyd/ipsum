package com.capt.video.upload_manager;

import com.capt.domain.video.model.Video;


/**
 * Created by richi on 2016.03.14..
 */
public interface VideoUploadManager {
    void startUploading(Video _model);
    void startPendingUploading();
    void completePendingUploading(String _id);
    void cancelUpload(String _id);
    void retry(String _id);
}
