package com.capt.profile.marketplace.base.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.data.video.disk.DraftRepositoryImpl;
import com.capt.data.video.video.VideoRepositoryImpl;
import com.capt.data.video.video.VideoService;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.global.Constants;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.video.draft.DraftRepository;
import com.capt.domain.video.draft.SaveDraftInteractor;
import com.capt.domain.video.draft.UpdatePendingInteractor;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.video.DownloadVideoInteractor;
import com.capt.domain.video.video.PendingVideoInteractor;
import com.capt.domain.video.video.VideoRepository;
import com.capt.domain.video.draft.GetAllDraftInteractor;
import com.capt.profile.marketplace.base.BaseMarketplaceListContract;
import com.capt.profile.marketplace.base.BaseMarketplaceListPresenter;
import com.capt.profile.marketplace.drafts.DraftsPresenter;
import com.capt.profile.marketplace.recent.RecentPresenter;
import com.capt.profile.marketplace.sold.SoldPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by root on 14.04.16.
 */
@Module
public class MarketplaceListModule {

	private BaseMarketplaceListContract.View mView;
	private String mType;

	public MarketplaceListModule(BaseMarketplaceListContract.View _view, String _type) {
		mView = _view;
		mType = _type;
	}

	@Provides
	@PerFragment
	protected String provideType() {
		return mType;
	}

	@Provides @PerFragment
	protected BaseMarketplaceListContract.Presenter providePresenter(DraftsPresenter _draftsPresenter,
                                                                     RecentPresenter _recentPresenter,
                                                                     SoldPresenter _soldPresenter) {
        switch (mType) {
            case Constants.MarketplaceTypes.DRAFTS:
                return _draftsPresenter;
            case Constants.MarketplaceTypes.RECENT:
                return _recentPresenter;
            case Constants.MarketplaceTypes.SOLD:
                return _soldPresenter;
            default:
                throw new IllegalStateException();
        }
	}

	@Provides @PerFragment
	protected BaseMarketplaceListContract.View provideView() {
		return mView;
	}

	@Provides
	@PerFragment
	VideoService provideVideoDownloadService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
	                                                              Retrofit _retrofit) {
		return _retrofit.create(VideoService.class);
	}

	@Provides
	@PerFragment
	VideoRepository provideVideoDownloadRepository(VideoRepositoryImpl _repository) {
		return _repository;
	}

	@Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.PENDING_VIDEO_INTERACTOR)
    BaseInteractor providePendingInteractor(PendingVideoInteractor _interactor) {
        return _interactor;
    }

	@Provides
	@PerFragment
	protected DraftRepository provideDraftRepository(DraftRepositoryImpl _repository) {
		return _repository;
	}

	@Provides
	@PerFragment
	@Named(Constants.NamedAnnotation.SAVE_DRAFT_INTERACTOR)
	protected BasePostInteractor<Video> provideDraftInteractor(SaveDraftInteractor _interactor) {
		return _interactor;
	}

	@Provides
	@PerFragment
	protected BasePostInteractor<RequestParamsModel> provideDownloadVideoInteractor(VideoRepository _repository,
																					DraftRepository _draftsSource,
																					PostExecutionThread _postExecutionThread) {
		if (mType.equals(Constants.MarketplaceTypes.DRAFTS)) {
			return new GetAllDraftInteractor(_draftsSource, _postExecutionThread);
		} else {
			return new DownloadVideoInteractor(_repository, _postExecutionThread);
		}
	}
}
