package com.capt.video.camera.activity.dagger;

import com.capt.base.dagger.BaseActivityComponent;
import com.capt.data.annotation.PerActivity;
import com.capt.video.camera.fragment.dagger.CameraFragmentComponent;
import com.capt.video.camera.fragment.dagger.CameraFragmentModule;

import dagger.Subcomponent;

/**
 * Created by Svyd on 07.04.2016.
 */
@PerActivity
@Subcomponent(modules = CameraActivityModule.class)
public interface CameraActivityComponent extends BaseActivityComponent {
    CameraFragmentComponent createCameraComponent(CameraFragmentModule _module);
}
