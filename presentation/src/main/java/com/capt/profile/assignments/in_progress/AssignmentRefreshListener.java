package com.capt.profile.assignments.in_progress;

/**
 * Created by Svyd on 12.07.2016.
 */
public interface AssignmentRefreshListener {
    void onRefreshed();
}
