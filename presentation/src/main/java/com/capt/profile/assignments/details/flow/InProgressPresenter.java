package com.capt.profile.assignments.details.flow;

import com.capt.authorization.location.LocationInteractorManager;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.base.DateTimeUtility;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.profile.assignment.accept.model.AcceptAssignmentModel;
import com.capt.profile.assignments.details.DetailsContract;

/**
 * Created by rMozes on 8/4/16.
 */
public class InProgressPresenter extends DetailsPresenter {

    public InProgressPresenter(DateTimeUtility _utility,
                               DetailsContract.View _view,
                               BasePostInteractor<AcceptAssignmentModel> _interactor,
                               LocationInteractorManager _manager,
                               BaseExceptionDelegate _delegate) {
        super(_utility, _view, _interactor, _manager, _delegate);
    }

    @Override
    public void onRedBtnClicked() {
        mView.showCaptDialog(mAssignment);
    }
}
