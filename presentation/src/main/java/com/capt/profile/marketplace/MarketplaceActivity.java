package com.capt.profile.marketplace;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.capt.R;
import com.capt.application.dagger.HasComponent;
import com.capt.base.BaseActivity;
import com.capt.domain.global.Constants;
import com.capt.profile.marketplace.dagger.MarketPlaceModule;
import com.capt.profile.marketplace.dagger.MarketplaceActivityComponent;
import com.capt.profile.marketplace.dagger.MarketplaceActivityModule;

/**
 * Created by Svyd on 03.06.2016.
 */
public class MarketplaceActivity extends BaseActivity implements HasComponent<MarketplaceActivityComponent>{

    private MarketplaceActivityComponent mComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_marketplace);
        initComponent();
        inject();
        if (savedInstanceState == null) {
            initFragment(getIntent(), false);
        }
    }

    private void initFragment(Intent _intent, boolean _replace) {
        Constants.MarketplaceItems item =
                (Constants.MarketplaceItems) _intent.getSerializableExtra(Constants.Extra.EXTRA_SERIALIZABLE);
        if (_replace) {
            getFragmentNavigator().replaceFragmentWithoutBackStack(MarketPlaceFragment.newInstance(item));
        } else {
            getFragmentNavigator().addFragmentWithoutBackStack(MarketPlaceFragment.newInstance(item));
        }
    }

    private void initComponent() {
        mComponent = getApplicationComponent()
                .createMarketplaceComponent(new MarketplaceActivityModule(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setResult(resultCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RequestCode.SHOULD_UPDATE) {
            initFragment(data, true);
        }
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public int getContainerId() {
        return R.id.flContainer_AMP;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AMP;
    }

    @Override
    public MarketplaceActivityComponent getComponent() {
        return mComponent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
