package com.capt.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.authorization.AuthorizationActivity;
import com.capt.domain.global.Constants;
import com.capt.profile.ProfileActivity;

import javax.inject.Inject;

/**
 * Created by Svyd on 12.04.2016.
 */
public class SplashActivity extends AppCompatActivity {

    @Inject
    SharedPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initComponent();

        if (checkUser()) {
            startProfileActivity();
        } else {
            startAuthorizationActivity();
        }
        finish();
    }

    private void initComponent() {
        CaptApplication
                .getApplication()
                .getAppComponent()
                .inject(this);
    }

    private boolean checkUser() {
        return mPreferences.contains(Constants.PREFERENCES_USER_PROFILE);
    }

    private void startProfileActivity() {
        Intent intent = new Intent(SplashActivity.this, ProfileActivity.class);
        startActivity(intent);
    }

    private void startAuthorizationActivity() {
        startActivity(new Intent(SplashActivity.this, AuthorizationActivity.class));
    }

}
