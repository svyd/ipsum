package com.capt.controller.delegate_exception;

/**
 * Created by Svyd on 7/28/16.
 */
public class UnknownExceptionDelegate extends BaseExceptionDelegate<Throwable> {

    public UnknownExceptionDelegate() {
    }

    @Override
    public void handleException(Throwable _exception) {
        mBaseView.hideProgress();
        mBaseView.showError(_exception.getMessage());
    }
}
