package com.capt.authorization.code_verification;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.UserModel;

/**
 * Created by richi on 2016.03.19..
 */
public interface CodeVerificationContract {

    interface View extends BaseView {
        void startDashboardScreen(UserModel _model);
        void showMessage(String _message);
        void setEnabledSendAgain(boolean _enable);
        void setConfirmOnly();
    }

    abstract class Presenter extends BasePresenter<CodeVerificationContract.View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setCodeSignUpModel(CodeSignUpModel _data);
        public abstract void onNextClick(String _code);
        public abstract void sendAgainClick();
    }
}
