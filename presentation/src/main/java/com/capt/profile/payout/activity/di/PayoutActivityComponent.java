package com.capt.profile.payout.activity.di;

import com.capt.base.dagger.BaseActivityComponent;
import com.capt.data.annotation.PerActivity;

import dagger.Subcomponent;

/**
 * Created by rMozes on 8/3/16.
 */
@PerActivity
@Subcomponent(modules = PayoutActivityModule.class)
public interface PayoutActivityComponent extends BaseActivityComponent {
}
