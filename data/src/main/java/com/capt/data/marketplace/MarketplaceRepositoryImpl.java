package com.capt.data.marketplace;

import com.capt.data.base.BaseDAO;
import com.capt.data.base.TypeMapper;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.dashboard.model.Marketplace;
import com.capt.domain.profile.marketplace.MarketplaceRepository;
import com.capt.domain.profile.marketplace.model.MarketplaceItem;
import com.capt.domain.profile.marketplace.model.MarketplaceModel;
import com.capt.domain.video.model.Video;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public class MarketplaceRepositoryImpl implements MarketplaceRepository {

    private MarketplaceService mService;
    private TypeMapper<List<MarketplaceItem>, MarketplaceModel> mMapper;
    private BaseDAO<Video> mDao;

    @Inject
    public MarketplaceRepositoryImpl(
            @Named(Constants.NamedAnnotation.Mapper.MARKETPLACE_MAPPER)
            TypeMapper<List<MarketplaceItem>, MarketplaceModel> _mapper,
            BaseDAO<Video> _dao,
            MarketplaceService _service) {
        mService = _service;
        mMapper = _mapper;
        mDao = _dao;
    }

    @Override
    public Observable<List<MarketplaceItem>> getMarketplace() {
        return mService.getMarketplaceInfo()
                .map(marketplaceModel -> {
                    marketplaceModel.drafts = mDao.getDraftCount();
                    return marketplaceModel;
                })
                .map(mMapper::convert);
    }
}
