package com.capt.profile.settings.notification;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.settings.model.SettingsModel;

/**
 * Created by root on 29.04.16.
 */
public interface NotificationSettingsContract {

    interface View extends BaseView {
        void setSettingsOnView(SettingsModel settings);
        void showMessage(String _message);
        void setSettings(SettingsModel _settings);

    }

    abstract class Presenter extends BasePresenter<NotificationSettingsContract.View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setSettingsModel(SettingsModel settings);
        public abstract void onSwitchClick(int switchId);
    }
}
