package com.capt.profile.assignments.base;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.profile.assignments.RefreshListener;

import java.util.List;

/**
 * Created by root on 15.04.16.
 */
public interface BaseAssignmentsContract {
	interface View extends BaseView, RefreshListener {
		void removeItem(String _id);
        void updateItem(String _id, String _status);
		void showAssignments(List<Assignment> assignments);
		void addAssignments(List<Assignment> newAssignments);
		void showMessage(String _message);
		void showEmptyList();
		void disableLoadMore();
		void hideRefresh();
		void navigateToDetails(Assignment _model, String _type);
		void navigateToDetailsInReview(Assignment _model);
		void navigateToRejected(Assignment _model);
		void navigateToPurchased(Assignment _model);
		void clearList();
		void setReminderVisibility(boolean _visibility);
		void setListVisibility(boolean _visibility);
		void navigateToAddLocation();
	}

	abstract class Presenter extends BasePresenter<BaseAssignmentsContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void navigateToAssignmentDetails(Assignment assignment);
		public abstract void getMoreAssignments();
		public abstract void onActivityResult(int requestCode, int resultCode, Bundle data);
		public abstract void onAddLocationClick();
		public abstract void onRetryClick();
		public abstract void refresh();
		public abstract void onUploadError(String uploadId);
		public abstract void onUploadCompleted(String uploadId);
    }
}