package com.capt.data.db;

import android.provider.BaseColumns;

/**
 * Created by Svyd on 14.04.2016.
 */
public class CaptContract {

    public static final String DATABASE_NAME = "capt.db";

    public static final int DATABASE_VERSION = 1;

    public static final String DROP_COMMAND = "DROP TABLE IF EXISTS ";
    public static final String LIKE_OPERATOR = " LIKE ?";

    public static final class VideoEntry implements BaseColumns {

        public static final String TABLE_NAME = "videos";

        public static final String COLUMN_DRAFT = "draft";
        public static final String COLUMN_UPLOADING = "uploading";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_CURRENCY_ISO = "currency_iso";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_CURRENCY_ID = "currency_id";
        public static final String COLUMN_LICENSE = "license";
        public static final String COLUMN_CURRENCY_SYMBOL = "currency_sign";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_LATITUDE = "lat";
        public static final String COLUMN_LONGITUDE = "lon";
        public static final String COLUMN_DURATION = "duration";
        public static final String COLUMN_CITY = "city";
        public static final String COLUMN_QUALITY = "quality";
        public static final String COLUMN_COUNTRY = "country";
        public static final String COLUMN_CATEGORIES = "tags";
        public static final String COLUMN_PATH = "path";
        public static final String COLUMN_FORMATTED_LOCATION = "formatted_location";
        public static final String COLUMN_FORMATTED_SIZE = "formatted_size";
        public static final String COLUMN_DRAFT_ID = "draft_id";
        public static final String COLUMN_ERROR = "error";
        public static final String COLUMN_THUMBNAIL = "thumbnail";
        public static final String COLUMN_TIME_UPLOAD_STARTED = "time_upload_started";
        public static final String COLUMN_CURRENTLY_UPLOADING = "currently_uploading";

        public static final String SQL_CREATE_VIDEO_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_PATH + " TEXT NOT NULL, " +
                COLUMN_TITLE + " TEXT NOT NULL, " +
                COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                COLUMN_FORMATTED_LOCATION + " TEXT NOT NULL, " +
                COLUMN_FORMATTED_SIZE + " TEXT NOT NULL, " +
                COLUMN_PRICE + " REAL NOT NULL, " +
                COLUMN_CURRENCY_ID + " TEXT NOT NULL, " +
                COLUMN_THUMBNAIL + " TEXT NOT NULL, " +
                COLUMN_CURRENCY_ISO + " TEXT NOT NULL, " +
                COLUMN_QUALITY + " TEXT NOT NULL, " +
                COLUMN_COUNTRY + " TEXT NOT NULL, " +
                COLUMN_CITY + " TEXT NOT NULL, " +
                COLUMN_LICENSE + " TEXT NOT NULL, " +
                COLUMN_CURRENCY_SYMBOL + " TEXT NOT NULL, " +
                COLUMN_DATE + " INTEGER NOT NULL, " +
                COLUMN_DRAFT + " INTEGER, " +
                COLUMN_CURRENTLY_UPLOADING + " INTEGER, " +
                COLUMN_ERROR + " INTEGER, " +
                COLUMN_DRAFT_ID + " INTEGER, " +
                COLUMN_UPLOADING + " INTEGER, " +
                COLUMN_TIME_UPLOAD_STARTED + " INTEGER, " +
                COLUMN_LATITUDE + " REAL NOT NULL, " +
                COLUMN_LONGITUDE + " REAL NOT NULL, " +
                COLUMN_DURATION + " INTEGER NOT NULL, " +
                COLUMN_CATEGORIES + " TEXT NOT NULL " +
                " );";
    }

}
