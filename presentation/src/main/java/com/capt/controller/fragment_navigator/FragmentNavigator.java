package com.capt.controller.fragment_navigator;

import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.capt.base.BaseFragment;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.22..
 */
public abstract class FragmentNavigator {

    protected FragmentManager mFragmentManager;
    protected
    @IdRes
    int mContainerId = -1;

    public FragmentNavigator(FragmentManager _manager, @IdRes int _containerId) {
        if (_manager == null)
            throw new NullPointerException("FragmentManager is null");

        mFragmentManager = _manager;
        mContainerId = _containerId;
    }

    protected FragmentTransaction getTransaction() {
        return mFragmentManager.beginTransaction();
    }

    public abstract void clearBackStack();

    //Fragment transactions without back stack
    public abstract void replaceFragmentWithoutBackStack(BaseFragment _fragment);

    public abstract void addFragmentWithoutBackStack(BaseFragment _fragment);

    public abstract void addFragmentWithContainer(BaseFragment _fragment, @IdRes int _containerId);

    //Fragment transactions with back stack
    public abstract void replaceFragmentWithBackStack(BaseFragment _fragment);
    public abstract void replaceFragmentWithBackStack(BaseFragment _fragment, @IdRes int _containerId);

    public abstract void addFragmentWithBackStack(BaseFragment _fragment);

    public abstract void popBackStack();

    public abstract BaseFragment getTopFragment();
    public abstract BaseFragment getTopFragment(@IdRes int _containerId);

}
