package com.capt.authorization.location;

import com.capt.data.annotation.PerFragment;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import rx.Observer;

/**
 * Created by Svyd on 16.03.2016.
 */
@PerFragment
public class LocationInteractorManager {
    BasePostInteractor<String> mStringInteractor;
    BasePostInteractor<LocationModel> mLatLngInteractor;
    BasePostInteractor<String> mDetailsInteractor;

    @Inject
    public LocationInteractorManager(@Named(Constants.NamedAnnotation.LOCATION_STRING_INTERACTOR)
                                     BasePostInteractor<String> _stringInteractor,
                                     @Named(Constants.NamedAnnotation.LOCATION_LAT_LNG_INTERACTOR)
                                     BasePostInteractor<LocationModel> _locationInteractor,
                                     @Named(Constants.NamedAnnotation.LOCATION_DETAILS_INTERACTOR)
                                     BasePostInteractor<String> _detailsInteractor) {
        mDetailsInteractor = _detailsInteractor;
        mStringInteractor = _stringInteractor;
        mLatLngInteractor = _locationInteractor;
    }

    public void executeDetails(String _data, Observer<LocationModel> _observer) {
        mDetailsInteractor.execute(_data, _observer);
    }

    public void execute(String _data, Observer<PredictionsModel> _observer) {
        mStringInteractor.execute(_data, _observer);
    }

    public void execute(LocationModel _data, Observer<PredictionsModel> _observer) {
        mLatLngInteractor.execute(_data, _observer);
    }

    public void unSubscribe() {
        mLatLngInteractor.unSubscribe();
        mStringInteractor.unSubscribe();
        mDetailsInteractor.unSubscribe();
    }
}
