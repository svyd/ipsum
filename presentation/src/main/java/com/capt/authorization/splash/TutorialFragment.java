package com.capt.authorization.splash;

import android.os.Bundle;
import android.view.View;

import com.capt.R;
import com.capt.authorization.join_fragment.JoinFragment;
import com.capt.base.BaseFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 15.03.2016.
 */
public class TutorialFragment extends BaseFragment {

    public static TutorialFragment newInstance() {
        Bundle args = new Bundle();

        TutorialFragment fragment = new TutorialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_tutorial);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getToolbarManager().hideToolbar();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @OnClick(R.id.btnJoin_FT)
    protected void onJoinClick() {

        getFragmentNavigator().replaceFragmentWithBackStack(JoinFragment.newInstance());
    }
}
