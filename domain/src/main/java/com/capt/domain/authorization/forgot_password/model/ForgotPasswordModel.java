package com.capt.domain.authorization.forgot_password.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 22.03.2016.
 */
public class ForgotPasswordModel {

    public ForgotPasswordModel(String _email) {
        value = _email;
    }

    @SerializedName("email")
    @Expose
    public String value;
}
