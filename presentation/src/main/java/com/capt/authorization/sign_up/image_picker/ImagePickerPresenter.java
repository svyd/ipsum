package com.capt.authorization.sign_up.image_picker;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;

import com.capt.application.CaptApplication;
import com.capt.authorization.sign_up.PhotoPathGenerator;
import com.capt.base.GetFilePathFromDevice;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

import javax.inject.Inject;

/**
 * Created by richi on 2016.03.20..
 */
public class ImagePickerPresenter extends ImagePickerContract.Presenter {

    private ImagePickerContract.View mView;
    private PhotoPathGenerator mGenereator;
    private GetFilePathFromDevice mFilePicker;
    private String mPhotoPath;

    @Inject
    public ImagePickerPresenter(ImagePickerContract.View _view, PhotoPathGenerator _generator,
                                GetFilePathFromDevice _filePicker, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mGenereator = _generator;
        mFilePicker = _filePicker;
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onCameraClick() {
        mView.startCameraIntent();
    }

    @Override
    public void onGalleryClick() {
        mView.startGalleryIntent();
    }

    @Override
    public void onResultGallery(Intent _data) {
        Uri uri = _data.getData();
        mPhotoPath = mFilePicker.getPath(uri);

        mView.setResult(mPhotoPath);
    }

    @Override
    public String getPhotoPath() {
        return mGenereator.getAvatarPath();
    }

    @Override
    public void onResultCamera() {
        mPhotoPath = mGenereator.getAvatarPath();
        mView.setResult(mPhotoPath);
    }
}
