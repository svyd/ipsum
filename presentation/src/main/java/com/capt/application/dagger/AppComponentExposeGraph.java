package com.capt.application.dagger;

import android.content.Context;
import android.content.SharedPreferences;

import com.capt.authorization.sign_up.PhotoPathGenerator;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.global.Constants;
import com.google.android.gms.common.api.GoogleApiClient;

import java.net.CookieStore;

import javax.inject.Named;

import retrofit2.Retrofit;

/**
 * Created by richi on 2016.03.16..
 */
public interface AppComponentExposeGraph {
    Context context();
    @Named(Constants.NamedAnnotation.PREFERENCES_COOKIES)
    SharedPreferences cookiesPreferences();
    SharedPreferences sharedPreferences();
    GoogleApiClient.Builder apiClientBuilder();
    PostExecutionThread postExecutionThread();
    @Named(Constants.NamedAnnotation.MAIN_RETROFIT)  Retrofit retrofit();
    CookieStore cookieStore();
    PhotoPathGenerator photoGenerator();
}
