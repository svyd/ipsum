package com.capt.domain.video.upload;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.profile.assignment.model.Assignment;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 16.05.2016.
 */
public class UploadForAssignmentInteractor extends BasePostInteractor<Assignment> {

    private VideoUploadRepository mRepository;

    @Inject
    public UploadForAssignmentInteractor(VideoUploadRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);

        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(Assignment _data) {
        return mRepository.uploadForAssignment(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
