package com.capt.profile.settings.password;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

/**
 * Created by Svyd on 23.06.2016.
 */
public interface PasswordContract {

    interface View extends BaseView {
        void hideCurrentPassword();
        void showCurrentPassword();
        void hideNewPassword();
        void showNewPassword();
        void hideConfirmPassword();
        void showConfirmPassword();
        void showToast(String _message);
        String getCurrentPassword();
        String getNewPassword();
        String getConfirmPassword();
        void navigateBack();
    }

    abstract class Presenter extends BasePresenter<PasswordContract.View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onCurrentEyeClick();
        public abstract void onNewEyeClick();
        public abstract void onConfirmEyeClick();
        public abstract void onChangeClick();
    }
}
