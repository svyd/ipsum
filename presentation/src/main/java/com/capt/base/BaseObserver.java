package com.capt.base;

import rx.Observer;

/**
 * Created by rMozes on 7/28/16.
 */
public abstract class BaseObserver<T> implements Observer<T> {

    private BasePresenter mPresenter;

    public BaseObserver(BasePresenter _presenter) {
        mPresenter = _presenter;
    }

    @Override
    public void onError(Throwable e) {
        mPresenter.getErrorHandler().onError(e);
    }

    @Override
    public void onCompleted() {

    }
}
