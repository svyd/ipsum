package com.capt.video.camera.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.video.camera.BaseAdapter;
import com.capt.video.view.VerticalTextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by richi on 2016.03.28..
 */
public class SettingsPickerAdapter implements BaseAdapter {

    private VerticalTextView mMainView;
    private List<String> mList = new ArrayList<>();

    @Inject
    public SettingsPickerAdapter() {}

    public SettingsPickerAdapter(List<String> _list) {
        mList = _list;
    }

    @Override
    public void setItems(List _items) {
        mList = _items;
    }

    @Override
    public View getMainView(ViewGroup _parent) {
        if (mMainView == null) {
            mMainView = (VerticalTextView) LayoutInflater.from(CaptApplication.getApplication())
                    .inflate(R.layout.list_item_settings_picker, _parent, false);
        }
        return mMainView;
    }

    @Override
    public View getView(int _position, ViewGroup _parent) {
        String string = mList.get(_position);

        VerticalTextView textView = (VerticalTextView) LayoutInflater.from(CaptApplication.getApplication())
                .inflate(R.layout.list_item_settings_picker, _parent, false);
        textView.setText(string);
        return textView;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public String getItem(int _position) {
        return mList.get(_position);
    }

    @Override
    public void notifyDataChanged() {
        if (mMainView == null) {
            return;
        }
        mMainView.setText(mList.get(0));
    }
}
