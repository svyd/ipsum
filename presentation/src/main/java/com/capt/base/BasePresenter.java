package com.capt.base;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;

/**
 * Created by richi on 2016.02.23..
 */
public abstract class BasePresenter<T extends BaseView> {

    private BaseExceptionDelegate mExceptionDelegate;

    public BasePresenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
        this.mExceptionDelegate = _delegate;
        mExceptionDelegate.setView(_baseView);
    }

    public void error(Throwable _error) {
        mExceptionDelegate.onError(_error);
    }

    public BaseExceptionDelegate getErrorHandler() {
        return mExceptionDelegate;
    }

    public void initialize() {

    }
    public  void onStart() {

    }

    public abstract void onStop();
}
