package com.capt.profile.assignments.base;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.profile.ScrollForLoadMoreListener;
import com.capt.profile.assignments.AssignmentsAdapter;
import com.capt.profile.assignments.base.dagger.BaseAssignmentsComponent;
import com.capt.profile.assignments.base.dagger.BaseAssignmentsModule;
import com.capt.profile.assignments.details.AssignmentDetailsActivity;
import com.capt.profile.assignments.in_progress.AssignmentRefreshListener;
import com.capt.profile.dagger.ProfileActivityComponent;
import com.capt.profile.edit_profile.EditProfileActivity;
import com.capt.video.VideoActivity;

import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by root on 15.04.16.
 */
public abstract class BaseAssignmentsFragment extends BaseToolbarFragment
        implements BaseAssignmentsContract.View, ScrollForLoadMoreListener.OnLoadMoreListener {

    @Inject
    protected BaseAssignmentsContract.Presenter mPresenter;

    @Inject
    AssignmentsAdapter mAdapter;

    @Bind(R.id.rv_videos_FL)
    RecyclerView rvAssignments;

    @Bind(R.id.pb_fl)
    ProgressBar progressBar;

    @Bind(R.id.tv_rv_is_empty)
    TextView tvEmptyList;

    @Bind(R.id.cvLocationReminder_FL)
    CardView rlLocationReminder;

    @Bind(R.id.llRetry_FL)
    LinearLayout llRetry;

    private Bundle mArgs;

    private ScrollForLoadMoreListener mScrollListener;
    private BaseAssignmentsComponent mComponent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_list);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        setUpView();
        if (!checkResult()) {
            mPresenter.initialize();
        }
    }

    @Override
    public void removeItem(String _id) {
        mAdapter.removeItem(_id);
    }

    @Override
    public void updateItem(String _id, String _status) {
        mAdapter.updateItem(_id, _status);
    }

    private boolean checkResult() {
        if (mArgs == null)
            return false;

        if (mArgs.containsKey(Constants.RESULT_BUNDLE)) {
            Bundle bundle = mArgs.getBundle(Constants.RESULT_BUNDLE);
            if (bundle != null) {
                mPresenter.onActivityResult(
                        bundle.getInt(Constants.REQUEST_CODE),
                        bundle.getInt(Constants.RESULT_CODE),
                        bundle);
                return true;
            }
        }
        return false;
    }

    @Override
    public void refresh() {
        if (mPresenter != null)
            mPresenter.refresh();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(ProfileActivityComponent.class)
                .createBaseAssignmentsComponent(new BaseAssignmentsModule(this, getType()));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    @Override
    protected void setUpActionBar(ActionBar actionBar) {
        super.setUpActionBar(actionBar);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle bundle;
        if (data != null && data.getExtras() != null) {
            bundle = data.getExtras();
        } else {
            bundle = new Bundle();
        }
        bundle.putInt(Constants.REQUEST_CODE, requestCode);
        bundle.putInt(Constants.RESULT_CODE, resultCode);

        mArgs = new Bundle();
        mArgs.putBundle(Constants.RESULT_BUNDLE, bundle);
    }

    public void setUpView() {
        rvAssignments.setLayoutManager(new LinearLayoutManager(getActivity(),
                android.support.v7.widget.LinearLayoutManager.VERTICAL, false));
        rvAssignments.setAdapter(mAdapter);
        mAdapter.setOnAssignmentClickListener(mPresenter::navigateToAssignmentDetails);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onStart();
        getActivity().registerReceiver(mReceiver, new IntentFilter("com.capt.uploadservice.broadcast.status"));
    }

    @Override
    public void setReminderVisibility(boolean _visibility) {
        rlLocationReminder.setVisibility(_visibility ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setListVisibility(boolean _visibility) {
        rvAssignments.setVisibility(_visibility ? View.VISIBLE : View.GONE);
    }

    @Override
    public void clearList() {
        mAdapter.clear();
    }

    @Override
    public void showAssignments(List<Assignment> assignments) {
        mAdapter.setData(assignments, getType());
        rvAssignments.setVisibility(View.VISIBLE);
        tvEmptyList.setVisibility(View.INVISIBLE);
        mScrollListener = new ScrollForLoadMoreListener(this);
        rvAssignments.addOnScrollListener(mScrollListener);
    }

    @Override
    public void showEmptyList() {
        rvAssignments.setVisibility(View.INVISIBLE);
        tvEmptyList.setVisibility(View.VISIBLE);
    }

    @Override
    public void addAssignments(List<Assignment> newAssignments) {
        ((AssignmentRefreshListener) getParentFragment()).onRefreshed();
        mAdapter.addModels(newAssignments);
        mScrollListener.enable();
    }

    @Override
    public void showMessage(String _message) {
        super.showMessage(_message);
    }

    @OnClick(R.id.rlLocationBtn_FL)
    void onAddLocationClick() {
        mPresenter.onAddLocationClick();
    }

    @Override
    public void navigateToAddLocation() {
        EditProfileActivity.startLocation(getActivity());
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    protected abstract String getType();

    @Override
    public void onLoadMore() {
        mPresenter.getMoreAssignments();
    }

    @Override
    public void disableLoadMore() {
        ((AssignmentRefreshListener) getParentFragment()).onRefreshed();
        rvAssignments.removeOnScrollListener(mScrollListener);
    }

    @Override
    public void hideRefresh() {
        ((AssignmentRefreshListener) getParentFragment()).onRefreshed();
    }

    @Override
    public void navigateToDetails(Assignment _model, String _type) {
        Intent intent = new Intent(getActivity(), AssignmentDetailsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, _model);
        args.putString(Constants.Extra.EXTRA_STRING, _type);

        intent.putExtras(args);
        getActivity().startActivityForResult(intent, Constants.RequestCode.ASSIGNMENT);
    }

    @Override
    public void navigateToDetailsInReview(Assignment _model) {
        VideoActivity.startAssignmentDetails(_model, getActivity());
    }

    @Override
    public void navigateToRejected(Assignment _model) {
        VideoActivity.startRejectedAssignment(_model, getActivity());
    }

    @Override
    public void navigateToPurchased(Assignment _model) {
        VideoActivity.startPurchasedAssignment(_model, getActivity());
    }

    @Override
    public void showNoInternetConnectionError() {
        ((AssignmentRefreshListener) getParentFragment()).onRefreshed();
        llRetry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoInternetConnectionError() {
        llRetry.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnTryAgain_IR)
    void onRetryClick() {
        mPresenter.onRetryClick();
    }


    private UploadServiceBroadcastReceiver mReceiver = new UploadServiceBroadcastReceiver() {

        @Override
        public void onError(String uploadId, Exception exception) {
            super.onError(uploadId, exception);
            mPresenter.onUploadError(uploadId);
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, byte[] serverResponseBody) {
            super.onCompleted(uploadId, serverResponseCode, serverResponseBody);
            mPresenter.onUploadCompleted(uploadId);
        }
    };
}
