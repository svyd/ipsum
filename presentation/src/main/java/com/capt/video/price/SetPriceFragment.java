package com.capt.video.price;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.global.Constants;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.video.model.Video;
import com.capt.video.category.CategoryFragment;
import com.capt.video.currency.CurrencyFragment;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.price.dagger.SetPriceComponent;
import com.capt.video.price.dagger.SetPriceModule;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by root on 25.03.16.
 */
public class SetPriceFragment extends BaseToolbarFragment implements SetPriceContract.View {

    @Inject
    SetPriceContract.Presenter mPresenter;
    @Inject
    ToolbarManager mToolbar;

    @Bind(R.id.tvCurrency_FSP)
    TextView tvCurrency;
    @Bind(R.id.tvPrice_FSP)
    TextView tvPrice;
    @Bind(R.id.et_price_FSP)
    EditText etPrice;

    @Bind(R.id.progress_FSP)
    ProgressBar pbCurrency;

    public static final int CURRENCY_REQUEST_CODE = 107;

    private SetPriceComponent mComponent;
    private CurrencyModel selectedCurrency;

    // TODO: 12.04.2016 Vasya, remove from here the mData!!
    private Video mData;

    public static SetPriceFragment newInstance(Bundle _args) {

        SetPriceFragment fragment = new SetPriceFragment();
        fragment.setArguments(_args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_set_price);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO: 12.04.2016 move savedInstanceState to presenter

        initComponent();
        inject();
        showToolbar();
        if (savedInstanceState != null && savedInstanceState.containsKey(Constants.BUNDLE_VIDEO_UPLOAD_MODEL)) {
            mData = (Video) savedInstanceState.getSerializable(Constants.BUNDLE_VIDEO_UPLOAD_MODEL);
            mPresenter.setVideoUploadModel(mData);
        } else {
            checkExtra();
        }
        if (selectedCurrency == null) {
            mPresenter.initialize();
        }
    }

    private void checkExtra() {
        if (getArguments().containsKey(Constants.BUNDLE_VIDEO)) {
            mData = (Video) getArguments().getSerializable(Constants.BUNDLE_VIDEO);
            mPresenter.setVideoUploadModel(mData);
        }
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(VideoActivityComponent.class)
                .createSetPriceComponent(new SetPriceModule(this));
    }

    private void inject() {
        if (mToolbar != null)
            return;

        mComponent.inject(this);
    }

    private void showToolbar() {
        mToolbar.showToolbar();
        getActivity().setTitle(getString(R.string.set_price));
    }

    @Override
    protected void setUpActionBar(ActionBar actionBar) {
        super.setUpActionBar(actionBar);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close);
    }

    @OnClick({R.id.tvCurrency_FSP})
    protected void onCurrencyClick() {
        mPresenter.navigateToCurrencySelection();
    }

    @Override
    public void navigateToCurrencyScreen() {

        BaseFragment fragment = CurrencyFragment.newInstance(selectedCurrency);
        fragment.setTargetFragment(this, CURRENCY_REQUEST_CODE);

        getFragmentNavigator().replaceFragmentWithBackStack(fragment);
    }

    @Override
    public void navigateToCategoryScreen(Bundle _data) {
        _data.putInt(Constants.FLOW, getArguments().getInt(Constants.FLOW));
        getFragmentNavigator().replaceFragmentWithBackStack(CategoryFragment.newInstance(_data));
    }

    @Override
    public void renderRealPrice(String _price) {
        if (tvPrice != null)
            tvPrice.setText(_price);
    }

    @Override
    public void setPrice(String price) {
        etPrice.setText(price);
    }

    @Override
    public void setCurrency(CurrencyModel price) {
        selectedCurrency = price;
        tvCurrency.setText(price.getSymbol());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == CURRENCY_REQUEST_CODE) {
            selectedCurrency = (CurrencyModel) data.getSerializableExtra(Constants.Extra.EXTRA_CURRENCY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Constants.BUNDLE_VIDEO_UPLOAD_MODEL, mData);
    }

    @Override
    public void showProgress() {
        pbCurrency.setVisibility(View.VISIBLE);
        tvCurrency.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        if (pbCurrency == null)
            return;

        pbCurrency.setVisibility(View.GONE);
        tvCurrency.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void showValidateMessage() {
        showMessage(getString(R.string.error_empty_price));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setData();
    }

    private void setData() {
        if (selectedCurrency != null && selectedCurrency.getCurrencyISO() != null
                && tvCurrency != null) {
            tvCurrency.setText(selectedCurrency.getCurrencyISO());
        }
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void showCurrency(CurrencyModel currencyModel) {
        selectedCurrency = currencyModel;
        setData();
    }

    @Override
    public void showMessage(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public CurrencyModel getSelectedCurrency() {
        return selectedCurrency;
    }

    @Override
    public String getPrice() {
        return etPrice.getText().toString();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;

            case R.id.action_next:
                mPresenter.navigateToCategory();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnTextChanged(R.id.et_price_FSP)
    protected void onTextChanged(CharSequence _text) {
        mPresenter.onPriceChanged(_text);
    }
}
