package com.capt.domain.video.video;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 25.05.2016.
 */
public class PendingVideoInteractor extends BaseInteractor {

    private VideoRepository mRepository;

    @Inject
    public PendingVideoInteractor(VideoRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);

        mRepository = _repository;
    }

    @Override
    protected Observable buildGetObserver() {
        return mRepository.getPendingVideos();
    }
}
