package com.capt.domain.video.upload;

import com.capt.domain.video.model.Video;

import java.util.List;

import rx.Observable;

/**
 * Created by richi on 2016.03.14..
 */
public interface PendingUploadCache {
    Observable<String> cancelPendingUpload(String _id);
    Observable<String> completePendingUpload(String _id);
    Observable<String> cachePendingVideo(Video _id);
    Observable<List<Video>> getPendingUploads();
}
