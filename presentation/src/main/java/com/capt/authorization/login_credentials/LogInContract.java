package com.capt.authorization.login_credentials;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.authorization.model.UserModel;

/**
 * Created by richi on 2016.03.20..
 */
public interface LogInContract {

    interface View extends BaseView {
        void startProfile(UserModel _model);
        void showMessage(String _message);
        void showPassword();
        void hidePassword();
        String getEmail();
        String getPassword();
        void setEnableJoin(boolean _enable);
    }

    abstract class Presenter extends BasePresenter<LogInContract.View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onJoinClick(SignInModel.Builder _builder);
        public abstract void onTextChanged();
        public abstract void onEyeClick();
    }
}
