package com.capt.domain.profile.assignment;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.model.RequestParamsModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public class AssignmentsInteractor extends BasePostInteractor<RequestParamsModel> {

    private AssignmentsRepository mRepository;

    @Inject
    public AssignmentsInteractor(AssignmentsRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(RequestParamsModel _data) {
        return mRepository.getAssignments(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
