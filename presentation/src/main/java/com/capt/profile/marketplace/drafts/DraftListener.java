package com.capt.profile.marketplace.drafts;

import com.capt.domain.video.model.Video;

/**
 * Created by Svyd on 26.05.2016.
 */
public interface DraftListener {
    void addDraft(Video _draft);
}
