package com.capt.domain.authorization.location;

import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PlaceModel;
import com.capt.domain.authorization.location.model.PredictionsModel;

import rx.Observable;

/**
 * Created by Svyd on 16.03.2016.
 */
public interface LocationDataStore {
    Observable<PredictionsModel> getAddressList(String _query);
    Observable<LocationModel> getPlaceDetails(String _placeReference);
    Observable<PredictionsModel> getAddressList(LocationModel _model);
}
