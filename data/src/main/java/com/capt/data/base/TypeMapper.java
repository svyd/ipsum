package com.capt.data.base;

/**
 * Created by Svyd on 16.03.2016.
 */
public interface TypeMapper<T, V> {
    T convert(V _value);
}
