package com.capt.data.video.upload.data_source;

import com.capt.domain.video.DiskDataSource;
import com.capt.domain.video.upload.PendingUploadCache;
import com.google.gson.Gson;

import net.gotev.uploadservice.HttpUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by richi on 2016.03.11..
 */
public class UploadVideoDataSourceFactory {

    private UploadVideoDataSource mDataSource;

    //Dependencies
    private Provider<HttpUploadRequest> mUploadRequestProvider;
    private PendingUploadCache mVideoCache;
    private DiskDataSource mDiskDataSource;
    private Provider<UploadNotificationConfig> mConfig;
    private Gson mGson;
    private UploadPayPalCodeService mService;

    @Inject
    public UploadVideoDataSourceFactory(Gson _gson,
                                        Provider<HttpUploadRequest> _requestProvider,
                                        PendingUploadCache _pendingUploadCache,
                                        DiskDataSource _videoDao,
                                        Provider<UploadNotificationConfig> _config,
                                        UploadPayPalCodeService _service) {
        mUploadRequestProvider = _requestProvider;
        mVideoCache = _pendingUploadCache;
        mDiskDataSource = _videoDao;
        mConfig = _config;
        mGson = _gson;
        mService = _service;
    }

    public UploadVideoDataSource createCloudDataSource() {
        if (mDataSource == null)
            mDataSource = new CloudUploadVideoSourceImpl(mGson, mUploadRequestProvider, mVideoCache,
                    mDiskDataSource, mConfig, mService);

        return mDataSource;
    }
}
