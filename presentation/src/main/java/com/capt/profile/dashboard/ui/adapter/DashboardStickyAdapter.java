package com.capt.profile.dashboard.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capt.R;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.capt.profile.dashboard.ui.listener.OnDelegateItemClickListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 25.03.2016.
 */
@SuppressWarnings("unchecked")
@PerFragment
public class DashboardStickyAdapter extends SectioningAdapter {

    private DashboardDelegateManager mDelegateManager;
    private List<Section> mSections;

    @Inject
    public DashboardStickyAdapter(DashboardDelegateManager _manager) {
        mSections = new ArrayList<>();
        mDelegateManager = _manager;
    }

    public void setOnDelegateItemClickListener(OnDelegateItemClickListener _listener) {
        mDelegateManager.setOnDelegateItemClickListener(_listener);
    }

    public void setItems(List<Section> _items) {
        mSections = _items;
        notifyAllSectionsDataSetChanged();
    }

    @Override
    protected DashboardDelegateManager getDelegateManager() {
        return mDelegateManager;
    }

    @Override
    protected List<DisplayableItem> getItems(int sectionIndex) {
        return mSections.get(sectionIndex).getItems();
    }

    @Override
    public int getNumberOfSections() {
        return mSections.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return mSections.get(sectionIndex).size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return mSections.get(sectionIndex).hasHeader();
    }

    @Override
    public void onBindDelegateViewHolder(ViewHolder holder, int section, int positionInSection) {
        mDelegateManager.onBindViewHolder(mSections.get(section).getItems(), positionInSection, holder);
    }

    @Override
    public ViewHolder onCreateDelegateViewHolder(ViewGroup parent, int viewType) {
        return (ViewHolder) mDelegateManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_sticky_header, parent, false);
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(ViewHolder viewHolder, int sectionIndex) {
        HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
        Section section = mSections.get(sectionIndex);
        holder.tvTitle.setText(section.getHeaderTitle());
        if (section.hasButton()) {
            holder.tvOption.setVisibility(View.VISIBLE);
            holder.tvOption.setText(section.getHeaderDescription());
        } else {
            holder.tvOption.setVisibility(View.GONE);
        }
    }

    class HeaderViewHolder extends ViewHolder {

        @Bind(R.id.tvLeft_LISH)
        TextView tvTitle;

        @Bind(R.id.tvRightLISH)
        TextView tvOption;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
