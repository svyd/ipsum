package com.capt.domain.video.draft;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 25.04.2016.
 */
public class GetDraftInteractor extends BasePostInteractor<String>{

    private DraftRepository mDataSource;

    @Inject
    public GetDraftInteractor(DraftRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mDataSource = _repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mDataSource.get(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
