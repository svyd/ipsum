package com.capt.profile.assignments.details.overview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.capt.R;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.profile.assignments.details.dagger.DetailsActivityComponent;
import com.capt.profile.assignments.details.overview.dagger.OverviewComponent;
import com.capt.profile.assignments.details.overview.dagger.OverviewModule;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by root on 25.04.16.
 */
public class OverviewFragment extends BaseToolbarFragment implements OverviewContract.View {

    @Inject
    OverviewContract.Presenter mPresenter;

    @Bind(R.id.tv_description_FAO)
    TextView tvDescription;
    @Bind(R.id.tv_who_AH)
    TextView tvWho;
    @Bind(R.id.tv_when_AH)
    TextView tvWhen;
    @Bind(R.id.tv_where_AH)
    TextView tvWhere;
    @Bind(R.id.tv_what_AH)
    TextView tvWhat;
    @Bind(R.id.tv_how_much_AH)
    TextView tvHowMuch;
    @Bind(R.id.tv_min_quality_AH)
    TextView tvMinQuality;

    private OverviewComponent mComponent;

    public static OverviewFragment newInstance(Assignment _model) {

        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, _model);
        OverviewFragment fragment = new OverviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_assignment_overview);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponent();
        inject();
        showToolbar();
        checkExtra();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(DetailsActivityComponent.class)
                .createOverviewComponent(new OverviewModule(this));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void showToolbar() {
        getToolbarManager().showToolbar();
        getActivity().setTitle(getString(R.string.overview_title));
    }

    private void checkExtra() {
        if (getArguments().containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            Assignment model = (Assignment) getArguments().getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
            mPresenter.setAssignmentModel(model);
        }
    }

    @Override
    public void setUpView(Assignment _model) {
        tvDescription.setText(_model.description);
        tvWho.setText(_model.who);
        tvWhat.setText(_model.title);
        tvWhen.setText(_model.when);
        tvWhere.setText(_model.where);
        tvHowMuch.setText(_model.price);
        tvMinQuality.setText(_model.formattedQuality);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    protected void setUpActionBar(ActionBar actionBar) {
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_next:
                mPresenter.navigateToAssignments();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void navigateToAssignmentsScreen() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
//        getFragmentNavigator().replaceFragmentWithoutBackStack(AssignmentsTabFragment.newInstance(Constants.AssignmentTypes.IN_PROGRESS));
//        Intent intent = new Intent(getActivity(), ProfileActivity.class);
//        intent.putExtra(Constants.Extra.EXTRA_STRING, Constants.Profile.ASSIGNMENTS);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        getActivity().finishWithResult();
    }

    @OnClick(R.id.btn_add_to_calendar_FAO)
    @Override
    public void onCalendarClick() {
        mPresenter.addToCalendar();
    }

    @Override
    public void sendCalendarIntent(Intent intent) {
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
