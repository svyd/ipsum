package com.capt.data.payout;

import android.annotation.SuppressLint;

import com.capt.data.base.TypeMapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by rMozes on 8/3/16.
 */
@Singleton
public class DateConverter implements TypeMapper<String, String> {

    private SimpleDateFormat mServerFormat;
    private SimpleDateFormat mLocalFormatter;

    @SuppressLint("SimpleDateFormat")
    @Inject
    public DateConverter() {
        mServerFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        mLocalFormatter = new SimpleDateFormat("dd.MM.yyyy.");
    }

    @Override
    public String convert(String _value) {
        try {
            return mLocalFormatter.format(mServerFormat.parseObject(_value));
        } catch (ParseException e) {
            throw  new RuntimeException("Server date format is not correct");
        }
    }
}
