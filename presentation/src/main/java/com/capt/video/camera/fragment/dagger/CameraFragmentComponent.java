package com.capt.video.camera.fragment.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.camera.fragment.CaptCameraFragment;

import dagger.Subcomponent;

/**
 * Created by richi on 2016.03.28..
 */
@PerFragment
@Subcomponent(modules = CameraFragmentModule.class)
public interface CameraFragmentComponent {
    void inject(CaptCameraFragment _fragment);
}
