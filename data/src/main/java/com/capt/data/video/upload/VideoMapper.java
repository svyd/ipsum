package com.capt.data.video.upload;

import com.capt.data.base.TimeUtil;
import com.capt.data.base.TypeMapper;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.model.VideoUploadModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 26.04.2016.
 */
public class VideoMapper implements TypeMapper<List<Video>, List<VideoUploadModel>> {

    TimeUtil mTimeUtil;

    @Inject
    public VideoMapper(TimeUtil _timeUtil) {
        mTimeUtil = _timeUtil;
    }

    @Override
    public List<Video> convert(List<VideoUploadModel> _value) {
        List<Video> videos = new ArrayList<>();
        for (VideoUploadModel model: _value) {
            Video video = new Video();
            video.title = model.getTitle();
            video.description = model.getDescription();
            video.duration = mTimeUtil.getTimeFromIntegerMinutes(model.getDuration());
            video.thumbnail = model.getFilePath();
            video.price = String.valueOf(model.getPrice());
            video.size = model.getFormattedSize();
            if (model.isDraft()) {
                videos.add(video);
            }
        }
        return videos;
    }
}
