package com.capt.domain.authorization.interactor;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 03.03.2016.
 */
public class CheckCodeAndSignInInteractor extends BasePostInteractor<CodeSignUpModel> {

    private AuthRepository mRepository;

    @Inject
    public CheckCodeAndSignInInteractor(AuthRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(CodeSignUpModel _data) {
        if (_data.getSignUpModel() != null)
            return mRepository.verifyCodeAndSignUp(_data);
        else
            return mRepository.verifyCodeAndGetProfile(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
