package com.capt.profile.dashboard.ui.listener;

/**
 * Created by Svyd on 28.03.2016.
 */
public interface OnDelegateItemClickListener {
    void onDelegateClick(int delegateType, int position);
}
