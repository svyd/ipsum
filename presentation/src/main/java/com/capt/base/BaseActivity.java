package com.capt.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;

import com.capt.application.CaptApplication;
import com.capt.application.dagger.AppComponent;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.splash.SplashActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by richi on 2016.02.22..
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    @Inject
    FragmentNavigator mFragmentNavigator;
    @Inject
    KeyboardUtil mKeyboardUtil;
    @Inject
    ToolbarManager mToolbarManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        printHashKey();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        if (getToolbarId() == 0)
            return;

        Toolbar toolbar = (Toolbar) findViewById(getToolbarId());
        if (toolbar != null)
            setSupportActionBar(toolbar);
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.capt",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("HASH KEY:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

    protected AppComponent getApplicationComponent() {
        return CaptApplication.getApplication().getAppComponent();
    }

    public FragmentNavigator getFragmentNavigator() {
        return mFragmentNavigator;
    }

    public KeyboardUtil getKeyboardUtil() {
        return mKeyboardUtil;
    }

    public ToolbarManager getToolbarManager() {
        return mToolbarManager;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showNoInternetConnectionError() {

    }

    @Override
    public void hideNoInternetConnectionError() {

    }

    @Override
    public void showError(String _error) {

    }

    @Override
    public void navigateToJoinScreen() {
        CaptApplication.getApplication().getAppComponent()
                .sharedPreferences()
                .edit()
                .clear()
                .apply();

        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    //AbstractMethods
    public abstract
    @IdRes
    int getContainerId();

    public abstract
    @IdRes
    int getToolbarId();
}
