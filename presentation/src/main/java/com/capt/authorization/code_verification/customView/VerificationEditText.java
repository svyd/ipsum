package com.capt.authorization.code_verification.customView;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import java.util.ArrayList;

/**
 * Created by Svyd on 16.03.2016.
 */
public class VerificationEditText extends LinearLayout {

    /**
     * Change the following field for desired cell count
     *
     * Could be done with custom attributes, sorry
     */
    private final int CELL_COUNT = 4;

    private static final String TAG = VerificationEditText.class.getSimpleName();
    private ArrayList<CountableEditText> mEntities = new ArrayList<>();
    private int mCurrentField;
    private boolean isFilled;
    private FilledListener mListener;

    public VerificationEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VerificationEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public VerificationEditText(Context context) {
        this(context, null);
    }

    /**
     *
     * @return true if every cell is filled
     */

    public boolean isFilled() {
        return isFilled;
    }

    /**
     * sets the @FilledListener to listen events for filling all the cells
     * @param _listener instance of @FilledListener
     */

    public void setFilledListener(FilledListener _listener) {
        mListener = _listener;
    }

    /**
     * Method to get input
     *
     * @return @String with concatenated cells
     */

    public String getText() {
        StringBuilder builder = new StringBuilder();
        if (isFilled) {
            for (EditText text : mEntities) {
                builder.append(text.getText().toString());
            }
            return builder.toString();
        } else {
            return null;
        }
    }

    private void init(Context context) {
        setOrientation(HORIZONTAL);

        TableLayout.LayoutParams params =
                new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f);

        //creating separate EditText instances

        for (int i = 0; i < CELL_COUNT; i++) {
            mEntities.add(new CountableEditText(context, i));
        }

        //initializing separate EditText instances

        for (int i = 0; i < CELL_COUNT; i++) {
            EditText e = mEntities.get(i);
            e.setLayoutParams(params);
            e.setTextColor(Color.rgb(0, 0, 0));
            e.setGravity(Gravity.CENTER);
            e.setOnFocusChangeListener(mFocusListener);
            e.addTextChangedListener(mTextWatcher);
            e.setInputType(InputType.TYPE_CLASS_NUMBER);
            e.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});

            addView(e);
        }
    }

    private OnFocusChangeListener mFocusListener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (v instanceof CountableEditText) {
                mCurrentField = ((CountableEditText) v).getPosition();
            }
        }
    };

    private void log() {
        Log.d(TAG, isFilled + "");
    }

    private void validateFilling() {
        for (EditText text : mEntities) {
            if (text.length() == 0) {
                isFilled = false;
                if (mListener != null) {
                    mListener.onEmpty();
                }
                log();
                return;
            }
        }
        isFilled = true;
        if (mListener != null) {
            mListener.onFilled();
        }
        log();
    }

    private void moveForward() {
        if (mCurrentField + 1 < mEntities.size()) {
            mEntities.get(mCurrentField + 1).requestFocus();
        }
    }

    private void moveBack() {
        if (mCurrentField > 0) {
            mEntities.get(mCurrentField - 1).requestFocus();
        }
    }

    private TextWatcher mTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //stub
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() != 0) {
                moveForward();
            } else {
                moveBack();
            }
            validateFilling();
        }

        @Override
        public void afterTextChanged(Editable s) {
            //stub
        }
    };

    /**
     * This could be done with the #View.setTag()
     */

    private class CountableEditText extends EditText {

        private int mPosition;

        public CountableEditText(Context context, int _position) {
            super(context);
            mPosition = _position;
        }

        public int getPosition() {
            return mPosition;
        }
    }

    public interface FilledListener {
        /**
         * Fires in case of every field is filled
         */
        void onFilled();

        /**
         * Fires in case if not every field is filled
         */
        void onEmpty();
    }
}

