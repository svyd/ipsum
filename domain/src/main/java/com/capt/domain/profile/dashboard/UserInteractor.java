package com.capt.domain.profile.dashboard;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 4/23/16.
 */
public class UserInteractor extends BaseInteractor {

    private UserRepository mRepository;

    @Inject
    public UserInteractor(UserRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildGetObserver() {
        return mRepository.getUser();
    }
}
