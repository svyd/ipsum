package com.capt.profile.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.capt.R;
import com.capt.application.dagger.HasComponent;
import com.capt.base.BaseActivity;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.profile.settings.currency.CurrencySettingsFragment;
import com.capt.profile.settings.dagger.AccountComponent;
import com.capt.profile.settings.dagger.AccountModule;
import com.capt.profile.settings.notification.NotificationSettingsFragment;

import javax.inject.Inject;

/**
 * Created by Svyd on 30.06.2016.
 */
@SuppressWarnings("FieldCanBeLocal")
public class AccountSettingsActivity extends BaseActivity
        implements HasComponent<AccountComponent>, AccountContract.View {

    public static final String CODE = "code";

    public static final int CODE_NOTIFICATIONS = 100;
    public static final int CODE_CURRENCY = 101;

    private AccountComponent mComponent;

    @Inject
    AccountContract.Presenter mPresenter;

    public static void startNotificationSettings(Activity _activity, SettingsModel _model) {
        Intent intent = new Intent(_activity, AccountSettingsActivity.class);
        intent.putExtra(CODE, CODE_NOTIFICATIONS);
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _model);

        _activity.startActivityForResult(intent, CODE_NOTIFICATIONS);
    }

    public static void startCurrencySettings(Activity _activity, SettingsModel _model) {
        Intent intent = new Intent(_activity, AccountSettingsActivity.class);
        intent.putExtra(CODE, CODE_CURRENCY);
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _model);

        _activity.startActivityForResult(intent, CODE_CURRENCY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        initComponent();
        inject();
        mPresenter.setArgs(getIntent().getExtras());
        mPresenter.initialize();
    }

    private void initComponent() {
        mComponent = getApplicationComponent().createAccountComponent(new AccountModule(this));
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public int getContainerId() {
        return R.id.flContainer_AAS;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AAS;
    }

    @Override
    public AccountComponent getComponent() {
        return mComponent;
    }

    @Override
    public void navigateToCurrency(SettingsModel _model) {
        getFragmentNavigator().addFragmentWithoutBackStack(CurrencySettingsFragment.newInstance(_model));
    }

    @Override
    public void navigateToNotifications(SettingsModel _model) {
        getFragmentNavigator().addFragmentWithoutBackStack(NotificationSettingsFragment.newInstance(_model));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showNoInternetConnectionError() {

    }

    @Override
    public void hideNoInternetConnectionError() {

    }

    @Override
    public void showError(String _error) {

    }
}
