package com.capt.authorization.forgot_password.dagger;

import com.capt.authorization.forgot_password.ForgotPasswordContract;
import com.capt.authorization.forgot_password.ForgotPasswordPresenter;
import com.capt.data.annotation.PerFragment;
import com.capt.data.authorization.forgot_password.ForgotPasswordDataStoreImpl;
import com.capt.data.authorization.net.retrofit.ForgotPasswordService;
import com.capt.domain.authorization.forgot_password.ForgotPasswordDataStore;
import com.capt.domain.authorization.forgot_password.ForgotPasswordInteractor;
import com.capt.domain.authorization.forgot_password.model.ForgotPasswordModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Svyd on 22.03.2016.
 */
@Module
public class ForgotPasswordModule {

    private ForgotPasswordContract.ForgotPasswordView mView;

    public ForgotPasswordModule(ForgotPasswordContract.ForgotPasswordView _view) {
        mView = _view;
    }

    @Provides
    @PerFragment
    protected ForgotPasswordContract.ForgotPasswordView provideForgotPasswordView() {
        return mView;
    }

    @Provides
    @PerFragment
    protected ForgotPasswordContract.ForgotPasswordPresenter provideForgotPasswordPresenter(ForgotPasswordPresenter _presenter) {
        return _presenter;
    }

    @Provides @PerFragment
    protected ForgotPasswordService provideForgotPasswordService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                                 Retrofit _retrofit) {
        return _retrofit.create(ForgotPasswordService.class);
    }

    @Provides
    @PerFragment
    protected ForgotPasswordDataStore provideDataStore(ForgotPasswordDataStoreImpl _store) {
        return _store;
    }

    @Provides @PerFragment
    @Named(Constants.NamedAnnotation.FORGOT_PASSWORD_INTERACTOR)
    protected BasePostInteractor<ForgotPasswordModel> provideForgotPasswordInteractor(ForgotPasswordInteractor _interactor) {
        return _interactor;
    }
}
