package com.capt.profile.dashboard.ui.delegate;

import android.view.LayoutInflater;

import com.capt.domain.profile.dashboard.model.Tutorial;
import com.capt.profile.dashboard.ui.adapter.NonRecycleAdapter;
import com.capt.profile.dashboard.ui.adapter.ViewTypeConstants;
import com.capt.profile.dashboard.ui.delegate.base.BaseNonRecycleDelegate;
import com.capt.profile.dashboard.ui.view.NonRecycleList;

import javax.inject.Inject;

/**
 * Created by Svyd on 25.03.2016.
 */
public class TutorialNonRecycleDelegate extends BaseNonRecycleDelegate<Tutorial> {

    @Inject
    public TutorialNonRecycleDelegate(LayoutInflater _layout,
                                      NonRecycleAdapter _adapter) {
        super(_layout, _adapter, ViewTypeConstants.VIEW_TYPE_TUTORIAL, Tutorial.class);
    }

    @Override
    public void onClick(int position, NonRecycleList _list) {
        mListener.onDelegateClick(ViewTypeConstants.VIEW_TYPE_TUTORIAL, position);
    }
}
