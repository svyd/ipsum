package com.capt.video.category.flow;

import android.content.Intent;
import android.os.Bundle;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.category.model.CategoryModel;
import com.capt.domain.video.model.Video;
import com.capt.video.category.CategoryContract;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 02.05.2016.
 */
public abstract class AbstractCategoryPresenter extends CategoryContract.Presenter {

    private CategoryContract.View mView;
    private BaseInteractor mInteractor;
    private List<CategoryModel> mCategories;
    private Video mData;

    public AbstractCategoryPresenter(@Named(Constants.NamedAnnotation.CATEGORY_INTERACTOR)
                                     BaseInteractor _interactor, CategoryContract.View _view,
                                     BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
    }

    @Override
    public void initialize() {
        mInteractor.execute(new CategoryObserver(this));
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    protected abstract void doNext();

    private List<CategoryModel> filterCategories(String[] _tags) {
        List<CategoryModel> categories = new ArrayList<>();

        for (String categoryName : _tags) {
            for (CategoryModel model : mCategories) {
                if (model.name.equals(categoryName)) {
                    categories.add(model);
                }
            }
        }
        return categories;
    }

    protected Video getData() {
        return mData;
    }

    protected CategoryContract.View getView() {
        return mView;
    }

    private boolean validateInput() {
        return mView.getSelectedList().size() > 0;
    }

    @Override
    public void onNextClick() {
        if (validateInput()) {
            Set<Integer> selection = mView.getSelectedList();
            String[] tags = new String[selection.size()];

            int i = 0;
            for (Integer tag : selection) {
                tags[i] = mCategories.get(tag).name;
                i++;
            }
            mData.tags = tags;
            mData.categories = filterCategories(tags);


            doNext();
        } else {
            mView.showToast("You must select at least one category");
        }
    }

    @Override
    public void setData(Bundle _args) {
        if (_args != null) {
            if (_args.containsKey(Constants.BUNDLE_VIDEO)) {
                mData = (Video) _args.getSerializable(Constants.BUNDLE_VIDEO);
            } else {
                throw new IllegalStateException("Arguments should contain VideoUploadModel");
            }
        }

    }

    private void selectCategories(List<CategoryModel> categories) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < categories.size(); i++) {
            for (int j = 0; j < mCategories.size(); j++) {
                if (mCategories.get(j).id.equals(categories.get(i).id)) {
                    integers.add(j);
                    break;
                }
            }
        }
        int[] positions = new int[integers.size()];
        for (Integer integer: integers) {
            positions[integers.indexOf(integer)] = integer;
        }
        mView.selectCategories(positions);
    }

    private void setCategoriesToView(List<CategoryModel> _data) {
        String[] categories = new String[_data.size()];

        for (int i = 0; i < categories.length; i++) {
            categories[i] = _data.get(i).name;
        }
        mView.setData(categories);
    }

    class CategoryObserver extends BaseObserver<List<CategoryModel>> {

        public CategoryObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(List<CategoryModel> categoryModels) {
            mCategories = categoryModels;
            setCategoriesToView(categoryModels);
            if (mData.categories != null && !mData.categories.isEmpty()) {
                selectCategories(mData.categories);
            }
        }
    }


}
