package com.capt.profile.settings.notification;

import com.capt.R;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.profile.settings.model.UpdateSettingsModel;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by root on 29.04.16.
 */
public class NotificationSettingsPresenter extends NotificationSettingsContract.Presenter {

    private NotificationSettingsContract.View mView;
    private BasePostInteractor<UpdateSettingsModel> mPostInteractor;
    private UpdateSettingsModel mUpdateSettingsModel = new UpdateSettingsModel();
    private SettingsModel mSettings;

    @Inject
    public NotificationSettingsPresenter(NotificationSettingsContract.View _view,
                                         @Named(Constants.NamedAnnotation.SETTINGS_INTERACTOR)
                                         BasePostInteractor<UpdateSettingsModel> _postInteractor,
                                         BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mPostInteractor = _postInteractor;
    }

    @Override
    public void onStop() {
        mPostInteractor.unSubscribe();
    }

    @Override
    public void setSettingsModel(SettingsModel settings) {
        mSettings = settings;
        mUpdateSettingsModel.notifications = settings.notifications;
        mView.setSettingsOnView(settings);
    }

    @Override
    public void onSwitchClick(int switchId) {
        switch (switchId) {

            case R.id.sw_marketplace_FNS:
                mUpdateSettingsModel.notifications.videoIsSold = !mUpdateSettingsModel.notifications.videoIsSold;
                break;

            case R.id.sw_assignment_sold_FNS:
                mUpdateSettingsModel.notifications.assignmentIsSold = !mUpdateSettingsModel.notifications.assignmentIsSold;
                break;

            case R.id.sw_assignment_not_sold_FNS:
                mUpdateSettingsModel.notifications.assignmentIsNotSold = !mUpdateSettingsModel.notifications.assignmentIsNotSold;
                break;

            case R.id.sw_new_assignment_FNS:
                mUpdateSettingsModel.notifications.newAssignment = !mUpdateSettingsModel.notifications.newAssignment;
                break;

            case R.id.sw_level_up_FNS:
                mUpdateSettingsModel.notifications.levelUp = !mUpdateSettingsModel.notifications.levelUp;
                break;
        }
        mSettings.notifications = mUpdateSettingsModel.notifications;
        mView.setSettings(mSettings);
        updateSettings();
    }

    private void updateSettings() {
        mPostInteractor.execute(mUpdateSettingsModel, new UpdateSettingsSubscriber(this));
    }

    private class UpdateSettingsSubscriber extends BaseObserver<SettingsModel> {
        public UpdateSettingsSubscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(SettingsModel settingsModel) {
            mView.hideProgress();
        }
    }
}