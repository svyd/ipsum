package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Svyd on 07.03.2016.
 */
public class UserModel implements Serializable {

    @Expose
    @SerializedName("_id")
    private String userId;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("location")
    private double[] location;

    @Expose
    @SerializedName("fullName")
    private String name;

    @Expose
    @SerializedName("password")
    private String password;

    @Expose
    @SerializedName("phone")
    private PhoneNumberModel phone;

    @Expose
    @SerializedName("avatar")
    private String avatarUrl;

    @Expose
    @SerializedName("uploaded")
    private String uploaded;

    @Expose
    @SerializedName("assignments")
    private String assignments;

    @Expose
    @SerializedName("country")
    private String country;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("isSocial")
    private boolean isSocial;

    @Expose
    private Level level;

    @Expose
    private long birthday;

    @Expose
    private int gender;

    public boolean isSocial() {
        return isSocial;
    }

    public class Level implements Serializable {

        @Expose
        private String name;

        @Expose
        private int number;

        @Expose
        private int progress;

        public String getName() {
            return name;
        }

        public void setName(String intern) {
            this.name = intern;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public int getProgress() {
            return progress;
        }

        public void setProgress(int progress) {
            this.progress = progress;
        }
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PhoneNumberModel getPhone() {
        return phone;
    }

    public void setPhone(PhoneNumberModel phone) {
        this.phone = phone;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }


    public String getUploaded() {
        return uploaded;
    }

    public void setUploaded(String uploaded) {
        this.uploaded = uploaded;
    }

    public String getAssignments() {
        return assignments;
    }

    public void setAssignments(String assignments) {
        this.assignments = assignments;
    }

    public static class Builder {

        private UserModel mUserModel;

        public Builder() {
            mUserModel = new UserModel();
        }

        public Builder setEmail(String _email) {
            mUserModel.setEmail(_email);
            return this;
        }

        public Builder setFullName(String _fullName) {
            mUserModel.setName(_fullName);
            return this;
        }

        public Builder setPassword(String _password) {
            mUserModel.setPassword(_password);
            return this;
        }

        public Builder setAvatar(String _fileName) {
            mUserModel.setAvatarUrl(_fileName);
            return this;
        }

        public Builder setCountry(String _country) {
            mUserModel.setCountry(_country);
            return this;
        }

        public Builder setPhone(PhoneNumberModel _number) {
            mUserModel.setPhone(_number);
            return this;
        }

        public UserModel build() {
            return mUserModel;
        }
    }
}
