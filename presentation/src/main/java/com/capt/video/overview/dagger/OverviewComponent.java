package com.capt.video.overview.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.overview.OverviewFragment;

import dagger.Subcomponent;

/**
 * Created by Svyd on 10.04.2016.
 */
@PerFragment
@Subcomponent(modules = OverviewModule.class)
public interface OverviewComponent {
    void inject(OverviewFragment _fragment);
}
