package com.capt.profile.dashboard;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.authorization.sign_up.CircleTransform;
import com.capt.authorization.sign_up.image_picker.ImagePickerFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.capt.domain.video.model.Video;
import com.capt.profile.HeaderElevationUtil;
import com.capt.profile.ProfileTabListener;
import com.capt.profile.assignments.details.AssignmentDetailsActivity;
import com.capt.profile.dagger.ProfileActivityComponent;
import com.capt.profile.dashboard.ui.adapter.DashboardStickyAdapter;
import com.capt.profile.dashboard.ui.adapter.StickyHeaderLayoutManager;
import com.capt.profile.dashboard.ui.adapter.ViewTypeConstants;
import com.capt.profile.dashboard.ui.listener.OnDelegateItemClickListener;
import com.capt.profile.dashboard.dagger.DashboardComponent;
import com.capt.profile.dashboard.dagger.DashboardModule;
import com.capt.profile.edit_profile.EditProfileActivity;
import com.capt.profile.marketplace.MarketplaceActivity;
import com.capt.profile.payout.activity.PayOutActivity;
import com.capt.video.VideoActivity;
import com.hannesdorfmann.adapterdelegates.AdapterDelegatesManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 22.03.2016.
 */
public class DashboardFragment extends BaseToolbarFragment implements
        DashboardContract.DashboardView,
        SwipeRefreshLayout.OnRefreshListener,
        OnDelegateItemClickListener {

    private static final String TAG = "DashboardFragment";

    private static final int PICTURE_REQUEST_CODE = 198;

    private boolean isItemSelected = false;

    @Bind(R.id.rvContent_FD)
    RecyclerView mRecyclerView;

    @Bind(R.id.tvLevel_PH)
    TextView tvLevel;

    @Bind(R.id.tvLevelName_PH)
    TextView tvLevelName;

    @Bind(R.id.pbLevel_PH)
    ProgressBar pbLevel;

    @Bind(R.id.tvName_PH)
    TextView tvName;

    @Bind(R.id.tvLocation_PH)
    TextView tvLocation;

    @Bind(R.id.srlDashboard_FD)
    SwipeRefreshLayout srlDashboard;

    @Bind(R.id.ivAvatar_PH)
    ImageView ivAvatar;

    @Bind(R.id.progress_FD)
    ProgressBar mProgress;

    @Bind(R.id.pbAvatar_PH)
    ProgressBar pbAvatar;

    @Bind(R.id.llRetry_FD)
    LinearLayout llRetry;

    DashboardComponent mComponent;

    @Inject
    DashboardStickyAdapter mAdapter;

    @Inject
    DashboardContract.DashboardPresenter mPresenter;

    @Inject
    AdapterDelegatesManager mDeleagteManager;

    @Inject
    CircleTransform mTransform;

    @Inject
    RecyclerView.LayoutManager mLayoutManager;

    @Inject
    RecyclerView.ItemDecoration mItemDecoration;

    @Inject
    HeaderElevationUtil mElevationUtil;

    private ValueAnimator mProgressAnimator;

    public static DashboardFragment newInstance(Bundle args) {

        DashboardFragment fragment = new DashboardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        isItemSelected = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_dashboard);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated() called with: " + "savedInstanceState = [" + savedInstanceState + "]");
        initComponent();
        inject();
        initContent();
        getToolbarManager().hideToolbar();
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.setArgs(getArguments());
    }

    private void inject() {
        mComponent.inject(this);
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(ProfileActivityComponent.class)
                .createDashboardComponent(new DashboardModule(this));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    private void initContent() {
        srlDashboard.setOnRefreshListener(this);
//        mAdapter = new DashboardStickyAdapter((DashboardDelegateManager) mDeleagteManager);
        mAdapter.setOnDelegateItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        ((StickyHeaderLayoutManager) mLayoutManager)
                .setHeaderPositionChangedCallback((sectionIndex, header, oldPosition, newPosition)
                        -> mElevationUtil.onHeaderPositionChanged(sectionIndex, header, oldPosition, newPosition));
        // Set layout manager
        if (mRecyclerView.getLayoutManager() == null) {
            mRecyclerView.setLayoutManager(mLayoutManager);
        }
    }

    @Override
    public void setHeader(UserModel _userModel) {
        downloadAvatar(_userModel.getAvatarUrl());
        tvName.setText(_userModel.getName());
        tvLevel.setText(CaptApplication.getApplication().getString(R.string.level, _userModel.getLevel().getNumber()));
        tvLevelName.setText(_userModel.getLevel().getName());
        tvLocation.setText(_userModel.getCountry());
        animateProgressTo(_userModel.getLevel().getProgress());
    }

    private void animateProgressTo(int _to) {
        if (mProgressAnimator != null && mProgressAnimator.isRunning())
            mProgressAnimator.cancel();

        mProgressAnimator = ObjectAnimator.ofInt(pbLevel, "progress", pbLevel.getProgress(), _to * 10);
        mProgressAnimator.setInterpolator(new DecelerateInterpolator());
        mProgressAnimator.setDuration(1000);
        mProgressAnimator.start();
    }

    @Override
    public void onDelegateClick(int delegateType, int _position) {
        int localPosition = getLocalPositionIndex(_position);
        switch (delegateType) {
            case ViewTypeConstants.VIEW_TYPE_ASSIGNMENTS:
                mPresenter.onAssignmentsClick(localPosition);
                break;
            case ViewTypeConstants.VIEW_TYPE_MARKETPLACE:
                mPresenter.onMarketplaceClick(localPosition);
                break;
            case ViewTypeConstants.VIEW_TYPE_VIDEO:
                mPresenter.onVideoClick(localPosition);
                break;
            case ViewTypeConstants.VIEW_TYPE_REMINDER:
                mPresenter.onReminderClick(localPosition);
                break;
        }
    }

    @Override
    public void navigateToImagePicker() {
        ImagePickerFragment.showWithTarget(this, PICTURE_REQUEST_CODE);
    }

    @Override
    public void navigateToAssignment(Assignment _model, String _type) {
        Intent intent = new Intent(getActivity(), AssignmentDetailsActivity.class);
        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, _model);
        args.putString(Constants.Extra.EXTRA_STRING, _type);

        intent.putExtras(args);
        getActivity().startActivity(intent);
    }

    @Override
    public void hideNoInternetConnectionError() {
        llRetry.setVisibility(View.GONE);
    }

    @Override
    public void setContent(List<Section> _sections) {
        mProgress.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mAdapter.setItems(_sections);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == PICTURE_REQUEST_CODE) {
            setUrlToPresenter(data.getStringExtra(Constants.Extra.EXTRA_STRING));
        } else if (resultCode == Activity.RESULT_OK && requestCode == EditProfileActivity.REMINDER_REQUEST_CODE) {
            mPresenter.onEditReminderResult(data);
        }
    }

    private void setUrlToPresenter(String _localPath) {
        if (mPresenter == null)
            return;

        mPresenter.setLocalPath(_localPath);
    }

    @Override
    public void downloadAvatar(String _url) {
        if (isDetached())
            return;

        Glide.with(CaptApplication.getApplication())
                .load(_url)
                .fitCenter()
                .centerCrop()
                .animate(android.R.anim.fade_in)
                .transform(mTransform)
                .error(R.drawable.ic_camera)
                .placeholder(R.drawable.ic_camera)
                .listener(mListener)
                .into(ivAvatar);
    }

    private RequestListener<String, GlideDrawable> mListener = new RequestListener<String, GlideDrawable>() {
        @Override
        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
            hideAvatarProgress();
            return false;
        }

        @Override
        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                       boolean isFromMemoryCache, boolean isFirstResource) {
            hideAvatarProgress();
            return false;
        }
    };

    @Override
    public void showAvatarProgress() {
        pbAvatar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideAvatarProgress() {
        pbAvatar.setVisibility(View.GONE);
    }

    @Override
    public void navigateToMarketPlace(Constants.MarketplaceItems _item) {
        Intent intent = new Intent(getActivity(), MarketplaceActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _item);
        getActivity().startActivityForResult(intent, Constants.RequestCode.SHOULD_UPDATE);
    }

    @Override
    public void navigateToVideoDetails(Video _video) {
        VideoActivity.startOverview(_video, getActivity());
    }

    @Override
    public void navigateToPayouts() {
        //temporary kostyl due to not working dashboard item yet
        isItemSelected = false;
        PayOutActivity.startPayout(getActivity());
    }

    @Override
    public void navigateToPendingAssignments() {
        if (getActivity() instanceof ProfileTabListener) {
            ((ProfileTabListener) getActivity()).checkTab(2);
        } else {
            throw new IllegalStateException("ParentActivity should implement ProfileTabListener");
        }
    }

    @Override
    public int getItemTypeCount() {
        return mAdapter.getNumberOfSections();
    }

    @Override
    public void hideRefresh() {
        srlDashboard.setRefreshing(false);
    }

    @Override
    public void navigateToEditProfile() {
        EditProfileActivity.startEditProfileFlow(getActivity());
    }

    @Override
    public void navigateToLocation() {
        EditProfileActivity.startLocation(getActivity());
    }

    @Override
    public int getLocalPositionIndex(int _index) {
        return mAdapter.getLocalPosition(_index);
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnTryAgain_IR)
    void retry() {
        mPresenter.onRetryClick();
    }

    @Override
    public void showNoInternetConnectionError() {
        llRetry.setVisibility(View.VISIBLE);
    }

    //OnClick
    @OnClick(R.id.ivAvatar_PH)
    protected void onAvatarClick() {
        mPresenter.avatarClicked();
    }

    @Override
    public void onRefresh() {
        mPresenter.onRefresh();
    }
}
