package com.capt.video.currency;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.currency.model.CurrencyModel;

import java.util.List;

/**
 * Created by root on 25.03.16.
 */
public interface CurrencyContract {

	interface View extends BaseView {
		void navigateToSetPriceScreen(CurrencyModel currencyModel);
		void showCurrencies(List<CurrencyModel> currencyModels);
		void showMessage(String _message);
	}

	abstract class Presenter extends BasePresenter<CurrencyContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void onCurrencySelected(CurrencyModel currencyModel);
	}
}
