package com.capt.profile.marketplace;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.capt.R;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.trim.time_util.TimeUtilImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by root on 11.04.16.
 */
@PerFragment
public class VideoAdapter extends RecyclerView.Adapter<VideoHolder> {

    private Context mContext;
    private List<Video> mData = new ArrayList<>();
    protected OnVideoClickListener mListener;
    private TimeUtilImpl mTimeUtil;
    private ErrorOptionsListener mErrorListener;

    public void setListener(OnVideoClickListener _listener) {
        mListener = _listener;
    }

    @Inject
    public VideoAdapter(TimeUtilImpl _timeUtil, Context _context) {
        mContext = _context;
        mTimeUtil = _timeUtil;
    }

    public void remove(int _position) {
        mData.remove(_position);
        notifyItemRemoved(_position);
        notifyItemRangeChanged(_position, mData.size());
    }

    public void add(Video _video, int _position) {
        mData.add(_position, _video);
        notifyItemInserted(_position);
        notifyItemRangeChanged(_position, mData.size());
    }

    public void swap(int firstPosition, int secondPosition){
        Collections.swap(mData, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public void setErrorListener(ErrorOptionsListener _listener) {
        mErrorListener = _listener;
    }

    public void setData(List<Video> _data) {
        mData = _data;
        notifyDataSetChanged();
    }

    public void addModels(List<Video> models) {
        int size = getItemCount();
        mData.addAll(models);
        notifyItemRangeInserted(size, models.size());
    }

    protected void onVideoItemClickListener(Video videoModel) {
        if (mListener != null) {
            mListener.onVideoClick(videoModel);
        }
    }

    public int getVideoPositionById(String _id) {
        for (Video video : mData) {
            if (video.id != null && video.id.equals(_id)) {
                return mData.indexOf(video);
            }
        }
        return -1;
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.list_item_video, parent, false);
        return new VideoHolder(view);
    }

    @Override
    @SuppressLint("SetTextI18n")
    public void onBindViewHolder(VideoHolder holder, int position) {

        View view = holder.getView();
        view.setTag(holder);

        Video video = mData.get(position);

        String thumb;
        if (video.thumbnail == null) {
            thumb = video.path;
        } else {
            thumb = video.thumbnail;
        }
        Glide.with(mContext)
                .load(thumb)
                .fitCenter()
                .centerCrop()
                .into(holder.ivThumbnail);

        if (video.isUploading()) {
            if (video.isError()) {
                holder.onError();
            } else if (video.isCurrentlyUploading()) {
                holder.onProgress(video.progress);
            } else {
                holder.setInQueue();
            }
        } else {
            holder.setRemote();
            holder.tvName.setText(video.getTitle());
            holder.tvDescription.setText(video.getDescription());

            String duration;
            if (video.isDraft()) {
                duration = getDraftProperties(Math.round(Double.parseDouble(video.duration)), video.path);
            } else {
                duration = getFormattedProperties(Math.round(Double.parseDouble(video.duration)), video.size);
            }
            holder.tvDuration.setText(duration);
            holder.tvPrice.setText(video.currency.getSymbol() + video.getPrice());
            holder.cvContainer.setOnClickListener(v -> onVideoItemClickListener(video));
        }
        holder.setErrorOptionsListener(mErrorListener);
    }

    private String getDraftProperties(long _duration, String _path) {
        File file = new File(_path);
        long size = file.length();

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    private String getFormattedProperties(long _duration, String _size) {
        long size = Long.parseLong(_size);

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void replace(int _position, Video _new) {
        mData.remove(_position);
        notifyItemRemoved(_position);
        mData.add(_position, _new);
        notifyItemInserted(_position);
    }
}
