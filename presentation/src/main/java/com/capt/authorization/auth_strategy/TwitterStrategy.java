package com.capt.authorization.auth_strategy;

import android.content.Intent;

import com.capt.base.BaseFragment;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.global.Constants;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

/**
 * Created by richi on 2016.02.24..
 */
public class TwitterStrategy implements AuthorizationStrategy {

    protected TwitterAuthClient mClient;
    protected WeakReference<CaptCallback> mCallbackReference;

    @Inject
    public TwitterStrategy(TwitterAuthClient _client) {
        mClient = _client;
    }

    @Override
    public void logIn(BaseFragment _fragment, CaptCallback _callback) {
        mCallbackReference = new WeakReference<>(_callback);
        mClient.authorize(_fragment.getActivity(), mCallback);
    }

    @Override
    public void onActivityResult(int _resultCode, int _requestCode, Intent _intent) {
        mClient.onActivityResult(_requestCode, _resultCode, _intent);
    }

    private Callback<TwitterSession> mCallback = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {
            if (mCallbackReference.get() == null)
                return;

            SocNetAuthModel.Builder model = new SocNetAuthModel.Builder()
                    .setOathToken(result.data.getAuthToken().token)
                    .setOauthTokenSecret(result.data.getAuthToken().secret)
                    .setUserId(String.valueOf(result.data.getUserId()))
                    .setNetwork(Constants.AuthType.TWITTER);

            mCallbackReference.get().onSuccess(model);
        }

        @Override
        public void failure(TwitterException e) {
            if (mCallbackReference.get() == null)
                return;

//            mCallbackReference.get().onError(e);
        }
    };
}
