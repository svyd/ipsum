package com.capt.video.description.dagger;

import com.capt.authorization.location.LocationInteractorManager;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.annotation.PerFragment;
import com.capt.video.Flow;
import com.capt.video.description.DescriptionContract;
import com.capt.video.description.flow.AbstractDescriptionPresenter;
import com.capt.video.description.flow.DescriptionEditDraftPresenter;
import com.capt.video.description.flow.DescriptionEditPresenter;
import com.capt.data.base.TimeUtil;
import com.capt.video.description.flow.DescriptionUploadPresenter;
import com.capt.video.trim.time_util.TimeUtilImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 03.04.16.
 */
@Module
public class DescriptionModule {

	private DescriptionContract.View mView;
	private int mFlow;

	public DescriptionModule(DescriptionContract.View _view, int _flow) {
		mView = _view;
		mFlow = _flow;
	}

	@Provides @PerFragment
	protected DescriptionContract.View provideView() {
		return mView;
	}

	@Provides @PerFragment
	protected TimeUtil provideTimeUtil(TimeUtilImpl _timeUtil) {
		return _timeUtil;
	}


    @Provides
	@PerFragment
	AbstractDescriptionPresenter providePresenter(LocationInteractorManager _manager,
												  TimeUtilImpl _timeUtil,
												  BaseExceptionDelegate _delegate) {
		switch (mFlow) {
			case Flow.VIDEO_UPLOAD:
				return new DescriptionUploadPresenter(mView, _manager, _timeUtil, _delegate);
			case Flow.ASSIGNMENT_REJECTED:
				return new DescriptionEditPresenter(mView, _manager, _timeUtil, _delegate);
			case Flow.EDIT:
				return new DescriptionEditPresenter(mView, _manager, _timeUtil, _delegate);
			case Flow.DRAFT:
				return new DescriptionEditDraftPresenter(mView, _manager, _timeUtil, _delegate);
			default:
				throw new IllegalArgumentException("Illegal flow: " + mFlow);
		}
	}
}
