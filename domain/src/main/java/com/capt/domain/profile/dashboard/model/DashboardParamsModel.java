package com.capt.domain.profile.dashboard.model;

import com.capt.domain.model.RequestParamsModel;

/**
 * Created by Svyd on 04.04.2016.
 */
public class DashboardParamsModel {

    private RequestParamsModel mAssignmentsParams;
    private RequestParamsModel mVideoParams;

    public DashboardParamsModel(RequestParamsModel _assignments,
                                RequestParamsModel _video) {
        mAssignmentsParams = _assignments;
        mVideoParams = _video;
    }

    public RequestParamsModel getAssignmentsParams() {
        return mAssignmentsParams;
    }

    public RequestParamsModel getVideoParams() {
        return mVideoParams;
    }
}
