package com.capt.domain.profile.update_profile;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 4/29/16.
 */
public class UpdateProfileInteractor extends BasePostInteractor<UserModel> {

    protected AuthRepository mRepository;

    @Inject
    public UpdateProfileInteractor(PostExecutionThread postExecutionThread, AuthRepository _repository) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(UserModel _data) {
        return mRepository.updateUserModel(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
