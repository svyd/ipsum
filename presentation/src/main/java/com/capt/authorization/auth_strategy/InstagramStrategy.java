package com.capt.authorization.auth_strategy;

import android.app.Activity;
import android.content.Intent;

import com.capt.authorization.instagram.InstaAuthModel;
import com.capt.authorization.instagram.InstagramActivity;
import com.capt.base.BaseFragment;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.global.Constants;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

/**
 * Created by Svyd on 24.02.2016.
 */
public class InstagramStrategy implements AuthorizationStrategy {

    private WeakReference<CaptCallback> mCallback;

    @Inject
    public InstagramStrategy() {
    }

    private String getTokenByUri(String _uri) {
        String token = _uri.split("=")[1];
        return token;
    }

    @Override
    public void logIn(BaseFragment _fragment, CaptCallback _callback) {
        mCallback = new WeakReference<>(_callback);

        Intent intent = new Intent(_fragment.getActivity(), InstagramActivity.class);
        InstaAuthModel model = new InstaAuthModel();
        model.setAuthUrl(Constants.Credentials.INSTAGRAM_AUTH_URL);
        model.setClientId(Constants.Credentials.INSTAGRAM_CLIENT_ID);
        model.setRedirectUri(Constants.Credentials.REDIRECT_URI);
        intent.putExtra(Constants.BUNDLE_INSTAGRAM_MODEL, model);
        _fragment.startActivityForResult(intent, Constants.INSTA_AUTH_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int _resultCode, int _requestCode, Intent _intent) {
        if (_requestCode == Constants.INSTA_AUTH_ACTIVITY_REQUEST_CODE &&
                _resultCode == Activity.RESULT_OK) {
            if (mCallback.get() == null) {
                return;
            }
            String redirectedUri = _intent.getExtras().getString(Constants.BUNDLE_REDIRECTED_URI);
            String accessToken = getTokenByUri(redirectedUri);

            SocNetAuthModel.Builder model = new SocNetAuthModel.Builder()
                    .setNetwork(Constants.AuthType.INSTAGRAM)
                    .setAccessToken(accessToken);

            mCallback.get().onSuccess(model);
        }
    }
}
