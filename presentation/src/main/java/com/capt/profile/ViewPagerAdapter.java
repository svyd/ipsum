package com.capt.profile;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.capt.data.annotation.PerFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by root on 15.04.16.
 */
@PerFragment
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
	private final List<Fragment> mFragmentList = new ArrayList<>();
	private final List<String> mTitleList = new ArrayList<>();

	@Inject
	public ViewPagerAdapter(FragmentManager manager) {
		super(manager);
	}

	@Override
	public Fragment getItem(int position) {
		return mFragmentList.get(position);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return mTitleList.get(position);
	}

	@Override
	public int getCount() {
		return mTitleList.size();
	}

	public void addFrag(Fragment fragment, String title) {
		mFragmentList.add(fragment);
		mTitleList.add(title);
	}
}
