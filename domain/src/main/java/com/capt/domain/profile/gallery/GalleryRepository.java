package com.capt.domain.profile.gallery;

import com.capt.domain.profile.gallery.model.GalleryModel;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 4/23/16.
 */
public interface GalleryRepository {
    Observable<List<GalleryModel>> getVideoList();
}
