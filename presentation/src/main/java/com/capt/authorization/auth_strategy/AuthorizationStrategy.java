package com.capt.authorization.auth_strategy;

import android.content.Intent;

import com.capt.base.BaseFragment;
import com.capt.domain.authorization.model.SocNetAuthModel;

/**
 * Created by richi on 2016.02.24..
 */
public interface AuthorizationStrategy {
    void logIn(BaseFragment _fragment, CaptCallback _callback);
    void onActivityResult(int _resultCode, int _requestCode, Intent _intent);

    interface CaptCallback {
        void onSuccess(SocNetAuthModel.Builder _model);
        void onError(Throwable _error);
    }
}
