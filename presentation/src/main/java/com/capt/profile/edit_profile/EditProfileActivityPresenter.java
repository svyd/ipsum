package com.capt.profile.edit_profile;

import android.os.Bundle;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.video.Flow;

import javax.inject.Inject;

/**
 * Created by Svyd on 18.05.2016.
 */
public class EditProfileActivityPresenter extends EditProfileActivityContract.Presenter {

    EditProfileActivityContract.View mView;
    int mFlow;

    @Inject
    public EditProfileActivityPresenter(EditProfileActivityContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
    }

    @Override
    public void setArgs(Bundle _args) {
        if (_args.containsKey(Constants.FLOW)) {
            mFlow = _args.getInt(Constants.FLOW);
        } else {
            throw new IllegalArgumentException(EditProfileActivityPresenter.class.getSimpleName() +
                    " should be provided with a flow");
        }
    }

    @Override
    public void initialize() {
        switch (mFlow) {
            case Flow.UPDATE_PROFILE:
                mView.navigateToEditProfileFragment();
                break;
            case Flow.ADD_LOCATION:
                mView.navigateToLocationFragment();
                break;
            case Flow.CHANGE_PASSWORD:
                mView.navigateToPasswordFragment();
                break;
        }
    }

    @Override
    public void onStop() {

    }
}
