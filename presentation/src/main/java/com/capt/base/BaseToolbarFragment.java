package com.capt.base;

import android.os.Bundle;
import android.support.v7.app.ActionBar;

/**
 * Created by richi on 2016.03.20..
 */
public abstract class BaseToolbarFragment extends BaseFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        BaseActivity activity = (BaseActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        setUpActionBar(actionBar);
    }

    protected void setUpActionBar(ActionBar actionBar) {
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
