package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Svyd on 03.03.2016.
 */
public class SignInModel {

    @Expose
    private String email;

    @Expose
    private String password;

    @Expose
    private String deviceId;

    @Expose
    private String deviceToken;

    @Expose
    private String provider = "GOOGLE";

    public SignInModel(String _email, String _password) {
        email = _email;
        password = _password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public static class Builder {
        private SignInModel mModel;

        public Builder(String _email, String _password) {
            mModel = new SignInModel(_email, _password);
        }

        public Builder setDeviceId(String _id) {
            mModel.setDeviceId(_id);
            return this;
        }

        public Builder setToken(String _token) {
            mModel.setDeviceToken(_token);
            return this;
        }

        public SignInModel build() {
            return mModel;
        }
    }
}
