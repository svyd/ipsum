package com.capt.domain.currency;

import java.util.List;

import rx.Observable;


/**
 * Created by root on 31.03.16.
 */
public interface CurrencyRepository {
	Observable<List<com.capt.domain.currency.model.CurrencyModel>> getCurrencies();
}
