package com.capt.video.dagger;

import com.capt.authorization.dagger.RestModule;
import com.capt.base.dagger.BaseActivityComponent;
import com.capt.data.annotation.PerActivity;
import com.capt.video.VideoActivity;
import com.capt.video.camera.fragment.dagger.CameraFragmentComponent;
import com.capt.video.camera.fragment.dagger.CameraFragmentModule;
import com.capt.video.category.dagger.CategoryComponent;
import com.capt.video.category.dagger.CategoryModule;
import com.capt.video.currency.dagger.CurrencyComponent;
import com.capt.video.currency.dagger.CurrencyModule;
import com.capt.video.description.dagger.DescriptionComponent;
import com.capt.video.description.dagger.DescriptionModule;
import com.capt.video.map.dagger.MapComponent;
import com.capt.video.map.dagger.MapModule;
import com.capt.video.overview.dagger.OverviewComponent;
import com.capt.video.overview.dagger.OverviewModule;
import com.capt.video.price.dagger.SetPriceComponent;
import com.capt.video.price.dagger.SetPriceModule;
import com.capt.video.trim.dagger.FFMpegComponent;
import com.capt.video.trim.dagger.FFMpegModule;
import com.capt.video.trim.dagger.TrimModule;

import dagger.Subcomponent;

/**
 * Created by root on 29.03.16.
 */
@PerActivity
@Subcomponent(modules = {VideoActivityModule.class, RestModule.class})
public interface VideoActivityComponent extends BaseActivityComponent {
    void inject(VideoActivity _activity);

    SetPriceComponent createSetPriceComponent(SetPriceModule _module);
	CurrencyComponent createCurrencyComponent(CurrencyModule _module);
	DescriptionComponent createDescriptionComponent(DescriptionModule _module);
	CameraFragmentComponent createCameraComponent(CameraFragmentModule _module);
	MapComponent createMapComponent(MapModule _module);
	CategoryComponent createCategoryComponent(CategoryModule _module);
	FFMpegComponent createFFMpegComponent(FFMpegModule _module, TrimModule _trim);
	OverviewComponent createOverviewComponent(OverviewModule _module);
}
