package com.capt.domain.video.upload;


import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.DiskDataSource;
import com.capt.domain.video.draft.DraftRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by richi on 2016.03.14..
 */
@Singleton
public class CancelPendingVideoIdInteractor extends BasePostInteractor<String> {

    private PendingUploadCache mPendingCache;
    private DraftRepository mRepository;


    @Inject
    public CancelPendingVideoIdInteractor(DraftRepository _repository,
                                          PostExecutionThread postExecutionThread,
                                          PendingUploadCache _cache) {
        super(postExecutionThread);
        mPendingCache = _cache;
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(final String _data) {
        return mPendingCache.cancelPendingUpload(_data).flatMap(new Func1<String, Observable<?>>() {
            @Override
            public Observable<?> call(String s) {
                return mRepository.deletePending(_data);
            }
        });
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
