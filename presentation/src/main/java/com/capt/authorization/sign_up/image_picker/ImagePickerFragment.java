package com.capt.authorization.sign_up.image_picker;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.application.dagger.HasComponent;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.sign_up.image_picker.dagger.ImagePickerComponent;
import com.capt.authorization.sign_up.image_picker.dagger.ImagePickerModule;
import com.capt.base.BaseActivity;
import com.capt.base.BaseFragment;
import com.capt.domain.global.Constants;

import java.io.File;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by richi on 2016.03.20..
 */
@RuntimePermissions
public class ImagePickerFragment extends BottomSheetDialogFragment
        implements ImagePickerContract.View {

    public static final int REQUEST_GALLERY_INTENT_CALLED           = 124;
    public static final int REQUEST_TAKE_PHOTO                      = 125;

    @Inject ImagePickerContract.Presenter mPresenter;

    private ImagePickerComponent mComponent;

    public static void showWithTarget(BaseFragment _fragment, int _requestCode) {
        DialogFragment fragment = new ImagePickerFragment();
        fragment.setTargetFragment(_fragment, _requestCode);

        fragment.show(_fragment.getFragmentManager(), fragment.getClass().getSimpleName());
    }

    private void startCamera() {
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(mPresenter.getPhotoPath())));
        startActivityForResult(takePhotoIntent, REQUEST_TAKE_PHOTO);
    }

    private void startGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_GALLERY_INTENT_CALLED);
    }
    //region camera permission
    //------------------------------CameraPermission---------------------------------------
    @NeedsPermission(Manifest.permission.CAMERA)
    protected void showCameraPermission() {
        startCamera();
    }

    @OnShowRationale(Manifest.permission.CAMERA)
    protected void showCameraRational(final PermissionRequest request) {
        new AlertDialog.Builder(getActivity(), R.style.CaptAlertDialogStyle)
                .setTitle(R.string.permission)
                .setMessage(R.string.camera_rational)
                .setPositiveButton(R.string.allow, (dialog, which) -> {
                    request.proceed();
                })
                .setNegativeButton(R.string.deny, (dialog, which) -> {
                    request.cancel();
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    protected void showCameraPermissionDenied() {
        Toast.makeText(getActivity(), R.string.camera_deny, Toast.LENGTH_SHORT)
                .show();
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    protected void showCameraPermissionNeverAsk() {
        Toast.makeText(getActivity(), R.string.camera_never_ask_again, Toast.LENGTH_SHORT)
                .show();
    }
    //------------------------------CameraPermission---------------------------------------
    //endregion
    //region storage permission
    //------------------------------Write external storage---------------------------------------
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void showWritePermission() {
        startGallery();
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void showWritePermissionRational(final PermissionRequest request) {
        new AlertDialog.Builder(getActivity(), R.style.CaptAlertDialogStyle)
                .setTitle(R.string.permission)
                .setMessage(R.string.gallery_rational)
                .setPositiveButton(R.string.allow, (dialog, which) -> {
                    request.proceed();
                })
                .setNegativeButton(R.string.deny, (dialog, which) -> {
                    request.cancel();
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void showWritePermissionDenied() {
        Toast.makeText(getActivity(), R.string.gallery_deny, Toast.LENGTH_SHORT)
                .show();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void showWritePermissionNeverAsk() {
        Toast.makeText(getActivity(), R.string.gallery_never_ask_again, Toast.LENGTH_SHORT)
                .show();
    }
    //------------------------------Write external storage---------------------------------------
    //endregion
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ImagePickerFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_pick_image, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = CaptApplication.getApplication().getAppComponent()
                .createImagePickerComponent(new ImagePickerModule(this));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    @Override
    public void startCameraIntent() {
        ImagePickerFragmentPermissionsDispatcher.showCameraPermissionWithCheck(this);
    }

    @Override
    public void startGalleryIntent() {
        ImagePickerFragmentPermissionsDispatcher.showWritePermissionWithCheck(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            mPresenter.onResultCamera();
        } else if (requestCode == REQUEST_GALLERY_INTENT_CALLED && resultCode == Activity.RESULT_OK) {
            mPresenter.onResultGallery(data);
        }
    }

    @Override
    public void setResult(String _path) {
        Intent data = new Intent();
        data.putExtra(Constants.Extra.EXTRA_STRING, _path);

        Fragment fragment = getTargetFragment();
        fragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);

        dismiss();
    }

    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        if (!(getActivity() instanceof HasComponent))
            return null;

        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    //OnClick
    @OnClick(R.id.tvCamera_DFPI)
    protected void onCameraClick() {
        mPresenter.onCameraClick();
    }

    @OnClick(R.id.tvGallery_DFPI)
    protected void onGalleryClick() {
        mPresenter.onGalleryClick();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showNoInternetConnectionError() {

    }

    @Override
    public void hideNoInternetConnectionError() {

    }

    @Override
    public void showError(String _error) {

    }

    @Override
    public void navigateToJoinScreen() {

    }
}
