package com.capt.video.category.flow;

import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.category.CategoryContract;

import javax.inject.Named;

import rx.Observable;
import rx.Observer;

/**
 * Created by Svyd on 10.05.2016.
 */
public class UploadRejectedCategoryPresenter extends AbstractCategoryPresenter {

    private static final String TAG = UploadRejectedCategoryPresenter.class.getSimpleName();
    private BasePostInteractor<Video> mInteractor;

    public UploadRejectedCategoryPresenter(BaseInteractor _interactor,
                                           BasePostInteractor<Video> _rejectedInteractor,
                                           CategoryContract.View _view, BaseExceptionDelegate _delegate) {
        super(_interactor, _view, _delegate);
        mInteractor = _rejectedInteractor;
    }

    @Override
    protected void doNext() {
        mInteractor.execute(getData(), new RejectedObserver(this));
    }

    @Override
    public void onStop() {
        super.onStop();
        mInteractor.unSubscribe();
    }

    private class RejectedObserver extends BaseObserver<Video> {
        public RejectedObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(Video video) {
            getView().hideProgress();
            getView().navigateBack();
            Log.d(TAG, "onNext() called with: " + "video = [" + video + "]");
        }
    }
}
