package com.capt.domain.video.upload;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.draft.DraftRepository;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Svyd on 27.05.2016.
 */
public class CompleteUploadInteractor extends BasePostInteractor<String> {

    private PendingUploadCache mCache;
    private DraftRepository mRepository;

    @Inject
    public CompleteUploadInteractor(DraftRepository _repository, PendingUploadCache _source, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mCache = _source;
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(final String _data) {
        return mCache.completePendingUpload(_data)
                .flatMap(new Func1<String, Observable<?>>() {
                    @Override
                    public Observable<?> call(String s) {
                        return mRepository.deletePending(_data);
                    }
                });
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
