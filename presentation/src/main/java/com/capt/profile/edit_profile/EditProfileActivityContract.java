package com.capt.profile.edit_profile;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.video.model.Video;

/**
 * Created by Svyd on 18.05.2016.
 */
public interface EditProfileActivityContract {

    interface View extends BaseView {
        void navigateToEditProfileFragment();
        void navigateToLocationFragment();
        void navigateToPasswordFragment();
    }

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setArgs(Bundle _args);
    }
}
