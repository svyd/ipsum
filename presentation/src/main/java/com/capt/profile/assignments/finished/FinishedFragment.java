package com.capt.profile.assignments.finished;

import com.capt.domain.global.Constants;
import com.capt.profile.assignments.base.BaseAssignmentsFragment;

/**
 * Created by root on 15.04.16.
 */
public class FinishedFragment extends BaseAssignmentsFragment {

    @Override
    protected String getType() {
        return Constants.AssignmentTypes.FINISHED;
    }

}
