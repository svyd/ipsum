package com.capt.video.camera;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;

/**
 * Created by Svyd on 15.06.2016.
 */
public class RevealUtil {

    public static void makeReveal(View _view, int _x, int _y) {
        ViewTreeObserver viewTreeObserver = _view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    circularRevealActivity(_view, _x, _y);
                    _view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void circularRevealActivity(View _view, int _cx, int _cy) {

        float finalRadius = Math.max(_view.getWidth(), _view.getHeight());

        // create the animator for this view (the start radius is zero)
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(_view, _cx, _cy, 0, finalRadius);
        circularReveal.setDuration(250);

        // make the view visible and start the animation
        _view.setVisibility(View.VISIBLE);

        circularReveal.start();
    }
}
