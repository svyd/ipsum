package com.capt.profile.assignments.base;

import android.app.Activity;
import android.os.Bundle;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.global.AssignmentState;

import java.util.List;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by root on 15.04.16.
 */
public class BaseAssignmentsPresenter extends BaseAssignmentsContract.Presenter {

    protected BaseAssignmentsContract.View mView;
    private BasePostInteractor<RequestParamsModel> mInteractor;
    private BaseInteractor mUserInteractor;
    private List<Assignment> mAssignments;
    protected RequestParamsModel requestModel;
    private String mAssignmentType;
    private boolean isItemSelected = false;

    @Inject
    public BaseAssignmentsPresenter(BaseInteractor _userInteractor,
                                    BaseAssignmentsContract.View _view,
                                    BasePostInteractor<RequestParamsModel> _interactor,
                                    BaseExceptionDelegate _delegate,
                                    String _assignmentType) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
        mAssignmentType = _assignmentType;
        mUserInteractor = _userInteractor;
    }

    @Override
    public void initialize() {
        if (mAssignmentType.equals(Constants.AssignmentTypes.FOR_GRABS)) {
            mView.showProgress();
            mUserInteractor.execute(new UserObserver());
        } else {
            mView.setReminderVisibility(false);
            mView.setListVisibility(true);
            loadInitialData();
        }
    }

    @Override
    public void onStart() {
        isItemSelected = false;
    }

    @Override
    public void onStop() {
        mUserInteractor.unSubscribe();
        mInteractor.unSubscribe();
    }

    @Override
    public void navigateToAssignmentDetails(Assignment assignment) {
        if (!isItemSelected) {
            isItemSelected = true;
            if ((mAssignmentType.equals(Constants.AssignmentTypes.FOR_GRABS)) ||
                    (mAssignmentType.equals(Constants.AssignmentTypes.IN_PROGRESS))) {
                mView.navigateToDetails(assignment, mAssignmentType);
            } else if (mAssignmentType.equals(Constants.AssignmentTypes.FINISHED)) {
                onCompletedClick(assignment);
            }
        }
    }

    private void onCompletedClick(Assignment _model) {
        switch (_model.video.state) {
            case AssignmentState.ERROR:
                mView.navigateToDetailsInReview(_model);
                break;
            case AssignmentState.UPLOADING:
                mView.navigateToDetailsInReview(_model);
                break;
            case AssignmentState.IN_REVIEW:
                mView.navigateToDetailsInReview(_model);
                break;
            case AssignmentState.PURCHASED:
                mView.navigateToPurchased(_model);
                break;
            case AssignmentState.REJECTED:
                mView.navigateToRejected(_model);
                break;
        }
    }

    @Override
    public void getMoreAssignments() {
        mView.showProgress();
        requestModel.offset = mAssignments.size();
        mInteractor.execute(requestModel, new LoadAssignmentsSubscriber(this));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Bundle data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestModel != null) {
                requestModel.offset = 0;
            }
            mView.clearList();
            mView.setReminderVisibility(false);
            mView.setListVisibility(true);
            loadInitialData();
        }
    }

    @Override
    public void onAddLocationClick() {
        mView.navigateToAddLocation();
    }

    @Override
    public void onRetryClick() {
        mView.hideNoInternetConnectionError();
        mView.showProgress();
        initialize();
    }

    @Override
    public void refresh() {
        loadInitialData();
    }

    @Override
    public void onUploadError(String uploadId) {
        if (uploadId.startsWith(Constants.ASSIGNMENT_PREFIX)) {
            mView.updateItem(uploadId.substring(Constants.ASSIGNMENT_PREFIX.length()), AssignmentState.ERROR);
        }
    }

    @Override
    public void onUploadCompleted(String uploadId) {
        if (uploadId.startsWith(Constants.ASSIGNMENT_PREFIX)) {
            mView.removeItem(uploadId.substring(Constants.ASSIGNMENT_PREFIX.length()));
        }
    }

    protected void loadInitialData() {
        mView.showProgress();
        requestModel = new RequestParamsModel(mAssignmentType, 10, 0);
        mInteractor.execute(requestModel, new LoadAssignmentsSubscriber(this));
    }

    private void showAssignmentsOnView(List<Assignment> assignments) {
        if (requestModel.offset > 0) {
            if (assignments.isEmpty()) {
                mView.disableLoadMore();
            } else {
                mAssignments.addAll(assignments);
                mView.addAssignments(assignments);
                mView.hideRefresh();
            }
        } else {
            if (assignments.isEmpty()) {
                mView.showEmptyList();
                mView.hideRefresh();
            } else {
                mView.setListVisibility(true);
                mAssignments = assignments;
                mView.showAssignments(assignments);
                mView.hideRefresh();
            }
        }
    }

    private boolean checkLocation(double[] _location) {
        return !(_location[0] == 0 && _location[1] == 0);
    }

    private class UserObserver implements Observer<UserModel> {

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
            mView.showNoInternetConnectionError();
        }

        @Override
        public void onNext(UserModel userModel) {
            if (checkLocation(userModel.getLocation())) {
                mView.setListVisibility(true);
                mView.setReminderVisibility(false);
                loadInitialData();
            } else {
                mView.setListVisibility(false);
                mView.setReminderVisibility(true);
            }
        }
    }

    private class LoadAssignmentsSubscriber extends BaseObserver<List<Assignment>> {
        public LoadAssignmentsSubscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            mView.hideRefresh();
        }

        @Override
        public void onNext(List<Assignment> assignments) {
            showAssignmentsOnView(assignments);
        }
    }
}
