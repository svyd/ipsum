package com.capt.video.trim.design.view;

import android.animation.Animator;

/**
 * Created by Svyd on 09.04.2016.
 */
public abstract class OnAnimationEndListener implements Animator.AnimatorListener {
    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
