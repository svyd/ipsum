package com.capt.video.overview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.domain.category.model.CategoryModel;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.video.model.Video;
import com.capt.profile.ProfileActivity;
import com.capt.video.Flow;
import com.capt.video.description.DescriptionFragment;
import com.capt.video.trim.view_presenter.TrimFragment;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;

import com.bumptech.glide.Glide;
import com.capt.R;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.profile.dashboard.ui.adapter.NonRecycleAdapter;
import com.capt.profile.dashboard.ui.view.NonRecycleList;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.overview.dagger.OverviewComponent;
import com.capt.video.overview.dagger.OverviewModule;
import com.capt.video.overview.flow.AbstractPresenter;
import com.capt.video.view.ListenableVideoView;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;

import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 10.04.2016.
 */
@SuppressLint("SetTextI18n")
public class OverviewFragment extends BaseToolbarFragment implements
        OverviewContract.View,
        ListenableVideoView.VideoListener {

    private static final String TAG = OverviewFragment.class.getSimpleName();
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 59;

    private static PayPalConfiguration config = new PayPalConfiguration()

            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)

            .clientId(Constants.Credentials.PAYPAL_CONFIG_CLIENT_ID)

            // Minimally, you will need to set three merchant information properties.
            // These should be the same values that you provided to PayPal when you registered your app.
            .merchantName("Capt Store")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));


    private OverviewComponent mComponent;

    @Inject
    AbstractPresenter mPresenter;

    @Inject
    MediaController mController;

    @Inject
    NonRecycleAdapter mAdapter;

    @Bind(R.id.btnFrame_FO)
    Button btnFrame;

    @Bind(R.id.tvDetailsTextBtn_FO)
    TextView tvDetailsTextBtn;

    @Bind(R.id.vv_video_IVH)
    ListenableVideoView vvNewVideo;

    @Bind(R.id.llDescription_FO)
    LinearLayout llDescription;

    @Bind(R.id.iv_play_IVH)
    ImageView ivPlay;

    @Bind(R.id.llButtons_FO)
    LinearLayout llButtons;

    @Bind(R.id.flSectionList_FO)
    LinearLayout llList;

    @Bind(R.id.tv_video_properties_IVH)
    TextView tvVideoProperties;

    @Bind(R.id.et_title_IVH)
    EditText etTitle;

    @Bind(R.id.tv_title_IVH)
    TextView tvTitle;

    @Bind(R.id.rl_location_IVH)
    RelativeLayout rlLocation;

    @Bind(R.id.tvDescription_FO)
    TextView tvDescription;

    @Bind(R.id.ivThumb_IVH)
    ImageView ivThumb;

    @Bind(R.id.tvTags_FO)
    TextView tvTags;

    @Bind(R.id.tv_location_IVH)
    TextView tvLocation;

    @Bind(R.id.tvPrice_FO)
    TextView tvPrice;

    @Bind(R.id.svContainer_FO)
    ScrollView svContainer;

    @Bind(R.id.nrlOverview_FO)
    NonRecycleList nrlOverview;

    @Bind(R.id.llButtonsDetails_FO)
    LinearLayout llDetailButtons;

    @Bind(R.id.tvWarning_FO)
    TextView tvWarning;

    @Bind(R.id.tvTextBtn_FO)
    TextView tvTextBtn;

    @Bind(R.id.btnRed_FVV)
    Button btnRed;

    @Bind(R.id.llCompletedText)
    LinearLayout llCompletedText;

    @Bind(R.id.tvCompletedText_FO)
    TextView tvCompletedText;

    @Bind(R.id.llErrorOptions_IVH)
    LinearLayout llErrorOptions;

    @Bind(R.id.flProgress_IVH)
    FrameLayout flProgress;

    @Bind(R.id.pbProgress_IVH)
    ProgressBar pbProgress;

    public static OverviewFragment newInstance(Bundle _bundle) {

        OverviewFragment fragment = new OverviewFragment();
        fragment.setArguments(_bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(checkForOptions());
        setContentView(R.layout.fragment_overview);

    }

    @Override
    protected void setUpActionBar(ActionBar actionBar) {
        super.setUpActionBar(actionBar);
        actionBar.setHomeAsUpIndicator(0);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        showToolbar();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        startPayPalService();

        mPresenter.setData(getArguments());
        mPresenter.initialize();
    }

    private boolean checkForOptions() {
        if (getArguments().containsKey(Constants.FLOW)) {
            return getArguments().getInt(Constants.FLOW) == Flow.DRAFT;
        } else {
            throw new IllegalStateException(OverviewFragment.class.getSimpleName()
                    + " should be instantiated with specified flow");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_draft_edit, menu);
    }

    private void initComponent() {
        if (mComponent != null) {
            return;
        }
        if (!getArguments().containsKey(Constants.FLOW)) {
            throw new IllegalStateException("OverviewFragment should be instantiated with specified flow");
        }
        int flow = getArguments().getInt(Constants.FLOW);
        mComponent = getComponent(VideoActivityComponent.class)
                .createOverviewComponent(new OverviewModule(this, flow));
    }

    private void inject() {
        mComponent.inject(this);
    }

    private void startPayPalService() {
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);
    }

    private void initList() {
        nrlOverview.setAdapter(mAdapter);
    }

    private void showToolbar() {
        getToolbarManager().showToolbar();
    }

    @Override
    public void setLocation(String _location) {
        tvLocation.setText(_location);
    }

    @Override
    public void playVideo() {
        vvNewVideo.start();
    }

    @Override
    public void setPrice(String _price) {
        tvPrice.setText(_price);
    }

    @Override
    public void setVideoDescription(String _description) {
        tvDescription.setText(_description);
    }

    @Override
    public void navigateToDescription(Bundle _args) {
        getFragmentNavigator().replaceFragmentWithBackStack(DescriptionFragment.newInstance(_args));
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(mReceiver, new IntentFilter("com.capt.uploadservice.broadcast.status"));
    }

    @Override
    public void setCategories(List<CategoryModel> _categories) {
        StringBuilder builder = new StringBuilder();
        for (CategoryModel category : _categories) {
            builder.append(category.name)
                    .append(" ")
                    .append(getString(R.string.category_separator))
                    .append(" ");
        }
        String categories = builder.substring(0, builder.length() - 3);
        tvTags.setText(categories);
    }

    @Override
    public void setResult(Intent _intent, int _result) {
        getActivity().setResult(_result, _intent);
    }

    @Override
    public void navigateToOverview(Bundle _args) {
        getFragmentNavigator().addFragmentWithBackStack(
                OverviewFragment.newInstance(_args));
    }

    @Override
    public void setFrameBtnTitle(String _text) {
        btnFrame.setText(_text);
    }

    @Override
    public void setDetailsTextBtnTitle(String _text) {
        tvDetailsTextBtn.setText(_text);
    }

    @Override
    public void setEditableTitle(boolean _editable) {
        etTitle.setVisibility(_editable ? View.VISIBLE : View.GONE);
        tvTitle.setVisibility(_editable ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setVideoView(String _path) {
        vvNewVideo.setVideoPath(_path);
        vvNewVideo.setVideoListener(this);
        vvNewVideo.setMediaController(mController);
        vvNewVideo.setOnPreparedListener(mp -> initMediaPlayer());
    }

    private void initMediaPlayer() {
        vvNewVideo.start();
        vvNewVideo.pause();
    }

    @Override
    public void setVideoProperties(String _properties) {
        tvVideoProperties.setText(_properties);
    }

    @Override
    public void setVideoTitle(String _title) {
        tvTitle.setText(_title);
    }

    @Override
    public void setVideoPlayability(boolean _visible) {
        if (_visible) {
            vvNewVideo.setVisibility(View.VISIBLE);
            ivPlay.setVisibility(View.VISIBLE);
            ivThumb.setVisibility(View.GONE);
        } else {
            vvNewVideo.setVisibility(View.GONE);
            ivPlay.setVisibility(View.GONE);
            ivThumb.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setButtonsVisibility(boolean _visible) {
        llButtons.setVisibility(_visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setListVisibility(boolean _visible) {
        llList.setVisibility(_visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setWarningVisibility(boolean _visible) {
        tvWarning.setVisibility(_visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setTextBtnTitle(String _title) {
        tvTextBtn.setText(_title);
    }

    @Override
    public void setListData(List<Item> _items) {
        mAdapter.setData(_items);
        initList();
    }

    @Override
    public void setThumbnail(String _url) {
        Glide.with(getActivity())
                .load(_url)
                .asBitmap()
                .centerCrop()
                .into(ivThumb);
    }

    @Override
    public void setTitle(String _title) {
        getActivity().setTitle(_title);
    }

    @Override
    public void onVideoPlay() {
        ivPlay.setVisibility(View.GONE);
    }

    @Override
    public void onVideoPause() {
        ivPlay.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.iv_play_IVH)
    void onPlayClick() {
        mPresenter.onPlayClick();
        mController.hide();
    }

    @Override
    public void onFuturePayment() {
        Intent intent = new Intent(getActivity(), PayPalFuturePaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PayPalProfileSharingActivity.EXTRA_REQUESTED_SCOPES, getOauthScopes());
        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
    }

    private PayPalOAuthScopes getOauthScopes() {
        Set<String> scopes = new HashSet<>(
                Collections.singletonList(PayPalOAuthScopes.PAYPAL_SCOPE_EMAIL));
        return new PayPalOAuthScopes(scopes);
    }

    @Override
    public void navigateToDashboard() {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.putExtra(Constants.FLAG_TO_RECENT, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void setDetailsBtnVisibility(boolean visibility) {
        llDetailButtons.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    @Override
    public void navigateBack() {
        Intent intent = new Intent();
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, Constants.MarketplaceItems.Recent);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().onBackPressed();
    }

    @Override
    public void navigateBack(Intent _intent) {
        getActivity().setResult(Activity.RESULT_OK, _intent);
        getActivity().onBackPressed();
    }

    @Override
    public void navigateToDraftEdit(Bundle _args) {
        getFragmentNavigator().replaceFragmentWithBackStack(TrimFragment.newInstance(_args));
    }

    @Override
    public void showLocation(boolean _show) {
        rlLocation.setVisibility(_show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setRedBtnText(String _text) {
        btnRed.setText(_text);
    }

    @Override
    public void showDescription(boolean _show) {
        llDescription.setVisibility(_show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setCompletedText(String _text) {
        tvCompletedText.setText(_text);
    }

    @Override
    public void setCompletedVisibility(boolean _visibility) {
        llCompletedText.setVisibility(_visibility ? View.VISIBLE : View.GONE);
    }

    @Override
    public void finishWithResult(boolean _toDrafts) {
        Intent intent;
        if (_toDrafts) {
            intent = getActivity().getIntent();
        } else {
            intent = new Intent();
            intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, Constants.MarketplaceItems.Recent);
        }
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void showErrorOptions() {
        llErrorOptions.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorOptions() {
        llErrorOptions.setVisibility(View.GONE);
    }

    @Override
    public void showVideoProgress() {
        flProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideVideoProgress() {
        flProgress.setVisibility(View.GONE);
    }

    @Override
    public void setVideoProgress(int _progress) {
        pbProgress.setProgress(_progress);
        tvVideoProperties.setText(_progress + "%");
    }

    @Override
    public void showDenial() {
        Toast.makeText(getActivity(), "Please wait until the last assignment is uploaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.action_edit:
                mPresenter.onActionEdit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btnFrame_FO)
    void onFrameBtnClick() {
        mPresenter.onFrameBtnClick();
    }

    @OnClick(R.id.tvDetailsTextBtn_FO)
    void onDetailsTextBtnClick() {
        mPresenter.onDetailsTextBtnClick();
    }

    @OnClick(R.id.btnRed_FVV)
    void onUploadClick() {
        mPresenter.onRedBtnClick();
    }

    @OnClick(R.id.tvTextBtn_FO)
    void onTextBtnClick() {
        mPresenter.onTextBtnClick();
    }

    @Override
    public void onDestroyView() {
        getActivity().stopService(new Intent(getActivity(), PayPalService.class));
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePayment", auth.toJSONObject().toString(4));
                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePayment", authorization_code);

                        mPresenter.sendAuthorizationToServer(auth);

                    } catch (JSONException e) {
                        Log.e("FuturePayment", "failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePayment", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePayment",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    @OnClick(R.id.tvRetry_FO)
    void onUploadRetryClick() {
        mPresenter.onUploadRetryClick();
    }

    @OnClick(R.id.tvCancel_FO)
    void onUploadCancelClick() {
        mPresenter.onUploadCancelClick();
    }


    private UploadServiceBroadcastReceiver mReceiver = new UploadServiceBroadcastReceiver() {

        @Override
        public void onError(String uploadId, Exception exception) {
            super.onError(uploadId, exception);
            mPresenter.onUploadError(uploadId);
        }

        @Override
        public void onProgress(String uploadId, int progress) {
            super.onProgress(uploadId, progress);
            mPresenter.onProgress(uploadId, progress);
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, byte[] serverResponseBody) {
            super.onCompleted(uploadId, serverResponseCode, serverResponseBody);
            mPresenter.onUploadCompleted(uploadId);
        }
    };
}
