package com.capt.profile.edit_profile.dagger;

import com.capt.authorization.dagger.RestComponent;
import com.capt.authorization.dagger.RestModule;
import com.capt.base.dagger.BaseActivityComponent;
import com.capt.data.annotation.PerActivity;
import com.capt.profile.edit_profile.EditProfileActivity;
import com.capt.profile.settings.password.dagger.PasswordComponent;
import com.capt.profile.settings.password.dagger.PasswordModule;

import dagger.Subcomponent;

/**
 * Created by Svyd on 4/29/16.
 */
@PerActivity
@Subcomponent(modules = {EditProfileActivityModule.class, RestModule.class})
public interface EditProfileActivityComponent extends BaseActivityComponent, RestComponent {
    void inject(EditProfileActivity _activity);
    EditProfileFragmentComponent createEditProfileComponent(EditProfileFragmentModule _module);
    PasswordComponent createPasswordComponent(PasswordModule _module);
}
