package com.capt.video.trim.design.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.FileDescriptorBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.VideoBitmapDecoder;
import com.bumptech.glide.signature.StringSignature;
import com.capt.R;
import com.capt.data.annotation.PerFragment;

import javax.inject.Inject;

/**
 * Created by Svyd on 06.04.2016.
 */
@PerFragment
public class BunchAdapter extends RecyclerView.Adapter<BunchAdapter.RangeHolder> {

    private final int MINUTE = 60;
    private final int MILLISECOND = 1000;
    private final int MICROSECOND = MILLISECOND * MILLISECOND;

    private float mItemWidth;
    private int mDuration;
    private int mInterval;

    private Uri mData;
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    @Inject
    public BunchAdapter(Context _context) {
        mLayoutInflater = LayoutInflater.from(_context);
        mContext = _context;
    }

    @Override
    public RangeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RangeHolder(mLayoutInflater.inflate(R.layout.list_item_frame, parent, false), mItemWidth);
    }

    @Override
    public void onBindViewHolder(RangeHolder holder, int position) {
        setFrameFor(holder.ivFrame, getTimeFor(position));
    }

    @Override
    public int getItemCount() {
        return getItemCount(mDuration);
    }

    public void setItemWidth(int _width) {
        mItemWidth = _width;
    }

    public void setDuration(int _duration) {
        mDuration = _duration;
    }

    public void setData(Uri _data) {
        if (mItemWidth == 0 || mDuration ==0) {
            throw new IllegalStateException("You have to set itemWidth & duration first!");
        }
        mData = _data;
        mInterval = calcInterval(getItemCount(mDuration), mDuration);
        notifyDataSetChanged();
    }

    private int calcInterval(int _count, int _duration) {
        _duration = _duration / MILLISECOND;
        return _duration / _count;
    }

    private void setFrameFor(ImageView _view, int _microsecond) {
        BitmapPool bitmapPool = Glide.get(mContext).getBitmapPool();
        FileDescriptorBitmapDecoder decoder = new FileDescriptorBitmapDecoder(
                new VideoBitmapDecoder(_microsecond),
                bitmapPool,
                DecodeFormat.PREFER_RGB_565);

        Glide.with(mContext)
                .load(mData)
                .asBitmap()
                .animate(android.R.anim.fade_in)
                .signature(new StringSignature(mData + String.valueOf(_microsecond)))
                .override(250, 250)
                .videoDecoder(decoder)
                .into(_view);
    }

    private int getTimeFor(int _position) {
        return mInterval * MICROSECOND * _position;
    }

    private int getItemCount(int _duration) {
        int minutes = _duration / MINUTE / MILLISECOND;
        int count;
        if (minutes < 5) {
            count = 4;
        } else {
            count = minutes;
        }
        return count;
    }

    public static class RangeHolder extends RecyclerView.ViewHolder {

        ImageView ivFrame;

        public RangeHolder(View itemView, float _width) {
            super(itemView);
            itemView.setLayoutParams(new FrameLayout.LayoutParams((int) (_width), ViewGroup.LayoutParams.WRAP_CONTENT));
            ivFrame = (ImageView) itemView.findViewById(R.id.ivFrame);
        }
    }
}
