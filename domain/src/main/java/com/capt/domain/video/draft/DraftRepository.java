package com.capt.domain.video.draft;

import com.capt.domain.video.model.Video;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 02.05.2016.
 */
public interface DraftRepository {
    Observable<Integer> deleteDraft(String _id);
    Observable<Integer> deletePending(String _id);
    Observable<Long> updateDraft(Video _video);
    Observable<Video> updatePending(Video _video);
    Observable<List<Video>> getAll();
    Observable<Video> get(String _id);
    Observable<Long> add(Video _model);
}
