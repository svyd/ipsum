package com.capt.domain.video.video;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.model.RequestParamsModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public class DownloadVideoInteractor extends BasePostInteractor<RequestParamsModel> {

    private VideoRepository mRepository;

    @Inject
    public DownloadVideoInteractor(VideoRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(RequestParamsModel _data) {
        return mRepository.getVideos(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
