package com.capt.profile.assignments;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;

import javax.inject.Inject;

/**
 * Created by root on 26.04.16.
 */
public class AssignmentsTabPresenter extends AssignmentsTabContract.Presenter {

    private AssignmentsTabContract.View mView;

    @Inject
    public AssignmentsTabPresenter(AssignmentsTabContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        this.mView = _view;
    }

    @Override
    public void setType(String _type) {
        if (_type != null) {
            switch (_type) {
                case Constants.AssignmentTypes.FOR_GRABS:
                    mView.setViewPagerItem(0);
                    break;

                case Constants.AssignmentTypes.IN_PROGRESS:
                    mView.setViewPagerItem(1);
                    break;

                case Constants.AssignmentTypes.FINISHED:
                    mView.setViewPagerItem(2);
                    break;
            }
        }
    }

    @Override
    public void initialize() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

}
