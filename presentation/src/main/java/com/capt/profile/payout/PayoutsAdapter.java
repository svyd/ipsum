package com.capt.profile.payout;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capt.R;
import com.capt.domain.profile.payout.PayoutModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rMozes on 8/3/16.
 */
public class PayoutsAdapter extends BasePayoutsAdapter<PayoutsAdapter.PayOutViewHolder, PayoutModel> {

    private LayoutInflater mInflater;

    private List<PayoutModel> mModel;

    @Inject
    public PayoutsAdapter(LayoutInflater _inflater) {
        mModel = new ArrayList<>();

        mInflater = _inflater;
    }

    @Override
    public PayOutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PayOutViewHolder(mInflater.inflate(R.layout.item_payout, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(PayOutViewHolder holder, int position) {
        PayoutModel model = mModel.get(position);

        holder.tvTitle.setText(model.getTitle());
        holder.tvDescription.setText(model.getDescription());
        holder.tvPrice.setText(model.getPrice());
        holder.tvType.setText(model.getType());
        holder.tvDate.setText(model.getUpdatedAt());

        if (position == mModel.size() - 1)
            holder.vDivider.setVisibility(View.GONE);
        else
            holder.vDivider.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }

    @Override
    public void setData(List<PayoutModel> _data) {
        mModel = _data;
    }

    @Override
    public void addData(List<PayoutModel> _data) {
        mModel.addAll(_data);
    }

    public class PayOutViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvTitle_IP)
        TextView tvTitle;

        @Bind(R.id.tvDescription_IP)
        TextView tvDescription;

        @Bind(R.id.tvPrice_IP)
        TextView tvPrice;

        @Bind(R.id.tvType_IP)
        TextView tvType;

        @Bind(R.id.tvDate_IP)
        TextView tvDate;

        @Bind(R.id.vDivider_IP)
        View vDivider;

        public PayOutViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
