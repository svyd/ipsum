package com.capt.domain.video.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import jdk.Exported;

/**
 * Created by Svyd on 09.08.2016.
 */
public class PayPalModel {

    public PayPalModel(String _code) {
        code = _code;
    }

    @Expose
    @SerializedName("code")
    String code;
}
