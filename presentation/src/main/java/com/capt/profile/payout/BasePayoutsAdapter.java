package com.capt.profile.payout;

import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by rMozes on 8/3/16.
 */
public abstract class BasePayoutsAdapter<T extends RecyclerView.ViewHolder, Model> extends RecyclerView.Adapter<T> {

    public abstract void setData(List<Model> _data);
    public abstract void addData(List<Model> _data);
}
