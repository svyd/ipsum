package com.capt.profile.assignments.details.dagger;

import com.capt.authorization.dagger.RestModule;
import com.capt.base.dagger.BaseActivityComponent;
import com.capt.data.annotation.PerActivity;
import com.capt.profile.assignments.details.capt_dialog.dagger.CaptDialogComponent;
import com.capt.profile.assignments.details.capt_dialog.dagger.CaptDialogModule;
import com.capt.profile.assignments.details.overview.dagger.OverviewComponent;
import com.capt.profile.assignments.details.overview.dagger.OverviewModule;

import dagger.Subcomponent;

/**
 * Created by Svyd on 28.04.2016.
 */
@PerActivity
@Subcomponent(modules = {DetailsActivityModule.class, RestModule.class})
public interface DetailsActivityComponent extends BaseActivityComponent {
    DetailsComponent createAssignmentDetailsComponent(DetailsModule _module);
    CaptDialogComponent createCaptDialogComponent(CaptDialogModule _module);
    OverviewComponent createOverviewComponent(OverviewModule _module);
}
