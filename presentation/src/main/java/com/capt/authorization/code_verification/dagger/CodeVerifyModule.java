package com.capt.authorization.code_verification.dagger;

import com.capt.authorization.code_verification.CodeVerificationContract;
import com.capt.authorization.code_verification.CodeVerifyPresenter;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.authorization.interactor.CheckCodeAndSignInInteractor;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.03.19..
 */
@Module
public class CodeVerifyModule {

    private CodeVerificationContract.View mView;

    public CodeVerifyModule(CodeVerificationContract.View _view) {
        mView = _view;
    }

    @Provides @PerFragment
    protected CodeVerificationContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    protected CodeVerificationContract.Presenter providePresenter(CodeVerifyPresenter _presenter) {
        return _presenter;
    }

    @Provides @PerFragment
    @Named(Constants.NamedAnnotation.CODE_CHECK_INTERACTOR)
    BasePostInteractor<CodeSignUpModel> provideCodeCheckInteractor(CheckCodeAndSignInInteractor _interactor) {
        return _interactor;
    }
}
