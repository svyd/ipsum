package com.capt.profile.marketplace;

import com.capt.domain.video.model.Video;

/**
 * Created by Svyd on 26.05.2016.
 */
public interface MarketplaceTabListener {
    void onAddDraft(Video _draft);
}
