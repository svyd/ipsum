package com.capt.base;

/**
 * Created by richi on 2016.02.23..
 */
public interface BaseView {
    void showProgress();
    void hideProgress();
    void showNoInternetConnectionError();
    void hideNoInternetConnectionError();
    void showError(String _error);
    void navigateToJoinScreen();
}
