package com.capt.video.overview.flow;

import android.os.Bundle;
import android.util.Log;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.base.TimeUtil;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.video.Flow;
import com.capt.video.overview.OverviewContract;

import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 06.05.2016.
 */
public class RejectedPresenter extends AbstractPresenter {

    private static final String TAG = RejectedPresenter.class.getSimpleName();
    private Assignment mData;
    private TimeUtil mTimeUtil;
    private BaseInteractor mUserInteractor;
    BasePostInteractor<String> mDeleteInteractor;
    private UserModel mUser;

    public RejectedPresenter(BasePostInteractor<String> _deleteRejectedInteractor,
                             BaseInteractor _userInteractor,
                             OverviewContract.View _view,
                             TimeUtil _timeUtil, BaseExceptionDelegate _delegate,
                             @Named(Constants.PendingVideoConstants.PAY_PAL_INTERACTOR)
                             BasePostInteractor<PayPalVideoModel> _payPalInteractor) {
        super(_view, _delegate, _payPalInteractor);
        mTimeUtil = _timeUtil;
        mUserInteractor = _userInteractor;
        mDeleteInteractor = _deleteRejectedInteractor;
    }

    @Override
    public void initialize() {
        mUserInteractor.execute(new UserObserver(this));
    }

    private void initUi() {
        getView().setEditableTitle(false);
        getView().setDetailsBtnVisibility(false);
        getView().setVideoPlayability(false);
        getView().setButtonsVisibility(true);
        getView().setListVisibility(false);
        getView().showLocation(false);
        getView().showDescription(false);
        getView().setCompletedVisibility(true);
        getView().setWarningVisibility(false);
        getView().setTextBtnTitle("DISCARD");
        getView().setCompletedText(CaptApplication.getApplication().getString(R.string.text_rejected_assignment_FO, mUser.getName()));
        getView().setTitle("Completed");
        getView().setRedBtnText("UPLOAD");
        getView().setThumbnail(mData.video.thumbnail);
        getView().setTitle(mData.title);
        getView().setVideoProperties(getFormattedProperties(Long.parseLong(mData.video.duration), String.valueOf(mData.video.size)));

        getView().setPrice(mData.price);
        getView().setVideoDescription(mData.description);
        getView().setLocation(mData.where);
        getView().setVideoTitle(mData.title);
    }

    protected String getFormattedProperties(long _duration, String _size) {
        long size = Long.parseLong(_size);

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    @Override
    public void onTextBtnClick() {
        mDeleteInteractor.execute(mData.id, new DeleteObserver(this));
    }

    @Override
    public void onRedBtnClick() {
        Bundle args = new Bundle();
        mData.video.id = mData.id;
        args.putSerializable(Constants.BUNDLE_VIDEO, mData.video);
        args.putInt(Constants.FLOW, Flow.ASSIGNMENT_REJECTED);
        getView().navigateToDescription(args);
    }

    @Override
    public void setData(Bundle _model) {
        if (_model.containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            mData = (Assignment) _model.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        } else {
            throw new IllegalStateException("Arguments should contain AssignmentModel");
        }
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mUserInteractor.unSubscribe();
        mDeleteInteractor.unSubscribe();
    }

    private class DeleteObserver extends BaseObserver<SuccessModel> {
        public DeleteObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(SuccessModel successModel) {
            getView().navigateBack();
            Log.d(TAG, "onNext() called with: " + "successModel = [" + successModel + "]");
        }
    }

    private class UserObserver extends BaseObserver<UserModel> {
        public UserObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(UserModel userModel) {
            mUser = userModel;
            initUi();
        }
    }
}
