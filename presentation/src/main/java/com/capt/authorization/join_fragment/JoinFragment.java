package com.capt.authorization.join_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.capt.R;
import com.capt.authorization.CaptAuthActivity;
import com.capt.authorization.auth_strategy.AuthController;
import com.capt.authorization.join_fragment.dagger.JoinComponent;
import com.capt.authorization.join_fragment.dagger.JoinModule;
import com.capt.authorization.join_fragment.dagger.StrategyModule;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.login_credentials.LogInFragment;
import com.capt.authorization.phone_number.PhoneNumberFragment;
import com.capt.authorization.sign_up.SignUpFragment;
import com.capt.base.BaseFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.profile.ProfileActivity;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by richi on 2016.02.23..
 */
public class JoinFragment extends BaseFragment implements JoinContract.View {

    @Inject JoinContract.Presenter mPresenter;
    @Inject AuthController mAuthController;
    @Inject ToolbarManager mToolbarManager;

    @Bind(R.id.ibInstagram_FCL) ImageButton ibInstagram;
    @Bind(R.id.ibFacebook_FCL)  ImageButton ibFacebook;
    @Bind(R.id.ibTwitter_FCL)   ImageButton ibTwitter;

    //This is the injector
    protected JoinComponent mComponent;

    public static BaseFragment newInstance() {
        return new JoinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_join);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initializeInjector();
        inject();
        mPresenter.initialize();
        mToolbarManager.hideToolbar();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onStart();
    }

    @Override
    public void onStop() {
        mPresenter.onStop();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void initializeInjector() {
        if (mComponent != null)
            return;

        mComponent = getComponent(AuthActivityComponent.class)
                .createAuthorizationComponent(new JoinModule(this), new StrategyModule());
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.onActivityResult();
        mAuthController.onActivityResult(resultCode, requestCode, data);
    }

    @Override
    public void showToast(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void performLogIn() {
        mAuthController.logIn(this, mPresenter);
    }

    @Override
    public void startDashboard(UserModel _model) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.putExtra(Constants.BUNDLE_USER_MODEL, _model);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void setEnabledSocBtn(boolean _isEnable) {
        ibFacebook.setEnabled(_isEnable);
        ibInstagram.setEnabled(_isEnable);
        ibTwitter.setEnabled(_isEnable);
    }

    //OnClickListener
    @OnClick(R.id.ibFacebook_FCL)
    protected void onFacebookClicked() {
        mPresenter.onFacebookClick();
    }

    @OnClick(R.id.ibTwitter_FCL)
    protected void onTwitterClicked() {
        mPresenter.onTwitterClick();
    }

    @OnClick(R.id.ibInstagram_FCL)
    protected void onInstagramClicked() {
        mPresenter.onInstagramClick();
    }

    @OnClick(R.id.btnRegister_FCL)
    protected void onRegisterClick() {
        CaptAuthActivity.startSignUpFlow(getActivity());
    }

    @OnClick(R.id.tvLogIn_FCL)
    protected void onLogInClick() {
        CaptAuthActivity.startLogInFlow(getActivity());
    }

    @Override
    public void startPhoneNumberScreen() {
        CaptAuthActivity.startPhoneNumberFlow(getContext());
    }

}
