package com.capt.video.price.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.price.SetPriceFragment;

import dagger.Subcomponent;

/**
 * Created by root on 25.03.16.
 */
@PerFragment
@Subcomponent(modules = SetPriceModule.class)
public interface SetPriceComponent {
	void inject(SetPriceFragment _fragment);
}
