package com.capt.application.dagger;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.capt.data.assignment.PreferencesAssignmentCacheImpl;
import com.capt.data.base.BaseDAO;
import com.capt.data.base.TimeUtil;
import com.capt.data.db.CaptDbHelper;
import com.capt.data.video.disk.DraftRepositoryImpl;
import com.capt.data.video.upload.VideoUploadUploadRepositoryImpl;
import com.capt.data.video.upload.data_source.UploadPayPalCodeService;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.profile.assignment.cache.AssignmentCache;
import com.capt.domain.profile.assignment.cache.ClearAssignmentInteractor;
import com.capt.domain.profile.assignment.cache.UpdateAssignmentStateInteractor;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.draft.DraftRepository;
import com.capt.domain.video.draft.UpdatePendingInteractor;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.DiskDataSource;
import com.capt.data.video.upload.data_source.DiskDataSourceImpl;
import com.capt.domain.video.upload.CancelPendingVideoIdInteractor;
import com.capt.domain.video.upload.CompleteUploadInteractor;
import com.capt.domain.video.upload.PayPalInteractor;
import com.capt.domain.video.upload.PendingUploadCache;
import com.capt.data.video.upload.cache.PendingUploadCacheImpl;
import com.capt.data.db.VideoDAO;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.video.upload.RetryInteractor;
import com.capt.domain.video.upload.UploadForAssignmentInteractor;
import com.capt.domain.video.upload.UploadInteractor;
import com.capt.domain.video.upload.VideoUploadRepository;
//import VideoUploadModel;
import com.capt.domain.global.Constants;
import com.capt.video.trim.time_util.TimeUtilImpl;
import com.capt.video.upload_manager.VideoUploadManager;
import com.capt.video.upload_manager.VideoUploadManagerImpl;

import net.gotev.uploadservice.HttpUploadRequest;
import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by richi on 2016.03.14..
 */
@Module
public class VideoUploadModule {

    private String mServerUrl;

    public VideoUploadModule(String _serverUrl) {
        mServerUrl = _serverUrl;
    }

    @Provides
    protected UploadNotificationConfig provideNotificationConfig() {
        return new UploadNotificationConfig()
                .setInProgressMessage("In progress...")
                .setIcon(android.R.drawable.ic_menu_upload)
                .setRingToneEnabled(false)
                .setAutoClearOnSuccess(true);
    }

    @Provides
    protected HttpUploadRequest provideMultipartRequest(Context _context) {
        return new MultipartUploadRequest(_context, mServerUrl);
    }

    @Provides
    @Singleton
    protected VideoUploadRepository provideVideoRepository(VideoUploadUploadRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @Singleton
    @Named(Constants.PendingVideoConstants.UPLOAD_VIDEO)
    protected BasePostInteractor<Video> provideVideoUploadInteractor(UploadInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @Singleton
    @Named(Constants.PendingVideoConstants.RETRY_UPLOAD_VIDEO)
    protected BasePostInteractor<String> provideRetryInteractor(RetryInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @Singleton
    @Named(Constants.PendingVideoConstants.UPLOAD_FOR_ASSIGNMENT)
    protected BasePostInteractor<Assignment> provideVideoUploadForAssignmentInteractor(UploadForAssignmentInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @Singleton
    @Named(Constants.PendingVideoConstants.CANCEL_PENDING_VIDEO_ID)
    protected BasePostInteractor<String> provideDeletePendingIdInteractor(CancelPendingVideoIdInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @Singleton
    @Named(Constants.PendingVideoConstants.COMPLETE_PENDING_VIDEO_ID)
    protected BasePostInteractor<String> provideDeleteSourceVideoInteractor(CompleteUploadInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @Singleton
    protected SQLiteOpenHelper provideSQLiteHelper(CaptDbHelper _helper) {
        return _helper;
    }

    @Provides
    @Singleton
    protected BaseDAO<Video> provideVideoDao(VideoDAO _dao) {
        return _dao;
    }

    @Provides
    @Singleton
    protected TimeUtil provideTimeUtil(TimeUtilImpl _util) {
        return _util;
    }

    @Provides
    @Singleton
    protected UploadPayPalCodeService providePayPalService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                           Retrofit _retrofit) {
        return _retrofit.create(UploadPayPalCodeService.class);
    }

    @Provides
    @Singleton
    protected DiskDataSource provideDiskDataSource(DiskDataSourceImpl _source) {
        return _source;
    }

    @Provides
    @Singleton
    protected PendingUploadCache providePendingUploadCache(PendingUploadCacheImpl _cache) {
        return _cache;
    }

    @Provides
    @Singleton
    protected VideoUploadManager providePendingUploadManager(VideoUploadManagerImpl _manager) {
        return _manager;
    }

    @Provides
    @Singleton
    protected DraftRepository provideDraftUpdateRepository(DraftRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.PENDING_UPDATE_INTERACTOR)
    BasePostInteractor<Video> provideUpdateInteractor(UpdatePendingInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @Singleton
    AssignmentCache provideAssignmentCache(PreferencesAssignmentCacheImpl _cache) {
        return _cache;
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.ASSIGNMENT_CACHE_UPDATE)
    BasePostInteractor<String> provideUpdateStatusInteractor(UpdateAssignmentStateInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @Singleton
    @Named(Constants.NamedAnnotation.ASSIGNMENT_CACHE_CLEAR)
    BaseInteractor provideClearAssignmentCacheInteractor(ClearAssignmentInteractor _interactor) {
        return _interactor;
    }

}
