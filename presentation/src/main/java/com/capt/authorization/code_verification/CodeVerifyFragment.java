package com.capt.authorization.code_verification;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.capt.R;
import com.capt.authorization.code_verification.customView.VerificationEditText;
import com.capt.authorization.code_verification.dagger.CodeVerifyComponent;
import com.capt.authorization.code_verification.dagger.CodeVerifyModule;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.profile.ProfileActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 16.03.2016.
 */
public class CodeVerifyFragment extends BaseToolbarFragment
        implements CodeVerificationContract.View, VerificationEditText.FilledListener {

    @Inject
    CodeVerificationContract.Presenter mPresenter;

    @Bind(R.id.vetCode_FCV)
    VerificationEditText vetCode;

    @Bind(R.id.tvSendAgain_FCV)
    TextView tvSendAgain;

    @Bind(R.id.tvInfoText_FSU)
    TextView tvInfoText;

    @Bind(R.id.tvTerms_FSU)
    TextView tvTerms;

    @Bind(R.id.btnJoin_FCV)
    Button btnJoin;

    private CodeVerifyComponent mComponent;

    public static CodeVerifyFragment newInstance(Bundle _model) {

        CodeVerifyFragment fragment = new CodeVerifyFragment();
        fragment.setArguments(_model);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_code_verify);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        checkExtra();
        initPresenter(savedInstanceState);
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(AuthActivityComponent.class)
                .createCodeVerifyComponent(new CodeVerifyModule(this));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void checkExtra() {
        Bundle args = getArguments();
        if (args != null && !args.isEmpty()) {

            if (args.containsKey(Constants.Extra.EXTRA_USER)) {
                setConfirmOnly();
            }

            CodeSignUpModel model = (CodeSignUpModel) args.getSerializable(Constants.Extra.EXTRA_CODE_SIGN_UP);

            mPresenter.setCodeSignUpModel(model);
        }
    }


    private void initPresenter(Bundle _bundle) {
        mPresenter.initialize();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void startDashboardScreen(UserModel _model) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void setEnabledSendAgain(boolean _enable) {
        tvSendAgain.setEnabled(_enable);
    }

    @Override
    public void setConfirmOnly() {
        tvInfoText.setVisibility(View.INVISIBLE);
        tvTerms.setVisibility(View.INVISIBLE);
        btnJoin.setText("Confirm");
    }

    @Override
    public void showMessage(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onFilled() {

    }

    @Override
    public void onEmpty() {

    }

    @Override
    public void navigateToJoinScreen() {
        super.navigateToJoinScreen();
    }

    @OnClick(R.id.btnJoin_FCV)
    protected void onJoinClick() {
        mPresenter.onNextClick(vetCode.getText());
    }

    @OnClick(R.id.tvSendAgain_FCV)
    protected void onSendAgainClick() {
        mPresenter.sendAgainClick();
    }
}
