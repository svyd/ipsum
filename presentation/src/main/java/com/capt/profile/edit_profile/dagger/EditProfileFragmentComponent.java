package com.capt.profile.edit_profile.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.edit_profile.EditProfileFragment;

import dagger.Subcomponent;

/**
 * Created by Svyd on 4/29/16.
 */
@PerFragment
@Subcomponent(modules = EditProfileFragmentModule.class)
public interface EditProfileFragmentComponent {
    void inject(EditProfileFragment _fragment);
}
