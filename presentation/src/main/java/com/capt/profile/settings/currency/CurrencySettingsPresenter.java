package com.capt.profile.settings.currency;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.profile.settings.model.UpdateSettingsModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by root on 29.04.16.
 */
public class CurrencySettingsPresenter extends CurrencySettingsContract.Presenter {

    private CurrencySettingsContract.View mView;
    private BaseInteractor mInteractor;
    private BasePostInteractor<UpdateSettingsModel> mPostInteractor;
    private boolean isSelected;

    @Inject
    public CurrencySettingsPresenter(CurrencySettingsContract.View _view,
                                     @Named(Constants.NamedAnnotation.CURRENCY_INTERACTOR)
                                     BaseInteractor _interactor,
                                     @Named(Constants.NamedAnnotation.SETTINGS_INTERACTOR)
                                     BasePostInteractor<UpdateSettingsModel> _postInteractor,
                                     BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
        mPostInteractor = _postInteractor;
        isSelected = false;
    }

    @Override
    public void initialize() {
        mInteractor.execute(new Subscriber(this));
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    private void showCurrenciesOnView(List<CurrencyModel> currencyModels) {
        mView.showCurrencies(currencyModels);
    }

    @Override
    public void onCurrencySelected(CurrencyModel currencyModel) {
        if (!isSelected) {
            UpdateSettingsModel model = new UpdateSettingsModel();
            model.defaultCurrencyId = currencyModel.getCurrencyId();
            mPostInteractor.execute(model, new UpdateSettingsSubscriber(this));
            isSelected = true;
        }
    }

    private class Subscriber extends BaseObserver<List<CurrencyModel>> {
        public Subscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onNext(List<CurrencyModel> currencyModels) {
            showCurrenciesOnView(currencyModels);
        }
    }

    private class UpdateSettingsSubscriber extends BaseObserver<SettingsModel> {
        public UpdateSettingsSubscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(SettingsModel settingsModel) {
            mView.navigateToSettingsScreen(settingsModel);
        }
    }
}
