package com.capt.data.video.video;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.model.VideoModelWrapper;
import com.capt.domain.video.model.VideoPatch;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public interface VideoService {

    @GET("mobile/video")
    Observable<VideoModelWrapper> getVideos(
            @Query("limit") int _limit,
            @Query("offset") int _offset,
            @Query("type") String _type);

    @PATCH("/mobile/video/{id}")
    Observable<Video> patchVideo(@Path("id") String _id, @Body VideoPatch _patch);

    @DELETE("mobile/video/{id}")
    Observable<SuccessModel> deleteVideo(@Path("id") String _id);
}
