package com.capt.domain.profile.marketplace.model;

import com.capt.domain.profile.dashboard.model.base.DisplayableItem;

/**
 * Created by Svyd on 26.06.2016.
 */
public class MarketplaceItem implements DisplayableItem {

    public String name;
    public String number;

    public MarketplaceItem(String _name, String _number) {
        name = _name;
        number = _number;
    }

    @Override
    public String getHeaderTitle() {
        return "Marketplace";
    }

    @Override
    public String getHeaderDescription() {
        return null;
    }
}
