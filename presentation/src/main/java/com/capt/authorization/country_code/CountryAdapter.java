package com.capt.authorization.country_code;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.capt.R;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.authorization.model.CountryModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by richi on 2016.03.17..
 */
@PerFragment
public class CountryAdapter extends BaseAdapter implements Filterable {

    private List<CountryModel> mList;
    private Filter mFilter;

    @Inject
    public CountryAdapter() {
        mList = new ArrayList<>();
    }

    public void setData(List<CountryModel> _data) {
        mList = _data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public CountryModel getItem(int _position) {
        return mList.get(_position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int _position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_country_code, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvCountryName = (TextView) convertView.findViewById(R.id.tvCountryName_LICC);
            viewHolder.tvCountryCode = (TextView) convertView.findViewById(R.id.tvCountryCode_LICC);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CountryModel model = getItem(_position);

        viewHolder.tvCountryCode.setText(model.getCode());
        viewHolder.tvCountryName.setText(model.getName());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null)
            mFilter = new CountryFilter(mList);

        return mFilter;
    }

    public class CountryFilter extends Filter {

        private List<CountryModel> mData;

        public CountryFilter(List<CountryModel> _data) {
            mData = new ArrayList<>(_data);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            List<CountryModel> filterData = new ArrayList<>();

            for (CountryModel model : mData) {
                if (model.getName().toLowerCase()
                        .contains(constraint.toString().toLowerCase()))
                    filterData.add(model);
            }

            results.values = filterData;
            results.count = filterData.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            setData((List<CountryModel>) results.values);
        }
    }

    private class ViewHolder {
        public TextView tvCountryName, tvCountryCode;
    }
}
