package com.capt.data.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.capt.data.base.BaseDAO;
import com.capt.domain.category.model.CategoryModel;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.model.Quality;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by richi on 2016.03.14..
 */
@Singleton
public class VideoDAO implements BaseDAO<Video> {

    private CaptDbHelper mHelper;
    private Gson mGson;

    @Inject
    public VideoDAO(CaptDbHelper _helper, Gson _gson) {
        mHelper = _helper;
        mGson = _gson;
    }

    @Override
    public List<Video> getAll() {
        synchronized (VideoDAO.this) {

            List<Video> result = new ArrayList<>();
            SQLiteDatabase db = mHelper.getWritableDatabase();
            Cursor cursor = db.query(
                    CaptContract.VideoEntry.TABLE_NAME,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            );

            while (cursor.moveToNext()) {
                result.add(getFromCursor(cursor));
            }
            cursor.close();
            db.close();
            return result;
        }
    }

    @Override
    public Video get(String _id) {
        synchronized (VideoDAO.this) {

            SQLiteDatabase db = mHelper.getWritableDatabase();

            String selection = CaptContract.VideoEntry._ID + CaptContract.LIKE_OPERATOR;
            String[] args = {_id};

            Cursor cursor = db.query(
                    CaptContract.VideoEntry.TABLE_NAME,
                    null,
                    selection,
                    args,
                    null,
                    null,
                    null
            );
            cursor.moveToFirst();

            Video model = getFromCursor(cursor);
            cursor.close();
            db.close();

            return model;
        }
    }

    @Override
    public long add(Video _model) {
        synchronized (VideoDAO.this) {

            SQLiteDatabase db = mHelper.getWritableDatabase();
            long id = db.insertOrThrow(CaptContract.VideoEntry.TABLE_NAME, null, getContentValues(_model));
            db.close();
            return id;
        }
    }

    @Override
    public long updateDraft(Video _model) {
        synchronized (VideoDAO.this) {

            SQLiteDatabase db = mHelper.getWritableDatabase();
            long id = db.update(
                    CaptContract.VideoEntry.TABLE_NAME,
                    getContentValues(_model),
                    CaptContract.VideoEntry.COLUMN_DRAFT_ID + "='" + _model.draftId + "'",
                    null);
            db.close();
            return id;
        }
    }

    @Override
    public long updatePending(Video _model) {
        synchronized (VideoDAO.this) {

            SQLiteDatabase db = mHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(CaptContract.VideoEntry.COLUMN_ERROR, _model.isErrorInt());
            values.put(CaptContract.VideoEntry.COLUMN_CURRENTLY_UPLOADING, _model.isCurrentlyUploadingInt());
            if (_model.startUploadTime > 0) {
                values.put(CaptContract.VideoEntry.COLUMN_TIME_UPLOAD_STARTED, _model.startUploadTime);
            }
            long id = db.update(
                    CaptContract.VideoEntry.TABLE_NAME,
                    values,
                    CaptContract.VideoEntry._ID + "='" + _model.id + "'",
                    null);
            db.close();
            return id;
        }
    }

    @Override
    public int deleteByDraftId(String _id) {
        synchronized (VideoDAO.this) {

            return delete(new String[]{_id}, CaptContract.VideoEntry.COLUMN_DRAFT_ID);
        }
    }

    @Override
    public int deleteById(String _id) {
        synchronized (VideoDAO.this) {

            return delete(new String[]{_id}, CaptContract.VideoEntry._ID);
        }
    }

    @Override
    public int getDraftCount() {
        synchronized (VideoDAO.this) {

            SQLiteDatabase db = mHelper.getWritableDatabase();
            String selection = CaptContract.VideoEntry.COLUMN_DRAFT + CaptContract.LIKE_OPERATOR;
            String[] args = {"1"};
            Cursor cursor = db.query(
                    CaptContract.VideoEntry.TABLE_NAME,
                    null,
                    selection,
                    args,
                    null,
                    null,
                    null
            );
            int count = cursor.getCount();
            cursor.close();
            return count;
        }
    }

    private ContentValues getContentValues(Video _model) {
        String categories = mGson.toJson(_model.categories);
        String quality = mGson.toJson(_model.quality);

        ContentValues values = new ContentValues();

        values.put(CaptContract.VideoEntry.COLUMN_TITLE, _model.title);
        values.put(CaptContract.VideoEntry.COLUMN_DESCRIPTION, _model.description);
        values.put(CaptContract.VideoEntry.COLUMN_PRICE, Double.parseDouble(_model.price));
        values.put(CaptContract.VideoEntry.COLUMN_CURRENCY_ID, _model.currency.getCurrencyId());
        values.put(CaptContract.VideoEntry.COLUMN_CURRENCY_ISO, _model.currency.getCurrencyISO());
        values.put(CaptContract.VideoEntry.COLUMN_LICENSE, _model.license);
        values.put(CaptContract.VideoEntry.COLUMN_CURRENCY_SYMBOL, _model.currency.getSymbol());
        values.put(CaptContract.VideoEntry.COLUMN_LATITUDE, _model.location[0]);
        values.put(CaptContract.VideoEntry.COLUMN_LONGITUDE, _model.location[1]);
        values.put(CaptContract.VideoEntry.COLUMN_DURATION, Integer.parseInt(_model.getDuration()));
        values.put(CaptContract.VideoEntry.COLUMN_PATH, _model.path);
        values.put(CaptContract.VideoEntry.COLUMN_FORMATTED_LOCATION, _model.formattedLocation);
        values.put(CaptContract.VideoEntry.COLUMN_FORMATTED_SIZE, _model.formattedSize);
        values.put(CaptContract.VideoEntry.COLUMN_CATEGORIES, categories);
        values.put(CaptContract.VideoEntry.COLUMN_UPLOADING, _model.isUploadingInt());
        values.put(CaptContract.VideoEntry.COLUMN_DRAFT, _model.isDraftInt());
        values.put(CaptContract.VideoEntry.COLUMN_CURRENTLY_UPLOADING, _model.isCurrentlyUploadingInt());
        values.put(CaptContract.VideoEntry.COLUMN_COUNTRY, _model.country);
        values.put(CaptContract.VideoEntry.COLUMN_THUMBNAIL, _model.thumbnail);
        values.put(CaptContract.VideoEntry.COLUMN_CITY, _model.city);
        values.put(CaptContract.VideoEntry.COLUMN_QUALITY, quality);
        values.put(CaptContract.VideoEntry.COLUMN_DATE, _model.date_upload.getTime());
        values.put(CaptContract.VideoEntry.COLUMN_DRAFT_ID, _model.draftId);
        values.put(CaptContract.VideoEntry.COLUMN_ERROR, _model.isErrorInt());
        values.put(CaptContract.VideoEntry.COLUMN_TIME_UPLOAD_STARTED, _model.startUploadTime);

        return values;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private int delete(String[] _args, String _column) {
        String selection = _column + CaptContract.LIKE_OPERATOR;
        SQLiteDatabase db = mHelper.getWritableDatabase();
        Cursor cursor = db.query(
                CaptContract.VideoEntry.TABLE_NAME,
                new String[]{CaptContract.VideoEntry.COLUMN_PATH},
                selection,
                _args, null, null, null
        );
        cursor.moveToFirst();

        int row = db.delete(CaptContract.VideoEntry.TABLE_NAME, _column + "='" + _args[0] + "'", null);
        cursor.close();
        db.close();
        return row;
    }

    private Video getFromCursor(Cursor _cursor) {
        CurrencyModel currency = new CurrencyModel();
        currency.setSymbol(_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_CURRENCY_SYMBOL)));
        currency.setCurrencyId(_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_CURRENCY_ID)));
        currency.setCurrencyISO(_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_CURRENCY_ISO)));

        double[] location = new double[2];
        location[0] = _cursor.getDouble(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_LONGITUDE));
        location[1] = _cursor.getDouble(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_LATITUDE));

        String tagString = _cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_CATEGORIES));
        List<CategoryModel> categories = mGson.fromJson(tagString, new TypeToken<List<CategoryModel>>() {
        }.getType());

        String qualityString = _cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_QUALITY));
        Quality quality = mGson.fromJson(qualityString, Quality.class);

        Video video = new Video();
        video.id = String.valueOf(_cursor.getInt(_cursor.getColumnIndex(CaptContract.VideoEntry._ID)));
        video.title = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_TITLE)));
        video.description = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_DESCRIPTION)));
        video.price = String.valueOf(_cursor.getDouble(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_PRICE)));
        video.license = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_LICENSE)));
        video.path = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_PATH)));
        video.date_upload = (new Date(_cursor.getLong(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_DATE))));
        video.duration = String.valueOf(_cursor.getLong(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_DURATION)));
        video.formattedLocation = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_FORMATTED_LOCATION)));
        video.formattedSize = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_FORMATTED_SIZE)));
        video.country = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_COUNTRY)));
        video.city = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_CITY)));
        video.thumbnail = (_cursor.getString(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_THUMBNAIL)));
        video.draftId = (_cursor.getLong(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_DRAFT_ID)));
        video.currency = currency;
        video.location = location;
        video.quality = quality;
        video.categories = categories;
        video.date_upload = new Date(_cursor.getInt(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_DATE)));
        video.startUploadTime = _cursor.getLong(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_TIME_UPLOAD_STARTED));
        video.setDraftInt(_cursor.getInt(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_DRAFT)));
        video.setCurrentlyUploading(_cursor.getInt(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_CURRENTLY_UPLOADING)));
        video.setUploadingInt(_cursor.getInt(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_UPLOADING)));
        video.setErrorInt(_cursor.getInt(_cursor.getColumnIndex(CaptContract.VideoEntry.COLUMN_ERROR)));
        return video;
    }
}
