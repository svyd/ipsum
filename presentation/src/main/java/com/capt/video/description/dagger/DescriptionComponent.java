package com.capt.video.description.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.video.description.DescriptionFragment;

import dagger.Subcomponent;

/**
 * Created by root on 03.04.16.
 */
@PerFragment
@Subcomponent(modules = DescriptionModule.class)
public interface DescriptionComponent {
	void inject(DescriptionFragment _fragment);
}

