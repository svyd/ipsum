package com.capt.profile.dashboard.ui.delegate;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.domain.profile.marketplace.model.MarketplaceItem;
import com.capt.profile.dashboard.ui.adapter.ViewTypeConstants;
import com.capt.profile.dashboard.ui.delegate.base.BaseSimpleDelegate;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 26.06.2016.
 */
@SuppressWarnings("ALL")
public class MarketplaceDelegate extends BaseSimpleDelegate<MarketplaceItem> {

    @Inject
    public MarketplaceDelegate(LayoutInflater _layoutInflater) {
        super(_layoutInflater, ViewTypeConstants.VIEW_TYPE_MARKETPLACE, MarketplaceItem.class);
    }

    @Override
    protected int getType() {
        return ViewTypeConstants.VIEW_TYPE_MARKETPLACE;
    }

    @Override
    public void onBindViewHolder(@NonNull List<DisplayableItem> items, int position, @NonNull RecyclerView.ViewHolder holder) {
        SimpleViewHolder simpleHolder = (SimpleViewHolder) holder;
        MarketplaceItem item = (MarketplaceItem) items.get(position);

        simpleHolder.tvLeft.setText(item.name);
        simpleHolder.tvRight.setText(item.number);
    }
}
