package com.capt.data.authorization.net.retrofit;

import com.capt.domain.authorization.forgot_password.model.ForgotPasswordModel;
import com.capt.domain.authorization.model.SuccessModel;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Svyd on 22.03.2016.
 */
public interface ForgotPasswordService {

    @POST("/mobile/user/forgotPassword")
    Observable<SuccessModel> forgotPassword(@Body ForgotPasswordModel _model);

}
