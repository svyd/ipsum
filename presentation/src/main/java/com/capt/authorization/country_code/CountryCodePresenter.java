package com.capt.authorization.country_code;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.CountryModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

/**
 * Created by richi on 2016.03.16..
 */
public class CountryCodePresenter extends CountryContract.Presenter {

    private CountryContract.View mView;
    private BaseInteractor mInteractor;
    private PublishSubject<String> mSubject;
    private CountryAdapter mAdapter;

    @Inject
    public CountryCodePresenter(CountryContract.View _view,
                                @Named(Constants.NamedAnnotation.COUNTRY_CODE_INTERACTOR) BaseInteractor _interactor,
                                CountryAdapter _adapter, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
        mAdapter = _adapter;

        initSubject();
    }

    private void initSubject() {
        mSubject = PublishSubject.create();
        mSubject.debounce(500, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mSearchTextListener);
    }

    @Override
    public void initialize() {
        mInteractor.execute(new Subscriber(this));
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onSearchTextChanged(String _text) {
        mSubject.onNext(_text);
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }


    private Action1<String> mSearchTextListener = new Action1<String>() {
        @Override
        public void call(String _query) {
            mAdapter.getFilter().filter(_query);
        }
    };

    private class Subscriber extends BaseObserver<List<CountryModel>> {
        public Subscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onNext(List<CountryModel> countryModels) {
            mView.setData(countryModels);
        }
    }
}
