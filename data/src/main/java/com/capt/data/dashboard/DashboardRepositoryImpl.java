package com.capt.data.dashboard;

import com.capt.data.authorization.repository.datasource.UserCache;
import com.capt.domain.authorization.model.UpdateAvatarResponse;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.profile.assignment.AssignmentsRepository;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.DashboardRepository;
import com.capt.domain.profile.dashboard.UserRepository;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.profile.dashboard.model.DashboardModel;
import com.capt.domain.profile.dashboard.model.DashboardParamsModel;
import com.capt.domain.profile.dashboard.model.Marketplace;
import com.capt.domain.profile.dashboard.model.base.Section;
import com.capt.domain.profile.marketplace.MarketplaceRepository;
import com.capt.domain.profile.marketplace.model.MarketplaceItem;
import com.capt.domain.video.video.VideoRepository;
import com.capt.domain.video.model.Video;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public class DashboardRepositoryImpl implements DashboardRepository {

    private VideoRepository mVideo;
    private AssignmentsRepository mAssignments;
    private MarketplaceRepository mMarketplace;
    private UserService mUserService;
    private UserCache mUserCahce;

    @Inject
    public DashboardRepositoryImpl(VideoRepository _videoRepository,
                                   AssignmentsRepository _assignmentRepository,
                                   MarketplaceRepository _marketplaceRepository,
                                   UserService _service,
                                   UserCache _userCache) {
        mAssignments = _assignmentRepository;
        mMarketplace = _marketplaceRepository;
        mVideo = _videoRepository;
        mUserService = _service;
        mUserCahce = _userCache;
    }

    @Override
    public Observable<DashboardModel> getDashboard(DashboardParamsModel _model) {
        return Observable.zip(
                mAssignments.getAssignmentModels(_model.getAssignmentsParams()),
                mVideo.getVideos(_model.getVideoParams()),
                mUserService
                        .getUser()
                        .flatMap(userModel -> mUserCahce.cacheUser(userModel)),
                mMarketplace.getMarketplace(),
                this::zipToDashboard);
    }

    private DashboardModel zipToDashboard(List<Assignment> assignments,
                                          List<Video> videos,
                                          UserModel _user,
                                          List<MarketplaceItem> marketplace) {
        DashboardModel model = new DashboardModel();
        model.setUser(_user);

        Section<Video> videoSection = new Section<>(videos, true);
        videoSection.setHeaderTitle("Recent Videos");
//        videoSection.setHeaderDescription("VIEW ALL");
//        videoSection.setHasButton(true);

        if (assignments.isEmpty()) {
            Assignment assignment = new Assignment();
            assignment.who = "No active assignments. Tap here to check for new opportunities.";
            assignment.stub = true;
            assignments.add(assignment);
        }
        Section<Assignment> assignmentSection = new Section<>(assignments, true);
        assignmentSection.setHeaderTitle("Active Assignments");

        Section<MarketplaceItem> marketplaceSection = new Section<>(marketplace, true);
        marketplaceSection.setHeaderTitle("Marketplace");

        model.setAssignments(assignmentSection);
        model.setMarketplace(marketplaceSection);
        model.setVideos(videoSection);

        return model;
    }

    @Override
    public Observable<UpdateAvatarResponse> update(File _file) {
        RequestBody avatar = RequestBody.create(MediaType.parse("multipart/form-data"), _file);
        return Observable.zip(mUserService.updateAvatar(avatar), mUserCahce.getUser(),
                (updateAvatarResponse, userModel) -> {
                    updateAvatarResponse.url = updateAvatarResponse.url + "?timestamp=" + System.currentTimeMillis();
                    userModel.setAvatarUrl(updateAvatarResponse.url);
                    mUserCahce.cacheUser(userModel)
                            .publish()
                            .connect();
                    return updateAvatarResponse;
                });
    }
}
