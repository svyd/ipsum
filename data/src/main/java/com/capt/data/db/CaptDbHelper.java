package com.capt.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Inject;

/**
 * Created by Svyd on 14.04.2016.
 */
public class CaptDbHelper extends SQLiteOpenHelper {

    @Inject
    public CaptDbHelper(Context context) {
        super(context, CaptContract.DATABASE_NAME, null, CaptContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CaptContract.VideoEntry.SQL_CREATE_VIDEO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(CaptContract.DROP_COMMAND + CaptContract.VideoEntry.TABLE_NAME);
        onCreate(db);
    }
}
