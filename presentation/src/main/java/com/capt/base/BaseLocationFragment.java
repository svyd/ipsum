package com.capt.base;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by richi on 2016.03.18..
 */
@RuntimePermissions
public class BaseLocationFragment extends BaseToolbarFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult> {

    public static final int REQUEST_LOCATION_CHANGE = 256;

    private GoogleApiClient mApiClient;
    private boolean mIsNeedToUpdateLocation = true;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        buildLocationApi();
        if (mIsNeedToUpdateLocation)
            updateLocation();
    }

    protected void updateLocation() {
        BaseLocationFragmentPermissionsDispatcher.startLocationUpdateWithCheck(this);
    }

    protected void needUpdateLocation(boolean _need) {
        mIsNeedToUpdateLocation = _need;
    }

    @Override
    public void onDestroyView() {
        stopLocationUpdate();
        super.onDestroyView();
    }

    public void stopLocationUpdate() {
        if (!mApiClient.isConnected())
            return;
        LocationServices.FusedLocationApi.removeLocationUpdates(mApiClient, this);
        getGoogleApiClient().disconnect();
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    protected void startLocationUpdate() {
        if (!getGoogleApiClient().isConnected())
            getGoogleApiClient().connect();
        else
            onConnected(null);    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    protected void showRationaleForCamera(PermissionRequest request) {
        new AlertDialog.Builder(getActivity(), R.style.CaptAlertDialogStyle)
                .setTitle(R.string.permission)
                .setMessage(R.string.location_rational)
                .setPositiveButton(R.string.allow, (dialog, which) -> {
                    request.proceed();
                })
                .setNegativeButton(R.string.deny, (dialog, which) -> {
                    request.cancel();
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    protected void showDeniedForLocation() {
        Toast.makeText(CaptApplication.getApplication(), R.string.location_deny, Toast.LENGTH_SHORT)
                .show();
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    protected void showNeverAskForLocation() {
        Toast.makeText(CaptApplication.getApplication(), R.string.location_never_ask, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        BaseLocationFragmentPermissionsDispatcher.onRequestPermissionsResult(this,
                requestCode, grantResults);
    }

    private void buildLocationApi() {
        mApiClient = CaptApplication.getApplication()
                .getAppComponent().apiClientBuilder()
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(CaptApplication.getApplication(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(CaptApplication.getApplication(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mApiClient);
        if (location == null)
            checkLocationSettingsAndChangeIfNeed();
        else
            onLocationChanged(location);
    }

    private void checkLocationSettingsAndChangeIfNeed() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(createLocationRequest());

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(getGoogleApiClient(),
                        builder.build());

        result.setResultCallback(this);
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                requestLocationUpdate();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                resolutionRequired(status);
                break;
        }
    }

    private void requestLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(CaptApplication.getApplication(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(CaptApplication.getApplication(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
            return;

        LocationServices.FusedLocationApi.requestLocationUpdates(mApiClient,
                createLocationRequest(),
                this);
    }

    private void resolutionRequired(Status _status) {
        try {
            _status.startResolutionForResult(getActivity(), REQUEST_LOCATION_CHANGE);
        } catch (IntentSender.SendIntentException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCATION_CHANGE && resultCode == Activity.RESULT_OK) {
            requestLocationUpdate();
        } else if (requestCode == REQUEST_LOCATION_CHANGE && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(getActivity(), R.string.location_deny, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(CaptApplication.getApplication(), "Something went wrong", Toast.LENGTH_SHORT)
                .show();
    }

    private LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    protected GoogleApiClient getGoogleApiClient() {
        return mApiClient;
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
