package com.capt.authorization.country_code.dagger;

import com.capt.authorization.country_code.CountryCodePresenter;
import com.capt.authorization.country_code.CountryContract;
import com.capt.data.annotation.PerFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.03.16..
 */
@Module
public class CountryModule {

    private CountryContract.View mView;

    public CountryModule(CountryContract.View _view) {
        mView = _view;
    }

    @Provides @PerFragment
    protected CountryContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    protected CountryContract.Presenter providePresenter(CountryCodePresenter _presenter) {
        return _presenter;
    }
}
