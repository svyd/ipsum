package com.capt.domain.global;

/**
 * Created by Svyd on 19.05.2016.
 */
public abstract class AssignmentState {
    public static final String IN_REVIEW = "IN_REVIEW";
    public static final String REJECTED = "REJECTED";
    public static final String PURCHASED = "PURCHASED";
    public static final String UPLOADING = "UPLOADING";
    public static final String ERROR = "ERROR";
}
