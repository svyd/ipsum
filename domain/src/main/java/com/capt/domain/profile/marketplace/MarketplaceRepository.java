package com.capt.domain.profile.marketplace;

import com.capt.domain.profile.dashboard.model.Marketplace;
import com.capt.domain.profile.marketplace.model.MarketplaceItem;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 29.03.2016.
 */
public interface MarketplaceRepository {
    Observable<List<MarketplaceItem>> getMarketplace();
}
