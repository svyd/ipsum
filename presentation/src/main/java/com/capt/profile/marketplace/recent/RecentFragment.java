package com.capt.profile.marketplace.recent;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.profile.marketplace.ErrorOptionsListener;
import com.capt.profile.marketplace.MarketplaceTabListener;
import com.capt.profile.marketplace.VideoHolder;
import com.capt.profile.marketplace.base.BaseMarketPlaceListFragment;
import com.google.gson.Gson;

import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by root on 08.04.16.
 */
public class RecentFragment extends BaseMarketPlaceListFragment implements ErrorOptionsListener{

    @Override
    protected String getType() {
        return Constants.MarketplaceTypes.RECENT;
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(mReceiver, new IntentFilter("com.capt.uploadservice.broadcast.status"));
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    protected Constants.MarketplaceItems getItemType() {
        return Constants.MarketplaceItems.Recent;
    }

    @Override
    public void showVideos(List<Video> videoModels) {
        super.showVideos(videoModels);
        mAdapter.setErrorListener(this);
    }

    @Override
    public void setPending(int _position) {
        applyToHolder(VideoHolder::setInQueue, _position);
    }

    @Override
    public void onSaveDraft(Video _draft) {
        if (getParentFragment() instanceof MarketplaceTabListener) {
            ((MarketplaceTabListener) getParentFragment()).onAddDraft(_draft);
        } else {
            throw new RuntimeException("Parent fragment should implement MarketplaceTabListener");
        }
    }

    @Override
    public void onRetryClick(int _position) {
        mPresenter.onRetryClick(_position);
    }

    @Override
    public void onCancelClick(int _position) {
        mPresenter.onCancelClick(_position);
    }

    public void setProgress(int _position, int progress) {
        applyToHolder(_holder -> _holder.onProgress(progress), _position);
    }

    public void setError(int _position) {
        applyToHolder(VideoHolder::onError, _position);
    }

    @Override
    public void swap(int _first, int _second) {
        mAdapter.swap(_first, _second);
    }

    private void applyToHolder(HolderAction _action, int _position) {
        VideoHolder holder = getHolder(_position);
        if (holder != null) {
            _action.doAction(holder);
        }
    }

    private VideoHolder getHolder(int _position) {
        View view = rvVideos.getLayoutManager().findViewByPosition(_position);
        if (view != null) {
            return  (VideoHolder) view.getTag();
        } else {
            return null;
        }
    }

    private int getPosition(String _id) {
        return mAdapter.getVideoPositionById(_id);
    }

    private UploadServiceBroadcastReceiver mReceiver = new UploadServiceBroadcastReceiver() {

        @Override
        public void onStarted(String uploadId) {
            mPresenter.onStartUploading(getPosition(uploadId));
        }

        @Override
        public void onError(String uploadId, Exception exception) {
            super.onError(uploadId, exception);
            mPresenter.onUploadError(getPosition(uploadId));
        }

        @Override
        public void onProgress(String uploadId, int progress) {
            super.onProgress(uploadId, progress);
            mPresenter.onProgress(getPosition(uploadId), progress);
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, byte[] serverResponseBody) {
            super.onCompleted(uploadId, serverResponseCode, serverResponseBody);
            try {
                String body = new String(serverResponseBody, "UTF-8");
                Video video = new Gson().fromJson(body, Video.class);
                mPresenter.onUploadCompleted(getPosition(uploadId), video);
                getActivity().setResult(Activity.RESULT_OK);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    };

    private interface HolderAction {
        void doAction(VideoHolder _holder);
    }
}
