package com.capt.authorization.sign_up.image_picker.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.authorization.sign_up.image_picker.ImagePickerContract;
import com.capt.authorization.sign_up.image_picker.ImagePickerPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.03.20..
 */
@Module
public class ImagePickerModule {

    private ImagePickerContract.View mView;

    public ImagePickerModule(ImagePickerContract.View _view) {
        mView = _view;
    }

    @Provides @PerFragment
    protected ImagePickerContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    protected ImagePickerContract.Presenter providePresenter(ImagePickerPresenter _presenter) {
        return _presenter;
    }
}
