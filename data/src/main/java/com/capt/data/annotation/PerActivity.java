package com.capt.data.annotation;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by richi on 2016.02.22..
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {}