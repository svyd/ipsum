package com.capt.video.camera.fragment.flow;

import android.os.Bundle;
import android.os.Handler;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.model.Video;
import com.capt.video.camera.BaseAdapter;
import com.capt.video.camera.fragment.CameraContract;
import com.capt.video.trim.file_manager.FileManager;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Svyd on 13.05.2016.
 */
public class AssignmentCameraPresenter extends AbstractCameraPresenter {

    private Assignment mAssignment;

    @Inject
    public AssignmentCameraPresenter(Handler _handler,
                                     FileManager _manager,
                                     CameraContract.View _view,
                                     @Named(Constants.NamedAnnotation.TOP_ADAPTER)
                                     BaseAdapter _topAdapter,
                                     @Named(Constants.NamedAnnotation.BOTTOM_ADAPTER)
                                     BaseAdapter _bottomAdapter, BaseExceptionDelegate _delegate) {
        super(_handler, _manager, _view, _topAdapter, _bottomAdapter, _delegate);
    }

    @Override
    public void setArgs(Bundle _args) {
        if (_args.containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            mAssignment = (Assignment) _args.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        } else {
            throw new IllegalArgumentException(
                    AssignmentCameraPresenter.class.getSimpleName()
                    + " should be instantiated with assignment"
            );
        }
    }

    @Override
    protected void stopRecording() {
        getView().showRightBar();
        getView().stopRecording();
        Video video = new Video();
        video.path = getPath();
        mAssignment.video = video;
        getView().startAssignmentFlow(mAssignment);
    }
}
