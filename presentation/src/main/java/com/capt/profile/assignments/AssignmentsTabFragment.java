package com.capt.profile.assignments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;

import com.capt.R;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.global.Constants;
import com.capt.profile.ViewPagerAdapter;
import com.capt.profile.assignments.dagger.AssignmentsTabComponent;
import com.capt.profile.assignments.dagger.AssignmentsTabModule;
import com.capt.profile.assignments.in_progress.AssignmentRefreshListener;
import com.capt.profile.dagger.ProfileActivityComponent;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by root on 15.04.16.
 */
public class AssignmentsTabFragment extends BaseToolbarFragment implements AssignmentsTabContract.View,
        SwipeRefreshLayout.OnRefreshListener,
        AssignmentRefreshListener {

    @Inject
    ViewPagerAdapter mAdapter;

    @Inject
    AssignmentsTabContract.Presenter mPresenter;

    @Inject
    List<Fragment> mFragments;

    @Bind(R.id.tab_layout_FAT)
    TabLayout tabLayout;

    @Bind(R.id.view_pager_FAT)
    ViewPager viewPager;

    @Bind(R.id.srlAssignments_FAT)
    SwipeRefreshLayout srlAssignments;

    private AssignmentsTabComponent mComponent;

    public static AssignmentsTabFragment newInstance(String assignmentType) {

        Bundle args = new Bundle();
        args.putString(Constants.Extra.EXTRA_STRING, assignmentType);
        AssignmentsTabFragment fragment = new AssignmentsTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_assignments_tab);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        setUpTabs();
        showToolbar();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(ProfileActivityComponent.class)
                .createAssignmentsTabComponent(new AssignmentsTabModule(this, this));
    }

    private void showToolbar() {
        getToolbarManager().showToolbar();
        getActivity().setTitle(getString(R.string.text_assignments));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : mFragments) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void inject() {
        mComponent.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        initRefreshLayout();
    }

    private void initRefreshLayout() {
        srlAssignments.setOnRefreshListener(this);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                srlAssignments.setEnabled(state == ViewPager.SCROLL_STATE_IDLE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        srlAssignments.setRefreshing(false);
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void setUpTabs() {
        viewPager.setOffscreenPageLimit(3);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        Bundle bundle = getArguments();
        if ((bundle != null) && (bundle.containsKey(Constants.Extra.EXTRA_STRING))) {
            String type = bundle.getString(Constants.Extra.EXTRA_STRING);
            mPresenter.setType(type);
        }
    }

    @Override
    public void setViewPagerItem(int position) {
        viewPager.setCurrentItem(position);
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPager.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        ((RefreshListener) mAdapter.getItem(viewPager.getCurrentItem())).refresh();
    }

    @Override
    public void onRefreshed() {
        if (srlAssignments.isRefreshing()) {
            srlAssignments.setRefreshing(false);
        }
    }
}
