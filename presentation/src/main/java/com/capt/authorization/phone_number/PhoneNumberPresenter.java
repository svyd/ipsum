package com.capt.authorization.phone_number;

import android.os.Bundle;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.CodeModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.CountryModel;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by richi on 2016.03.17..
 */
public class PhoneNumberPresenter extends PhoneNumberContract.Presenter {

    public static final String RESTORE_CODE_SIGN_UP = "restore_code_sign_up_model";
    public static final String RESTORE_PHONE_NUMBER_BUILDER = "restore_phone_number";

    private PhoneNumberContract.View mView;

    //Interactors
    private BasePostInteractor<PhoneNumberModel> mVerificationInteractor;
    private BaseInteractor mCountryCodeInteractor;

    private List<CountryModel> mListCountries;
    private PhoneNumberModel.Builder mPhoneModel;
    private CodeSignUpModel mMainSignUpModel;

    @Inject
    public PhoneNumberPresenter(PhoneNumberContract.View _view,
                                @Named(Constants.NamedAnnotation.VERIFICATION_INTERACTOR)
                                BasePostInteractor<PhoneNumberModel> _verificationInteractor,
                                @Named(Constants.NamedAnnotation.COUNTRY_CODE_INTERACTOR)
                                BaseInteractor _countryCodeInteractor, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mCountryCodeInteractor = _countryCodeInteractor;
        mVerificationInteractor = _verificationInteractor;

        mPhoneModel = new PhoneNumberModel.Builder();

        downloadCountryCode();
    }

    private void downloadCountryCode() {
        mCountryCodeInteractor.execute(new CountryCodeObserver(this));
    }

    @Override
    public void onTextChanged(String _text) {
        if (_text.trim().length() > 0)
            mView.setEnableNext(true);
        else
            mView.setEnableNext(false);

        mPhoneModel.setNumber(_text);
    }

    @Override
    public void onCodeClick() {
        mView.startCountryCodeFragment();
    }

    @Override
    public void onNext() {
        mView.setEnableNext(false);
        mVerificationInteractor.execute(mPhoneModel.build(), new VerificationObserver(this));
    }

    @Override
    public void savedInstanceState(Bundle _bundle) {
        _bundle.putSerializable(RESTORE_PHONE_NUMBER_BUILDER, mPhoneModel);
        _bundle.putSerializable(RESTORE_CODE_SIGN_UP, mMainSignUpModel);
    }

    @Override
    public void restoreInstanceState(Bundle _bundle) {
        if (_bundle == null)
            return;

        mPhoneModel = (PhoneNumberModel.Builder) _bundle.getSerializable(RESTORE_PHONE_NUMBER_BUILDER);
        mMainSignUpModel = (CodeSignUpModel) _bundle.getSerializable(RESTORE_CODE_SIGN_UP);
    }

    @Override
    public void setCountryCode(String _code) {
        if (_code == null)
            return;

        mPhoneModel.setCode(_code);
        mView.onCountryCodeSet(_code);
    }

    @Override
    public void initialize() {
        if (mPhoneModel != null && mPhoneModel.getCode() != null && mPhoneModel.getCode().trim().length() != 0)
            mView.onCountryCodeSet(mPhoneModel.getCode());
    }

    @Override
    public void setModel(CodeSignUpModel _model) {
        mMainSignUpModel = _model;
    }

    @Override
    public CodeSignUpModel getModel() {
        return mMainSignUpModel;
    }

    @Override
    public void onStop() {
        mCountryCodeInteractor.unSubscribe();
        mVerificationInteractor.unSubscribe();
    }

    private void findCountryCode() {
        if (mMainSignUpModel == null || mMainSignUpModel.getSignUpModel() == null)
            return;

        for (CountryModel model : mListCountries) {
            if (mMainSignUpModel.getSignUpModel().getCountry().toLowerCase()
                    .contains(model.getName().toLowerCase())) {
                setCountryCode(model.getCode());
            }
        }
    }

    public class CountryCodeObserver extends BaseObserver<List<CountryModel>> {
        public CountryCodeObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(List<CountryModel> countryModels) {
            mListCountries = countryModels;
            findCountryCode();
        }
    }

    public class VerificationObserver extends BaseObserver<SuccessModel> {
        public VerificationObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            mView.setEnableNext(true);
        }

        @Override
        public void onNext(SuccessModel successModel) {
            mView.setEnableNext(true);
            mMainSignUpModel.setPhoneNumber(mPhoneModel.build().getPhone());
            mMainSignUpModel.setCodeModel(new CodeModel(mPhoneModel.build().getCode()));
            mView.startCodeConfirmScreen();
        }
    }
}
