package com.capt.video.description;

import android.location.Location;
import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

/**
 * Created by root on 03.04.16.
 */
public interface DescriptionContract {


	interface View extends BaseView {
        void setEditableTitle(boolean _editable);
        void showToolbar(boolean _show);
		void setVideoVisibility(boolean _visible);
		void setToolbarTitle(String _title);
        void setThumbnail(String _url);
		void showMessage(String _message);
		void showValidateMessage();
		void navigateToSetPriceScreen(Bundle _data);
		void navigateToMapScreen();
		void setVideoProperties(String _properties);
		void setVideoView(String _path);
		void setLocationName(String _name);
		void needToUpdateLocation(boolean _isNeeded);
		void playVideo();
        void setTitle(String _title);
        void setDescription(String _description);
		String getTitle();
		String getDescription();
	}

	abstract class Presenter extends BasePresenter<DescriptionContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void navigateToMap();
		public abstract void onNextClick();
		public abstract void setLatLng(String[] latLng);
		public abstract void setData(Bundle _args);
		public abstract void onLocationChanged(Location _location);
		public abstract void locationNameSelected(String _countryName);
		public abstract void onPlayClick();
		public abstract void deleteVideo();
	}
}