package com.capt.profile.assignments.details.overview;

import android.content.Intent;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.assignment.model.Assignment;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.TimeZone;

import javax.inject.Inject;

/**
 * Created by root on 25.04.16.
 */
public class OverviewPresenter extends OverviewContract.Presenter {

    private OverviewContract.View mView;
    private Assignment mAssignment;

    @Inject
    public OverviewPresenter(OverviewContract.View _view,
                             BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        this.mView = _view;
    }

    @Override
    public void onStop() {

    }

    @Override
    public void navigateToAssignments() {
        mView.navigateToAssignmentsScreen();
    }

    @Override
    public void setAssignmentModel(Assignment _model) {
        mAssignment = _model;
        mView.setUpView(mAssignment);
    }

    @Override
    public void addToCalendar() {

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateTime dt = formatter.parseDateTime(mAssignment.date);
        Calendar startTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        startTime.set(dt.getYear(), dt.getMonthOfYear() - 1, dt.getDayOfMonth(), dt.getHourOfDay(), dt.getMinuteOfHour());
        Calendar endTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        endTime.set(dt.getYear(), dt.getMonthOfYear() - 1, dt.getDayOfMonth(), dt.getHourOfDay() + 1, dt.getMinuteOfHour());

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setType("vnd.android.cursor.item/event")
                .setData(Events.CONTENT_URI)
                .putExtra(Events.TITLE, mAssignment.who)
                .putExtra(Events.DESCRIPTION, mAssignment.title)
                .putExtra(Events.EVENT_LOCATION, mAssignment.where)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis());

        mView.sendCalendarIntent(intent);
    }
}
