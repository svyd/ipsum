package com.capt.domain.authorization.country_code;

import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

import rx.Observable;

/**
 * Created by richi on 2016.03.16..
 */
public interface CountryRepository {
    Observable<List<CountryModel>> getCountryModels();
}
