package com.capt.profile.assignments.base.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.data.assignment.AssignmentsMapper;
import com.capt.data.assignment.AssignmentsRepositoryImpl;
import com.capt.data.assignment.AssignmentsService;
import com.capt.data.base.TypeMapper;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.model.RequestParamsModel;
import com.capt.domain.profile.assignment.AssignmentModelsInteractor;
import com.capt.domain.profile.assignment.AssignmentsRepository;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.profile.assignments.base.BaseAssignmentsContract;
import com.capt.profile.assignments.base.BaseAssignmentsPresenter;

import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by root on 15.04.16.
 */
@Module
public class BaseAssignmentsModule {

    private BaseAssignmentsContract.View mView;
    private String mType;

    public BaseAssignmentsModule(BaseAssignmentsContract.View _view, String _type) {
        mView = _view;
        mType = _type;
    }

    @Provides
    @PerFragment
    protected String provideType() {
        return mType;
    }

    @Provides
    @PerFragment
    protected BaseAssignmentsContract.Presenter providePresenter(BaseAssignmentsPresenter _presenter) {
        return _presenter;
    }

    @Provides
    @PerFragment
    protected BaseAssignmentsContract.View provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    AssignmentsService provideAssignmentsService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                                 Retrofit _retrofit) {
        return _retrofit.create(AssignmentsService.class);
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.Mapper.ASSIGNMENTS_MAPPER)
    TypeMapper<List<Assignment>, List<Assignment>> provideAssignmentsMapper(AssignmentsMapper _mapper) {
        return _mapper;
    }

    @Provides
    @PerFragment
    AssignmentsRepository provideAssignmentsRepository(AssignmentsRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    protected BasePostInteractor<RequestParamsModel> provideAssignmentModelsInteractor(AssignmentModelsInteractor _interactor) {
        return _interactor;
    }
}
