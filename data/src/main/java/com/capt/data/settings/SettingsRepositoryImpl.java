package com.capt.data.settings;

import com.capt.data.authorization.repository.datasource.UserCache;
import com.capt.data.base.TypeMapper;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.profile.dashboard.model.DashboardModel;
import com.capt.domain.profile.dashboard.model.Marketplace;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.profile.settings.SettingsRepository;
import com.capt.domain.profile.settings.model.Account;
import com.capt.domain.profile.settings.model.Profile;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.profile.settings.model.Support;
import com.capt.domain.profile.settings.model.UpdateSettingsModel;
import com.capt.domain.video.model.Video;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;

/**
 * Created by root on 04.05.16.
 */
public class SettingsRepositoryImpl implements SettingsRepository {

    private SettingsService mService;
    private TypeMapper<SettingsModel, SettingsModel> mMapper;
    private UserCache mCache;

    @Inject
    public SettingsRepositoryImpl(UserCache _cache,
                                  @Named(Constants.NamedAnnotation.Mapper.SETTINGS_MAPPER)
                                  TypeMapper<SettingsModel, SettingsModel> _mapper,
                                  SettingsService _service) {
        mService = _service;
        mMapper = _mapper;
        mCache = _cache;
    }

    @Override
    public Observable<SettingsModel> getSettings() {
        return Observable.zip(
                mCache.getUser(),
                mService.getSettings(),
                (UserModel user, SettingsModel settings) -> {
                    settings.setSocial(user.isSocial());
                    return settings;
                })
                .map(mMapper::convert);
    }

    @Override
    public Observable<SettingsModel> updateSettings(UpdateSettingsModel _model) {
        return Observable.zip(
                mCache.getUser(),
                mService.updateSettings(_model),
                (UserModel user, SettingsModel settings) -> {
                    settings.setSocial(user.isSocial());
                    return settings;
                })
                .map(mMapper::convert);
    }
}
