package com.capt.domain.video.model;

import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.domain.category.model.CategoryModel;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.video.model.Quality;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by Svyd on 25.03.2016.
 */
public class Video implements DisplayableItem, Comparable<Video> {

    @Expose
    @SerializedName("_id")
    public String id;

    @Expose
    @SerializedName("title")
    public String title;

    @Expose
    @SerializedName("description")
    public String description;

    @Expose
    @SerializedName("price")
    public String price;

    @Expose
    @SerializedName("date")
    String date;

    @Expose
    @SerializedName("location")
    public double[] location;

    @Expose
    @SerializedName("country")
    public String country;

    @Expose
    @SerializedName("city")
    public String city;

    @Expose
    @SerializedName("categories")
    public List<CategoryModel> categories;

    @Expose
    @SerializedName("currency")
    public CurrencyModel currency;

    @Expose
    @SerializedName("duration")
    public String duration;

    @Expose
    @SerializedName("size")
    public String size;

    @Expose
    @SerializedName("thumbnailUrl")
    public String thumbnail;

    @Expose
    @SerializedName("state")
    public String state;

    public long startUploadTime;

    public String assignmentId;

    public long draftId = -1;

    public String formattedLocation;

    public String formattedSize;

    public Date date_upload;

    public String[] tags;

    @Expose
    public String path;

    @Expose
    public String sourcePath;

    private int isDraft;

    private int isUploading;

    private int error;

    private int isCurrentlyUploading;

    public Quality quality;

    public String license;

    public int progress;

    public void setDraftInt(int draft) {
        isDraft = draft;
    }

    public void setDraft(boolean draft) {
        isDraft = draft ? 1 : 0;
    }

    public void setCurrentlyUploading(boolean _uploading) {
        isCurrentlyUploading = _uploading ? 1 : 0;
    }

    public boolean isCurrentlyUploading() {
        return isCurrentlyUploading == 1;
    }

    public void setCurrentlyUploading(int _uploading) {
        isCurrentlyUploading = _uploading;
    }

    public int isCurrentlyUploadingInt() {
        return isCurrentlyUploading;
    }

    public void setUploading(boolean uploading) {
        isUploading = uploading ? 1 : 0;
    }

    public void setError(boolean _error) {
        error = _error ? 1 : 0;
    }

    public void setErrorInt(int _error) {
        error = _error;
    }

    public boolean isError() {
        return error == 1;
    }

    public int isErrorInt() {
        return error;
    }

    public void setUploadingInt(int uploading) {
        isUploading = uploading;
    }

    public boolean isDraft() {
        return isDraft == 1;
    }

    public boolean isUploading() {
        return isUploading == 1;
    }

    public int isUploadingInt() {
        return isUploading;
    }

    public int isDraftInt() {
        return isDraft;
    }

    @Override
    public String getHeaderTitle() {
        return "Recent Videos";
    }

    @Override
    public String getHeaderDescription() {
        return "VIEW ALL";
    }

    public String getDuration() {
        return duration;
    }

    public String getSize() {
        return size;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public String getBigThumbnail() {
        return thumbnail;
    }

    @Override
    public int compareTo(Video o) {
        return (int) (startUploadTime - o.startUploadTime);
    }
}
