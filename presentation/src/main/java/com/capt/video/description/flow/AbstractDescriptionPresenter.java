package com.capt.video.description.flow;

import android.location.Location;
import android.os.Bundle;

import com.capt.authorization.location.LocationInteractorManager;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.base.TimeUtil;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.description.DescriptionContract;
import com.capt.video.trim.time_util.TimeUtilImpl;

import rx.Observer;

/**
 * Created by Svyd on 01.05.2016.
 */
public abstract class AbstractDescriptionPresenter extends DescriptionContract.Presenter {

    private DescriptionContract.View mView;
    private LocationInteractorManager mLocationManager;
    private String mLocationName;
    private String[] mLocation;
    private TimeUtilImpl mTimeUtil;
    private Video mData;

    public AbstractDescriptionPresenter(DescriptionContract.View _view,
                                        LocationInteractorManager _manager,
                                        TimeUtilImpl _timeUtil,
                                        BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mLocationManager = _manager;
        mTimeUtil = _timeUtil;
    }

    @Override
    public void deleteVideo() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void initialize() {
        mView.setEditableTitle(true);
    }

    //    @Override
//    public void initialize() {
//        mView.setVideoView(mData.path);
//        mView.setVideoProperties(getFormattedProperties());
//        if (mLocationName != null)
//            mView.setLocationName(mLocationName);
//    }

    protected DescriptionContract.View getView() {
        return mView;
    }

    protected Video getData() {
        return mData;
    }

    protected TimeUtil getTimeUtil() {
        return mTimeUtil;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mLocationManager.unSubscribe();
    }

    @Override
    public void onPlayClick() {
        mView.playVideo();
    }

    @Override
    public void setLatLng(String[] latLng) {
        mLocation = latLng;
        mView.needToUpdateLocation(false);
        getPrediction(Double.parseDouble(mLocation[0]), Double.parseDouble(mLocation[1]));
    }

    @Override
    public void setData(Bundle _args) {
        if (_args.containsKey(Constants.BUNDLE_VIDEO)) {
            mData = (Video) _args.getSerializable(Constants.BUNDLE_VIDEO);
        } else throw new IllegalStateException("");
    }

    @Override
    public void onLocationChanged(Location _location) {
        if (mLocationName != null && mLocationName.trim().length() != 0)
            return;

        getPrediction(_location.getLatitude(), _location.getLongitude());
    }

    private void getPrediction(double _lat, double _lon) {
        mLocation = new String[]{String.valueOf(_lon),
                String.valueOf(_lat)};
        mLocationManager.execute(new LocationModel(_lat,
                _lon), new LocationObserver());
    }

    @Override
    public void navigateToMap() {
        mView.navigateToMapScreen();
    }

    @Override
    public void onNextClick() {
        if (validateInputFields()) {
            mData.title = mView.getTitle();
            mData.description = mView.getDescription();
            mData.location = new double[]{Double.parseDouble(mLocation[0]), Double.parseDouble(mLocation[1])};
            mData.formattedLocation = mLocationName;

            Bundle args = new Bundle();
            args.putSerializable(Constants.BUNDLE_VIDEO, mData);

            mView.navigateToSetPriceScreen(args);
        } else {
            mView.showValidateMessage();
        }
    }

    private boolean validateInputFields() {
        return !(mView.getTitle().isEmpty()
                || mView.getDescription().isEmpty()
                || mLocationName == null
                || mLocation == null);
    }

    @Override
    public void locationNameSelected(String _countryName) {
        if (_countryName == null || _countryName.equals(""))
            return;

        mLocationName = _countryName;
        mView.setLocationName(_countryName);
    }

    private class LocationObserver implements Observer<PredictionsModel> {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            getErrorHandler().onError(e);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            mData.country = predictionsModel.predictions[0].getCountry();
            String city = "Unnamed road";
            if (predictionsModel.predictions[0].getCity() != null) {
                city = predictionsModel.predictions[0].getCity();
            }
            mData.city = city;

            if (mLocationName == null || mLocationName.trim().length() == 0) {
                mLocationName = predictionsModel.predictions[0].address;
                locationNameSelected(mLocationName);
            }
        }
    }

}
