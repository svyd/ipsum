package com.capt.profile.marketplace.base.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.marketplace.base.BaseMarketPlaceListFragment;
import com.capt.profile.marketplace.base.dagger.MarketplaceListModule;

import dagger.Subcomponent;

/**
 * Created by root on 12.04.16.
 */
@PerFragment
@Subcomponent(modules = {MarketplaceListModule.class})
public interface BaseMarketPlaceListComponent {
	void inject(BaseMarketPlaceListFragment _fragment);
}
