package com.capt.domain.video.draft;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.draft.DraftRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 02.05.2016.
 */
public class DeleteDraftInteractor extends BasePostInteractor<String> {

    DraftRepository mRepository;

    @Inject
    public DeleteDraftInteractor(DraftRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.deleteDraft(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
