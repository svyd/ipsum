package com.capt.domain.profile.assignment.cache;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 07.07.2016.
 */
public class ClearAssignmentInteractor extends BaseInteractor {

    AssignmentCache mCache;

    @Inject
    public ClearAssignmentInteractor(AssignmentCache _cache, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mCache = _cache;
    }

    @Override
    protected Observable buildGetObserver() {
        return mCache.clear();
    }
}
