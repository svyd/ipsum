package com.capt.profile.dagger;

import com.capt.data.annotation.PerActivity;
import com.capt.base.dagger.BaseActivityComponent;
import com.capt.authorization.dagger.RestModule;
import com.capt.profile.ProfileActivity;
import com.capt.profile.assignments.base.dagger.BaseAssignmentsComponent;
import com.capt.profile.assignments.base.dagger.BaseAssignmentsModule;
import com.capt.profile.assignments.dagger.AssignmentsTabComponent;
import com.capt.profile.assignments.dagger.AssignmentsTabModule;
import com.capt.profile.assignments.details.capt_dialog.dagger.CaptDialogComponent;
import com.capt.profile.assignments.details.capt_dialog.dagger.CaptDialogModule;
import com.capt.profile.assignments.details.dagger.DetailsComponent;
import com.capt.profile.assignments.details.dagger.DetailsModule;
import com.capt.profile.assignments.details.overview.dagger.OverviewComponent;
import com.capt.profile.assignments.details.overview.dagger.OverviewModule;
import com.capt.profile.dashboard.dagger.DashboardComponent;
import com.capt.profile.dashboard.dagger.DashboardModule;
import com.capt.profile.gallery.dagger.GalleryComponent;
import com.capt.profile.gallery.dagger.GalleryModule;
import com.capt.profile.marketplace.base.dagger.MarketplaceListModule;
import com.capt.profile.marketplace.dagger.MarketPlaceComponent;
import com.capt.profile.marketplace.dagger.MarketPlaceModule;
import com.capt.profile.marketplace.base.dagger.BaseMarketPlaceListComponent;
import com.capt.profile.settings.SettingsFragment;
import com.capt.profile.settings.currency.dagger.CurrencySettingsComponent;
import com.capt.profile.settings.currency.dagger.CurrencySettingsModule;
import com.capt.profile.settings.dagger.AccountComponent;
import com.capt.profile.settings.dagger.AccountModule;
import com.capt.profile.settings.dagger.SettingsComponent;
import com.capt.profile.settings.dagger.SettingsModule;
import com.capt.profile.settings.notification.dagger.NotificationSettingsComponent;
import com.capt.profile.settings.notification.dagger.NotificationSettingsModule;
import com.capt.video.trim.dagger.FFMpegModule;

import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by Svyd on 25.03.2016.
 */
@PerActivity
@Subcomponent(modules = {ProfileActivityModule.class, RestModule.class})
public interface ProfileActivityComponent extends BaseActivityComponent{
    void inject(ProfileActivity _activity);
    DashboardComponent createDashboardComponent(DashboardModule _module);
    AssignmentsTabComponent createAssignmentsTabComponent(AssignmentsTabModule _module);
    BaseAssignmentsComponent createBaseAssignmentsComponent(BaseAssignmentsModule _module);
    SettingsComponent createSettingsComponent(SettingsModule _module);
}
