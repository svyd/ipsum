package com.capt.authorization.country_code;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ListView;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import com.capt.R;
import com.capt.authorization.country_code.dagger.CountryComponent;
import com.capt.authorization.country_code.dagger.CountryModule;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.dagger.RestComponent;
import com.capt.authorization.phone_number.PhoneNumberFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.BaseFragment;
import com.capt.domain.authorization.model.CountryModel;
import com.capt.domain.global.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

/**
 * Created by richi on 2016.03.16..
 */
public class CountryFragment extends BaseToolbarFragment
        implements CountryContract.View, SearchView.OnQueryTextListener {

    @Inject CountryAdapter mAdapter;
    @Inject CountryContract.Presenter mPresenter;

    @Bind(R.id.lvCounties_FCC) ListView llCountries;

    private CountryComponent mComponent;

    public static BaseFragment newInstance() {
        Bundle args = new Bundle();

        BaseFragment fragment = new CountryFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_country_code);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_country_fragment, menu);
        SearchView searchView = (SearchView) MenuItemCompat
                .getActionView(menu.findItem(R.id.svSearch_MCF));
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mPresenter.onSearchTextChanged(newText);
        return true;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeComponent();
        inject();
        setAdapter();
        setUpToolbar();
        mPresenter.initialize();
    }

    private void initializeComponent() {
        mComponent = getComponent(RestComponent.class)
                .createCountryComponent(new CountryModule(this));
    }

    private void inject() {
        mComponent.inject(this);
    }

    private void setAdapter() {
        llCountries.setAdapter(mAdapter);
    }

    private void setUpToolbar() {
        getActivity().setTitle("Country code");
    }

    @Override
    public void setData(List<CountryModel> _data) {
        mAdapter.setData(_data);
    }

    @Override
    public void showMessage(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onStart();
    }

    @Override
    public void onStop() {
        mPresenter.onStop();
        super.onStop();
    }

    @OnItemClick(R.id.lvCounties_FCC)
    protected void onItemClick(int _position) {
        Intent data = new Intent();
        data.putExtra(Constants.Extra.EXTRA_STRING, mAdapter.getItem(_position).getCode());

        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
        getFragmentNavigator().popBackStack();
    }
}
