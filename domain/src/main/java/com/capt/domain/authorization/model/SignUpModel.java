package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Svyd on 03.03.2016.
 */
public class SignUpModel implements Serializable {

    @Expose
    String email;

    @Expose
    String password;

    @Expose
    String fullName;

    @Expose
    String deviceId;

    @Expose
    String deviceToken;

    @Expose
    String[] location;

    String country;

    @Expose
    long birthday = -1;

    @Expose
    int gender = -1;

    @Expose
    final String provider = "GOOGLE";

    File avatar;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAvatar(File avatar) {
        this.avatar = avatar;
    }

    public File getAvatar() {
        if (avatar != null && avatar.exists()) {
            return avatar;
        }
        return null;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getProvider() {
        return provider;
    }

    public String[] getLocation() {
        return location;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setLocation(String[] location) {
        this.location = location;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getBirthday() {
        return birthday;
    }

    public void setBirthday(long birthday) {
        this.birthday = birthday;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public static class Builder {

        private SignUpModel mSignUpModel;

        public Builder() {
            mSignUpModel = new SignUpModel();
        }

        public Builder setEmail(String _email) {
            mSignUpModel.setEmail(_email);
            return this;
        }

        public Builder setFullName(String _fullName) {
            mSignUpModel.setFullName(_fullName);
            return this;
        }

        public Builder setPassword(String _password) {
            mSignUpModel.setPassword(_password);
            return this;
        }

        public Builder setAvatar(String _fileName) {
            if (_fileName == null || _fileName.length() == 0)
                return this;

            mSignUpModel.setAvatar(new File(_fileName));
            return this;
        }

        public Builder setCountry(String _country) {
            mSignUpModel.setCountry(_country);
            return this;
        }

        public Builder setDeviceId(String _deviceId) {
            mSignUpModel.setDeviceId(_deviceId);
            return this;
        }

        public Builder setDeviceToken(String _deviceToken) {
            mSignUpModel.setDeviceToken(_deviceToken);
            return this;
        }

        public Builder setLocation(String[] _location) {
            mSignUpModel.setLocation(_location);
            return this;
        }

        public Builder setBirthday(long _birthday) {
            mSignUpModel.setBirthday(_birthday);
            return this;
        }

        public Builder setGender(int _gender) {
            mSignUpModel.setGender(_gender);
            return this;
        }

        public SignUpModel build() {
            return mSignUpModel;
        }
    }
}