package com.capt.video.map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.base.BaseLocationFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.global.Constants;
import com.capt.video.dagger.VideoActivityComponent;
import com.capt.video.description.DescriptionFragment;
import com.capt.video.map.dagger.MapComponent;
import com.capt.video.map.dagger.MapModule;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

/**
 * Created by root on 05.04.16.
 */
public class MapFragment extends BaseFragment
        implements MapContract.View, OnMapReadyCallback, GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener {

    @Inject
    MapContract.Presenter mPresenter;
    @Inject
    ToolbarManager mToolbar;

    private MapComponent mComponent;
    private SupportMapFragment fragment;
    private GoogleMap mMap;
    private LocationModel locationModel;

    public static BaseFragment newInstance(LocationModel locationModel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, locationModel);
        MapFragment mapFragment = new MapFragment();
        mapFragment.setArguments(bundle);
        return mapFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_map);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        showToolbar();

        fragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_root, fragment);
        fragmentTransaction.commit();

//		FragmentManager fm = getChildFragmentManager();
//		fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
//		if (fragment == null) {
//			fragment = SupportMapFragment.newInstance();
//			fm.beginTransaction().replace(R.id.map, fragment).commit();
//		}

        fragment.getMapAsync(MapFragment.this);
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(VideoActivityComponent.class)
                .createMapComponent(new MapModule(this));
    }

    private void inject() {
        if (mToolbar != null)
            return;

        mComponent.inject(this);
    }

    private void showToolbar() {
        mToolbar.showToolbar();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();
    }

    private void setUpMap() {
        // Hide the zoom controls as the button panel will cover it.
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        // Set listener for marker events.
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);

        locationModel = (LocationModel) getArguments().getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);

        if ((locationModel != null) && (locationModel.getLat() != 0) && (locationModel.getLon() != 0)) {
            mPresenter.getAddress();
            addMarkerToMap(new LatLng(locationModel.getLat(), locationModel.getLon()));
        }
    }

    private void addMarkerToMap(final LatLng location) {
        mMap.clear();
        // Creates a draggable marker. Long press to drag.
        Marker currentPositionMarker = mMap.addMarker(new MarkerOptions()
                .position(location)
                .title("Current Location")
                .snippet("CAPT")
                .draggable(true));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14.0f));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        setLocation(marker.getPosition().latitude, marker.getPosition().longitude);
    }

    @Override
    public void navigateToDescriptionScreen(LocationModel _locationModel) {

        Intent data = new Intent();
        data.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _locationModel);

        getTargetFragment().onActivityResult(DescriptionFragment.LOCATION_REQUEST_CODE, Activity.RESULT_OK, data);
        getFragmentNavigator().popBackStack();
    }

    @Override
    public LocationModel getCurrentLocation() {
        return locationModel;
    }

    @Override
    public void setTitle(String title) {
        getActivity().setTitle(title);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;

            case R.id.action_next:
                mPresenter.navigateToDescription();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        setLocation(latLng.latitude, latLng.longitude);
        addMarkerToMap(latLng);
    }

    private void setLocation(double lat, double lng) {
        locationModel = new LocationModel(lat, lng);
        mPresenter.getAddress();
    }
}
