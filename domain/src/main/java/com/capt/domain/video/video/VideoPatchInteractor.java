package com.capt.domain.video.video;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.model.Video;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 02.05.2016.
 */
public class VideoPatchInteractor extends BasePostInteractor<Video> {

    private VideoRepository mRepository;

    @Inject
    public VideoPatchInteractor(VideoRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(Video _data) {
        return mRepository.patchVideo(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
