package com.capt.profile.settings.dagger;

import com.capt.base.BaseActivity;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.data.annotation.PerActivity;
import com.capt.profile.settings.AccountContract;
import com.capt.profile.settings.AccountPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 30.06.2016.
 */
@Module
public class AccountModule {

    BaseActivity mActivity;

    public AccountModule(BaseActivity _activity) {
        mActivity = _activity;
    }

    @Provides
    @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }

    @PerActivity
    @Provides
    protected BaseActivity provideBaseActivity() {
        return mActivity;
    }

    @Provides
    @PerActivity
    protected AccountContract.View provideView() {
        return ((AccountContract.View) mActivity);
    }

    @Provides
    @PerActivity
    protected AccountContract.Presenter providePresenter(AccountPresenter _presenter) {
        return _presenter;
    }
}
