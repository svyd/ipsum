package com.capt.video.camera.activity.dagger;

import com.capt.base.BaseActivity;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.data.annotation.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 07.04.2016.
 */
@Module
public class CameraActivityModule {
    private BaseActivity mBaseActivity;

    public CameraActivityModule(BaseActivity _activity) {
        mBaseActivity = _activity;
    }

    @Provides
    @PerActivity
    protected BaseActivity provideActivity() {
        return mBaseActivity;
    }

    @Provides
    @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }
}
