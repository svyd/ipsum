package com.capt.base;

import android.view.View;
import android.view.inputmethod.InputMethodManager;


import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by richi on 2016.03.20..
 */
@Singleton
public class KeyboardUtil {

    private InputMethodManager mManager;

    @Inject
    public KeyboardUtil(InputMethodManager _manager) {
        mManager = _manager;
    }

    public void hideKeyboard(View _view) {
        mManager.hideSoftInputFromWindow(_view.getWindowToken(), 0);
    }
}
