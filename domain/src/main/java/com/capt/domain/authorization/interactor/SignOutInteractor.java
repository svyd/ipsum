package com.capt.domain.authorization.interactor;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 07.03.2016.
 */
public class SignOutInteractor extends BaseInteractor {

    private AuthRepository mRepository;

    @Inject
    public SignOutInteractor(AuthRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildGetObserver() {
        return mRepository.signOut();
    }
}
