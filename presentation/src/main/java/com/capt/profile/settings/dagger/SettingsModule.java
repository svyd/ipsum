package com.capt.profile.settings.dagger;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.capt.data.annotation.PerActivity;
import com.capt.data.annotation.PerFragment;
import com.capt.data.base.TypeMapper;
import com.capt.data.settings.SettingsMapper;
import com.capt.data.settings.SettingsRepositoryImpl;
import com.capt.data.settings.SettingsService;
import com.capt.domain.authorization.interactor.SignOutInteractor;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.profile.settings.GetSettingsInteractor;
import com.capt.domain.profile.settings.SettingsRepository;
import com.capt.domain.profile.settings.model.Account;
import com.capt.domain.profile.settings.model.Profile;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.profile.settings.model.Support;
import com.capt.profile.dashboard.ui.adapter.StickyHeaderLayoutManager;
import com.capt.profile.settings.SettingsContract;
import com.capt.profile.settings.SettingsPresenter;
import com.capt.profile.settings.ui.SettingsAdapter;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Svyd on 20.04.2016.
 */

@Module
public class SettingsModule {

    private SettingsContract.View mView;

    public SettingsModule(SettingsContract.View _view) {
        mView = _view;
    }

    @Provides
    @PerFragment
    protected SettingsContract.View provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    SettingsContract.Presenter providePresenter(SettingsPresenter _presenter) {
        return _presenter;
    }

    @Provides
    @PerFragment
    protected RecyclerView.LayoutManager provideLeyoutManager(Context _context) {
        return new StickyHeaderLayoutManager();
    }

    @Provides
    @PerFragment
    protected StickyRecyclerHeadersDecoration provideDecoration(SettingsAdapter _adapter) {
        return new StickyRecyclerHeadersDecoration(_adapter);
    }

    @Provides
    @PerFragment
    SettingsService provideSettingsService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                           Retrofit _retrofit) {
        return _retrofit.create(SettingsService.class);
    }

    @Provides
    @PerFragment
    SettingsRepository provideSettingsRepository(SettingsRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.GET_SETTINGS_INTERACTOR)
    protected BaseInteractor provideGetSettingsInteractor(GetSettingsInteractor _interactor) {
        return _interactor;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.SIGN_OUT_INTERACTOR)
    protected BaseInteractor provideSignOutInteractor(SignOutInteractor _interactor) {
        return _interactor;
    }
}
