package com.capt.video.flow;

import android.os.Bundle;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.Video;
import com.capt.video.VideoContract;

import javax.inject.Inject;

/**
 * Created by Svyd on 15.05.2016.
 */
public abstract class AbstractVideoPresenter extends VideoContract.Presenter {

    private VideoContract.View mView;
    private Bundle mData;

    public AbstractVideoPresenter(VideoContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
    }

    protected VideoContract.View getView() {
        return mView;
    }

    protected void setData(Bundle _data) {
        mData = _data;
    }

    protected Bundle getData() {
        if (mData == null) {
            mData = new Bundle();
        }
        return mData;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
