package com.capt.data.base;

/**
 * Created by Svyd on 26.04.2016.
 */
public interface DateTimeUtility {
    String formatDate(String _date, boolean _withTime);
}
