package com.capt.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.capt.R;
import com.capt.application.dagger.HasComponent;
import com.capt.authorization.dagger.RestModule;
import com.capt.base.BaseActivity;
import com.capt.data.authorization.net.ApiConstants;
import com.capt.domain.global.Constants;
import com.capt.profile.assignments.AssignmentsTabFragment;
import com.capt.profile.custom_view.CenteredRadioButton;
import com.capt.profile.dagger.ProfileActivityComponent;
import com.capt.profile.dagger.ProfileActivityModule;
import com.capt.profile.dashboard.DashboardFragment;
import com.capt.profile.edit_profile.EditProfileActivity;
import com.capt.profile.gallery.GalleryFragment;
import com.capt.profile.settings.SettingsFragment;
import com.capt.video.Flow;
import com.capt.video.camera.activity.CameraActivity;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 22.03.2016.
 */
@RuntimePermissions
public class ProfileActivity extends BaseActivity
        implements HasComponent<ProfileActivityComponent>, RadioGroup.OnCheckedChangeListener, ProfileTabListener {

    @Bind(R.id.bottomBar_FP)
    RadioGroup rgMenu;

    @Bind(R.id.rbProfile_BB)
    CenteredRadioButton rbProfile;

    @Bind(R.id.rbUpload_BB)
    CenteredRadioButton rbUpload;

    @Bind(R.id.rbNotification_BB)
    CenteredRadioButton rbNotification;

    @Bind(R.id.rbSettings_BB)
    CenteredRadioButton rbSettings;

    @Bind(R.id.ivCamera_BB)
    ImageView ivCamera;

    @Bind(R.id.flDashboardContainer_AP)
    FrameLayout flDashboardContainer;

    @Bind(R.id.flGalleryContainer_AP)
    FrameLayout flGalleryContainer;

    @Bind(R.id.flAssignmentsContainer_AP)
    FrameLayout flAssignmentsContainer;

    @Bind(R.id.flSettingsContainer_AP)
    FrameLayout flSettingsContainer;

    @Inject
    Handler mHandler;

    private int mCheckedId = R.id.rbProfile_BB;

    private ProfileActivityComponent mComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initComponent();
        inject();
        ButterKnife.bind(this);
        initUi();
        if (savedInstanceState == null)
            initFragments();
        checkExtra(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.Extra.EXTRA_INT, mCheckedId);
    }

    private void initFragments(String _assignmentType) {
        Bundle galleryArgs = new Bundle();
        galleryArgs.putInt(Constants.FLOW, Flow.VIDEO_UPLOAD);

        Bundle dashboardArgs = new Bundle();
        dashboardArgs.putBoolean(Constants.FLAG_TO_RECENT, getIntent().getBooleanExtra(Constants.FLAG_TO_RECENT, false));

        getFragmentNavigator().addFragmentWithContainer(GalleryFragment.newInstance(galleryArgs), R.id.flGalleryContainer_AP);
        getFragmentNavigator().addFragmentWithContainer(AssignmentsTabFragment.newInstance(_assignmentType), R.id.flAssignmentsContainer_AP);
        getFragmentNavigator().addFragmentWithContainer(SettingsFragment.newInstance(), R.id.flSettingsContainer_AP);
        getFragmentNavigator().addFragmentWithContainer(DashboardFragment.newInstance(dashboardArgs), R.id.flDashboardContainer_AP);
    }

    private void initFragments() {
        initFragments(Constants.AssignmentTypes.FOR_GRABS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onFragmentResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && (requestCode == Constants.RequestCode.SHOULD_UPDATE
                || requestCode == EditProfileActivity.REMINDER_REQUEST_CODE)) {
            remainderOrUpdateResult();
        } else if (resultCode == RESULT_OK && requestCode == Constants.RequestCode.ASSIGNMENT) {
            assignmentResult(data);
        }
    }

    private void onFragmentResult(int requestCode, int resultCode, Intent data) {
        getFragmentNavigator().getTopFragment(R.id.flDashboardContainer_AP).onActivityResult(requestCode, resultCode, data);
        getFragmentNavigator().getTopFragment(R.id.flGalleryContainer_AP).onActivityResult(requestCode, resultCode, data);
        getFragmentNavigator().getTopFragment(R.id.flAssignmentsContainer_AP).onActivityResult(requestCode, resultCode, data);
        getFragmentNavigator().getTopFragment(R.id.flSettingsContainer_AP).onActivityResult(requestCode, resultCode, data);
    }

    private void remainderOrUpdateResult() {
        initFragments();
        navigateToDashboard();
        checkTab(0);
    }

    private void assignmentResult(Intent data) {
        if (data != null && data.hasExtra(Constants.Extra.EXTRA_STRING))
            initFragments(data.getStringExtra(Constants.Extra.EXTRA_STRING));
        else
            initFragments();

        navigateToAssignments();
    }

    private void checkExtra(Bundle _savedInstanceState) {
        if (getIntent().hasExtra(Constants.Extra.EXTRA_STRING))
            setUpVisibilityIfExtraExists();
        else
            setUpVisibilityIfStateSaved(_savedInstanceState);
    }

    private void setUpVisibilityIfExtraExists() {
        switch (getIntent().getStringExtra(Constants.Extra.EXTRA_STRING)) {
            case Constants.Profile.ASSIGNMENTS:
                navigateToAssignments();
                rbNotification.setChecked(true);
                break;
            case Constants.Profile.GALLERY:
                navigateToGallery();
                rbUpload.setChecked(true);
                break;
        }
    }

    private void setUpVisibilityIfStateSaved(Bundle _savedInstanceState) {
        if (_savedInstanceState == null) {
            navigateToDashboard();
            rbProfile.setChecked(true);
        } else {
            changeTab(_savedInstanceState.getInt(Constants.Extra.EXTRA_INT));
            checkTab(_savedInstanceState.getInt(Constants.Extra.EXTRA_INT));
        }
    }

    private void inject() {
        mComponent.inject(this);
    }

    private void initComponent() {
        mComponent = getApplicationComponent()
                .createProfileActivityComponent(new ProfileActivityModule(this), new RestModule(ApiConstants.GOOGLE_MAPS_API_ENDPOINT));
    }

    private void initUi() {
        rgMenu.setOnCheckedChangeListener(this);
    }

    @Override
    public int getContainerId() {
        return R.id.flDashboardContainer_AP;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AP;
    }

    @Override
    public ProfileActivityComponent getComponent() {
        return mComponent;
    }

    private void navigateToDashboard() {
        mHandler.post(() -> {
            getToolbarManager().hideToolbar();

            flDashboardContainer.setVisibility(View.VISIBLE);
            flGalleryContainer.setVisibility(View.INVISIBLE);
            flAssignmentsContainer.setVisibility(View.INVISIBLE);
            flSettingsContainer.setVisibility(View.INVISIBLE);
        });
    }

    private void navigateToGallery() {
        mHandler.post(() -> {
            getToolbarManager().showToolbar();
            setTitle("Upload video");

            flDashboardContainer.setVisibility(View.INVISIBLE);
            flGalleryContainer.setVisibility(View.VISIBLE);
            flAssignmentsContainer.setVisibility(View.INVISIBLE);
            flSettingsContainer.setVisibility(View.INVISIBLE);
        });
    }

    private void navigateToAssignments() {
        mHandler.post(() -> {
            getToolbarManager().showToolbar();
            setTitle("Assignments");

            flDashboardContainer.setVisibility(View.INVISIBLE);
            flGalleryContainer.setVisibility(View.INVISIBLE);
            flAssignmentsContainer.setVisibility(View.VISIBLE);
            flSettingsContainer.setVisibility(View.INVISIBLE);
        });
    }

    private void navigateToSettings() {
        mHandler.post(() -> {
            getToolbarManager().showToolbar();
            setTitle("Settings");

            flDashboardContainer.setVisibility(View.INVISIBLE);
            flGalleryContainer.setVisibility(View.INVISIBLE);
            flAssignmentsContainer.setVisibility(View.INVISIBLE);
            flSettingsContainer.setVisibility(View.VISIBLE);
        });
    }

    @OnClick(R.id.ivCamera_BB)
    protected void onCameraClick() {
        ProfileActivityPermissionsDispatcher.showCameraPermissionWithCheck(this);
    }

    private void startCameraActivity() {
        Intent intent = new Intent(ProfileActivity.this, CameraActivity.class);
        intent.putExtra(Constants.FLOW, Flow.VIDEO_UPLOAD);
        intent.putExtra("x", rgMenu.getX() + rgMenu.getWidth() / 2);
        intent.putExtra("y", rgMenu.getY() + ivCamera.getHeight() / 2 + getPixel(8));
        startActivity(intent);
    }

    private float getPixel(float _dimen) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                _dimen, getResources().getDisplayMetrics());
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    protected void showCameraPermission() {
        startCameraActivity();
    }

    @OnShowRationale(Manifest.permission.CAMERA)
    protected void showCameraRational(final PermissionRequest request) {
        new AlertDialog.Builder(this, R.style.CaptAlertDialogStyle)
                .setTitle(R.string.permission)
                .setMessage(R.string.camera_rational)
                .setPositiveButton(R.string.allow, (dialog, which) -> {
                    request.proceed();
                })
                .setNegativeButton(R.string.deny, (dialog, which) -> {
                    request.cancel();
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    protected void showCameraPermissionDenied() {
        Toast.makeText(this, R.string.camera_deny, Toast.LENGTH_SHORT)
                .show();
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    protected void showCameraPermissionNeverAsk() {
        Toast.makeText(this, R.string.camera_never_ask_again, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ProfileActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void changeTab(int checkedId) {
        mCheckedId = checkedId;
        switch (checkedId) {
            case R.id.rbProfile_BB:
                navigateToDashboard();
                break;
            case R.id.rbUpload_BB:
                navigateToGallery();
                break;
            case R.id.rbNotification_BB:
                navigateToAssignments();
                break;
            case R.id.rbSettings_BB:
                navigateToSettings();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        changeTab(checkedId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void checkTab(int _index) {
        switch (_index) {
            case 0:
                rbProfile.setChecked(true);
                break;
            case 1:
                rbUpload.setChecked(true);
                break;
            case 2:
                rbNotification.setChecked(true);
                break;
            case 3:
                rbSettings.setChecked(true);
        }
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }
}
