package com.capt.profile;

/**
 * Created by Svyd on 01.05.2016.
 */
public interface ProfileTabListener {
    void checkTab(int _index);
}
