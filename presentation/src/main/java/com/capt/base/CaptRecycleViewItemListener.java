package com.capt.base;

/**
 * Created by Svyd on 4/23/16.
 */
public interface CaptRecycleViewItemListener<T> {
    void onItemClick(T _data, int _position);
}
