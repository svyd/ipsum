package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by richi on 2016.03.16..
 */
public class CountryModel {

    @Expose
    private String name;

    @Expose
    private List<String> code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCodeList() {
        return code;
    }

    public String getCode() {
        String codeString = "";
        if (code != null && !code.isEmpty())
            codeString = code.get(0);

        return codeString;
    }

    public void setCode(List<String> code) {
        this.code = code;
    }
}
