package com.capt.data.currency;

import com.capt.domain.currency.CurrencyRepository;
import com.capt.domain.currency.model.CurrencyModel;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by root on 31.03.16.
 */
public class CurrencyRepositoryImpl implements CurrencyRepository {

	private CurrencyService mService;

	@Inject
	public CurrencyRepositoryImpl(CurrencyService _service) {

		mService = _service;
	}

	@Override public Observable<List<CurrencyModel>> getCurrencies() {
		return mService.getCurrencies();
	}
}
