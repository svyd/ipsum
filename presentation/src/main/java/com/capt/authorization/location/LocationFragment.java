package com.capt.authorization.location;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.capt.R;
import com.capt.authorization.dagger.RestComponent;
import com.capt.base.BaseLocationFragment;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.KeyboardUtil;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.location.dagger.LocationComponent;
import com.capt.authorization.location.dagger.LocationModule;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PredictionsModel;
import com.capt.domain.global.Constants;
import com.capt.profile.ProfileActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by Svyd on 16.03.2016.
 */
public class LocationFragment extends BaseLocationFragment implements LocationContract.LocationView,
        SearchView.OnQueryTextListener {

    @Inject LocationContract.LocationPresenter mPresenter;
    @Inject LocationListAdapter mAdapter;
    @Inject KeyboardUtil mKeyboardUtil;

    @Bind(R.id.lvCountries_FL) ListView lvCountries;
    @Bind(R.id.pbUpdateLocation_FL) ProgressBar pbUpdateLocation;

    private ProgressDialog mProgressDialog;

    private SearchView mSearchView;
    protected LocationComponent mComponent;

    public static LocationFragment newInstance(Bundle _args) {

        LocationFragment fragment = new LocationFragment();
        fragment.setArguments(_args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        super.onViewCreated(view, savedInstanceState);

    }

    private void initProgress() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Updating...");
        mProgressDialog.setCancelable(false);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        mKeyboardUtil.hideKeyboard(mSearchView);
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_location_fragment, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(item);
        mSearchView.setOnQueryTextListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setContentView(R.layout.fragment_location);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        needUpdateLocation(false);
        super.onActivityCreated(savedInstanceState);

        initializeInjector();
        inject();
        setUpToolbar();
        setAdapter();
        initProgress();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    private void initializeInjector() {
        if (mComponent != null)
            return;

        if (getArguments().containsKey(Constants.FLOW)) {

            int flow = getArguments().getInt(Constants.FLOW);

            mComponent = getComponent(RestComponent.class)
                    .provideLocationComponent(new LocationModule(this, flow));
        } else {
            throw new IllegalArgumentException(LocationFragment.class.getSimpleName() + " has to be instantiated with specified flow");
        }

    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void setAdapter() {
        lvCountries.setAdapter(mAdapter);
    }

    private void setUpToolbar() {
        getActivity().setTitle("Choose location");
    }

    @Override
    public void setAddressList(PredictionsModel _addresses) {
        mAdapter.setData(_addresses.predictions);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showToast(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void passActivityResult(LocationModel _model) {
        Intent data = new Intent();
        data.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _model);

        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
        getFragmentNavigator().popBackStack();
    }

    @Override
    public void finishWithResult(Intent _intent) {
        getActivity().setResult(Activity.RESULT_OK, _intent);
        getActivity().finish();
    }

    @Override
    public void navigateToBack() {
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
        getFragmentNavigator().popBackStack();
    }

    @Override
    public void doLocationUpdate() {
        updateLocation();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mPresenter.getAddressList(newText);
        return true;
    }

    @OnItemClick(R.id.lvCountries_FL)
    protected void onItemClick(int _position) {
        mPresenter.getAddressDetails(mAdapter.getItem(_position).reference);
    }

    @OnClick(R.id.rlHeaderContainer_FL)
    protected void onHeaderClick() {
        mPresenter.onHeaderClicked();
    }

    @Override
    public void onLocationChanged(Location location) {
        mPresenter.onLocationChanged(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void showProgress() {
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void showUpdateLocation() {
        pbUpdateLocation.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideUpdateLocation() {
        pbUpdateLocation.setVisibility(View.GONE);
    }
}
