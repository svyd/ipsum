package com.capt.profile.marketplace;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.capt.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Svyd on 26.05.2016.
 */
public class VideoHolder extends RecyclerView.ViewHolder {

    public View view;
    private ValueAnimator mProgressAnimator;
    private ErrorOptionsListener mListener;

    @Bind(R.id.ivThumbnail_LIV)
    ImageView ivThumbnail;

    @Bind(R.id.flQueue_LIV)
    FrameLayout flQueue;

    @Bind(R.id.tvName_LIV)
    TextView tvName;

    @Bind(R.id.tvDuration_LIV)
    TextView tvDuration;

    @Bind(R.id.tvPrice_LIV)
    TextView tvPrice;

    @Bind(R.id.tvDescription_LIV)
    TextView tvDescription;

    @Bind(R.id.cardView_LR)
    CardView cvContainer;

    @Bind(R.id.pbProgress_LIV)
    ProgressBar pbProgress;

    @Bind(R.id.rlDescription_LIV)
    RelativeLayout rlDescription;

    @Bind(R.id.flProgress_LIV)
    FrameLayout flProgress;

    @Bind(R.id.llErrorOptions_LIV)
    LinearLayout flErrorOptions;

    @Bind(R.id.flError)
    FrameLayout flError;

    @Bind(R.id.tvRetry_LIV)
    TextView tvRetry;

    @Bind(R.id.tvCancel_LIV)
    TextView tvCancel;

    @Bind(R.id.vOverlay_LIV)
    View vOverlay;

    private View mItemView;

    public VideoHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
        ButterKnife.bind(this, itemView);
        view = itemView;
    }

    @OnClick(R.id.tvCancel_LIV)
    void onCancelClick() {
        if (mListener != null) {
            mListener.onCancelClick(getLayoutPosition());
        }
    }

    @OnClick(R.id.tvRetry_LIV)
    void onRetryClick() {
        if (mListener != null) {
            mListener.onRetryClick(getLayoutPosition());
        }
    }

    public void setErrorOptionsListener(ErrorOptionsListener _listener) {
        mListener = _listener;
    }

    public View getView() {
        return mItemView;
    }

    private void animateProgressTo(int _to) {

        if (_to < pbProgress.getProgress()) {
            pbProgress.setProgress(_to);
            return;
        }

        if (mProgressAnimator != null && mProgressAnimator.isRunning()) {
            mProgressAnimator.cancel();
        }
        mProgressAnimator = ObjectAnimator.ofInt(pbProgress, "progress", pbProgress.getProgress(), _to);
        mProgressAnimator.setInterpolator(new LinearInterpolator());
        mProgressAnimator.setDuration(100);
        mProgressAnimator.start();
    }

    public void setInQueue() {
        vOverlay.setVisibility(View.VISIBLE);
        tvName.setText(R.string.uploading_your_video);
        flQueue.setVisibility(View.VISIBLE);
        flErrorOptions.setVisibility(View.GONE);
        flError.setVisibility(View.GONE);
        rlDescription.setVisibility(View.GONE);
        flProgress.setVisibility(View.GONE);
        cvContainer.setClickable(false);
        tvDuration.setText(R.string.zero_percent);
    }

    public void onError() {
        flErrorOptions.setVisibility(View.VISIBLE);
        flError.setVisibility(View.VISIBLE);
        rlDescription.setVisibility(View.GONE);
        flProgress.setVisibility(View.GONE);
        flQueue.setVisibility(View.GONE);
        cvContainer.setClickable(false);
        tvName.setText(R.string.upload_error_message);
    }

    public void setRemote() {
        rlDescription.setVisibility(View.VISIBLE);
        vOverlay.setVisibility(View.GONE);
        tvName.setText(R.string.uploading_your_video);
        flQueue.setVisibility(View.GONE);
        flErrorOptions.setVisibility(View.GONE);
        flError.setVisibility(View.GONE);
        flProgress.setVisibility(View.GONE);
    }

    public void onProgress(int _progress) {

        if (flProgress.getVisibility() != View.VISIBLE) {
            flProgress.setVisibility(View.VISIBLE);
            vOverlay.setVisibility(View.VISIBLE);
            flErrorOptions.setVisibility(View.GONE);
            rlDescription.setVisibility(View.GONE);
            flQueue.setVisibility(View.GONE);
            flError.setVisibility(View.GONE);
            tvName.setText(R.string.uploading_your_video);
            cvContainer.setClickable(false);
        }
        tvDuration.setText(_progress + "%");
        animateProgressTo(_progress);
    }

}