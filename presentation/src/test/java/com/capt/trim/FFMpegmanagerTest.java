//package com.capt.trim;
//
//import com.capt.BuildConfig;
//import com.capt.TestCaptApplication;
//import com.capt.trim.mock_module.MockFFMpegModule;
//import com.capt.trim.mock_module.TestTrimModule;
//import com.capt.video.trim.ffmpeg_command_builder.FFMPEGCommandBuilder;
//import com.capt.video.trim.ffmpeg_manager.FFMPEGManager;
//import com.capt.video.trim.dagger.DaggerFFMpegComponent;
//import com.capt.video.trim.dagger.FFMpegComponent;
//import com.capt.video.trim.file_manager.FileManager;
//import com.capt.video.trim.time_util.TimeUtil;
////import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.robolectric.RobolectricGradleTestRunner;
//import org.robolectric.RuntimeEnvironment;
//import org.robolectric.annotation.Config;
//
//import java.io.File;
//
///**
// * Created by richi on 2016.02.29..
// */
//@RunWith(RobolectricGradleTestRunner.class)
//@Config(constants = BuildConfig.class,
//        application = TestCaptApplication.class)
//public class FFMpegmanagerTest {
//
//    protected FFMPEGManager mFFmpegManager;
//
//    @Mock FileManager mMockManager;
//    @Mock TimeUtil mMockTimeUtil;
//    @Mock FFMPEGCommandBuilder mMockCommand;
////    @Mock FFmpeg mMockFFMPEG;
//
//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//
//        Mockito.when(mMockManager.getTrimPath(Mockito.anyString())).thenReturn(new File("Test"));
//
//        TestCaptApplication app = (TestCaptApplication) RuntimeEnvironment.application;
////        app.setTrimModule(new TestTrimModule(mMockFFMPEG));
//
//        FFMpegComponent component = DaggerFFMpegComponent.builder()
//                .appComponent(app.getAppComponent())
//                .fFMpegModule(new MockFFMpegModule(mMockTimeUtil,
//                        mMockManager, mMockCommand))
//                .build();
//
//        mFFmpegManager = component.ffmpegManager();
//    }
//
//    @Test
//    public void testFFMpegManagerInteractionWithFileManager() {
//        mFFmpegManager.trimVideo(3, 8, "test", new FFMPEGManager.SimpleFFMPEGCommandCallback());
//        Mockito.verify(mMockManager).getTrimPath(Mockito.anyString());
//    }
//
//    @Test
//    public void testFFMpegManagerInteractionWithTimeUtil() {
//        Mockito.when(mMockCommand.getTrimVideoCommand(Mockito.anyInt(),
//                Mockito.anyInt(), Mockito.anyString(), Mockito.anyString()))
//                .thenReturn("ffmpeg -i movie.mp4 -ss 00:00:03 -t 00:00:08 -async 1 cut.mp4");
//
//        mFFmpegManager.trimVideo(3, 8, "test", new FFMPEGManager.SimpleFFMPEGCommandCallback());
//        Mockito.verify(mMockCommand).getTrimVideoCommand(Mockito.anyInt(),
//                Mockito.anyInt(), Mockito.anyString(), Mockito.anyString());
//    }
//}
