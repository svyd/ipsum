package com.capt.video.trim.design.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;

/**
 * Created by Svyd on 06.04.2016.
 */
public class TrimmingFrameView extends View {

    static final int LEFT = -1;
    static final int RIGHT = 1;
    private Paint mOpacityPaint, mDragPaint, mStrokePaint, mPlayPaint, mPlayOpacityPaint;
    private boolean dragAvailable = false;
    private boolean mIsPlaying, mSeekDragDisable;
    private boolean isScrolling;
    float sideWidth;
    float xLeft = 50;
    float xRight = 300;
    float mLastX;
    float minWidth = 150;
    int mode = 0;
    int widthDisplay;
    float mWidth, mHeight, mTop, mBottom;
    float mStrokeWidth;
    float mMaxWidth;
    float mTouchArea;
    float mPlayValue, mRadius;
    float mPlayWidth;
    private RectF mPlayRect;

    private GestureDetector mDetector;
    private Scroller mScroller;

    private OnTrimListener mListener;

    private int mDuration = 20 * 1000;
    private ValueAnimator mPlayAnimator, mScrollerAnimator, mPlayAlphaAnimator;

    public TrimmingFrameView(Context _context) {
        this(_context, null);
    }

    public TrimmingFrameView(Context context, AttributeSet attrs) {
        super(context, attrs);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        widthDisplay = metrics.widthPixels;

        initGestureHelpers();
        initDimensions();
        initPaints();

    }

    public boolean isScrolling() {
        return isScrolling;
    }

    private void initGestureHelpers() {
        mDetector = new GestureDetector(getContext(), mGestureListener);
        mScroller = new Scroller(getContext(), new AccelerateInterpolator(), true);
    }

    private void initDimensions() {
        mStrokeWidth = getPixel(3);

        mRadius = getPixel(5);
        mPlayWidth = getPixel(8);

        sideWidth = getPixel(20);

        xLeft = getPixel(20);
        xRight = widthDisplay - getPixel(20);

        mTop = getPixel(5);

        mTouchArea = getPixel(10);
        mMaxWidth = widthDisplay;
    }

    private void initPaints() {
        mOpacityPaint = new Paint();
        mOpacityPaint.setAntiAlias(true);
        mOpacityPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mOpacityPaint.setStrokeWidth(mStrokeWidth);
        mOpacityPaint.setColor(Color.argb(200, 0, 0, 0));

        mDragPaint = new Paint();
        mDragPaint.setAntiAlias(true);
        mDragPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mDragPaint.setStrokeWidth(mStrokeWidth);
        mDragPaint.setColor(Color.argb(200, 255, 0, 0));

        mStrokePaint = new Paint();
        mStrokePaint.setAntiAlias(true);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeWidth(mStrokeWidth);
        mStrokePaint.setColor(Color.argb(200, 255, 0, 0));

        mPlayPaint = new Paint();
        mPlayPaint.setAntiAlias(true);
        mPlayPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPlayPaint.setColor(Color.argb(200, 0, 255, 0));

        mPlayOpacityPaint = new Paint();
        mPlayOpacityPaint.setAntiAlias(true);
        mPlayOpacityPaint.setStyle(Paint.Style.FILL);
        mPlayOpacityPaint.setColor(Color.argb(170, 0, 0, 0));
    }

    public void setTrimListener(OnTrimListener _listener) {
        mListener = _listener;
    }

    public void setMaxWidth(float _maxWidth) {
        mMaxWidth = _maxWidth;
        xLeft = (mWidth / 2) - (_maxWidth / 2);
        xRight = xLeft + (_maxWidth / 2);
        if (mListener != null) {
            mListener.onLeftBoundChanged(xLeft);
            mListener.onRightBoundChanged(xRight);
        }
    }

    public void setMinWidth(float _minWidth) {
        new Handler().post(() -> {
            minWidth = _minWidth;
            xLeft = (mWidth / 2) - sideWidth - (_minWidth / 2);
            xRight = xLeft + _minWidth + sideWidth;
            if (mListener != null) {
                mListener.onLeftBoundChanged(xLeft + sideWidth);
                mListener.onRightBoundChanged(xRight);
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent _event) {
        return mDetector.onTouchEvent(_event);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        mTop = 0;
        mBottom = mHeight;
//        mBottom = mHeight - mTop;

        initPlayRect();
    }

    private void initPlayRect() {
        mPlayRect = new RectF(mPlayValue, 0, mPlayWidth + mPlayValue, mHeight);
    }

    private float getPixel(float _dimen) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                _dimen, getResources().getDisplayMetrics());
    }

    private GestureDetector.OnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onDown(MotionEvent _event) {
            if ((_event.getX() > (xLeft - mTouchArea))
                    && (_event.getX() < (xLeft + sideWidth + mTouchArea))) {
                dragAvailable = true;
                mLastX = _event.getX();
                mode = LEFT;
            } else if ((_event.getX() > (xRight - mTouchArea))
                    && (_event.getX() < (xRight + sideWidth + mTouchArea))) {
                dragAvailable = true;
                mLastX = _event.getX();
                mode = RIGHT;
            } else {
                dragAvailable = false;
            }

            if (_event.getX() < mPlayValue + mPlayWidth + mTouchArea
                    && _event.getX() > mPlayValue - mTouchArea) {

            }

            return dragAvailable;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            dragAvailable = false;
            mLastX = 0;
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (!dragAvailable)
                return false;

            stopPlaying();

            if (mode == RIGHT)
                onMoveRight(-distanceX);
            else if (mode == LEFT)
                onMoveLeft(-distanceX);

            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (!dragAvailable)
                return false;

            stopPlaying();
            isScrolling = true;
            mScroller.forceFinished(true);

            if (mode == RIGHT)
                flingRight(velocityX);
            else if (mode == LEFT)
                flingLeft(velocityX);

            return true;
        }
    };

    private void flingLeft(float velocityX) {
        int minX = (int) (xRight - (mMaxWidth + sideWidth) < 0
                ? 0 : xRight - (mMaxWidth + sideWidth));
        mScroller.fling((int) xLeft, 0, (int) (velocityX / 2), 0,
                minX, (int) (xRight - (minWidth + sideWidth)), 0, 0);

        startAnimation(animation -> {
            if (!isScrollerFinished()) {
                xLeft = mScroller.getCurrX();
                if (mListener != null) {
                    mListener.onLeftBoundChanged(xLeft + sideWidth);
                }
                invalidate();
            } else {
                animation.cancel();
            }
        });
    }

    private void flingRight(float velocityX) {
        int maxX = (int) (xLeft + sideWidth + mMaxWidth > widthDisplay - sideWidth
                ? widthDisplay - sideWidth : xLeft + sideWidth + mMaxWidth);
        mScroller.fling((int) xRight, 0, (int) (velocityX / 2), 0,
                ((int) (xLeft + sideWidth + minWidth)), maxX, 0, 0);

        startAnimation(animation -> {
            if (!isScrollerFinished()) {
                xRight = mScroller.getCurrX();
                if (mListener != null) {
                    mListener.onRightBoundChanged(xRight);
                }
                invalidate();
            } else {
                animation.cancel();
            }
        });
    }

    private boolean isScrollerFinished() {
        if (!mScroller.isFinished()) {
            mScroller.computeScrollOffset();
            return false;
        } else
            return true;
    }

    private void startAnimation(ValueAnimator.AnimatorUpdateListener _listener) {
        if (mScrollerAnimator != null && mScrollerAnimator.isRunning())
            mScrollerAnimator.cancel();

        mScrollerAnimator = ValueAnimator.ofFloat(0, 1);
        mScrollerAnimator.setDuration(mScroller.getDuration());
        mScrollerAnimator.addUpdateListener(_listener);
        mScrollerAnimator.addListener(new OnAnimationEndListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                isScrolling = false;
                if (mListener != null)
                    mListener.onScrollFinished();
            }
        });
        mScrollerAnimator.start();
    }

    private void onMoveLeft(float _distance) {
        xLeft += _distance;
        if (xLeft < 0)
            xLeft = 0;

        if (xRight - (xLeft + sideWidth) < minWidth)
            xLeft = xRight - (minWidth + sideWidth);

        if (xRight - (xLeft + sideWidth) > mMaxWidth)
            xLeft = xRight - (mMaxWidth + sideWidth);

        if (mListener != null) {
            mListener.onLeftBoundChanged(xLeft + sideWidth);
        }

        invalidate();
    }

    private void onMoveRight(float _distance) {
        xRight += _distance;
        if (xRight > widthDisplay - sideWidth)
            xRight = widthDisplay - sideWidth;

        if (xRight - (xLeft + sideWidth) < minWidth)
            xRight = xLeft + minWidth + sideWidth;

        if (xRight - (xLeft + sideWidth) > mMaxWidth)
            xRight = xLeft + sideWidth + mMaxWidth;

        if (mListener != null) {
            mListener.onRightBoundChanged(xRight);
        }

        invalidate();
    }

    public void stopPlaying() {
        fadePlayBar(200, 0);
        mPlayAlphaAnimator.addListener(new OnAnimationEndListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mIsPlaying = false;
                invalidate();
                animation.removeAllListeners();
            }
        });
        if (mPlayAnimator != null) {
            mPlayAnimator.cancel();
        }
    }

    public void onPlayClick(float _position, int _duration) {
        mIsPlaying = true;
        mPlayValue = _position + sideWidth;
        mDuration = _duration;

        startPlay();
    }

    private void fadePlayBar(final int _from, final int _to) {
        if (mPlayAlphaAnimator != null
                && mPlayAlphaAnimator.isRunning()) {
            mPlayAlphaAnimator.cancel();
        }

        mPlayAlphaAnimator = ValueAnimator.ofInt(_from, _to);
        mPlayAlphaAnimator.setDuration(200);
        mPlayAlphaAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mPlayAlphaAnimator.addUpdateListener(animation -> {
            int alpha = (int) animation.getAnimatedValue();
            mPlayPaint.setAlpha(alpha);
            if (alpha <= 170) {
                mPlayOpacityPaint.setAlpha(alpha);
            }
            invalidate();
        });
        mPlayAlphaAnimator.start();
    }

    private void startPlay() {

        fadePlayBar(0, 200);

        if (mPlayAnimator != null && mPlayAnimator.isRunning()) {
            mPlayAnimator.cancel();
        }

        mPlayAnimator = ValueAnimator.ofFloat(mPlayValue, xRight - mPlayWidth);
        mPlayAnimator.setInterpolator(new LinearInterpolator());
        mPlayAnimator.setDuration(mDuration);
        mPlayAnimator.addListener(new OnAnimationEndListener() {

            @Override
            public void onAnimationCancel(Animator animation) {
                animation.removeAllListeners();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (mListener != null) {
                    mListener.onVideoEnd();
                }
                stopPlaying();
            }
        });
        mPlayAnimator.addUpdateListener(animation -> {
            mPlayValue = (float) animation.getAnimatedValue();
            mPlayRect.left = mPlayValue;
            mPlayRect.right = mPlayValue + mPlayWidth;
            invalidate();
        });
        mPlayAnimator.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(0, mTop, xLeft - mStrokeWidth, mBottom, mOpacityPaint); // draw left dark rectangle
        canvas.drawRect(xRight + sideWidth + mStrokeWidth, mTop, widthDisplay, mBottom, mOpacityPaint); // draw right dark rectangle

        if (mIsPlaying)
            canvas.drawRect(xLeft + sideWidth, mTop, mPlayValue, mBottom, mPlayOpacityPaint);

        canvas.drawRect(xLeft, mTop, xLeft + sideWidth, mBottom, mDragPaint); // draw rectangle
        canvas.drawRect(xRight, mTop, xRight + sideWidth, mBottom, mDragPaint); // draw other rectangle
        canvas.drawRect(xLeft + sideWidth, mTop, xRight, mBottom, mStrokePaint);

        if (mIsPlaying)
            canvas.drawRect(mPlayRect, mPlayPaint);
    }

    public interface OnTrimListener {
        void onLeftBoundChanged(float _leftBound);

        void onRightBoundChanged(float _rightBound);

        void onVideoEnd();

        void onScrollFinished();
    }
}