package com.capt.data.authorization.net.retrofit;

import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.location.model.PlaceModel;
import com.capt.domain.authorization.location.model.PredictionsModel;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Svyd on 17.03.2016.
 */
public interface PlacesService {

    @GET("maps/api/place/autocomplete/json?")
    Observable<PredictionsModel> getPlacesPredictions(@Query("input") String _input, @Query("key") String _key);

    @GET("maps/api/place/details/json?")
    Observable<LocationModel> getPlaceDetails(@Query("reference") String _reference, @Query("key") String _key);
}
