package com.capt.domain.profile.settings.model;

import com.capt.domain.profile.dashboard.model.base.BaseItems;

/**
 * Created by Svyd on 20.04.2016.
 */
public class Support extends BaseItems {
    @Override
    public String getHeaderTitle() {
        return "Support";
    }

    @Override
    public String getHeaderDescription() {
        return "";
    }
}
