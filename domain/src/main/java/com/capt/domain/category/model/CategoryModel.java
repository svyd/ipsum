package com.capt.domain.category.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Svyd on 22.04.2016.
 */
public class CategoryModel implements Serializable {

    @Expose
    @SerializedName("_id")
    public String id;

    @Expose
    @SerializedName("name")
    public String name;
}
