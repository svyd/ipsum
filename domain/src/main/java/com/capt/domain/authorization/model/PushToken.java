package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 21.06.2016.
 */
public class PushToken {

    @Expose
    @SerializedName("deviceToken")
    String token;

    public PushToken(String _token) {
        token = _token;
    }
}
