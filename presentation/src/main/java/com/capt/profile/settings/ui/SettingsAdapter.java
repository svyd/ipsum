package com.capt.profile.settings.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.capt.R;
import com.capt.data.annotation.PerFragment;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.profile.dashboard.ui.adapter.NonRecycleAdapter;
import com.capt.profile.dashboard.ui.listener.OnNonRecycleItemClickListener;
import com.capt.profile.dashboard.ui.listener.OnDelegateItemClickListener;
import com.capt.profile.dashboard.ui.view.NonRecycleList;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 20.04.2016.
 */
@PerFragment
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.SettingsViewHolder> implements OnNonRecycleItemClickListener, StickyRecyclerHeadersAdapter {

    private List<BaseItems> mItems;
    private LayoutInflater mInflater;
    private String mLastHeader;
    private int mLastHeaderIndex;
    private OnDelegateItemClickListener mListener;

    @Inject
    public SettingsAdapter(LayoutInflater _inflater) {
        mInflater = _inflater;
    }

    @Override
    public SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        NonRecycleList itemView = (NonRecycleList) mInflater.inflate(R.layout.non_recycle_list, parent, false);
        itemView.setOnItemClickListener(this);
        return new SettingsViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(SettingsViewHolder holder, int position) {
        BaseItems settings = mItems.get(position);
        NonRecycleAdapter adapter = new NonRecycleAdapter(mInflater);
//        adapter.setData(settings);
        holder.mList.setAdapter(adapter);
        holder.mList.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onClick(int position, NonRecycleList _list) {
        mListener.onDelegateClick((int)_list.getTag(), position);
    }

    @Override
    public long getHeaderId(int position) {
//        if (mItems.get(position).getHeaderTitle().equals(mLastHeader)) {
//            return mLastHeaderIndex;
//        }
//        mLastHeaderIndex = position;
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_sticky_header, parent, false);
        return new RecyclerView.ViewHolder(view) {
        };
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        String headerTitle = mItems.get(position).getHeaderTitle();
        TextView left = (TextView) holder.itemView.findViewById(R.id.tvLeft_LISH);
        TextView right = (TextView) holder.itemView.findViewById(R.id.tvRightLISH);
        left.setText(headerTitle);
        right.setText(mItems.get(position).getHeaderDescription());

        mLastHeader = headerTitle;
    }

    public void setItems(List<BaseItems> _items) {
        mItems = _items;
    }

    public void setOnItemClickListener(OnDelegateItemClickListener _listener) {
        mListener = _listener;
    }

    public class SettingsViewHolder extends RecyclerView.ViewHolder {

        public NonRecycleList mList;

        public SettingsViewHolder(View itemView, OnNonRecycleItemClickListener _listener) {
            super(itemView);
            mList = (NonRecycleList) itemView.findViewById(R.id.nonRecycleList);
            mList.setOnItemClickListener(_listener);
        }
    }
}
