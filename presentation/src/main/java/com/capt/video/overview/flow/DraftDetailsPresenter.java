package com.capt.video.overview.flow;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;
import com.capt.video.Flow;
import com.capt.video.overview.OverviewContract;
import com.capt.video.trim.time_util.TimeUtilImpl;
import com.capt.video.upload_manager.VideoUploadManager;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 02.05.2016.
 */
public class DraftDetailsPresenter extends AbstractPresenter {

    private static final String TAG = DraftDetailsPresenter.class.getSimpleName();
    private Video mData;
    private TimeUtilImpl mTimeUtil;
    private BasePostInteractor<String> mDiscardInteractor;

    @Inject
    public DraftDetailsPresenter(BasePostInteractor<String> _deleteInteractor,
                                 VideoUploadManager _manager,
                                 SharedPreferences _preferences,
                                 TimeUtilImpl _timeUtil,
                                 OverviewContract.View _view, BaseExceptionDelegate _delegate,
                                 @Named(Constants.PendingVideoConstants.PAY_PAL_INTERACTOR)
                                 BasePostInteractor<PayPalVideoModel> _payPalInteractor) {
        super(_preferences, _manager, _view, _delegate, _payPalInteractor);
        mTimeUtil = _timeUtil;
        mDiscardInteractor = _deleteInteractor;
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().setEditableTitle(false);
        getView().setDetailsBtnVisibility(false);
        getView().setVideoPlayability(false);
        getView().setButtonsVisibility(true);
        getView().setWarningVisibility(false);
        getView().setListVisibility(false);
        getView().setTitle("Draft");
        getView().showDescription(true);
        getView().setCompletedVisibility(false);
        getView().setTextBtnTitle("DISCARD");
        getView().setPrice(mData.currency.getSymbol() + mData.price);
        getView().setVideoDescription(mData.description);
        getView().setLocation(mData.country + ", " + mData.city);
        getView().setVideoTitle(mData.title);
        getView().setVideoProperties(getDraftProperties(Long.parseLong(mData.duration), mData.path));

        getView().setCategories(mData.categories);
        getView().setThumbnail(mData.path);
    }

    @Override
    public void setData(Bundle _model) {
        if (_model.containsKey(Constants.BUNDLE_VIDEO)) {
            mData = (Video) _model.getSerializable(Constants.BUNDLE_VIDEO);
        } else {
            throw new IllegalStateException("Arguments should contain Video");
        }
    }

    @Override
    protected Video getVideo() {
        return mData;
    }

    @Override
    public void onTextBtnClick() {
        mDiscardInteractor.execute(String.valueOf(mData.draftId), new DiscardObserver(this));
    }

    /**
     * Override the super method to execute the draft discard first, but only if PayPal is authorized
     */

    @Override
    public void uploadVideo() {
        mDiscardInteractor.execute(String.valueOf(mData.draftId), new UploadAndDiscardObserver(this));
    }

    @Override
    protected void onVideoUpload() {
        getView().finishWithResult(false);
    }

    private String getDraftProperties(long _duration, String _path) {
        File file = new File(_path);
        long size = file.length();

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    @Override
    public void onStop() {
        mDiscardInteractor.unSubscribe();
    }

    @Override
    public void onActionEdit() {
        Bundle args = new Bundle();
        args.putSerializable(Constants.BUNDLE_VIDEO, mData);
        args.putInt(Constants.FLOW, Flow.DRAFT);
        getView().navigateToDraftEdit(args);
    }

    /**
     * Invoke the super method to actually upload the video
     */

    private void superUploadVideo() {
        super.uploadVideo();
    }

    class UploadAndDiscardObserver extends BaseObserver<Integer> {
        public UploadAndDiscardObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(Integer integer) {
            superUploadVideo();
        }
    }

    class DiscardObserver extends BaseObserver<Integer> {
        public DiscardObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(Integer integer) {
            getView().finishWithResult(true);
            Log.d(TAG, "onNext() called with: " + "integer = [" + integer + "]");
        }
    }

}
