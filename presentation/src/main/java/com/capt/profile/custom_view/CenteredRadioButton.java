package com.capt.profile.custom_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.RadioButton;

import com.capt.R;

/**
 * Created by Svyd on 06.04.2016.
 */
public class CenteredRadioButton extends RadioButton {


    private Drawable mDrawable;

    public CenteredRadioButton(Context context) {
        super(context);
    }
    public CenteredRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CenteredRadioButton, 0, 0);
        mDrawable = a.getDrawable(R.styleable.CenteredRadioButton_drawable);
        setButtonDrawable(android.R.color.transparent);
        a.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mDrawable != null) {
            mDrawable.setState(getDrawableState());
            final int verticalGravity = getGravity() & Gravity.VERTICAL_GRAVITY_MASK;
            final int height = mDrawable.getIntrinsicHeight();

            int y = 0;

            switch (verticalGravity) {
                case Gravity.BOTTOM:
                    y = getHeight() - height;
                    break;
                case Gravity.CENTER_VERTICAL:
                    y = (getHeight() - height) / 2;
                    break;
            }

            int buttonWidth = mDrawable.getIntrinsicWidth();
            int buttonLeft = (getWidth() - buttonWidth) / 2;
            mDrawable.setBounds(buttonLeft, y, buttonLeft+buttonWidth, y + height);
            mDrawable.draw(canvas);
        }
    }

}
