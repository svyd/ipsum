package com.capt.domain.category;

import com.capt.domain.category.model.CategoryModel;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 22.04.2016.
 */
public interface VideoCategoryRepository {
    Observable<List<CategoryModel>>  getCategories();
}
