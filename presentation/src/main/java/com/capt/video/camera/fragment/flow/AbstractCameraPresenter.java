package com.capt.video.camera.fragment.flow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.OrientationEventListener;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.video.camera.BaseAdapter;
import com.capt.video.camera.custom_view.morphing_btn.MorphingButton;
import com.capt.video.camera.fragment.CameraContract;
import com.capt.video.trim.file_manager.FileManager;
import com.capt.video.trim.view_presenter.TrimContract;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Svyd on 13.05.2016.
 */
public abstract class AbstractCameraPresenter extends CameraContract.Presenter {

    private final int mDuration = 500;
    private final int MIN_DURATION = 10000;

    private Orientation mCurrentOrientation;
    private CameraContract.View mView;
    private BaseAdapter mTopAdapter, mBottomAdapter;
    private Camera.Parameters mParameters;
    private FileManager mFileManager;
    private String mPath;
    private List<Camera.Size> mCameraSizes;
    private boolean mOrientationCanBeChanged = true;
    private boolean mFlashAvailable;
    private boolean mMinDurationReached;
    private Handler mHandler;
    private boolean mIsInitialized;

    private boolean mIsRecording;

    public AbstractCameraPresenter(Handler _handler,
                                   FileManager _manager,
                                   CameraContract.View _view,
                                   @Named(Constants.NamedAnnotation.TOP_ADAPTER) BaseAdapter _topAdapter,
                                   @Named(Constants.NamedAnnotation.BOTTOM_ADAPTER) BaseAdapter _bottomAdapter,
                                   BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mFileManager = _manager;
        mTopAdapter = _topAdapter;
        mBottomAdapter = _bottomAdapter;
        mPath = mFileManager.getVideoPath(String.valueOf(System.currentTimeMillis())).getAbsolutePath() + ".mp4";
        mCameraSizes = new ArrayList<>();
        mHandler = _handler;
    }

    @Override
    public void setArgs(Bundle _args) {

    }

    @Override
    public void initialize() {

    }

    @Override
    public void onCameraPrepared() {
        mView.hidePlaceholder();
    }

    @Override
    public void onStart() {
        if (mOrientationListener.canDetectOrientation()) {
            mOrientationListener.enable();
        }
        morphToCircle(0);
    }

    private void checkOrientation(Orientation _orientation) {
        if (_orientation == Orientation.LANDSCAPE) {
            mView.showRightBar();
            mView.showMorphBtn();
        } else {
            mView.hideRightBar();
            mView.hideMorphBtn();
        }
    }

    @Override
    public void onStop() {
        mOrientationListener.disable();
        mHandler.removeCallbacks(mStopRecordRunnable);
        mHandler.removeCallbacks(mMinDurationRunnable);
        mIsRecording = false;
        mIsInitialized = false;
    }

    private Runnable mStopRecordRunnable = this::stopRecording;
    private Runnable mMinDurationRunnable = () -> mMinDurationReached = true;

    private OrientationEventListener mOrientationListener =
            new OrientationEventListener(CaptApplication.getApplication(), 1000000) {
                @Override
                public void onOrientationChanged(int _orientation) {
                    Orientation orientation;
                    Log.d("TAG", "onOrientationChanged: " + _orientation);
                    if ((_orientation < 45 || _orientation >= 315 ||
                            _orientation > 135 && _orientation <= 225) && _orientation != -1) {
                        orientation = Orientation.PORTRAIT;
                    } else
                        orientation = Orientation.LANDSCAPE;

                    if (!mIsInitialized) {
                        checkOrientation(orientation);
                        mIsInitialized = true;
                    }

                    if (orientation == mCurrentOrientation) {
                        return;
                    }

                    if (mOrientationCanBeChanged) {
                        if (orientation == Orientation.LANDSCAPE) {
                            animateToLandscape(_orientation);
                            mCurrentOrientation = Orientation.LANDSCAPE;
                        } else {
                            animateToPortrait();
                            mCurrentOrientation = Orientation.PORTRAIT;
                        }
                    }
                }
            };

    private void animateToLandscape(int _orientation) {
        mOrientationCanBeChanged = false;
        mView.hideText();
        mView.animateOverlayViewToLandscape(_orientation, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mOrientationCanBeChanged = true;
                if (!mIsRecording)
                    mView.showRightBar();
                mView.showMorphBtn();
            }
        });
    }

    private void animateToPortrait() {
        mOrientationCanBeChanged = false;
        mView.hideRightBar();
        mView.hideMorphBtn();
        mView.animateOverlayViewToPortrait(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mOrientationCanBeChanged = true;
                mView.showText();
            }
        });
    }

    @SuppressWarnings("Convert2streamapi")
    private void setUpBottomSettings() {

        List<String> sizeLabels = new ArrayList<>();

        for (Camera.Size size : mCameraSizes) {

            String sizeString = "" + size.width + "x" + size.height;
            sizeLabels.add(sizeString);
        }

        mBottomAdapter.setItems(sizeLabels);
        mView.notifyBottomSettings();
        mBottomAdapter.notifyDataChanged();

    }

    @Override
    public void onSaveInstanceState(Bundle _state) {
        _state.putString(Constants.BUNDLE_VIDEO_PATH, mPath);
    }

    @Override
    public void onRestoreInstanceState(Bundle _state) {
        if (_state != null && _state.containsKey(Constants.BUNDLE_VIDEO_PATH)) {
            mPath = _state.getString(Constants.BUNDLE_VIDEO_PATH, mPath);
        }
    }

    @Override
    public void onFlashAvailable(boolean _available) {
        mFlashAvailable = _available;
        if (!_available) {
            mView.hideBottomSettings();
        } else {
            mView.showBottomSettings();
            mView.notifyTopSettings();
            mTopAdapter.notifyDataChanged();
        }
    }

    @Override
    public void setCameraSizes(List<Camera.Size> _sizes) {
        mCameraSizes = _sizes;
        setUpBottomSettings();
    }

    @Override
    public void onCloseClick() {
        mView.close();
    }

    @Override
    public void onMorphingBtnClick() {
        if (mIsRecording) {
            onStopRecord();
        } else {
            onStartRecord();
        }
    }

    private void onStartRecord() {
        mHandler.postDelayed(
                mMinDurationRunnable,
                MIN_DURATION
        );
        morphToSquare(mDuration);
        startRecording();
        mIsRecording = true;
    }

    private void onStopRecord() {
        if (mMinDurationReached) {
            mIsRecording = false;
            mMinDurationReached = false;
            morphToCircle(mDuration);
            mHandler.postDelayed(mStopRecordRunnable, mDuration);
        } else {
            mView.showDurationToast(String.valueOf(MIN_DURATION / 1000));
        }
    }

    protected String getPath() {
        return mPath;
    }

    protected CameraContract.View getView() {
        return mView;
    }

    private void morphToSquare(int _duration) {
        MorphingButton.Params square = MorphingButton.Params.create()
                .duration(_duration)
                .cornerRadius((int) CaptApplication.getApplication().getResources().getDimension((R.dimen.mb_corner_radius_2)))
                .width((int) CaptApplication.getApplication().getResources().getDimension((R.dimen.mb_width)))
                .height((int) CaptApplication.getApplication().getResources().getDimension((R.dimen.mb_height)))
                .color(ContextCompat.getColor(CaptApplication.getApplication(), R.color.mb_red))
                .colorPressed(ContextCompat.getColor(CaptApplication.getApplication(), R.color.mb_red_dark));

        mView.morphingTo(square);
    }

    protected abstract void stopRecording();

    private void morphToCircle(int _duration) {
        MorphingButton.Params circle = MorphingButton.Params.create()
                .duration(_duration)
                .cornerRadius((int) CaptApplication.getApplication().getResources().getDimension((R.dimen.mb_radius)))
                .width((int) CaptApplication.getApplication().getResources().getDimension((R.dimen.mb_radius)))
                .height((int) CaptApplication.getApplication().getResources().getDimension((R.dimen.mb_radius)))
                .color(ContextCompat.getColor(CaptApplication.getApplication(), R.color.mb_red))
                .colorPressed(ContextCompat.getColor(CaptApplication.getApplication(), R.color.mb_red_dark));

        mView.morphingTo(circle);
    }

    private void startRecording() {
        mView.hideRightBar();
        mView.record(mPath);
    }

    @Override
    public void onTopMainClick() {
        mView.hideBottomSettings();
        mView.hideMorphBtn();
    }

    @Override
    public void onBottomMainClick() {
        mView.hideTopSettings();
        mView.hideMorphBtn();
    }

    @Override
    public void onTopClick(int _position) {
        mView.showMorphBtn();
        if (mFlashAvailable) {
            mView.showBottomSettings();
        }
        switch (_position) {
            case 0:
                mView.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                break;
            case 1:
                mView.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                break;
            case 2:
                mView.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                break;
        }
    }

    @Override
    public void onBottomClick(int _position) {
        mView.showMorphBtn();
        mView.showTopSettings();

        mView.setVideoSize(mCameraSizes.get(_position));
    }

    enum Orientation {
        PORTRAIT, LANDSCAPE
    }
}
