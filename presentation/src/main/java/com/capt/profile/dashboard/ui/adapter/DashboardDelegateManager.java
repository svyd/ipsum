package com.capt.profile.dashboard.ui.adapter;

import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.domain.global.Constants;
import com.capt.profile.dashboard.ui.delegate.base.BaseDelegate;
import com.capt.profile.dashboard.ui.listener.OnDelegateItemClickListener;
import com.hannesdorfmann.adapterdelegates.AdapterDelegatesManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Svyd on 28.03.2016.
 */
public class DashboardDelegateManager extends AdapterDelegatesManager<List<DisplayableItem>> {

    List<BaseDelegate> mDelegates;

    @Inject
    public DashboardDelegateManager(
            @Named(Constants.NamedAnnotation.Delegate.ASSIGNMENTS_DELEGATE)
            BaseDelegate _assignments,
            @Named(Constants.NamedAnnotation.Delegate.REMINDER_DELEGATE)
            BaseDelegate _reminder,
            @Named(Constants.NamedAnnotation.Delegate.MARKETPLACE_DELEGATE)
            BaseDelegate _marketplace,
            @Named(Constants.NamedAnnotation.Delegate.VIDEO_DELEGATE)
            BaseDelegate _video) {
        mDelegates = new ArrayList<>();
        mDelegates.add(_assignments);
        mDelegates.add(_marketplace);
        mDelegates.add(_video);
        mDelegates.add(_reminder);

        addDelegates();
    }

    public int getTypeCount() {
        return mDelegates.size();
    }

    public void setOnDelegateItemClickListener(OnDelegateItemClickListener _listener) {
        for (BaseDelegate delegate: mDelegates) {
            delegate.setDelegateItemClickListener(_listener);
        }
    }

    private void addDelegates() {
        for (BaseDelegate delegate: mDelegates) {
            addDelegate(delegate);
        }
    }
}
