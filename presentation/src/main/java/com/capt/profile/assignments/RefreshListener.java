package com.capt.profile.assignments;

/**
 * Created by Svyd on 12.07.2016.
 */
public interface RefreshListener {
    void refresh();
}
