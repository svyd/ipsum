package com.capt.video.camera.fragment;

import android.animation.Animator;
import android.hardware.Camera;
import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.video.camera.custom_view.morphing_btn.MorphingButton;

import java.util.List;

/**
 * Created by richi on 2016.03.28..
 */
public interface CameraContract {

    interface View extends BaseView {
        void showDurationToast(String _text);
        void close();
        void showText();
        void hideText();
        void morphingTo(MorphingButton.Params _params);
        void record(String _path);
        void stopRecording();
        void showRightBar();
        void hideRightBar();
        void animateOverlayViewToPortrait(Animator.AnimatorListener _listener);
        void animateOverlayViewToLandscape(int _orientation, Animator.AnimatorListener _listener);
        void showMorphBtn();
        void hideMorphBtn();
        void notifyBottomSettings();
        void notifyTopSettings();
        void hideTopSettings();
        void hideBottomSettings();
        void showBottomSettings();
        void showTopSettings();
        void hidePlaceholder();
        void setVideoSize(Camera.Size _size);
        void setFlashMode(String _mode);
        void startTrimFragment(String _path);
        void startAssignmentFlow(Assignment _assignment);
    }

    abstract class Presenter extends BasePresenter<CameraContract.View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setArgs(Bundle _args);
        public abstract void onSaveInstanceState(Bundle _state);
        public abstract void onRestoreInstanceState(Bundle _state);
        public abstract void onFlashAvailable(boolean _available);
        public abstract void setCameraSizes(List<Camera.Size> _sizes);
        public abstract void onCameraPrepared();
        public abstract void onCloseClick();
        public abstract void onMorphingBtnClick();
        public abstract void onTopMainClick();
        public abstract void onBottomMainClick();
        public abstract void onTopClick(int _position);
        public abstract void onBottomClick(int _position);
    }
}
