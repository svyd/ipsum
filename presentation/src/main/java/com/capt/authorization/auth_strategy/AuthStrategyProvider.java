package com.capt.authorization.auth_strategy;

import com.capt.data.annotation.PerFragment;
import com.capt.domain.global.Constants;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Lazy;

/**
 * Created by richi on 2016.03.10..
 */
@PerFragment
public class AuthStrategyProvider {

    protected Lazy<AuthorizationStrategy> mFacebookStrategy;
    protected Lazy<AuthorizationStrategy> mTwitterStrategy;
    protected Lazy<AuthorizationStrategy> mInstagramStrategy;

    @Inject
    public AuthStrategyProvider(@Named(Constants.AuthType.FACEBOOK) Lazy<AuthorizationStrategy> _facebookStrategy,
                                @Named(Constants.AuthType.TWITTER) Lazy<AuthorizationStrategy> _twitter,
                                @Named(Constants.AuthType.INSTAGRAM) Lazy<AuthorizationStrategy> _instagram) {
        mFacebookStrategy = _facebookStrategy;
        mTwitterStrategy = _twitter;
        mInstagramStrategy = _instagram;
    }

    public AuthorizationStrategy provideFacebookStrategy() {
        return mFacebookStrategy.get();
    }

    public AuthorizationStrategy provideTwitterStrategy() {
        return mTwitterStrategy.get();
    }

    public AuthorizationStrategy provideInstagramStrategy() {
        return mInstagramStrategy.get();
    }
}
