package com.capt.domain.profile.marketplace.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 04.04.2016.
 */
public class MarketplaceModel {

    @Expose
    @SerializedName("payouts")
    public int payouts;

    @Expose
    @SerializedName("recent")
    public int recent;

    @Expose
    @SerializedName("sold")
    public int sold;

    public int drafts;

}
