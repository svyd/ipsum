package com.capt.video.trim.dagger;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.capt.data.annotation.PerFragment;
import com.capt.video.Flow;
import com.capt.video.trim.view_presenter.TrimContract;
import com.capt.video.trim.view_presenter.flow.AssignmentTrimPresenter;
import com.capt.video.trim.view_presenter.flow.TrimPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 17.05.2016.
 */
@Module
public class TrimModule {
    private TrimContract.View mView;
    private int mFlow;

    public TrimModule(TrimContract.View _view, int _flow) {
        mView = _view;
        mFlow = _flow;
    }

    @Provides
    @PerFragment
    protected TrimContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    protected TrimContract.Presenter providePresenter(TrimPresenter _trimPresenter,
                                                      AssignmentTrimPresenter _assignmentPresenter) {
        switch (mFlow) {
            case Flow.CAPT_ASSIGNMENT:
                return _assignmentPresenter;
            default:
                return _trimPresenter;
        }
    }

    @Provides
    @PerFragment
    protected LinearLayoutManager provideLinearLayoutManager(Context _context) {
        LinearLayoutManager manager = new LinearLayoutManager(_context);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return manager;
    }
}
