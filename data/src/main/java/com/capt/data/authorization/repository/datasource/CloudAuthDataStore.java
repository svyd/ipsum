package com.capt.data.authorization.repository.datasource;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.capt.data.authorization.net.retrofit.AuthService;
import com.capt.data.cookies.PersistentCookieStore;
import com.capt.domain.authorization.model.CodeModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.PasswordModel;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.PushToken;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.authorization.model.SignUpModel;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UpdateAvatarResponse;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.google.gson.Gson;

import java.io.File;
import java.net.CookieManager;
import java.net.CookieStore;
import java.util.Arrays;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Svyd on 02.03.2016.
 */
public class CloudAuthDataStore implements AuthDataStore {

    private final AuthService mRestClient;
    private SharedPreferences mPreferences;
    private CookieStore mCookieStore;
    private Gson mGson;

    public CloudAuthDataStore(Gson _gson,
                              SharedPreferences _preferences,
                              AuthService _api,
                              CookieStore _store) {
        mRestClient = _api;
        mCookieStore = _store;
        mPreferences = _preferences;
        mGson = _gson;
    }

    @Override
    public Observable<SuccessModel> getAuthCode(PhoneNumberModel _model) {
        return mRestClient.getAuthenticationCode(_model);
    }

    @Override
    public Observable<UserModel> verifyCodeAndSignUp(CodeSignUpModel _code) {

        File file = _code.getSignUpModel().getAvatar();
        RequestBody avatar;
        if (file != null) {
            avatar = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        } else {
            avatar = null;
        }
        RequestBody email = RequestBody.create(MediaType.parse("multipart/form-data"), _code.getSignUpModel().getEmail());
        RequestBody password = RequestBody.create(MediaType.parse("multipart/form-data"), _code.getSignUpModel().getPassword());
        RequestBody fullName = RequestBody.create(MediaType.parse("multipart/form-data"), _code.getSignUpModel().getFullName());
        RequestBody deviceToken = RequestBody.create(MediaType.parse("multipart/form-data"), _code.getSignUpModel().getDeviceToken());
        RequestBody location = RequestBody.create(MediaType.parse("multipart/form-data"), Arrays.toString(_code.getSignUpModel().getLocation()));
        RequestBody provider = RequestBody.create(MediaType.parse("multipart/form-data"), _code.getSignUpModel().getProvider());
        RequestBody deviceId = RequestBody.create(MediaType.parse("multipart/form-data"), _code.getSignUpModel().getProvider());

        return mRestClient
                .verifyCode(_code.getCodeModel())
                .flatMap(success -> mRestClient.signUp(avatar, email, password, fullName, deviceToken,
                        location, provider, deviceId))
                .flatMap(model -> mRestClient.getProfile());
//                .flatMap(this::saveUser);
    }

    @Override
    public Observable<UserModel> verifyCodeAndGetProfile(CodeSignUpModel _data) {
        return mRestClient
                .verifyCode(_data.getCodeModel())
                .flatMap(successModel -> mRestClient.getProfile());
    }

    @Override
    public Observable<UserModel> signInWithSocNet(SocNetAuthModel _model) {
        return mRestClient.signInWithSocNet(_model.getNetworkType(), _model);
//                .flatMap(this::saveUser);
    }


    @Override
    public Observable<UserModel> signIn(SignInModel _model) {
        return mRestClient.signIn(_model);
//                .flatMap(this::saveUser);
    }

    @Override
    public Observable<SuccessModel> signOut() {
        return mRestClient.signOut();
    }

    @Override
    public Observable<SuccessModel<String>> checkEmail(String _email) {
        return mRestClient.checkEmail(_email)
                .map((Func1<? super SuccessModel, ? extends SuccessModel<String>>) successModel -> {
                    successModel.mModel = _email;
                    return successModel;
                });
    }

    @Override
    public Observable<UserModel> updateUserModel(UserModel _data) {
        File avatarFile = null;
        RequestBody avatar = null;
        RequestBody country = null;
        RequestBody city = null;

        String phone = mGson.toJson(_data.getPhone());

        String file = _data.getAvatarUrl();
        if (file != null)
            avatarFile = new File(file);

        if (avatarFile != null && avatarFile.exists()) {
            avatar = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
        }

        RequestBody email = RequestBody.create(MediaType.parse("multipart/form-data"), _data.getEmail());
        RequestBody fullName = RequestBody.create(MediaType.parse("multipart/form-data"), _data.getName());
        RequestBody location = RequestBody.create(MediaType.parse("multipart/form-data"), Arrays.toString(_data.getLocation()));
        RequestBody phoneBody = RequestBody.create(MediaType.parse("multipart/form-data"), phone);

        if (!TextUtils.isEmpty(_data.getCountry()))
            country = RequestBody.create(MediaType.parse("multipart/form-data"), _data.getCountry());

        if (!TextUtils.isEmpty(_data.getCity()))
            city = RequestBody.create(MediaType.parse("multipart/form-data"), _data.getCity());

        return mRestClient.updateUser(avatar, email, fullName, location, country, city, phoneBody);
    }

    @Override
    public Observable<SuccessModel> updatePushToken(String _data) {
        return mRestClient.updatePushToken(new PushToken(_data));
    }

    @Override
    public Observable<SuccessModel> changePassword(PasswordModel _password) {
        return mRestClient.changePassword(_password);
    }
}
