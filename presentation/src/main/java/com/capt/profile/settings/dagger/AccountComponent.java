package com.capt.profile.settings.dagger;

import com.capt.authorization.dagger.RestComponent;
import com.capt.base.BaseActivity;
import com.capt.base.dagger.BaseActivityComponent;
import com.capt.data.annotation.PerActivity;
import com.capt.profile.settings.AccountSettingsActivity;
import com.capt.profile.settings.currency.dagger.CurrencySettingsComponent;
import com.capt.profile.settings.currency.dagger.CurrencySettingsModule;
import com.capt.profile.settings.notification.dagger.NotificationSettingsComponent;
import com.capt.profile.settings.notification.dagger.NotificationSettingsModule;

import dagger.Subcomponent;

/**
 * Created by Svyd on 30.06.2016.
 */
@PerActivity
@Subcomponent(modules = {AccountModule.class})
public interface AccountComponent extends BaseActivityComponent {
    void inject(AccountSettingsActivity _activity);

    NotificationSettingsComponent createNotificationSettingsComponent (NotificationSettingsModule _module);
    CurrencySettingsComponent createCurrencySettingsComponent (CurrencySettingsModule _module);
}
