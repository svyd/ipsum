package com.capt.profile.settings.notification.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.settings.notification.NotificationSettingsFragment;

import dagger.Subcomponent;

/**
 * Created by root on 29.04.16.
 */
@PerFragment
@Subcomponent(modules = {NotificationSettingsModule.class})
public interface NotificationSettingsComponent {
	void inject (NotificationSettingsFragment _fragment);
}
