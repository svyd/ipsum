package com.capt.controller.delegate_exception;

import java.io.IOException;

/**
 * Created by Svyd on 7/28/16.
 */
public class IOExceptionDelegate extends BaseExceptionDelegate<IOException> {

    public IOExceptionDelegate(BaseExceptionDelegate _delegate) {
        mDelegate = _delegate;
    }

    @Override
    public void handleException(IOException _exception) {
        mBaseView.hideProgress();
        mBaseView.showNoInternetConnectionError();
    }
}
