package com.capt.domain.authorization.interactor;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 03.03.2016.
 */
public class CodeInteractor extends BasePostInteractor<PhoneNumberModel> {

    private AuthRepository mRepository;

    @Inject
    public CodeInteractor(AuthRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(PhoneNumberModel _data) {
        return mRepository.getAuthCode(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
