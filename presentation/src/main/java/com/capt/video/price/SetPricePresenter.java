package com.capt.video.price;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;

import com.capt.R;
import com.capt.application.CaptApplication;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.currency.model.CurrencyModel;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.video.model.Video;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by root on 25.03.16.
 */
public class SetPricePresenter extends SetPriceContract.Presenter {

    private SetPriceContract.View mView;
    private BaseInteractor mInteractor;
    private Video videoUploadModel;
    private String mDefaultCurrency = "";

    @Inject
    public SetPricePresenter(SetPriceContract.View _view,
                             @Named(Constants.NamedAnnotation.GET_SETTINGS_INTERACTOR)
                             BaseInteractor _interactor, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mView = _view;
        mInteractor = _interactor;
    }

    @Override
    public void navigateToCurrencySelection() {
        mView.navigateToCurrencyScreen();
    }

    @Override
    public void navigateToCategory() {
        if (validateInputFields()) {
            videoUploadModel.currency = mView.getSelectedCurrency();
            videoUploadModel.price = mView.getPrice();
            Bundle args = new Bundle();
            args.putSerializable(Constants.BUNDLE_VIDEO, videoUploadModel);
            mView.navigateToCategoryScreen(args);
        } else {
            mView.showValidateMessage();
        }
    }

    private boolean validateInputFields() {
        return !(mView.getPrice().isEmpty()
                || mView.getSelectedCurrency() == null);
    }

    @Override
    public void setVideoUploadModel(Video _model) {
        videoUploadModel = _model;
    }

    @Override
    public void initialize() {
        if (videoUploadModel.price != null) {
            mView.setPrice(videoUploadModel.price);
            mView.setCurrency(videoUploadModel.currency);
        } else {
            mInteractor.execute(new Subscriber(this));
        }
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onPriceChanged(CharSequence _price) {
        if (TextUtils.isEmpty(_price.toString().trim())) {
            mView.renderRealPrice("");
            return;
        }

        double price = getRealPrice(_price);
        String priceDescription = CaptApplication.getApplication()
                .getString(R.string.real_price_FSP, String.format("%.2f", price), mDefaultCurrency);
        mView.renderRealPrice(priceDescription);
    }

    private double getRealPrice(CharSequence _price) {
        double price = Double.parseDouble(_price.toString());
        double captMargin = (price * Constants.Payment.CAPT_MARGIN) / 100;
        double captServiceFee = ((captMargin + price) * Constants.Payment.CAPT_FEE) / 100;

        return price + captMargin + captServiceFee;
    }

    private void showCurrencyOnView(CurrencyModel currencyModel) {
        mView.showCurrency(currencyModel);
    }

    private class Subscriber extends BaseObserver<SettingsModel> {
        public Subscriber(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onNext(SettingsModel _settings) {
            mDefaultCurrency = _settings.defaultCurrency.getCurrencyISO();
            showCurrencyOnView(_settings.defaultCurrency);
        }
    }
}
