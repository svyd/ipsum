package com.capt.domain.authorization.interactor;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.model.PasswordModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 24.06.2016.
 */
public class ChangePasswordInteractor extends BasePostInteractor<PasswordModel> {

    AuthRepository mRepository;

    @Inject
    public ChangePasswordInteractor(AuthRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);

        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(PasswordModel _data) {
        return mRepository.changePassword(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
