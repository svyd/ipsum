package com.capt.data.authorization.repository.country;

import com.capt.data.authorization.net.retrofit.CountryService;
import com.capt.data.authorization.repository.country.cache.CountryCache;
import com.capt.data.authorization.repository.country.data_store.CloudCountryDataStore;
import com.capt.data.authorization.repository.country.data_store.CountryDataStore;
import com.capt.data.authorization.repository.country.data_store.MemoryCountryDataStore;

import javax.inject.Inject;

/**
 * Created by richi on 2016.03.16..
 */
public class CountryDataStoreFactory {

    private CountryCache mCache;
    private CountryService mService;

    @Inject
    public CountryDataStoreFactory(CountryCache _cache, CountryService _service) {
        mCache = _cache;
        mService = _service;
    }

    public CountryDataStore createStore() {
        if (mCache.isCachedInMemory())
            return new MemoryCountryDataStore(mCache);
        else
            return new CloudCountryDataStore(mService);
    }
}
