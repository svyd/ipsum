package com.capt.domain.video.draft;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.model.RequestParamsModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 25.04.2016.
 */
public class GetAllDraftInteractor extends BasePostInteractor<RequestParamsModel> {

    private DraftRepository mDataSource;

    public GetAllDraftInteractor(DraftRepository _source, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mDataSource = _source;
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }

    @Override
    protected Observable buildPostObservable(RequestParamsModel _data) {
        return mDataSource.getAll();
    }
}
