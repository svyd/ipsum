package com.capt.authorization.sign_up;

import android.location.Location;
import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.SignUpModel;

/**
 * Created by richi on 2016.03.18..
 */
public interface SignUpContract {

    interface View extends BaseView {
        String getPassword();
        String getConfirmedPassword();
        void startLocationFragment();
        void startPhoneNumberFragment(CodeSignUpModel _signUpModel);
        void showMessage(String _message);
        void takePhoto();
        void enableNextBtn(boolean _isEnable);
        void loadAvatarFrom(String _path);
        void setLocationName(String _name);
        void showEmailIsUsed(boolean _show);
        void showPassword();
        void hidePassword();
        void showConfirmPassword();
        void hideConfirmPassword();
        void needToUpdateLocation(boolean _isNeeded);
        void showDatePickerDialog(int _year, int _month, int _day);
        void renderBirthday(String _year, String _month, String _day);
    }

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setLatLng(String[] latLng);
        public abstract void onNextClick(SignUpModel.Builder _userModel);
        public abstract void onValidateCredentials(SignUpModel _model);
        public abstract void onEmailTextChanged(String _email);
        public abstract void onBirthdayClicked();
        public abstract void onBirthdayChanged(int _year, int _month, int _day);
        public abstract void onGenderChanged(int _status);
        public abstract void onLocationChooseClick();
        public abstract void onSaveInstanceState(Bundle _bundle);
        public abstract void onRestoreInstanceState(Bundle _bundle);
        public abstract void locationNameSelected(String _countryName);
        public abstract void setPhotoPath(String _photoPath);
        public abstract void onEyeClick();
        public abstract void onConfirmEyeClick();
        public abstract void onCameraClick();
        public abstract void onLocationChanged(Location _location);
    }
}
