package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by Svyd on 03.03.2016.
 */
public class PhoneNumberModel implements Serializable  {
    @Expose
    @SerializedName("number")
    String phone;

    @Expose
    @SerializedName("code")
    String code;

    public String getCode() {
        return code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public PhoneNumberModel(String _code, String _phone) {
        code = _code;
        phone = _phone;
    }

    public static class Builder implements Serializable {
        private String mCode;
        private String mNumber;

        public Builder setCode(String _string) {
            mCode = _string;
            return this;
        }

        public Builder setNumber(String _number) {
            mNumber = _number;
            return this;
        }

        public String getCode() {
            return mCode;
        }

        public PhoneNumberModel build() {
            return new PhoneNumberModel(mCode, mNumber);
        }
    }
}
