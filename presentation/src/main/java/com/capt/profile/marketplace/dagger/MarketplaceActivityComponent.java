package com.capt.profile.marketplace.dagger;

import com.capt.base.BaseActivity;
import com.capt.base.dagger.BaseActivityComponent;
import com.capt.data.annotation.PerActivity;
import com.capt.profile.marketplace.MarketplaceActivity;
import com.capt.profile.marketplace.base.dagger.BaseMarketPlaceListComponent;
import com.capt.profile.marketplace.base.dagger.MarketplaceListModule;

import dagger.Subcomponent;

/**
 * Created by Svyd on 03.06.2016.
 */
@PerActivity
@Subcomponent(modules = {MarketplaceActivityModule.class})
public interface MarketplaceActivityComponent extends BaseActivityComponent {
    MarketPlaceComponent createMarketPlaceComponent(MarketPlaceModule _module);
    BaseMarketPlaceListComponent createBaseMarketPlaceListComponent(MarketplaceListModule _base);
}
