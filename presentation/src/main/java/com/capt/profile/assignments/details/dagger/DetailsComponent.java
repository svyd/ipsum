package com.capt.profile.assignments.details.dagger;

import com.capt.authorization.dagger.RestModule;
import com.capt.data.annotation.PerFragment;
import com.capt.profile.assignments.details.DetailsFragment;

import dagger.Subcomponent;

/**
 * Created by root on 20.04.16.
 */
@PerFragment
@Subcomponent(modules = {DetailsModule.class})
public interface DetailsComponent {
	void inject (DetailsFragment _fragment);
}
