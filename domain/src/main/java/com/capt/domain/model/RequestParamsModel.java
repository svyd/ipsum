package com.capt.domain.model;

/**
 * Created by Svyd on 29.03.2016.
 */
public class RequestParamsModel {

    public RequestParamsModel(String _type, int _limit, int _offset) {
        type = _type;
        limit = _limit;
        offset = _offset;
    }

    public String type;
    public int limit;
    public int offset;
}
