package com.capt.profile.marketplace.dagger;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.capt.R;
import com.capt.base.BaseFragment;
import com.capt.data.annotation.PerFragment;
import com.capt.profile.ViewPagerAdapter;
import com.capt.profile.marketplace.drafts.DraftsFragment;
import com.capt.profile.marketplace.recent.RecentFragment;
import com.capt.profile.marketplace.sold.SoldFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 08.04.16.
 */
@Module
public class MarketPlaceModule {

    private BaseFragment mFragment;

    public MarketPlaceModule(BaseFragment _fragment) {
        mFragment = _fragment;
    }

    @Provides
    @PerFragment
    BaseFragment provideBaseFragment() {
        return mFragment;
    }

    @Provides
    @PerFragment
    FragmentManager provideFragmentManager(BaseFragment _fragment) {
        return _fragment.getChildFragmentManager();
    }

    @Provides
    @PerFragment
    ViewPagerAdapter provideViewPagerAdapter(FragmentManager _manager, Context _context) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(_manager);

        Fragment recentFragment = new RecentFragment();
        Fragment draftsFragment = new DraftsFragment();
        Fragment soldFragment = new SoldFragment();

        adapter.addFrag(recentFragment, _context.getString(R.string.recent).toUpperCase());
        adapter.addFrag(draftsFragment, _context.getString(R.string.drafts).toUpperCase());
        adapter.addFrag(soldFragment, _context.getString(R.string.sold).toUpperCase());

        return adapter;
    }
}
