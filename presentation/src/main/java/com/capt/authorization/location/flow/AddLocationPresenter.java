package com.capt.authorization.location.flow;

import android.content.Intent;
import android.util.Log;

import com.capt.authorization.location.LocationContract;
import com.capt.authorization.location.LocationInteractorManager;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 18.05.2016.
 */
public class AddLocationPresenter extends AbstractLocationTrackPresenter {

    private BasePostInteractor<LocationModel> mUpdateUserInteractor;

    @Inject
    public AddLocationPresenter(LocationContract.LocationView _view,
                                LocationInteractorManager _manager,
                                @Named(Constants.NamedAnnotation.UPDATE_LOCATION_INTERACTOR)
                                BasePostInteractor<LocationModel> _updateProfile, BaseExceptionDelegate _delegate) {
        super(_view, _manager, _delegate);
        mUpdateUserInteractor = _updateProfile;
    }

    @Override
    protected void doOnNext(LocationModel locationModel) {
        mUpdateUserInteractor.execute(locationModel, new UpdateObserver(this));
        getView().showProgress();
    }

    @Override
    public void onStop() {
        super.onStop();
        getView().hideProgress();
        mUpdateUserInteractor.unSubscribe();
    }

    private class UpdateObserver extends BaseObserver<UserModel> {
        public UpdateObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(UserModel userModel) {
            getView().hideProgress();
            Intent intent = new Intent();
            intent.putExtra(Constants.BUNDLE_USER_MODEL, userModel);
            getView().finishWithResult(intent);
            Log.d(TAG, "onNext() called with: " + "userModel = [" + userModel + "]");
        }
    }
}
