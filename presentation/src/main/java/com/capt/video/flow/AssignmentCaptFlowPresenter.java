package com.capt.video.flow;

import android.os.Bundle;
import android.provider.CalendarContract;

import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.global.Constants;
import com.capt.video.VideoContract;

/**
 * Created by Svyd on 15.05.2016.
 */
public class AssignmentCaptFlowPresenter extends AbstractVideoPresenter {

    public AssignmentCaptFlowPresenter(VideoContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
    }

    @Override
    public void setArgs(Bundle _args) {
        getData().putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, _args.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE));
        getData().putInt(Constants.FLOW, _args.getInt(Constants.FLOW));
    }

    @Override
    public void initialize() {
        getView().initTrimFragment(getData());
    }
}
