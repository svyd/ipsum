package com.capt.video.upload_manager;

import android.content.Context;
import android.widget.Toast;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;
import com.capt.domain.global.Constants;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import rx.Observer;

/**
 * Created by richi on 2016.03.14..
 */
@Singleton
public class VideoUploadManagerImpl implements VideoUploadManager {

    private BasePostInteractor<Video> mUploadInteractor;
    private BasePostInteractor<String> mCancelInteractor;
    private BasePostInteractor<String> mCompleteInteractor;
    private BasePostInteractor<String> mRetryInteractor;
    private Context mContext;

    @Inject
    public VideoUploadManagerImpl(
            Context _context,
            @Named(Constants.PendingVideoConstants.RETRY_UPLOAD_VIDEO)
            BasePostInteractor<String> _retryInteractor,
            @Named(Constants.PendingVideoConstants.UPLOAD_VIDEO)
            BasePostInteractor<Video> _uploadInteractor,
            @Named(Constants.PendingVideoConstants.CANCEL_PENDING_VIDEO_ID)
            BasePostInteractor<String> _completeInteractor,
            @Named(Constants.PendingVideoConstants.COMPLETE_PENDING_VIDEO_ID)
            BasePostInteractor<String> _deleteInteractor) {
        mUploadInteractor = _uploadInteractor;
        mCancelInteractor = _completeInteractor;
        mCompleteInteractor = _deleteInteractor;
        mRetryInteractor = _retryInteractor;
        mContext = _context;
    }

    @Override
    public void startUploading(Video _model) {
        mUploadInteractor.execute(_model, new SimpleObserver());
    }

    @Override
    public void startPendingUploading() {
        mUploadInteractor.execute(new SimpleObserver());
    }

    @Override
    public void completePendingUploading(String _id) {
        mCompleteInteractor.execute(_id, new SimpleObserver());
    }

    @Override
    public void cancelUpload(String _id) {
        mCancelInteractor.execute(_id, new SimpleObserver<>());
    }

    @Override
    public void retry(String _id) {
        mRetryInteractor.execute(_id, new SimpleObserver<>());
    }

    private class SimpleObserver<T> implements Observer<T> {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNext(T t) {

        }
    }
}
