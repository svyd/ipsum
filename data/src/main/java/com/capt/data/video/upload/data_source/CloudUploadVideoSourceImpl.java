package com.capt.data.video.upload.data_source;

import com.capt.data.authorization.net.ApiConstants;
import com.capt.domain.global.Constants;
import com.capt.domain.video.DiskDataSource;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.model.PayPalModel;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.upload.PendingUploadCache;
import com.capt.domain.authorization.model.SuccessModel;
import com.google.gson.Gson;

import net.gotev.uploadservice.HttpUploadRequest;
import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Provider;

import rx.Observable;

/**
 * Created by richi on 2016.03.11..
 */
public class CloudUploadVideoSourceImpl implements UploadVideoDataSource {

    private Provider<HttpUploadRequest> mUploadRequestProvider;
    private PendingUploadCache mVideoCache;
    private DiskDataSource mDiskDataSource;
    private Provider<UploadNotificationConfig> mConfigProvider;
    private UploadPayPalCodeService mService;
    private Gson mGson;

    public CloudUploadVideoSourceImpl(Gson _gson,
                                      Provider<HttpUploadRequest> _requestProvider,
                                      PendingUploadCache _pendingUploadCache,
                                      DiskDataSource _diskDataSource,
                                      Provider<UploadNotificationConfig> _config,
                                      UploadPayPalCodeService _uploadService) {
        mUploadRequestProvider = _requestProvider;
        mVideoCache = _pendingUploadCache;
        mDiskDataSource = _diskDataSource;
        mConfigProvider = _config;
        mGson = _gson;
        mService = _uploadService;
    }

    @Override
    public Observable<SuccessModel> uploadAssignmentVideo(Assignment _model) {
        return Observable.create(subscriber -> {
            try {
                MultipartUploadRequest uploadRequest = (MultipartUploadRequest) mUploadRequestProvider.get();
                uploadRequest.setUrl(ApiConstants.UPLOAD_FOR_ASSIGNMENT + _model.id);
                //Set up notification parameters
                UploadNotificationConfig config = mConfigProvider.get();
                config.setTitle(_model.title);
                uploadRequest.setNotificationConfig(config);

                String quality = mGson.toJson(_model.video.quality);

                //Add rest parameters
                uploadRequest.setId(Constants.ASSIGNMENT_PREFIX + _model.id);
                uploadRequest.addParameter("quality", quality);
                uploadRequest.addParameter("duration", String.valueOf(_model.video.duration));
                uploadRequest.addFileToUpload(_model.video.thumbnail, "thumbnail");
                uploadRequest.addFileToUpload(_model.video.path, "videoFile");

                //Start uploading
                uploadRequest.startUpload();


                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<SuccessModel> uploadToCloud(final Video _model) {
        return Observable.create(subscriber -> {
            try {
                MultipartUploadRequest uploadRequest = (MultipartUploadRequest) mUploadRequestProvider.get();

                //Set up notification parameters
                UploadNotificationConfig config = mConfigProvider.get();
                config.setTitle(_model.title);
                uploadRequest.setNotificationConfig(config);

                String quality = mGson.toJson(_model.quality);

                String[] categories = new String[_model.categories.size()];

                for (int i = 0; i < categories.length; i++) {
                    categories[i] = _model.categories.get(i).id;
                }

                String cat = mGson.toJson(categories);

                //Add rest parameters
                uploadRequest.setId(_model.id);
                uploadRequest.addParameter("categories", cat);
                uploadRequest.addParameter("country", _model.country);
                uploadRequest.addParameter("quality", quality);
                uploadRequest.addParameter("city", _model.city);
                uploadRequest.addParameter("duration", String.valueOf(_model.duration));
                uploadRequest.addParameter("title", _model.title);
                uploadRequest.addParameter("description", _model.description);
                uploadRequest.addParameter("price", String.valueOf(_model.price));
                uploadRequest.addParameter("currencyId", _model.currency.getCurrencyId());
                uploadRequest.addParameter("license", "license");
                uploadRequest.addParameter("location", Arrays.toString(_model.location));
                uploadRequest.addParameter("date", String.valueOf(_model.date_upload.getTime()));
                uploadRequest.addFileToUpload(_model.path, "videoFile");
                uploadRequest.addFileToUpload(_model.thumbnail, "thumbnail");

                //Start uploading
                uploadRequest.startUpload();
                subscriber.onNext(new SuccessModel());
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }

    private List<String> map(List<Video> _videos) {
        List<String> list = new ArrayList<>();
        for (Video video : _videos) {
            list.add(video.id);
        }
        return list;
    }

    @Override
    public Observable<SuccessModel> uploadPendingVideos() {
        return mVideoCache.getPendingUploads()
                .map(this::map)
                .concatMap(strings ->
                        Observable.from(strings)
                                .concatMap(mDiskDataSource::get)
                                .concatMap(video -> {
                                    video.setError(false);
                                    return mDiskDataSource.updatePending(video);
                                })
                                .flatMap(this::uploadToCloud));
    }

    @Override
    public Observable<SuccessModel> uploadPayPalCode(String _payPalCode) {
        return mService.uploadPayPalCode(new PayPalModel(_payPalCode));
    }
}
