package com.capt.domain.profile.settings;

import com.capt.domain.base.BaseInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by root on 04.05.16.
 */
public class GetSettingsInteractor extends BaseInteractor {

	SettingsRepository mRepository;

	@Inject
	public GetSettingsInteractor(SettingsRepository _repository, PostExecutionThread postExecutionThread) {
		super(postExecutionThread);
		this.mRepository = _repository;
	}


	@Override protected Observable buildGetObserver() {
		return mRepository.getSettings();
	}
}
