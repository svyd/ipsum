package com.capt.data.category;

import com.capt.domain.category.VideoCategoryRepository;
import com.capt.domain.category.model.CategoryModel;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 22.04.2016.
 */
public class VideoCategoryRepositoryImpl implements VideoCategoryRepository {

    VideoCategoryService mService;

    @Inject
    public VideoCategoryRepositoryImpl(VideoCategoryService _service) {
        mService = _service;
    }

    @Override
    public Observable<List<CategoryModel>> getCategories() {
        return mService.getCategories();
    }
}
