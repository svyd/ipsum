package com.capt.profile.marketplace;

/**
 * Created by Svyd on 26.05.2016.
 */
public interface ErrorOptionsListener {
    void onRetryClick(int _position);
    void onCancelClick(int _position);
}
