package com.capt.profile.marketplace.sold;

import android.os.Bundle;

import com.capt.domain.global.Constants;
import com.capt.profile.marketplace.base.BaseMarketPlaceListFragment;

/**
 * Created by root on 08.04.16.
 */
public class SoldFragment extends BaseMarketPlaceListFragment {
	@Override
	protected String getType() {
		return Constants.MarketplaceTypes.SOLD;
	}

    @Override
    protected Constants.MarketplaceItems getItemType() {
        return Constants.MarketplaceItems.Sold;
    }
}
