package com.capt.video.camera;

import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by richi on 2016.03.25..
 */
public interface BaseAdapter {
    void setItems(List _items);
    View getMainView(ViewGroup _parent);
    View getView(int _position, ViewGroup _parent);
    int getCount();
    Object getItem(int _position);
    void notifyDataChanged();
}
