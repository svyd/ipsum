package com.capt.domain.authorization.location.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Svyd on 17.03.2016.
 */
public final class PredictionsModel {

    @Expose
    public final Prediction predictions[];

    public PredictionsModel(Prediction[] _predictions) {
        this.predictions = _predictions;
    }

    public static final class Prediction {
        @Expose
        public String description;
        @Expose
        public String id;
        @Expose
        public String place_id;
        @Expose
        public String reference;
        @Expose
        public String address;
        @Expose
        public String[] types;

        public String city;

        public String country;

        public String getCity() {
            return city;
        }

        public String getCountry() {
            return country;
        }

        public PlaceModel details;

        public void setDescription(String description) {
            this.description = description;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public void setTypes(String[] types) {
            this.types = types;
        }

    }
}