package com.capt.data.video.disk;

import com.capt.domain.video.DiskDataSource;
import com.capt.domain.video.draft.DraftRepository;
import com.capt.domain.video.model.Video;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 02.05.2016.
 */
public class DraftRepositoryImpl implements DraftRepository {

    private DiskDataSource mDataSource;

    @Inject
    public DraftRepositoryImpl(DiskDataSource _source) {
        mDataSource = _source;
    }

    @Override
    public Observable<Integer> deleteDraft(String _id) {
        return mDataSource.deleteByDraftId(_id);
    }

    @Override
    public Observable<Integer> deletePending(String _id) {
        return mDataSource.deleteById(_id);
    }

    @Override
    public Observable<Long> updateDraft(Video _video) {
        return mDataSource.update(_video);
    }

    @Override
    public Observable<Video> updatePending(Video _video) {
        return mDataSource.updatePending(_video);
    }

    @Override
    public Observable<List<Video>> getAll() {
        return mDataSource.getAll();
    }

    @Override
    public Observable<Video> get(String _id) {
        return mDataSource.get(_id);
    }

    @Override
    public Observable<Long> add(Video _model) {
        return mDataSource.add(_model);
    }
}
