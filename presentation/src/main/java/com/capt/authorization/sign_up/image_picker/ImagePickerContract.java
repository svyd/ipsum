package com.capt.authorization.sign_up.image_picker;

import android.content.Intent;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

/**
 * Created by richi on 2016.03.20..
 */
public interface ImagePickerContract {

    interface View extends BaseView {
        void startCameraIntent();
        void startGalleryIntent();
        void setResult(String _path);
    }

    abstract class Presenter extends BasePresenter<ImagePickerContract.View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onCameraClick();
        public abstract void onGalleryClick();
        public abstract void onResultGallery(Intent _data);
        public abstract void onResultCamera();
        public abstract String getPhotoPath();
    }
}
