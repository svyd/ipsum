package com.capt.domain.authorization.interactor;

import com.capt.domain.authorization.AuthRepository;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by richi on 2016.02.23..
 */
public class SocNetAuthInteractor extends BasePostInteractor<SocNetAuthModel> {

    private AuthRepository mRepository;

    @Inject
    public SocNetAuthInteractor(AuthRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(SocNetAuthModel socNetAuthModel) {
        return mRepository.signInWithSocNet(socNetAuthModel);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
