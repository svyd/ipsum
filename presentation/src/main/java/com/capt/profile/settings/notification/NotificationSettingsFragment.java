package com.capt.profile.settings.notification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.capt.R;
import com.capt.base.BaseToolbarFragment;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.profile.dagger.ProfileActivityComponent;
import com.capt.profile.settings.dagger.AccountComponent;
import com.capt.profile.settings.notification.dagger.NotificationSettingsComponent;
import com.capt.profile.settings.notification.dagger.NotificationSettingsModule;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by root on 29.04.16.
 */
public class NotificationSettingsFragment extends BaseToolbarFragment implements NotificationSettingsContract.View {

    @Inject
    NotificationSettingsContract.Presenter mPresenter;
    @Bind(R.id.sw_marketplace_FNS)
    Switch swMarketplace;
    @Bind(R.id.sw_assignment_sold_FNS)
    Switch swAssignmentSold;
    @Bind(R.id.sw_assignment_not_sold_FNS)
    Switch swAssignmentNotSold;
    @Bind(R.id.sw_new_assignment_FNS)
    Switch swNewAssignment;
    @Bind(R.id.sw_level_up_FNS)
    Switch swLevelUp;

    private SettingsModel mSettings;

    private NotificationSettingsComponent mComponent;

    public static NotificationSettingsFragment newInstance(SettingsModel _model) {

        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, _model);
        NotificationSettingsFragment fragment = new NotificationSettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_notification_settings);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponent();
        inject();
        showToolbar();
        checkExtra();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(AccountComponent.class)
                .createNotificationSettingsComponent(new NotificationSettingsModule(this));
    }

    private void inject() {
        if (mPresenter != null)
            return;

        mComponent.inject(this);
    }

    private void showToolbar() {
        getToolbarManager().showToolbar();
        getActivity().setTitle(getString(R.string.notifications_title));
    }

    private void checkExtra() {
        SettingsModel settings = (SettingsModel) getArguments().getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        mPresenter.setSettingsModel(settings);
    }

    @Override
    public void showMessage(String _message) {
        super.showMessage(_message);
    }

    @Override
    public void setSettings(SettingsModel _settings) {
        Intent intent = new Intent();
        intent.putExtra(Constants.Extra.EXTRA_SERIALIZABLE, _settings);
        getActivity().setResult(Activity.RESULT_OK, intent);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void setSettingsOnView(SettingsModel settings) {
        swMarketplace.setChecked(settings.notifications.videoIsSold);
        swAssignmentSold.setChecked(settings.notifications.assignmentIsSold);
        swAssignmentNotSold.setChecked(settings.notifications.assignmentIsNotSold);
        swNewAssignment.setChecked(settings.notifications.newAssignment);
        swLevelUp.setChecked(settings.notifications.levelUp);

        swMarketplace.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.onSwitchClick(R.id.sw_marketplace_FNS));
        swAssignmentSold.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.onSwitchClick(R.id.sw_assignment_sold_FNS));
        swAssignmentNotSold.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.onSwitchClick(R.id.sw_assignment_not_sold_FNS));
        swNewAssignment.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.onSwitchClick(R.id.sw_new_assignment_FNS));
        swLevelUp.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.onSwitchClick(R.id.sw_level_up_FNS));
    }
}
