package com.capt.profile.dashboard.ui.delegate;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.capt.R;
import com.capt.domain.profile.dashboard.model.Progress;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.profile.dashboard.ui.adapter.SectioningAdapter;
import com.capt.profile.dashboard.ui.adapter.ViewTypeConstants;
import com.capt.profile.dashboard.ui.delegate.base.BaseDelegate;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Svyd on 25.03.2016.
 */
public class ProgressDelegate extends BaseDelegate {

    private LayoutInflater mInflater;

    @Inject
    public ProgressDelegate(Context _context) {
        super(ViewTypeConstants.VIEW_TYPE_PROGRESS);
        mInflater = LayoutInflater.from(_context);
    }

    @Override
    public boolean isForViewType(@NonNull List<DisplayableItem> items, int position) {
        return items.get(position) instanceof Progress;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ProgressHolder(mInflater.inflate(R.layout.list_item_progress, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull List<DisplayableItem> items, int position, @NonNull RecyclerView.ViewHolder holder) {

    }

    static class ProgressHolder extends SectioningAdapter.ViewHolder {

        public ProgressHolder(View itemView) {
            super(itemView);
        }
    }
}
