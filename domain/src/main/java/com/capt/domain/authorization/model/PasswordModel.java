package com.capt.domain.authorization.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 24.06.2016.
 */
public class PasswordModel {

    public PasswordModel(String _old, String _new) {
        oldPassword = _old;
        newPassword = _new;
    }

    @Expose
    @SerializedName("oldPassword")
    String oldPassword;

    @Expose
    @SerializedName("newPassword")
    String newPassword;
}
