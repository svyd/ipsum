package com.capt.domain.profile.assignment.accept.model;

import com.google.gson.annotations.Expose;

/**
 * Created by root on 22.04.16.
 */
public class AcceptAssignmentModel {

	public AcceptAssignmentModel(String id, boolean accept) {
		this.accept = accept;
		this.id = id;
	}

	@Expose
	public boolean accept;

	public String id;
}
