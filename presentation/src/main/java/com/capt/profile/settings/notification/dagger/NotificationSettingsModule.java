package com.capt.profile.settings.notification.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.data.base.TypeMapper;
import com.capt.data.settings.SettingsMapper;
import com.capt.data.settings.SettingsRepositoryImpl;
import com.capt.data.settings.SettingsService;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.settings.SettingsRepository;
import com.capt.domain.profile.settings.UpdateSettingsInteractor;
import com.capt.domain.profile.settings.model.SettingsModel;
import com.capt.domain.profile.settings.model.UpdateSettingsModel;
import com.capt.profile.settings.notification.NotificationSettingsContract;
import com.capt.profile.settings.notification.NotificationSettingsPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by root on 29.04.16.
 */
@Module
public class NotificationSettingsModule {
    NotificationSettingsContract.View mView;

    public NotificationSettingsModule(NotificationSettingsContract.View _view) {
        mView = _view;
    }

    @Provides
    @PerFragment
    protected NotificationSettingsContract.Presenter providePresenter(NotificationSettingsPresenter _presenter) {
        return _presenter;
    }

    @Provides
    @PerFragment
    protected NotificationSettingsContract.View provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    SettingsService provideSettingsService(@Named(Constants.NamedAnnotation.MAIN_RETROFIT)
                                           Retrofit _retrofit) {
        return _retrofit.create(SettingsService.class);
    }

    @Provides
    @PerFragment
    SettingsRepository provideSettingsRepository(SettingsRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.SETTINGS_INTERACTOR)
    protected BasePostInteractor<UpdateSettingsModel> provideUpdateSettingsInteractor(UpdateSettingsInteractor _interactor) {
        return _interactor;
    }

}
