package com.capt.profile.edit_profile.dagger;

import android.support.v4.app.FragmentManager;

import com.capt.base.BaseActivity;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.data.annotation.PerActivity;
import com.capt.profile.edit_profile.EditProfileActivityContract;
import com.capt.profile.edit_profile.EditProfileActivityPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 4/29/16.
 */
@Module
public class EditProfileActivityModule {

    private BaseActivity mBaseActivity;

    public EditProfileActivityModule(BaseActivity _activity) {
        mBaseActivity = _activity;
    }

    @PerActivity
    @Provides
    protected BaseActivity provideBaseActivity() {
        return mBaseActivity;
    }

    @PerActivity
    @Provides
    protected FragmentManager provideFragmentManager(BaseActivity _activity) {
        return _activity.getSupportFragmentManager();
    }


    @Provides
    @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }

    @Provides
    @PerActivity
    EditProfileActivityContract.View provideView() {
        return ((EditProfileActivityContract.View) mBaseActivity);
    }

    @Provides
    @PerActivity
    protected EditProfileActivityContract.Presenter providePresenter(EditProfileActivityPresenter _presenter) {
        return _presenter;
    }
}
