package com.capt.video.dagger;

import android.content.Context;
import android.widget.MediaController;

import com.capt.base.BaseActivity;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.controller.fragment_navigator.FragmentNavigator;
import com.capt.controller.fragment_navigator.FragmentNavigatorImpl;
import com.capt.data.annotation.PerActivity;
import com.capt.data.annotation.PerFragment;
import com.capt.video.Flow;
import com.capt.video.VideoContract;
import com.capt.video.flow.AbstractVideoPresenter;
import com.capt.video.flow.AssignmentCaptFlowPresenter;
import com.capt.video.flow.AssignmentDetailsPresenter;
import com.capt.video.flow.DetailsFlowPresenter;
import com.capt.video.flow.VideoFlowPresenter;
import com.capt.video.trim.file_manager.FileManager;
import com.capt.video.trim.file_manager.FileManagerImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 29.03.16.
 */
@Module
public class VideoActivityModule {

    private BaseActivity mActivity;
    private VideoContract.View mView;
    private int mFlow;

    public VideoActivityModule(BaseActivity _activity, int _flow) {
        mActivity = _activity;
        mView = (VideoContract.View) _activity;
        mFlow = _flow;
    }

    @Provides
    @PerActivity
    protected BaseActivity provideBaseActivity() {
        return mActivity;
    }

    @Provides
    @PerActivity
    public FragmentNavigator provideFragmentNavigator(BaseActivity _baseActivity) {
        return new FragmentNavigatorImpl(_baseActivity, _baseActivity.getSupportFragmentManager(),
                _baseActivity.getContainerId());
    }

    // TODO: 12.04.2016 TimeUtil here

    @Provides
    @PerActivity
    protected MediaController provideMediaController(BaseActivity _context) {
        return new MediaController(_context);
    }

    @Provides
    @PerActivity
    AbstractVideoPresenter providePresenter(BaseExceptionDelegate _delegate) {
        switch (mFlow) {
            case Flow.VIDEO_UPLOAD:
                return new VideoFlowPresenter(mView, _delegate);
            case Flow.DETAILS:
                return new DetailsFlowPresenter(mView, _delegate);
            case Flow.DRAFT:
                return new DetailsFlowPresenter(mView, _delegate);
            case Flow.CAPT_ASSIGNMENT:
                return new AssignmentCaptFlowPresenter(mView, _delegate);
            case Flow.ASSIGNMENT_DETAILS:
                return new AssignmentDetailsPresenter(mView, _delegate);
            case Flow.ASSIGNMENT_REJECTED:
                return new AssignmentDetailsPresenter(mView, _delegate);
            case Flow.ASSIGNMENT_PURCHASED:
                return new AssignmentDetailsPresenter(mView, _delegate);
            default:
                throw new IllegalArgumentException("Unsupported flow. Flow == " + mFlow);
        }
    }
}
