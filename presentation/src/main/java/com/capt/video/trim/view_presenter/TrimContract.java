package com.capt.video.trim.view_presenter;

import android.os.Bundle;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.video.model.Video;


/**
 * Created by richi on 2016.02.29..
 */
public interface TrimContract {

    interface View extends BaseView {
        void initVideoView(String _path);
        void navigateToDescription(Video _data);
        void navigateBack();
        void finish();
    }

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void setData(Bundle _path);
        public abstract void onTrimClick(long _left, long _right);
    }
}
