package com.capt.authorization.instagram;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.capt.R;
import com.capt.domain.global.Constants;

/**
 * Created by Svyd on 24.02.2016.
 */
@SuppressLint("SetJavaScriptEnabled")
public class InstagramActivity extends Activity {

    private WebView mWebView;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instagram);
        initUi();
        loadUrl(buildUrl());
    }

    private void initUi() {
        mWebView = (WebView) findViewById(R.id.wvInsta_AI);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new OAuthWebViewClient());
        mWebView.setVisibility(View.INVISIBLE);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.insta_auth_progress_title));
        mProgress.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        showProgress();
    }

    private String buildUrl() {
        final String ID_PARAM = "client_id";
        final String REDIRECT_PARAM = "redirect_uri";
        final String RESPONSE_PARAM = "response_type";
        InstaAuthModel model = (InstaAuthModel) getIntent().getSerializableExtra(Constants.BUNDLE_INSTAGRAM_MODEL);
        Uri uri = Uri.parse(model.getAuthUrl()).buildUpon()
                .appendQueryParameter(ID_PARAM, model.getClientId())
                .appendQueryParameter(REDIRECT_PARAM, model.getRedirectUri())
                .appendQueryParameter(RESPONSE_PARAM, model.getResponseType())
                .build();
        return uri.toString();
    }

    public void showProgress() {
        mProgress.show();
    }

    public void hideProgress() {
        mProgress.dismiss();
    }

    public void loadUrl(String _url) {
        mWebView.loadUrl(_url);
    }


    public static void showInBrowser(Context _context, String _link) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                _link));
        _context.startActivity(browserIntent);
    }

    private class OAuthWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgress();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgress();
            mWebView.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.equals(Constants.INSTAGRAM_PASSWORD_LINK)) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.INSTAGRAM_PASSWORD_LINK));
                startActivity(browserIntent);
                return true;
            } else if (url.contains(Constants.ACCESS_TOKEN)) {
                mWebView.setVisibility(View.INVISIBLE);
                Intent result = getIntent();
                result.putExtra(Constants.BUNDLE_REDIRECTED_URI, url);
                setResult(RESULT_OK, result);
                finish();
                return true;
            }
            return false;
        }
    }
}
