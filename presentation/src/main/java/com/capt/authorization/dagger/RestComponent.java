package com.capt.authorization.dagger;

import com.capt.authorization.country_code.dagger.CountryComponent;
import com.capt.authorization.country_code.dagger.CountryModule;
import com.capt.authorization.location.dagger.LocationComponent;
import com.capt.authorization.location.dagger.LocationModule;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;

import javax.inject.Named;

/**
 * Created by Svyd on 5/2/16.
 */
public interface RestComponent {
    CountryComponent createCountryComponent(CountryModule _module);
    LocationComponent provideLocationComponent(LocationModule _module);

    @Named(Constants.NamedAnnotation.COUNTRY_CODE_INTERACTOR)
    BaseInteractor countryCodeInteractor();
}
