package com.capt.video.overview.flow;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.capt.application.UploadReceiver;
import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.data.base.DateTimeUtility;
import com.capt.data.base.TimeUtil;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.AssignmentState;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.profile.dashboard.model.Assignments;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.Item;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.video.overview.OverviewContract;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by Svyd on 21.04.2016.
 */
public class AssignmentsPresenter extends AbstractPresenter {

    private Assignment mData;
    private TimeUtil mTimeUtil;
    private DateTimeUtility mDateUtility;
    private BasePostInteractor<Assignment> mAssignmentInteractor;
    private BaseInteractor mClearAssignmentCacheInteractor;

    @Inject
    public AssignmentsPresenter(BaseInteractor _clearAssignmentInteractor,
                                BasePostInteractor<Assignment> _assignmentVideoInteractor,
                                DateTimeUtility _dateUtility,
                                TimeUtil _timeUtil,
                                OverviewContract.View _view, BaseExceptionDelegate _delegate,
                                @Named(Constants.PendingVideoConstants.PAY_PAL_INTERACTOR)
                                BasePostInteractor<PayPalVideoModel> _payPalInteractor) {
        super(_view, _delegate, _payPalInteractor);
        mTimeUtil = _timeUtil;
        mDateUtility = _dateUtility;
        mAssignmentInteractor = _assignmentVideoInteractor;
        mClearAssignmentCacheInteractor = _clearAssignmentInteractor;
    }

    @Override
    public void setData(Bundle _model) {
        if (_model.containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            mData = (Assignment) _model.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        } else {
            throw new IllegalStateException("Arguments should contain AssignmentModel");
        }
    }

    @Override
    public void onProgress(String uploadId, int progress) {
        getView().hideErrorOptions();
        getView().setVideoProgress(progress);
    }

    @Override
    public void onUploadError(String uploadId) {
        getView().hideVideoProgress();
        getView().showErrorOptions();
        Intent intent = new Intent();
        intent.putExtra(Constants.Extra.EXTRA_STRING, Constants.AssignmentTypes.IN_PROGRESS);
        getView().setResult(intent, Activity.RESULT_OK);
    }

    @Override
    public void onUploadCompleted(String uploadId) {
        getView().hideVideoProgress();
        getView().hideErrorOptions();
        Intent intent = new Intent();
        intent.putExtra(Constants.Extra.EXTRA_STRING, Constants.AssignmentTypes.IN_PROGRESS);
        getView().setResult(intent, Activity.RESULT_OK);
        getView().setVideoProperties(getFormattedProperties(Long.parseLong(mData.video.getDuration()), mData.video.size));
    }

    @Override
    public void initialize() {
        getView().setEditableTitle(false);
        getView().setDetailsBtnVisibility(false);
        getView().setVideoPlayability(false);
        getView().setButtonsVisibility(false);
        getView().setListVisibility(true);
        getView().setTitle("Completed");
        getView().setListData(getItems(mData));
        getView().setPrice(mData.currency.getSymbol() + mData.price);
        getView().showDescription(true);
        getView().setCompletedVisibility(false);
        getView().setVideoDescription(mData.description);
        getView().setLocation(mData.city + ", " + mData.country);
        getView().setVideoTitle(mData.title);
        getView().setVideoProperties(getFormattedProperties(Math.round(Double.parseDouble(mData.video.getDuration())),
                mData.video.size));        getView().setThumbnail(mData.video.thumbnail);
        initUploading();
    }

    @Override
    public void onUploadCancelClick() {
        mClearAssignmentCacheInteractor.execute(new ClearAssignmentObserver(this));
    }

    @Override
    public void onUploadRetryClick() {
        mAssignmentInteractor.execute(mData, new VideoObserver(this));
        getView().hideErrorOptions();
        getView().showVideoProgress();
    }

    private void initUploading() {
        switch (mData.video.state) {
            case AssignmentState.ERROR:
                getView().showErrorOptions();
                break;
            case AssignmentState.UPLOADING:
                getView().showVideoProgress();
                break;
        }
    }

    protected String getFormattedProperties(long _duration, String _size) {
        long size = Long.parseLong(_size);

        // convert Bites to MB
        String videoSize = String.valueOf(size / Constants.CONVERT_TO_MB_COEF);
        String duration = mTimeUtil.getTimeFromIntegerMinutes(_duration);

        return duration + " / " + videoSize + "MB";
    }

    private List<Item> getItems(Assignment _model) {
        List<Item> mark = new ArrayList<>();
        mark.add(new Item("ASSIGNMENT", _model.creator.company));
        mark.add(new Item("DATE", mDateUtility.formatDate(_model.date, false)));
        mark.add(new Item("QUALITY", _model.quality.height + "p" + _model.quality.fps));
        mark.add(new Item("LENGTH", _model.video.duration));
        mark.add(new Item("SIZE", (Integer.parseInt(_model.video.size) / Constants.CONVERT_TO_MB_COEF) + "MB"));
        return mark;
    }

    private class ClearAssignmentObserver extends BaseObserver {

        public ClearAssignmentObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(Object o) {
            Intent intent = new Intent();
            intent.putExtra(Constants.Extra.EXTRA_STRING, Constants.AssignmentTypes.IN_PROGRESS);
            UploadReceiver.onAssignmentCacheCleared();
            getView().navigateBack(intent);
        }
    }

    private class VideoObserver extends BaseObserver<SuccessModel> {
        public VideoObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(SuccessModel successModel) {

        }
    }


    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
