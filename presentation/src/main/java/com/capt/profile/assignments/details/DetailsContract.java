package com.capt.profile.assignments.details;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.assignment.model.Assignment;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by root on 20.04.16.
 */
public interface DetailsContract {

	interface View extends BaseView {
		void showMessage(String _message);
		void setUpView(Assignment _model);
		void setForGrabs();
		void setInProgress();
		void navigateToMapScreen(double lat, double lon);
		void addMarkerToMap(LatLng _location);
		void setAddress(String _address);
		void showCaptDialog(Assignment _assignment);
		void navigateBackToAssignments();
		void navigateToOverviewScreen(Assignment _model);
	}

	abstract class Presenter extends BasePresenter<DetailsContract.View> {
		public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
			super(_baseView, _delegate);
		}

		public abstract void setAssignmentModel(Assignment _model);
		public abstract void setType(String _type);
		public abstract void setLocation();
		public abstract void onLocationClick();
		public abstract void onCaptClick();
		public abstract void onRedBtnClicked();
		public abstract void onAcceptClick();
		public abstract void onNoThanksClick();
	}
}
