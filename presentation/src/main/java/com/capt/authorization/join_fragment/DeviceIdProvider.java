package com.capt.authorization.join_fragment;

import android.content.Context;
import android.provider.Settings;

import com.capt.data.annotation.PerActivity;

import javax.inject.Inject;

/**
 * Created by richi on 2016.03.18..
 */
@PerActivity
public class DeviceIdProvider {

    private Context mContext;

    @Inject
    public DeviceIdProvider(Context _context) {
        mContext = _context;
    }

    public String getDeviceId() {
        return Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
