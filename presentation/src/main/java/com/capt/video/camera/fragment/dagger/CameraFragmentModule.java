package com.capt.video.camera.fragment.dagger;

import android.content.Context;

import com.capt.data.annotation.PerFragment;
import com.capt.domain.global.Constants;
import com.capt.video.Flow;
import com.capt.video.camera.BaseAdapter;
import com.capt.video.camera.fragment.CameraContract;
import com.capt.video.camera.fragment.flow.AssignmentCameraPresenter;
import com.capt.video.camera.fragment.flow.UploadCameraPresenter;
import com.capt.video.camera.fragment.SettingsPickerAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by richi on 2016.03.28..
 */
@Module
public class CameraFragmentModule {

    private CameraContract.View mView;
    private int mFlow;

    public CameraFragmentModule(CameraContract.View _view, int _flow) {
        mView = _view;
        mFlow = _flow;
    }

    @Provides @PerFragment
    protected CameraContract.View provideView() {
        return mView;
    }

    @Provides @PerFragment
    protected CameraContract.Presenter providePresenter(UploadCameraPresenter _presenter,
                                                        AssignmentCameraPresenter _assignmentPresenter) {
        switch (mFlow) {
            case Flow.CAPT_ASSIGNMENT:
                return _assignmentPresenter;
            case Flow.VIDEO_UPLOAD:
                return _presenter;
            default:
                throw new IllegalArgumentException("No such flow for current module. Flow == " + mFlow);
        }
    }

    @Provides @PerFragment
    protected List<String> provideCameraFlashMode(Context _context) {
        List<String> modes = new ArrayList<>();
        modes.add("Auto");
        modes.add("On");
        modes.add("Off");

        return modes;
    }

    @Provides @PerFragment @Named(Constants.NamedAnnotation.TOP_ADAPTER)
    protected BaseAdapter provideTopAdapter(List<String> _modes) {
        return new SettingsPickerAdapter(_modes);
    }

    @Provides @PerFragment @Named(Constants.NamedAnnotation.BOTTOM_ADAPTER)
    protected BaseAdapter provideBottomAdapter(SettingsPickerAdapter _adapter) {
        return _adapter;
    }
}
