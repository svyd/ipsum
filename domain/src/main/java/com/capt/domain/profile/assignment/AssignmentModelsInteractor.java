package com.capt.domain.profile.assignment;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.model.RequestParamsModel;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by root on 19.04.16.
 */
public class AssignmentModelsInteractor extends BasePostInteractor<RequestParamsModel> {

	private AssignmentsRepository mRepository;

	@Inject
	public AssignmentModelsInteractor(AssignmentsRepository _repository, PostExecutionThread postExecutionThread) {
		super(postExecutionThread);
		mRepository = _repository;
	}

	@Override protected Observable buildPostObservable(RequestParamsModel _data) {
		return mRepository.getAssignmentModels(_data);
	}

	@Override protected Observable buildGetObserver() {
		return null;
	}
}
