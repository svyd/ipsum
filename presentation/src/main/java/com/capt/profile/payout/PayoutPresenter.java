package com.capt.profile.payout;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.payout.PayoutScreenModel;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by rMozes on 8/3/16.
 */
public class PayoutPresenter extends PayoutsContract.PayoutsPresenter {

    private PayoutsContract.View mView;
    private BaseInteractor mInteractor;

    @Inject
    public PayoutPresenter(PayoutsContract.View _baseView,
                           BaseExceptionDelegate _delegate,
                           @Named(Constants.NamedAnnotation.PAYOUT)
                           BaseInteractor _interactor) {
        super(_baseView, _delegate);

        mView = _baseView;
        mInteractor = _interactor;
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void initialize() {
        mView.showProgress();
        mView.hideContainer();
        mView.hideEmptyView();
        mInteractor.execute(new PayoutObserver(this));
    }

    private class PayoutObserver extends BaseObserver<PayoutScreenModel> {
        public PayoutObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(PayoutScreenModel payoutScreenModel) {
            mView.hideProgress();

            mView.setDataList(payoutScreenModel.getModelList());
            mView.setTotalPrice(payoutScreenModel.getTotalPayout());

            if (payoutScreenModel.getModelList().isEmpty()) {
                mView.showEmptyView();
                mView.hideContainer();
            } else {
                mView.hideEmptyView();
                mView.showContainer();
            }
        }
    }
}
