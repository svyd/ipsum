package com.capt.authorization;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.capt.authorization.code_verification.CodeVerifyFragment;
import com.capt.authorization.login_credentials.LogInFragment;
import com.capt.authorization.phone_number.PhoneNumberFragment;
import com.capt.authorization.sign_up.SignUpFragment;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.twitter.sdk.android.tweetui.UserTimeline;

/**
 * Created by Svyd on 4/25/16.
 */
public class CaptAuthActivity extends AuthorizationActivity {

    public static final int FLOW_CODE = 0;
    public static final int FLOW_SIGN_UP = 1;
    public static final int FLOW_PHONE_NUMBER = 2;
    public static final int FLOW_LOG_IN = 3;

    public static void startSignUpFlow(Context _context) {
        startFlow(_context, FLOW_SIGN_UP);
    }

    public static void startLogInFlow(Context _context) {
        startFlow(_context, FLOW_LOG_IN);
    }

    public static void startCodeFlow(Context _context) {
        startFlow(_context, FLOW_CODE);
    }

    public static void startPhoneNumberFlow(Context _context) {
        startFlow(_context, FLOW_PHONE_NUMBER);
    }

    private static void startFlow(Context _context, int _flow) {
        Intent intent = new Intent(_context, CaptAuthActivity.class);
        intent.putExtra(Constants.Extra.EXTAR_AUTH_FLOW, _flow);
        _context.startActivity(intent);
    }

    @Override
    protected void initFragment(Bundle _savedInstanceState) {
        if (_savedInstanceState != null)
            return;

        int flow = getIntent().getIntExtra(Constants.Extra.EXTAR_AUTH_FLOW, FLOW_SIGN_UP);
        switch (flow) {
            case FLOW_SIGN_UP:
                navigateToSignUp();
                break;
            case FLOW_PHONE_NUMBER:
                navigateToPhoneNumber();
                break;
            case FLOW_LOG_IN:
                navigateToLogIn();
                break;
            case FLOW_CODE:
                navigateToCode();
                break;
        }
    }

    private void navigateToCode() {
        Bundle args = new Bundle();
        args.putBoolean(Constants.Extra.EXTRA_USER, true);
        getFragmentNavigator().addFragmentWithoutBackStack(CodeVerifyFragment.newInstance(args));
    }

    private void navigateToSignUp() {
        getFragmentNavigator().addFragmentWithoutBackStack(SignUpFragment.newInstance());
    }

    private void navigateToPhoneNumber() {
        getFragmentNavigator().addFragmentWithoutBackStack(PhoneNumberFragment.newInstance(new CodeSignUpModel()));
    }

    private void navigateToLogIn() {
        getFragmentNavigator().addFragmentWithoutBackStack(LogInFragment.newInstance());
    }
}
