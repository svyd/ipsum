package com.capt.data.video.upload;

import android.content.SharedPreferences;

import com.capt.domain.global.Constants;
import com.capt.domain.profile.assignment.cache.AssignmentCache;
import com.capt.domain.profile.assignment.model.Assignment;
import com.capt.domain.video.DiskDataSource;
import com.capt.data.video.upload.data_source.UploadVideoDataSourceFactory;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.video.model.PayPalVideoModel;
import com.capt.domain.video.model.Video;
import com.capt.domain.video.upload.PendingUploadCache;
import com.capt.domain.video.upload.VideoUploadRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by richi on 2016.03.11..
 */
public class VideoUploadUploadRepositoryImpl implements VideoUploadRepository {

    private UploadVideoDataSourceFactory mDataSourceFactory;
    private PendingUploadCache mPendingUploadCache;
    private DiskDataSource mDiskDataSource;
    private AssignmentCache mAssignmentCache;
    private SharedPreferences mPreferences;

    @Inject
    public VideoUploadUploadRepositoryImpl(AssignmentCache _cache,
                                           DiskDataSource _dataSource,
                                           UploadVideoDataSourceFactory _factory,
                                           PendingUploadCache _pendingUploadCache,
                                           SharedPreferences _sharedPreferences) {
        mDataSourceFactory = _factory;
        mPendingUploadCache = _pendingUploadCache;
        mDiskDataSource = _dataSource;
        mAssignmentCache = _cache;
        mPreferences = _sharedPreferences;
    }

    @Override
    public Observable<SuccessModel> uploadForAssignment(Assignment _model) {
        return mAssignmentCache.cacheAssignment(_model)
                .flatMap(assignment -> mDataSourceFactory.createCloudDataSource()
                        .uploadAssignmentVideo(assignment));
    }

    @Override
    public Observable<String> uploadToCloud(Video _model) {
        _model.setUploading(true);
        _model.setDraft(false);
        _model.draftId = -1;
        _model.startUploadTime = System.currentTimeMillis();
        return mDiskDataSource.add(_model)
                .flatMap(id -> {
                    _model.id = String.valueOf(id);
                    return mDataSourceFactory.createCloudDataSource()
                            .uploadToCloud(_model);
                }).flatMap(successModel1 -> mPendingUploadCache.cachePendingVideo(_model));
    }

    @Override
    public Observable<PayPalVideoModel> uploadWithPayPalCode(PayPalVideoModel _model) {
        return uploadPayPalCode(_model.getAuthCode())
                .flatMap(s1 ->
                        uploadToCloud(_model.getVideo())
                                .flatMap(s -> {
//                                    _model.getVideo().id = s;
                                    mPreferences.edit().putBoolean(Constants.Credentials.PAYPAL_REGISTERED, true)
                                            .apply();
                                    return Observable.just(_model);
                                }));
    }

    @Override
    public Observable<SuccessModel> uploadPayPalCode(String _payPalCode) {
        return mDataSourceFactory.createCloudDataSource().uploadPayPalCode(_payPalCode);
    }

    @Override
    public Observable<SuccessModel> retry(String _id) {
        return mDiskDataSource.get(_id)
                .flatMap(model -> {
                    model.setError(false);
                    model.startUploadTime = System.currentTimeMillis();
                    return mDiskDataSource.updatePending(model);
                }).concatMap(video -> mDataSourceFactory.createCloudDataSource()
                        .uploadToCloud(video));
    }

    @Override
    public Observable<SuccessModel> uploadPendingVideos() {
        return mDataSourceFactory.createCloudDataSource().uploadPendingVideos();
    }

}
