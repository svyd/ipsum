package com.capt.profile.gallery.dagger;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.capt.base.BaseFragment;
import com.capt.base.CaptRecycleViewItemListener;
import com.capt.data.annotation.PerFragment;
import com.capt.data.gallery.GalleryRepositoryImpl;
import com.capt.domain.base.BaseInteractor;
import com.capt.domain.global.Constants;
import com.capt.domain.profile.gallery.GalleryInteractor;
import com.capt.domain.profile.gallery.GalleryRepository;
import com.capt.domain.profile.gallery.model.GalleryModel;
import com.capt.profile.gallery.GalleryContract;
import com.capt.profile.gallery.flow.GalleryAssignmentPresenter;
import com.capt.profile.gallery.flow.GalleryPresenter;
import com.capt.video.Flow;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Svyd on 4/23/16.
 */
@Module
public class GalleryModule {

    private GalleryContract.View mView;
    private CaptRecycleViewItemListener<GalleryModel> mListener;
    private BaseFragment mFragment;
    private int mFlow;

    public GalleryModule(GalleryContract.View _view,
                         CaptRecycleViewItemListener<GalleryModel> _listener,
                         BaseFragment _fragment,
                         int _flow) {
        mView = _view;
        mFragment = _fragment;
        mListener = _listener;
        mFlow =_flow;
    }

    @Provides
    @PerFragment
    protected BaseFragment provideFragment() {
        return mFragment;
    }

    @Provides
    @PerFragment
    protected CaptRecycleViewItemListener<GalleryModel> provideListener() {
        return mListener;
    }

    @Provides
    @PerFragment
    RecyclerView.LayoutManager provideLayoutManager(Context _context) {
        return new GridLayoutManager(_context, 3, GridLayoutManager.VERTICAL, false);
    }

    @Provides
    @PerFragment
    protected GalleryContract.View provideView() {
        return mView;
    }

    @Provides
    @PerFragment
    protected GalleryContract.Presenter providePresenter(GalleryPresenter _presenter, GalleryAssignmentPresenter _uploadPresenter) {
        switch (mFlow) {
            case Flow.CAPT_ASSIGNMENT:
                return _uploadPresenter;
            default:
                return _presenter;
        }
    }

    @Provides
    @PerFragment
    GalleryRepository provideRepository(GalleryRepositoryImpl _repository) {
        return _repository;
    }

    @Provides
    @PerFragment
    @Named(Constants.NamedAnnotation.GALLERY_INTERACTOR)
    protected BaseInteractor provideInteractor(GalleryInteractor _interactor) {
        return _interactor;
    }
}
