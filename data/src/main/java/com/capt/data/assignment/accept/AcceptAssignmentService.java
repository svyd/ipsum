package com.capt.data.assignment.accept;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.profile.assignment.accept.model.AcceptAssignmentModel;

import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by root on 22.04.16.
 */
public interface AcceptAssignmentService {

	@POST("/mobile/invites/{id}") Observable<SuccessModel> acceptAssignment (@Path("id") String _id,
	                                                                     @Body AcceptAssignmentModel _model);
}
