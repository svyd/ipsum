package com.capt.domain.authorization;

import com.capt.domain.authorization.location.model.LocationModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.PasswordModel;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.authorization.model.SignUpModel;
import com.capt.domain.authorization.model.SocNetAuthModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UpdateAvatarResponse;
import com.capt.domain.authorization.model.UserModel;

import java.io.File;

import rx.Observable;
import rx.Observer;

/**
 * Created by Svyd on 02.03.2016.
 */
public interface AuthRepository {

    Observable<UserModel> signInWithSocNet(SocNetAuthModel _SocNet_authModel);
    Observable<SuccessModel> getAuthCode(PhoneNumberModel _model);
    Observable<UserModel> signIn(SignInModel _model);
    Observable<SuccessModel> signOut();
    Observable<SuccessModel<String>> checkEmail(String _email);
    Observable<UserModel> verifyCodeAndSignUp(CodeSignUpModel _data);
    Observable<UserModel> verifyCodeAndGetProfile(CodeSignUpModel _data);
    Observable<UserModel> updateUserModel(UserModel _data);
    Observable<UserModel> updateUserModel(LocationModel _data);
    Observable<SuccessModel> updatePushToken(String _data);
    Observable<SuccessModel> changePassword(PasswordModel _password);
}
