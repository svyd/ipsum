package com.capt.profile.dashboard.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.capt.profile.dashboard.ui.listener.OnNonRecycleItemClickListener;

/**
 * Created by Svyd on 25.03.2016.
 */
public class NonRecycleList extends LinearLayout implements View.OnClickListener {

    OnNonRecycleItemClickListener mListener;

    private BaseAdapter mAdapter;

    public NonRecycleList(Context context) {
        this(context, null);
    }

    public NonRecycleList(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NonRecycleList(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }

    public void setOnItemClickListener(OnNonRecycleItemClickListener _listener) {
        mListener = _listener;
    }

    private void init() {
        setOrientation(VERTICAL);
    }

    public void setAdapter(BaseAdapter _adapter) {
        mAdapter = _adapter;
        notifyDataSetChanged();
    }

    public void notifyDataSetChanged() {
        if (mAdapter == null) {
            throw new RuntimeException("You need to call setAdapter first!");
        }
        if (getChildCount() > 0) {
            return;
        }

        for (int i = 0; i < mAdapter.getCount(); i++) {
            View view = mAdapter.getView(i, null, this);
            view.setTag(i);
            view.setOnClickListener(this);
            addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick((int) v.getTag(), this);
        }
    }
}
