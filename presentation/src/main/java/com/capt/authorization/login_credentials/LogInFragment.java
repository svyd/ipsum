package com.capt.authorization.login_credentials;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.capt.R;
import com.capt.authorization.dagger.AuthActivityComponent;
import com.capt.authorization.forgot_password.ForgotPasswordFragment;
import com.capt.authorization.login_credentials.dagger.LogInComponent;
import com.capt.authorization.login_credentials.dagger.LogInModule;
import com.capt.base.BaseToolbarFragment;
import com.capt.base.ToolbarManager;
import com.capt.domain.authorization.model.SignInModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.global.Constants;
import com.capt.profile.ProfileActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Svyd on 16.03.2016.
 */
public class LogInFragment extends BaseToolbarFragment implements LogInContract.View {

    @Inject LogInContract.Presenter mPresenter;
    @Inject ToolbarManager mToolbar;

    @Bind(R.id.etEmail_FLC) EditText etEmail;
    @Bind(R.id.etPass_FLC)  EditText etPassword;
    @Bind(R.id.btnJoin_FLC) Button btnJoin;
    @Bind(R.id.ivEye_FLC)   ImageView ivEye;

    private LogInComponent mComponent;

    public static LogInFragment newInstance() {
        Bundle args = new Bundle();

        LogInFragment fragment = new LogInFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login_credentials);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponent();
        inject();
        showToolbar();
    }

    @OnClick(R.id.tvForgotPassword_FLC)
    void onForgotPasswordClick() {
        getFragmentNavigator().replaceFragmentWithBackStack(ForgotPasswordFragment.newInstance());
    }

    private void initComponent() {
        if (mComponent != null)
            return;

        mComponent = getComponent(AuthActivityComponent.class)
                .createLogInComponent(new LogInModule(this));
    }

    private void inject() {
        if (mToolbar != null)
            return;

        mComponent.inject(this);
    }

    private void showToolbar() {
        mToolbar.showToolbar();
        getActivity().setTitle("Log in");
    }

    @Override
    public void setEnableJoin(boolean _enable) {
        btnJoin.setEnabled(_enable);
    }

    @Override
    public void startProfile(UserModel _model) {
        Intent intent = new Intent(getActivity(),
                ProfileActivity.class);
        intent.putExtra(Constants.BUNDLE_USER_MODEL, _model);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void showMessage(String _message) {
        Toast.makeText(getActivity(), _message, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showPassword() {
        etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        etPassword.setSelection(etPassword.length());
        ivEye.setImageResource(R.drawable.ic_eye2);
    }

    @Override
    public void hidePassword() {
        etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        etPassword.setSelection(etPassword.length());
        ivEye.setImageResource(R.drawable.ic_eye);
    }

    @Override
    public String getEmail() {
        return etEmail.getText().toString();
    }

    @Override
    public String getPassword() {
        return etPassword.getText().toString();
    }

    @OnTextChanged({R.id.etEmail_FLC, R.id.etPass_FLC})
    protected void onCredentialsChanged() {
        mPresenter.onTextChanged();
    }

    //OnClick
    @OnClick(R.id.btnJoin_FLC)
    protected void onJoinClick() {
        mPresenter.onJoinClick(new SignInModel
                .Builder(etEmail.getText().toString(), etPassword.getText().toString()));
    }

    @OnClick(R.id.ivEye_FLC)
    protected void onEyeClick() {
        mPresenter.onEyeClick();
    }
}
