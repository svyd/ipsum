package com.capt.domain.video.draft;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.video.draft.DraftRepository;
import com.capt.domain.video.model.Video;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 03.05.2016.
 */
public class DraftUpdateInteractor extends BasePostInteractor<Video>{

    DraftRepository mRepository;

    @Inject
    public DraftUpdateInteractor(DraftRepository _repository, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mRepository = _repository;
    }

    @Override
    protected Observable buildPostObservable(Video _data) {
        return mRepository.updateDraft(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
