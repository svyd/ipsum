package com.capt.domain.base;


import com.capt.domain.executor.PostExecutionThread;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by richi on 2015.10.19..
 */
public abstract class BasePostInteractor<T> extends BaseInteractor {

    private Subscription mPostSubscription = Subscriptions.empty();

    public BasePostInteractor(PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
    }

    @SuppressWarnings("unchecked")
    public void execute(T _data, Observer _subscriber) {
        mPostSubscription = buildPostObservable(_data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(mPostExecutionThread.getScheduler())
                .subscribe(_subscriber);
    }

    protected abstract Observable buildPostObservable(T _data);

    @Override
    public void unSubscribe() {
        super.unSubscribe();
        if (!mPostSubscription.isUnsubscribed())
            mPostSubscription.unsubscribe();
    }
}
