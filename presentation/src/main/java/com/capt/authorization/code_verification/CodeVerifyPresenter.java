package com.capt.authorization.code_verification;

import android.util.Log;

import com.capt.base.BaseObserver;
import com.capt.base.BasePresenter;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.authorization.model.CodeModel;
import com.capt.domain.authorization.model.CodeSignUpModel;
import com.capt.domain.authorization.model.PhoneNumberModel;
import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.authorization.model.UserModel;
import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.global.Constants;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;

/**
 * Created by richi on 2016.03.19..
 */
public class CodeVerifyPresenter extends CodeVerificationContract.Presenter {

    private static final String TAG = "CodeVerifyPresenter";

    private CodeVerificationContract.View mView;
    private BasePostInteractor<CodeSignUpModel> mInteractor;
    private BasePostInteractor<PhoneNumberModel> mSendAgainInteractor;

    private CodeSignUpModel mModel;

    @Inject
    public CodeVerifyPresenter(@Named(Constants.NamedAnnotation.CODE_CHECK_INTERACTOR)
                                BasePostInteractor<CodeSignUpModel> _codeCheckerInteractor,
                               @Named(Constants.NamedAnnotation.VERIFICATION_INTERACTOR)
                                BasePostInteractor<PhoneNumberModel> _sendAgain,
                               CodeVerificationContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        mInteractor = _codeCheckerInteractor;
        mSendAgainInteractor = _sendAgain;
        mView = _view;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void setCodeSignUpModel(CodeSignUpModel _data) {
        if (_data == null) {
            _data = new CodeSignUpModel();
        }
        mModel = _data;
    }

    @Override
    public void onNextClick(String _code) {
        if (_code == null || _code.trim().length() < 4) {
            mView.showMessage("You have to fill the code");
            return;
        }

        mModel.setCodeModel(new CodeModel(_code));
        mInteractor.execute(mModel, new CodeVerifyObserver(this));
    }

    @Override
    public void sendAgainClick() {
        mView.setEnabledSendAgain(false);
        if (mModel.getPhoneNumber() == null) {
            return;
        }
        mSendAgainInteractor.execute(new PhoneNumberModel(mModel.getCodeModel().code,
                mModel.getPhoneNumber()),
                new SendAgainObserver(this));
    }

    private class CodeVerifyObserver extends BaseObserver<UserModel> {
        public CodeVerifyObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(UserModel userModel) {
            Log.d(TAG, "onNext:");
            mView.startDashboardScreen(userModel);
        }
    }

    private class SendAgainObserver extends BaseObserver<SuccessModel> {
        public SendAgainObserver(BasePresenter _presenter) {
            super(_presenter);
        }

        @Override
        public void onNext(SuccessModel successModel) {
            mView.setEnabledSendAgain(true);
        }


    }
}
