package com.capt.data.video.upload.data_source;

import com.capt.domain.authorization.model.SuccessModel;
import com.capt.domain.video.model.PayPalModel;

import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by rMozes on 8/2/16.
 */
public interface UploadPayPalCodeService {

    @POST("mobile/user/payPalEmail")
    Observable<SuccessModel> uploadPayPalCode(@Body PayPalModel _token);
}
