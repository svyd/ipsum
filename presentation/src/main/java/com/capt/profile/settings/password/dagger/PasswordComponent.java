package com.capt.profile.settings.password.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.settings.password.PasswordFragment;

import dagger.Subcomponent;

/**
 * Created by Svyd on 23.06.2016.
 */
@PerFragment
@Subcomponent(modules = {PasswordModule.class})
public interface PasswordComponent {
    void inject(PasswordFragment _fragment);
}
