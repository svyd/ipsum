package com.capt.profile.dashboard.ui.delegate.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.capt.R;
import com.capt.domain.profile.dashboard.model.base.BaseItems;
import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.profile.dashboard.ui.adapter.NonRecycleAdapter;
import com.capt.profile.dashboard.ui.adapter.SectioningAdapter;
import com.capt.profile.dashboard.ui.listener.OnNonRecycleItemClickListener;
import com.capt.profile.dashboard.ui.view.NonRecycleList;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 25.03.2016.
 */
public abstract class BaseNonRecycleDelegate<T> extends BaseDelegate implements OnNonRecycleItemClickListener {

    private static final String TAG = BaseNonRecycleDelegate.class.getSimpleName();
    private Class<T> mClazz;
    private LayoutInflater mInflater;
    private NonRecycleAdapter mAdapter;

    public BaseNonRecycleDelegate(LayoutInflater _layoutInflater,
                                  NonRecycleAdapter _adapter,
                                  int viewType,
                                  Class<T> _t) {
        super(viewType);
        mInflater = _layoutInflater;
        mAdapter = _adapter;
        mClazz = _t;
    }

    @Override
    public boolean isForViewType(@NonNull List<DisplayableItem> items, int position) {
        return mClazz.isInstance(items.get(position));
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        NonRecycleList itemView = (NonRecycleList) mInflater.inflate(R.layout.non_recycle_list, parent, false);
        itemView.setOnItemClickListener(this);
        return new AssignmentsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull List<DisplayableItem> items, int position, @NonNull RecyclerView.ViewHolder holder) {
        AssignmentsViewHolder vh = (AssignmentsViewHolder) holder;
        BaseItems assignments = (BaseItems) items.get(position);
//        mAdapter.setData(assignments);
        vh.mList.setAdapter(mAdapter);
    }

    static class AssignmentsViewHolder extends SectioningAdapter.ViewHolder {

        @Bind(R.id.nonRecycleList)
        public NonRecycleList mList;

        public AssignmentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
