package com.capt.domain.profile.assignment.cache;

import com.capt.domain.base.BasePostInteractor;
import com.capt.domain.executor.PostExecutionThread;
import com.capt.domain.profile.assignment.model.Assignment;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Svyd on 07.07.2016.
 */
public class UpdateAssignmentStateInteractor extends BasePostInteractor<String> {

    AssignmentCache mCache;

    @Inject
    public UpdateAssignmentStateInteractor(AssignmentCache _cache, PostExecutionThread postExecutionThread) {
        super(postExecutionThread);
        mCache = _cache;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mCache.updateStatus(_data);
    }

    @Override
    protected Observable buildGetObserver() {
        return null;
    }
}
