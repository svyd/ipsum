package com.capt.profile.assignments.dagger;

import com.capt.data.annotation.PerFragment;
import com.capt.profile.assignments.AssignmentsTabFragment;

import dagger.Subcomponent;

/**
 * Created by root on 15.04.16.
 */
@PerFragment
@Subcomponent(modules = AssignmentsTabModule.class)
public interface AssignmentsTabComponent {
	void inject(AssignmentsTabFragment _fragment);
}
