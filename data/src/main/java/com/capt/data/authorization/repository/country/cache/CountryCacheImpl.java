package com.capt.data.authorization.repository.country.cache;

import com.capt.domain.authorization.model.CountryModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by richi on 2016.03.16..
 */
@Singleton
public class CountryCacheImpl implements CountryCache {

    @Inject
    public CountryCacheImpl() {}

    private List<CountryModel> mList;

    @Override
    public void setToCache(List<CountryModel> _model) {
        mList = _model;
    }

    @Override
    public List<CountryModel> get() {
        return mList;
    }

    @Override
    public boolean isCachedInMemory() {
        return mList != null && !mList.isEmpty();
    }
}
