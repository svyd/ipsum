package com.capt.profile.settings;

import android.content.Intent;

import com.capt.base.BasePresenter;
import com.capt.base.BaseView;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;
import com.capt.domain.profile.settings.model.SettingsModel;

/**
 * Created by Svyd on 20.04.2016.
 */
public interface SettingsContract {
    interface View extends BaseView {
        void navigateToNotificationSettings(SettingsModel _model);
        void navigateToPermissionSettings();
        void navigateToSupportSettings();
        void navigateToCurrencySettings(SettingsModel _model);
        void navigateToPayPalSettings();
        void navigateToPasswordSettings();
        void logOut();
        void navigateToEditProfile();
        void showMessage(String _message);
        void showSettings(SettingsModel _model);
    }

    abstract class Presenter extends BasePresenter<View> {
        public Presenter(BaseView _baseView, BaseExceptionDelegate _delegate) {
            super(_baseView, _delegate);
        }

        public abstract void onAccountSettingsClick(int position);
        public abstract void onProfileSettingsClick(int position);
        public abstract void onSupportSettingsClick(int position);
        public abstract void onActivityResult(int requestCode, int resultCode, Intent data);
        public abstract void onRetryClick();
    }
}
