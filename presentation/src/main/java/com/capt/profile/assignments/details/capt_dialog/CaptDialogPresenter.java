package com.capt.profile.assignments.details.capt_dialog;

import com.capt.application.UploadReceiver;
import com.capt.controller.delegate_exception.BaseExceptionDelegate;

import javax.inject.Inject;

/**
 * Created by root on 22.04.16.
 */
public class CaptDialogPresenter extends CaptDialogContract.Presenter {

    private CaptDialogContract.View mView;

    @Inject
    public CaptDialogPresenter(CaptDialogContract.View _view, BaseExceptionDelegate _delegate) {
        super(_view, _delegate);
        this.mView = _view;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onCameraClick() {
        if (UploadReceiver.isUploading()) {
            mView.showDenial();
        } else {
            mView.navigateToCamera();
        }
    }

    @Override
    public void onGalleryClick() {
        if (UploadReceiver.isUploading()) {
            mView.showDenial();
        } else {
            mView.navigateToGallery();
        }
    }
}
