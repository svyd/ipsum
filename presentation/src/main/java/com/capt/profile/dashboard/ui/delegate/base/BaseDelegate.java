package com.capt.profile.dashboard.ui.delegate.base;

import com.capt.domain.profile.dashboard.model.base.DisplayableItem;
import com.capt.profile.dashboard.ui.listener.OnDelegateItemClickListener;
import com.hannesdorfmann.adapterdelegates.AbsAdapterDelegate;

import java.util.List;

/**
 * Created by Svyd on 28.03.2016.
 */
public abstract class BaseDelegate extends AbsAdapterDelegate<List<DisplayableItem>>  {

    protected OnDelegateItemClickListener mListener;

    public BaseDelegate(int viewType) {
        super(viewType);
    }

    public void setDelegateItemClickListener(OnDelegateItemClickListener _listener) {
        mListener = _listener;
    }
}
